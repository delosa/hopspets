from .models import *
from django import forms


class AbrirCajaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AbrirCajaForm, self).__init__(*args, **kwargs)
        self.fields['dinero_inicial'].required = False
        self.fields['dinero_inicial'].label = 'Especificar cantidad de dinero inicial'

    class Meta:
        model = Caja
        fields = (
            'dinero_inicial',
        )

        widgets = {
            'dinero_inicial': forms.NumberInput(attrs={'required': 'true', 'min': '0','id':'dinero_inicial_id'})
        }
