from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.http import JsonResponse
from datetime import datetime , timedelta
import decimal
from veteriariaTenant.models import Veterinaria
from clientes.models import Cliente
from tratamientos.models import Tratamiento
from estetica.models import FacturaEstetica, TipoEstetica
from mascotas.models import Mascota
from citas.models import HistoriaClinica,MotivoCita,MotivoArea
from productos.models import *
from gastos.models import *
from gastos.forms import *
from productos.forms import *
from desparasitacion.models import Desparasitacion, Desparasitante, AplicacionDesparasitacion
from vacunacion.models import Vacunacion, Vacuna, AplicacionVacunacion
from guarderia.models import Guarderia
from itertools import chain
from gastos.models import GastoProducto, GastoMedicamento
from decimal import Decimal
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
import math
from django.db.models import Count
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime
from categoriasInventario.models import *
from bodegas.models import Bodega

class ListarPagosHoy(ListView):
    model = Pago
    template_name = "listar_pagos_hoy.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagosHoy, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagosHoy, self).get_context_data(**kwargs)
        context['menu_caja'] = True
        cerrar_caja_emergencia()
        now = datetime.now()
        x = datetime(now.year, now.month, now.day)
        y = x + timedelta(days=1)
        caja = Caja.objects.filter(estado=True)
        caja_pago_selected = CajaPago.objects.filter(caja = caja[0]).exclude(pago__estado_pago='Anulada')
        caja_pago_selected2 = CajaCompra.objects.filter(caja = caja[0]).exclude(compra__estado_pago='Anulada')
        caja_abono_pago = CajaAbonoPago.objects.filter(caja=caja[0]).exclude(abono_pago__pago__estado_pago='Anulada')
        caja_abono_compra = CajaAbonoCompra.objects.filter(caja = caja[0]).exclude(abono_compra__gasto__estado_pago='Anulada')
        pagos = []
        compras = []
        total_venta = 0
        total_compra = 0
        pago_credito_values = []
        compra_credito_values = []
        for caja_p in caja_pago_selected:
            pago_temp = get_object_or_404(Pago,pk=caja_p.pago_id)      
            if pago_temp.forma_pago == 'Debito':
                pago_credito_values.append("-")
                total_venta += pago_temp.total_factura
                pagos.append(pago_temp)

        for abono_caja in caja_abono_pago:
            if abono_caja.valor_abonado == 0:
                pago_credito_values.append(abono_caja.abono_pago.valor_cuota)
                total_venta += abono_caja.abono_pago.valor_cuota
                pagos.append(abono_caja.abono_pago.pago)
            else:
                pago_credito_values.append(abono_caja.valor_abonado)
                total_venta += abono_caja.valor_abonado
                pagos.append(abono_caja.abono_pago.pago)

        for caja_co in caja_pago_selected2:
            compra_temp = get_object_or_404(Gasto,pk=caja_co.compra_id)
            if compra_temp.forma_pago == 'Debito':
                compra_credito_values.append("-")
                total_compra += compra_temp.valor_total
                compras.append(compra_temp)

        for abono_compra_caja in caja_abono_compra:
            if abono_compra_caja.valor_abonado == 0:
                compra_credito_values.append(abono_compra_caja.abono_compra.valor_cuota)
                total_compra += abono_compra_caja.abono_compra.valor_cuota
                compras.append(abono_compra_caja.abono_compra.gasto)
            else:
                compra_credito_values.append(abono_compra_caja.valor_abonado)
                total_compra += abono_compra_caja.valor_abonado
                compras.append(abono_compra_caja.abono_compra.gasto)

        motivos_pagos = []
        motivos_gastos = []
        
        for pago in pagos:
            
            motivos = []
            if PagoGuarderia.objects.filter(pago__id=pago.id):
                motivos.append("Guardería")

            if PagoProducto.objects.filter(pago__id=pago.id):
                motivos.append("Producto")

            if PagoMedicamento.objects.filter(pago__id=pago.id):
                motivos.append("Medicamento")

            if PagoVacuna.objects.filter(pago__id=pago.id):
                motivos.append("Vacuna")

            if PagoDesparasitante.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitante")

            if PagoTratamiento.objects.filter(pago__id=pago.id):
                motivos.append("Cita médica")

            if PagoEstetica.objects.filter(pago__id=pago.id):
                motivos.append("Estética")

            if PagoVacunacion.objects.filter(pago__id=pago.id):
                motivos.append("Vacunación")

            if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitación")

            if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                motivos.append("Procedimiento")

            if PagoOtro.objects.filter(pago__id=pago.id):
                motivos.append("Otro")

            if PagoElementoInventario.objects.filter(pago__id=pago.id):
                for pago in PagoElementoInventario.objects.filter(pago__id=pago.id):
                    if not pago.elemento.categoria.nombre in motivos:
                        motivos.append(pago.elemento.categoria.nombre)
            
            motivos_pagos.append(motivos)

        for gasto in compras:
         
            motivos = []
            if GastoProducto.objects.filter(gasto__id=gasto.id):
                motivos.append("Producto")

            if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                motivos.append("Medicamento")

            if GastoVacuna.objects.filter(gasto__id=gasto.id):
                motivos.append("Vacuna")

            if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                motivos.append("Desparasitante")

            if GastoServicio.objects.filter(gasto__id=gasto.id):
                motivos.append("Servicio")

            if GastoOtro.objects.filter(gasto__id=gasto.id):
                motivos.append("Otro")

            if GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                for gasto in GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                    if not gasto.elemento.categoria.nombre in motivos:
                        motivos.append(gasto.elemento.categoria.nombre)

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos
        context['motivos_pagos'] = motivos_pagos
        context['pagos_hoy'] = pagos
        context['compras_hoy'] = compras
        context['total_venta'] = total_venta
        context['total_compra'] = total_compra
        context['abonos_pago'] = pago_credito_values
        context['abonos_compra'] = compra_credito_values
        context['dinero_inicial'] = caja[0].dinero_inicial
        context['total'] = caja[0].dinero_inicial + total_venta - total_compra
        context['abonos_pagos'] = CajaAbonoPago.objects.filter(caja = caja[0]).exclude(pago__estado_pago='Anulada')
        context['abonos_compras'] = CajaAbonoCompra.objects.filter(caja=caja[0]).exclude(gasto__estado_pago='Anulada')
        for cajas in caja:
            cajas.dinero_final = caja[0].dinero_inicial + total_venta - total_compra
            cajas.save()
        context['caja'] = caja[0]

        total_efectivo = caja[0].dinero_inicial
        total_ventas_efectivo = 0
        total_tarjeta_debito = 0
        total_consignacion = 0
        total_tarjeta_credito = 0
        total_otros = 0

        for pago in pagos:
            if pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Efectivo':
                total_ventas_efectivo += pago.total_factura
                total_efectivo += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Tarjeta debito':
                total_tarjeta_debito += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Consignacion':
                total_consignacion += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Tarjeta credito':
                total_tarjeta_credito += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Otro':
                total_otros += pago.total_factura

        for abono in CajaAbonoPago.objects.filter(caja = caja[0]).exclude(pago__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_ventas_efectivo += abono.valor_abonado
                total_efectivo += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_otros += abono.valor_abonado

        total_compras_efectivo = 0
        total_compras_tarjeta_debito = 0
        total_compras_consignacion = 0
        total_compras_tarjeta_credito = 0
        total_compras_otros = 0

        for compra in compras:
            if compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Efectivo':
                total_compras_efectivo += compra.valor_total
                total_efectivo -= compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta debito':
                total_compras_tarjeta_debito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Consignacion':
                total_compras_consignacion += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta credito':
                total_compras_tarjeta_credito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Otro':
                total_compras_otros += compra.valor_total

        for abono in CajaAbonoCompra.objects.filter(caja=caja[0]).exclude(gasto__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_compras_efectivo += abono.valor_abonado
                total_efectivo -= abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_compras_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_compras_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_compras_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_compras_otros += abono.valor_abonado

        context['total_efectivo'] = total_efectivo
        context['total_ventas_efectivo'] = total_ventas_efectivo
        context['total_tarjeta_debito'] = total_tarjeta_debito
        context['total_consignacion'] = total_consignacion
        context['total_tarjeta_credito'] = total_tarjeta_credito
        context['total_otros'] = total_otros

        context['total_compras_efectivo'] = total_compras_efectivo
        context['total_compras_tarjeta_debito'] = total_compras_tarjeta_debito
        context['total_compras_consignacion'] = total_compras_consignacion
        context['total_compras_tarjeta_credito'] = total_compras_tarjeta_credito
        context['total_compras_otros'] = total_compras_otros

        return context

class ListarPagosSelected(ListView):
    model = Pago
    template_name = "detalle_caja.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagosSelected, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagosSelected, self).get_context_data(**kwargs)
        context['menu_caja'] = True
        caja_selected = get_object_or_404(Caja, pk=self.kwargs['id_caja'])
        caja_pago_selected = CajaPago.objects.filter(caja = caja_selected).exclude(pago__estado_pago='Anulada')
        caja_pago_selected2 = CajaCompra.objects.filter(caja = caja_selected).exclude(compra__estado_pago='Anulada')
        caja_abono_pago = CajaAbonoPago.objects.filter(caja=caja_selected).exclude(abono_pago__pago__estado_pago='Anulada')
        caja_abono_compra = CajaAbonoCompra.objects.filter(caja = caja_selected).exclude(abono_compra__gasto__estado_pago='Anulada')
        pagos = []
        compras = []
        inicio = datetime(caja_selected.fecha_inicio.year, caja_selected.fecha_inicio.month, caja_selected.fecha_inicio.day,caja_selected.fecha_inicio.hour,caja_selected.fecha_inicio.minute)
        inicio_modified = inicio - timedelta(hours= 5)
        final = datetime(caja_selected.fecha_final.year, caja_selected.fecha_final.month, caja_selected.fecha_final.day,caja_selected.fecha_final.hour,caja_selected.fecha_final.minute)
        final_modified = final - timedelta(hours= 5)
        total_venta = 0
        total_compra = 0
        pago_credito_values = []
        compra_credito_values = []
        for caja_p in caja_pago_selected:
            pago_temp = get_object_or_404(Pago,pk=caja_p.pago_id)      
            if pago_temp.forma_pago == 'Debito':
                pago_credito_values.append("-")
                total_venta += pago_temp.total_factura
                pagos.append(pago_temp)

        for abono_caja in caja_abono_pago:
            if abono_caja.valor_abonado == 0:
                pago_credito_values.append(abono_caja.abono_pago.valor_cuota)
                total_venta += abono_caja.abono_pago.valor_cuota
                pagos.append(abono_caja.abono_pago.pago)
            else:
                pago_credito_values.append(abono_caja.valor_abonado)
                total_venta += abono_caja.valor_abonado
                pagos.append(abono_caja.abono_pago.pago)

        for caja_co in caja_pago_selected2:
            compra_temp = get_object_or_404(Gasto,pk=caja_co.compra_id)
            if compra_temp.forma_pago == 'Debito':
                compra_credito_values.append("-")
                total_compra += compra_temp.valor_total
                compras.append(compra_temp)

        for abono_compra_caja in caja_abono_compra:
            if abono_compra_caja.valor_abonado == 0:
                compra_credito_values.append(abono_compra_caja.abono_compra.valor_cuota)
                total_compra += abono_compra_caja.abono_compra.valor_cuota
                compras.append(abono_compra_caja.abono_compra.gasto)
            else:
                compra_credito_values.append(abono_compra_caja.valor_abonado)
                total_compra += abono_compra_caja.valor_abonado
                compras.append(abono_compra_caja.abono_compra.gasto)

        #pagos = Pago.objects.filter(fecha__gte = caja[0].fecha_inicio,fecha__lte= y)
        #compras = Gasto.objects.filter(fecha__gte = caja[0].fecha_inicio,fecha__lte= y)
        
        motivos_pagos = []
        motivos_gastos = []


        
        for pago in pagos:
            
            motivos = []
            if PagoGuarderia.objects.filter(pago__id=pago.id):
                motivos.append("Guardería")

            if PagoProducto.objects.filter(pago__id=pago.id):
                motivos.append("Producto")

            if PagoMedicamento.objects.filter(pago__id=pago.id):
                motivos.append("Medicamento")

            if PagoVacuna.objects.filter(pago__id=pago.id):
                motivos.append("Vacuna")

            if PagoDesparasitante.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitante")

            if PagoTratamiento.objects.filter(pago__id=pago.id):
                motivos.append("Cita médica")

            if PagoEstetica.objects.filter(pago__id=pago.id):
                motivos.append("Estética")

            if PagoVacunacion.objects.filter(pago__id=pago.id):
                motivos.append("Vacunación")

            if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitación")

            if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                motivos.append("Procedimiento")

            if PagoOtro.objects.filter(pago__id=pago.id):
                motivos.append("Otro")

            if PagoElementoInventario.objects.filter(pago__id=pago.id):
                for pago in PagoElementoInventario.objects.filter(pago__id=pago.id):
                    if not pago.elemento.categoria.nombre in motivos:
                        motivos.append(pago.elemento.categoria.nombre)
            
            motivos_pagos.append(motivos)

        for gasto in compras:
         
            motivos = []
            if GastoProducto.objects.filter(gasto__id=gasto.id):
                motivos.append("Producto")

            if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                motivos.append("Medicamento")

            if GastoVacuna.objects.filter(gasto__id=gasto.id):
                motivos.append("Vacuna")

            if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                motivos.append("Desparasitante")

            if GastoServicio.objects.filter(gasto__id=gasto.id):
                motivos.append("Servicio")

            if GastoOtro.objects.filter(gasto__id=gasto.id):
                motivos.append("Otro")

            if GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                for gasto in GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                    if not gasto.elemento.categoria.nombre in motivos:
                        motivos.append(gasto.elemento.categoria.nombre)

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos
        context['motivos_pagos'] = motivos_pagos
        context['pagos_hoy'] = pagos
        context['compras_hoy'] = compras
        context['total_venta'] = total_venta
        context['total_compra'] = total_compra
        context['dinero_inicial'] = caja_selected.dinero_inicial
        context['total'] = caja_selected.dinero_inicial + total_venta - total_compra
        context['caja'] = caja_selected
        month = ['nothing','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
        context['inicio_d'] = inicio_modified
        context['month'] = month[inicio_modified.month]
        context['final'] = final_modified
        context['abonos_pago'] = pago_credito_values
        context['abonos_compra'] = compra_credito_values
        context['abonos_pagos'] = CajaAbonoPago.objects.filter(caja=caja_selected).exclude(pago__estado_pago='Anulada')
        context['abonos_compras'] = CajaAbonoCompra.objects.filter(caja=caja_selected).exclude(gasto__estado_pago='Anulada')

        total_efectivo = caja_selected.dinero_inicial
        total_ventas_efectivo = 0
        total_tarjeta_debito = 0
        total_consignacion = 0
        total_tarjeta_credito = 0
        total_otros = 0

        for pago in pagos:
            if pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Efectivo':
                total_ventas_efectivo += pago.total_factura
                total_efectivo += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Tarjeta debito':
                total_tarjeta_debito += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Consignacion':
                total_consignacion += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Tarjeta credito':
                total_tarjeta_credito += pago.total_factura
            elif pago.forma_pago == 'Debito' and pago.tipo_pago_debito == 'Otro':
                total_otros += pago.total_factura

        for abono in CajaAbonoPago.objects.filter(caja=caja_selected).exclude(pago__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_ventas_efectivo += abono.valor_abonado
                total_efectivo += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_otros += abono.valor_abonado

        total_compras_efectivo = 0
        total_compras_tarjeta_debito = 0
        total_compras_consignacion = 0
        total_compras_tarjeta_credito = 0
        total_compras_otros = 0

        for compra in compras:
            if compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Efectivo':
                total_compras_efectivo += compra.valor_total
                total_efectivo -= compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta debito':
                total_compras_tarjeta_debito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Consignacion':
                total_compras_consignacion += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta credito':
                total_compras_tarjeta_credito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Otro':
                total_compras_otros += compra.valor_total

        for abono in CajaAbonoCompra.objects.filter(caja=caja_selected).exclude(gasto__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_compras_efectivo += abono.valor_abonado
                total_efectivo -= abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_compras_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_compras_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_compras_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_compras_otros += abono.valor_abonado

        context['total_efectivo'] = total_efectivo
        context['total_ventas_efectivo'] = total_ventas_efectivo
        context['total_tarjeta_debito'] = total_tarjeta_debito
        context['total_consignacion'] = total_consignacion
        context['total_tarjeta_credito'] = total_tarjeta_credito
        context['total_otros'] = total_otros

        context['total_compras_efectivo'] = total_compras_efectivo
        context['total_compras_tarjeta_debito'] = total_compras_tarjeta_debito
        context['total_compras_consignacion'] = total_compras_consignacion
        context['total_compras_tarjeta_credito'] = total_compras_tarjeta_credito
        context['total_compras_otros'] = total_compras_otros

        return context

class ListarCajas(ListView):
    model = Pago
    template_name = "listar_cajas.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarCajas, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarCajas, self).get_context_data(**kwargs)
        context['menu_caja'] = True
        cajas = Caja.objects.all()
        cerrar_caja_emergencia()
        for caja_run in cajas:
            #if not caja_run.fecha_final:
                #z = datetime(caja_run.fecha_inicio.year, caja_run.fecha_inicio.month, caja_run.fecha_inicio.day)
                #w = z + timedelta(days=1)
                #caja_run.fecha_final = w
                #caja_run.estado = False
                #caja_run.save()
            #if caja_run.dinero_final == 0:
            total_pagos = 0
            total_compras = 0
            caja_pago_selected = CajaPago.objects.filter(caja = caja_run).exclude(pago__estado_pago='Anulada')
            caja_pago_selected2 = CajaCompra.objects.filter(caja = caja_run).exclude(compra__estado_pago='Anulada')
            caja_abono_venta = CajaAbonoPago.objects.filter(caja = caja_run).exclude(abono_pago__pago__estado_pago='Anulada')
            caja_abono_compra = CajaAbonoCompra.objects.filter(caja=caja_run).exclude(abono_compra__gasto__estado_pago='Anulada')

            for caja_p in caja_pago_selected:
                total_pagos += caja_p.pago.total_factura

            for caja_co in caja_pago_selected2:
                total_compras += caja_co.compra.valor_total

            for caja_abono in caja_abono_venta:
                if caja_abono.valor_abonado == 0:
                    total_pagos += caja_abono.abono_pago.valor_pagado
                else:
                    total_pagos += caja_abono.valor_abonado

            for caja_abono in caja_abono_compra:
                if caja_abono.valor_abonado == 0:
                    total_compras += caja_abono.abono_compra.valor_pagado
                else:
                    total_compras += caja_abono.valor_abonado

            caja_run.dinero_final = caja_run.dinero_inicial+ total_pagos - total_compras
            caja_run.save()
        context['cajas'] = cajas.order_by('-fecha_inicio')
        return context

def abrir_caja(request):
    if request.method == "GET":
        now = datetime.now()
        cerrar_caja_emergencia()   
        x = datetime(now.year, now.month, now.day)
        y = x + timedelta(days=1)

        caja_activa = False

        if Caja.objects.filter(estado=True):
            caja_activa = Caja.objects.filter(estado=True).first()
            if request.user.id == caja_activa.responsable.id:
                return redirect('/caja/listar_pagos_hoy/')

        cajas = Caja.objects.all().order_by('-fecha_inicio')
        for caja_run in cajas:
            #if not caja_run.fecha_final:
                #z = datetime(caja_run.fecha_inicio.year, caja_run.fecha_inicio.month, caja_run.fecha_inicio.day)
                #w = z + timedelta(days=1)
                #caja_run.fecha_final = w
                #caja_run.estado = False
                #caja_run.save()
            if caja_run.dinero_final == 0:
                total_pagos = 0
                total_compras = 0
                caja_pago_selected = CajaPago.objects.filter(caja = caja_run)
                caja_pago_selected2 = CajaCompra.objects.filter(caja = caja_run)
                pagos = []
                compras = []
                for caja_p in caja_pago_selected:
                    pago_temp = get_object_or_404(Pago,pk=caja_p.pago.id)
                    pagos.append(pago_temp)

                for caja_co in caja_pago_selected2:
                    compra_temp = get_object_or_404(Gasto,pk=caja_co.compra.id)
                    compras.append(compra_temp)

                for pago in pagos:
                    total_pagos += pago.total_factura
                for compras_ in compras:
                    total_compras += compras_.valor_total
                caja_run.dinero_final = caja_run.dinero_inicial+ total_pagos - total_compras
                caja_run.save()
        return render(request,'abrir_caja.html',{'form':AbrirCajaForm(),
        'menu_caja' : True, 'cajas': cajas, 'caja_activa': caja_activa})

def agregar_caja(request):
    responsable = get_object_or_404(Usuario, id=request.user.id)
    dinero = request.GET.get('dinero', '')
    now = datetime.now()
    cerrar_caja_emergencia()           
    add_caja = Caja(fecha_inicio = now,estado = True,dinero_inicial=dinero, responsable=responsable)
    add_caja.save()
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)

def cerrar_caja(request,id_caja):
    caja = get_object_or_404(Caja,pk=id_caja)
    now = datetime.now()
    caja.estado = False
    caja.fecha_final = now
    print(caja.estado,caja.fecha_final)
    caja.save()
    data = {
        'cerrar_caja': True
    }
    return JsonResponse(data)
def cerrar_caja_emergencia():
    cajas = Caja.objects.filter(estado=True)
    for caja in cajas:
        now = datetime.now()
        x = datetime(now.year, now.month, now.day)
        z = datetime(caja.fecha_inicio.year, caja.fecha_inicio.month, caja.fecha_inicio.day)
        y = x + timedelta(days=1)
        if z < x and not caja.fecha_final:
            caja_pago_selected = CajaPago.objects.filter(caja = caja).exclude(pago__estado_pago='Anulada')
            caja_pago_selected2 = CajaCompra.objects.filter(caja = caja).exclude(compra__estado_pago='Anulada')
            caja_abono_pago = CajaAbonoPago.objects.filter(caja=caja).exclude(abono_pago__pago__estado_pago='Anulada')
            caja_abono_compra = CajaAbonoCompra.objects.filter(caja = caja).exclude(abono_compra__gasto__estado_pago='Anulada')
            pagos = []
            total_venta = 0
            total_compra = 0
            for caja_p in caja_pago_selected:
                pago_temp = get_object_or_404(Pago,pk=caja_p.pago_id)      
                if pago_temp.forma_pago == 'Debito':
                    total_venta += pago_temp.total_factura

            for abono_caja in caja_abono_pago:
                if abono_caja.valor_abonado == 0:
                    total_venta += abono_caja.abono_pago.valor_cuota
                else:
                    total_venta += abono_caja.valor_abonado
            for caja_co in caja_pago_selected2:
                compra_temp = get_object_or_404(Gasto,pk=caja_co.compra_id)
                if compra_temp.forma_pago == 'Debito':
                    total_compra += compra_temp.valor_total

            for abono_compra_caja in caja_abono_compra:
                if abono_compra_caja.valor_abonado == 0:
                    total_compra += abono_compra_caja.abono_compra.valor_cuota
                else:
                    total_compra += abono_compra_caja.valor_abonado

            caja.dinero_final = caja.dinero_inicial + total_venta - total_compra
            caja.fecha_final = now
            caja.estado = False
            caja.save()

           
@login_required
def crearPago_caja(request):

    if request.method == 'POST':
        
        form_pago = RegistrarPagoForm(request.POST)

        """form_pago_producto = RegistrarPagoProductoForm(request.POST)
        if form_pago.is_valid() and form_pago_producto.is_valid():
            obj_datos_basicos = form_pago.save()
            obj_datos_educacion = form_pago_producto.save()

            return redirect('listar_pagos')
        else:
            print('Por favor verificar los campos en rojo del formulario')"""
        return redirect('/caja/listar_pagos_hoy/')
    else:
        form_pago = RegistrarPagoForm()
        all_productos = Producto.objects.filter(estado=True,bodega=1).exclude(tipo_uso='Interno').distinct('codigo')
        array_productos = json.dumps(list(
            Producto.objects.filter(estado=True,bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'codigo', 'tipo_producto',
                                                             'cantidad', 'precio_venta', 'cantidad_por_unidad',
                                                             'precio_por_unidad', 'cantidad_total_unidades',
                                                             'unidad_medida', 'cantidad_unidad', 'fecha_vencimiento',
                                                             'fecha_vencimiento')), cls=DjangoJSONEncoder)
        array_tratamientos = json.dumps(list(Tratamiento.objects.all().values_list('id', 'nombre', 'nombre', 'precio')))
        array_motivo_cita = json.dumps(list(MotivoCita.objects.all().values_list('id', 'nombre', 'descripcion', 'precio')))
        array_area_consulta = json.dumps(list(MotivoArea.objects.all().values_list('area_consulta', 'historia_clinica','estado')),cls=DecimalEncoder)

        array_medicamentos = json.dumps(list(
            Medicamento.objects.filter(estado='Activo',bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad',
                                                                    'precio_venta', 'cantidad_por_unidad',
                                                                    'precio_por_unidad', 'unidad_medida',
                                                                    'cantidad_unidad', 'cantidad_total_unidades',
                                                                    'codigo_inventario', 'fecha_vencimiento',
                                                                    'fecha_vencimiento')), cls=DjangoJSONEncoder)
        array_vacunas = json.dumps(list(
            Vacuna.objects.filter(estado='Activa',bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad', 'precio_venta',
                                                               'cantidad_por_unidad', 'precio_por_unidad',
                                                               'cantidad_total_unidades', 'unidad_medida',
                                                               'cantidad_unidad', 'codigo_inventario',
                                                               'fecha_vencimiento', 'fecha_vencimiento')),
            cls=DjangoJSONEncoder)
        array_desparasitante = json.dumps(list(
            Desparasitante.objects.filter(estado='Activo',bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad',
                                                                       'precio_venta', 'cantidad_por_unidad',
                                                                       'precio_por_unidad', 'cantidad_total_unidades',
                                                                       'unidad_medida', 'cantidad_unidad',
                                                                       'codigo_inventario', 'fecha_vencimiento',
                                                                       'fecha_vencimiento')), cls=DjangoJSONEncoder)

        productos_repetidos = []
        for element in list(Producto.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo').annotate(cantidad=Count('codigo'))):
            if element['cantidad'] > 1:
                productos_repetidos.append(element['codigo'])

        productos_por_codigo = json.dumps(list(chain(
            Producto.objects.distinct('codigo').filter(estado=True,bodega=1).exclude(codigo__isnull=True).exclude(tipo_uso='Interno').values_list('nombre',
                                                                                                             'codigo',
                                                                                                             'id',
                                                                                                             'estado'),
            Producto.objects.filter(codigo__isnull=True, estado=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre', 'codigo', 'id', 'estado'))),
                                          cls=DecimalEncoder)

        medicamentos_por_codigo = json.dumps(list(chain(
            Medicamento.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
                codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Medicamento.objects.filter(estado='Activo', codigo_inventario__isnull=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre',
                                                                                                    'codigo_inventario',
                                                                                                    'id', 'estado'))),
            cls=DecimalEncoder)

        medicamentos_repetidos = []
        for element in list(
                Medicamento.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                medicamentos_repetidos.append(element['codigo_inventario'])

        vacunas_por_codigo = json.dumps(list(chain(
            Vacuna.objects.distinct('codigo_inventario').filter(estado='Activa',bodega=1).exclude(
                codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Vacuna.objects.filter(estado='Activa', codigo_inventario__isnull=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre',
                                                                                               'codigo_inventario',
                                                                                               'id', 'estado'))),
            cls=DecimalEncoder)

        vacunas_repetidos = []
        for element in list(
                Vacuna.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                vacunas_repetidos.append(element['codigo_inventario'])

        desparasitantes_repetidos = []
        for element in list(
                Desparasitante.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                desparasitantes_repetidos.append(element['codigo_inventario'])

        desparasitantes_por_codigo = json.dumps(list(chain(
            Desparasitante.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
                codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Desparasitante.objects.filter(estado='Activo', codigo_inventario__isnull=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre',
                                                                                                       'codigo_inventario',
                                                                                                       'id',
                                                                                                       'estado'))),
            cls=DecimalEncoder)

        lista_categorias = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))

        return render(request, 'registrar_pago_caja.html', {
            'form_pago': form_pago,
            'all_productos': all_productos,
            'array': array_productos,
            'array_tratamientos': array_tratamientos,
            'menu_caja': True,
            'array_medicamentos': array_medicamentos,
            'array_vacunas': array_vacunas,
            'array_desparasitante': array_desparasitante,
            'array_motivo_cita': array_motivo_cita,
            'array_area_consulta':array_area_consulta,
            'array_id_productos': productos_repetidos,
            'productos_por_codigo': productos_por_codigo,
            'medicamentos_por_codigo': medicamentos_por_codigo,
            'array_id_medicamentos': medicamentos_repetidos,
            'vacunas_por_codigo': vacunas_por_codigo,
            'array_id_vacunas': vacunas_repetidos,
            'desparasitantes_por_codigo': desparasitantes_por_codigo,
            'array_id_desparasitantes': desparasitantes_repetidos,
            'lista_categorias': lista_categorias
        })

def registrarPago_caja(request):
    req = json.loads( request.body.decode('utf-8') )
    #print(req)
    #print("mascota: " + req['mascota'])

    responsable = get_object_or_404(Usuario, id=request.user.id)

    if req['cliente'] == 0:
        date_now = datetime.now()
        x = datetime(date_now.year, date_now.month, date_now.day)
        fecha = x.strftime("%Y-%m-%d")
        nuevo_pago = Pago(fecha=fecha,comentarios=req['comentarios'], total_factura=req['total'],total_impuesto=req['impuesto'],sub_total=req['subtotal'],descuento=req['descuento'],responsable=responsable, tipo_comprobante='Punto de Venta')
        nuevo_pago.save()
    else:
        date_now = datetime.now()
        x = datetime(date_now.year, date_now.month, date_now.day)
        fecha = x.strftime("%Y-%m-%d")
        cliente = get_object_or_404(Cliente, pk=req['cliente'])
        nuevo_pago = Pago(cliente=cliente, fecha=fecha,
                          comentarios=req['comentarios'], total_factura=req['total'], total_impuesto=req['impuesto'],
                          sub_total=req['subtotal'], descuento=req['descuento'], responsable=responsable, tipo_comprobante='Punto de Venta')
        nuevo_pago.save()
    

    nuevo_pago.forma_pago = req['forma_pago']

    if req['forma_pago'] == 'Debito':
        nuevo_pago.tipo_pago_debito = req['medio_pago_debito']
        caja_on = Caja.objects.filter(estado= True)
        if caja_on:
            now = datetime.now()
            x = datetime(now.year, now.month, now.day)
            z = datetime.strptime(nuevo_pago.fecha, "%Y-%m-%d")
            y = x + timedelta(days=1)
            if z >= x and z < y:
                caja_pago = CajaPago(caja = caja_on[0], pago=nuevo_pago)
                caja_pago.save()

    if req['forma_pago'] == 'Credito':
        nuevo_pago.numero_cuotas_credito = req['numero_cuotas']
        nuevo_pago.saldo = req['total']
        nuevo_pago.estado_pago = 'Pendiente'

        for cuota in req['cuotas']:
            mi_cuota = CuotaPago(pago=nuevo_pago, fecha_limite=cuota['fecha'], valor_cuota=cuota['abono'], saldo_cuota=cuota['abono'])
            if mi_cuota.fecha_limite == nuevo_pago.fecha:
                abono_pago = AbonoPago(pago=nuevo_pago, valor_abonado=mi_cuota.saldo_cuota, medio_pago=req['medio_pago_credito'])
                abono_pago.save()
                nuevo_pago.saldo = nuevo_pago.saldo - int(mi_cuota.saldo_cuota)
                nuevo_pago.save()
                mi_cuota.valor_pagado = int(mi_cuota.saldo_cuota)
                mi_cuota.saldo_cuota = 0
                mi_cuota.estado_pago = 'Pagada'
                mi_cuota.fecha_pago = mi_cuota.fecha_limite
                mi_cuota.save()
                caja_on = Caja.objects.filter(estado= True)
                if caja_on:
                    abono_caja_pago = CajaAbonoPago(caja = caja_on[0], abono_pago=mi_cuota, medio_pago=req['medio_pago_credito'],pago=nuevo_pago,valor_abonado=mi_cuota.valor_pagado)
                    abono_caja_pago.save()
            
            else:
                mi_cuota.save()

    messages.success(request, 'Venta registrada con exito!')

    nuevo_pago.save()

    for producto in req['productos']:

        if producto['tipo'] == 'Producto':
            mi_producto = get_object_or_404(Producto, pk=producto['id'])
            #print(producto)
            cantidad = float(producto['cantidad'])
            pago_producto = PagoProducto(producto=mi_producto,pago=nuevo_pago,cantidad=cantidad, total=int(producto['costo_total']))
            
            #print(pago_producto)
            #mi_producto.cantidad = mi_producto.cantidad - cantidad
            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_producto.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_producto.cantidad = int(mi_producto.cantidad) - cantidad

                mi_producto.cantidad_total_unidades = int(mi_producto.cantidad_total_unidades) - ( cantidad * int(mi_producto.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                       id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                #print(mi_producto.cantidad_total_unidades,mi_producto.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_producto.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_producto.cantidad_total_unidades = int(mi_producto.cantidad_total_unidades) - cantidad

                mi_producto.cantidad = math.floor( int(mi_producto.cantidad_total_unidades) / int(mi_producto.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_producto.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                       id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                #print(mi_producto.cantidad_total_unidades,mi_producto.cantidad,"a granel")
            pago_producto.save()
            mi_producto.save()

            

        elif producto['tipo'] == 'Medicamento':
            mi_medicamento = get_object_or_404(Medicamento, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_medicamento = PagoMedicamento(medicamento=mi_medicamento, pago=nuevo_pago, cantidad=cantidad,
                                         total=int(producto['costo_total']))
            #pago_medicamento.save()
            #mi_medicamento.cantidad = mi_medicamento.cantidad - cantidad
            #mi_medicamento.save()
            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_medicamento.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_medicamento.cantidad = int(mi_medicamento.cantidad) - cantidad

                mi_medicamento.cantidad_total_unidades = int(mi_medicamento.cantidad_total_unidades) - ( cantidad * int(mi_medicamento.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                #print(mi_medicamento.cantidad_total_unidades,mi_medicamento.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_medicamento.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_medicamento.cantidad_total_unidades = int(mi_medicamento.cantidad_total_unidades) - cantidad

                mi_medicamento.cantidad = math.floor( int(mi_medicamento.cantidad_total_unidades) / int(mi_medicamento.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_medicamento.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                #print(mi_medicamento.cantidad_total_unidades,mi_medicamento.cantidad,"a granel")

            pago_medicamento.save()
            mi_medicamento.save()

            
            #historia_transaccion.save()

        elif producto['tipo'] == 'Vacuna':
            mi_vacuna = get_object_or_404(Vacuna, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_vacuna = PagoVacuna(vacuna=mi_vacuna, pago=nuevo_pago, cantidad=cantidad,
                                         total=int(producto['costo_total']))
            
            #mi_vacuna.cantidad = mi_vacuna.cantidad - cantidad
            

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_vacuna.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_vacuna.cantidad = int(mi_vacuna.cantidad) - cantidad

                mi_vacuna.cantidad_total_unidades = int(mi_vacuna.cantidad_total_unidades) - ( cantidad * int(mi_vacuna.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                       id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                #print(mi_vacuna.cantidad_total_unidades,mi_vacuna.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_vacuna.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_vacuna.cantidad_total_unidades = int(mi_vacuna.cantidad_total_unidades) - cantidad

                mi_vacuna.cantidad = math.floor( int(mi_vacuna.cantidad_total_unidades) / int(mi_vacuna.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_vacuna.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                       id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                #print(mi_vacuna.cantidad_total_unidades,mi_vacuna.cantidad,"a granel")


            pago_vacuna.save()
            mi_vacuna.save()

        elif producto['tipo'] == 'Desparasitante':
            mi_desparasitante = get_object_or_404(Desparasitante, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_desparasitante = PagoDesparasitante(desparasitante=mi_desparasitante, pago=nuevo_pago, cantidad=cantidad,
                                         total=int(producto['costo_total']))

            #mi_desparasitante.cantidad = mi_desparasitante.cantidad - cantidad


            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_desparasitante.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_desparasitante.cantidad = int(mi_desparasitante.cantidad) - cantidad

                mi_desparasitante.cantidad_total_unidades = int(mi_desparasitante.cantidad_total_unidades) - ( cantidad * int(mi_desparasitante.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                       id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                print(mi_desparasitante.cantidad_total_unidades,mi_desparasitante.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_desparasitante.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_desparasitante.cantidad_total_unidades = int(mi_desparasitante.cantidad_total_unidades) - cantidad

                mi_desparasitante.cantidad = math.floor( int(mi_desparasitante.cantidad_total_unidades) / int(mi_desparasitante.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_desparasitante.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                       id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                print(mi_desparasitante.cantidad_total_unidades,mi_desparasitante.cantidad,"a granel")

            mi_desparasitante.save()
            pago_desparasitante.save()

        else:
            mi_elemento = get_object_or_404(ElementoCategoriaInventario, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_elemento = PagoElementoInventario(elemento=mi_elemento, pago=nuevo_pago,
                                                     cantidad=cantidad,
                                                     total=int(producto['costo_total']))

            # mi_desparasitante.cantidad = mi_desparasitante.cantidad - cantidad

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_elemento.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_elemento.cantidad = int(mi_elemento.cantidad) - cantidad

                mi_elemento.cantidad_total_unidades = int(mi_elemento.cantidad_total_unidades) - (
                            cantidad * int(mi_elemento.cantidad_unidad))

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                print(mi_elemento.cantidad_total_unidades, mi_elemento.cantidad, "unidad")

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_elemento.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_elemento.cantidad_total_unidades = int(mi_elemento.cantidad_total_unidades) - cantidad

                mi_elemento.cantidad = math.floor(
                    int(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_elemento.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
                print(mi_elemento.cantidad_total_unidades, mi_elemento.cantidad, "a granel")

            mi_elemento.save()
            pago_elemento.save()

    for vacunacion in req['vacunaciones']:
        mi_vacunacion = get_object_or_404(Vacunacion, pk=vacunacion['id'])
        pago_vacunacion = PagoVacunacion(vacunacion=mi_vacunacion, pago=nuevo_pago, total=int(vacunacion['costo']))
        pago_vacunacion.save()
        mi_vacunacion.costo = vacunacion['costo']
        mi_vacunacion.estado = 'Pagada'
        mi_vacunacion.save()

    for desparasitacion in req['desparasitaciones']:
        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=desparasitacion['id'])
        pago_desparasitacion = PagoDesparasitacion(desparasitacion=mi_desparasitacion, pago=nuevo_pago, total=int(desparasitacion['costo']))
        pago_desparasitacion.save()
        mi_desparasitacion.costo = desparasitacion['costo']
        mi_desparasitacion.estado = 'Pagada'
        mi_desparasitacion.save()

    for estetica in req['esteticas']:
        mi_estetica = get_object_or_404(FacturaEstetica, pk=estetica['id'])
        pago_estetica = PagoEstetica(estetica=mi_estetica, pago=nuevo_pago, total=int(estetica['valor_total']))
        pago_estetica.save()
        mi_estetica.saldo = 0
        mi_estetica.estado = 'Pagada'
        mi_estetica.save()

    for tratamiento in req['tratamientos']:
        if tratamiento['nombre_tratamiento'] == "Motivo Cita":
            mi_tratamiento = get_object_or_404(HistoriaClinica, pk=tratamiento['id'])
            pago_tratamiento = PagoTratamiento(tratamiento=mi_tratamiento, pago=nuevo_pago,
                                               total=int(tratamiento['costo']))
            pago_tratamiento.save()
            mi_tratamiento.costo = tratamiento['costo']
            mi_tratamiento.estado = 'Pagada'
            # print(str(pago_tratamiento),str(mi_tratamiento))
            mi_tratamiento.save()
        if tratamiento['nombre_tratamiento'] == "Area Consulta":
            mi_tratamiento = get_object_or_404(HistoriaClinica, pk=tratamiento['id'])
            mi_area = get_object_or_404(Tratamiento, pk=tratamiento['motivo_cita'])
            motivo_cita = get_object_or_404(MotivoArea, historia_clinica=mi_tratamiento, area_consulta=mi_area)
            pago_area = PagoAreaConsulta(area_consulta=motivo_cita, pago=nuevo_pago, total=int(tratamiento['costo']))
            pago_area.save()
            motivo_cita.estado = 'Pagada'
            motivo_cita.save()
            # print(str(pago_area),str(motivo_cita))

    for guarderia in req['guarderias']:
        mi_guarderia = get_object_or_404(Guarderia, pk=guarderia['id'])
        pago_guarderia = PagoGuarderia(guarderia=mi_guarderia, pago=nuevo_pago, total=int(guarderia['costo']))
        pago_guarderia.save()
        mi_guarderia.estado = 'Pagada'
        mi_guarderia.save()

    for otro in req['otros']:
        pago_otro = PagoOtro(descripcion=otro['nombre'], pago=nuevo_pago, total=int(otro['costo_total']))
        pago_otro.save()

    data = {
        'registrado': True,
        'id_pago': nuevo_pago.id
    }

    return JsonResponse(data)

class CrearGastoCaja(CreateView):
    model = Gasto
    form_class = CrearGastoForm
    template_name = "registrar_gasto_caja.html"
    success_url = reverse_lazy('listar_pagos_hoy')

    def form_valid(self,form):
        self.object = form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearGasto, self).get_context_data(**kwargs)
        context['gastos'] = True
        return context

def registrar_compra_caja(request):
    if request.method == 'POST':
        print("Prueba POST registrar compra")
        form_registrar_compra = Formulario_registrar_compra()
        req = json.loads( request.body.decode('utf-8') )
        date_now = datetime.now()
        x = datetime(date_now.year, date_now.month, date_now.day)
        fecha = x.strftime("%Y-%m-%d")
        comentarios = str(req['comentarios'])
        subtotal = str(req['subtotal'])
        descuento = str(req['descuento'])
        total_impuesto = str(req['total_impuesto'])
        total = str(req['total'])
        numero_comprobante = str(req['numero_comprobante'])

        responsable = get_object_or_404(Usuario, id=request.user.id)

        if req['proveedor']:

            id_proveedor = str(req['proveedor'])
            proveedor = get_object_or_404(Proveedor, pk=id_proveedor)

            nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                sub_total=subtotal, descuento=descuento, proveedor=proveedor, responsable=responsable,numero_comprobante=numero_comprobante, tipo_comprobante='Punto de Venta')
            nuevo_gasto.save()
            

        else:
            nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                sub_total=subtotal, descuento=descuento, responsable=responsable,numero_comprobante=numero_comprobante, tipo_comprobante='Punto de Venta')
            nuevo_gasto.save()
            
    

        nuevo_gasto.forma_pago = req['forma_pago']

        if req['forma_pago'] == 'Debito':
            nuevo_gasto.tipo_pago_debito = req['medio_pago_debito']
            caja_on = Caja.objects.filter(estado= True)
            if caja_on:
                now = datetime.now()
                x = datetime(now.year, now.month, now.day)
                z = datetime(x.year,x.month,x.day)
                y = x + timedelta(days=1)
                if z >= x and z < y:
                    caja_compra = CajaCompra(caja = caja_on[0], compra=nuevo_gasto)
                    caja_compra.save()

        if req['forma_pago'] == 'Credito':
            nuevo_gasto.numero_cuotas_credito = req['numero_cuotas']
            nuevo_gasto.saldo = int(req['total'])
            nuevo_gasto.estado_pago = 'Pendiente'

            for cuota in req['cuotas']:
                mi_cuota = CuotaGasto(gasto=nuevo_gasto, fecha_limite=cuota['fecha'], valor_cuota=cuota['abono'],
                                     saldo_cuota=cuota['abono'])
                fecha_comparar_init = datetime.strptime(mi_cuota.fecha_limite, "%Y-%m-%d")
                fecha_comparar = datetime(x.year,x.month,x.day)
                fecha_limite_comparar = datetime(fecha_comparar_init.year,fecha_comparar_init.month,fecha_comparar_init.day)

                if fecha_limite_comparar == fecha_comparar:
                    abono_gasto = AbonoGasto(gasto=nuevo_gasto, valor_abonado=mi_cuota.saldo_cuota,
                                           medio_pago=req['medio_pago_credito'])
                    abono_gasto.save()
                    nuevo_gasto.saldo = nuevo_gasto.saldo - int(mi_cuota.saldo_cuota)
                    nuevo_gasto.save()
                    mi_cuota.valor_pagado = int(mi_cuota.saldo_cuota)
                    mi_cuota.saldo_cuota = 0
                    mi_cuota.estado_pago = 'Pagada'
                    mi_cuota.fecha_pago = mi_cuota.fecha_limite
                    mi_cuota.save()
                    caja_on = Caja.objects.filter(estado= True)
                    if caja_on:
                        caja_compra = CajaAbonoCompra(caja = caja_on[0], abono_compra=mi_cuota, medio_pago=req['medio_pago_credito'],gasto=nuevo_gasto,valor_abonado=mi_cuota.valor_pagado)
                        caja_compra.save()
                    

                else:
                    mi_cuota.save()

        messages.success(request, 'Compra registrada con exito!')

        nuevo_gasto.save()


        for elemento in req['elementos_agregados']:

            if(elemento['tipo'] == 'Servicio'):

                gasto_servicio = GastoServicio(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                gasto_servicio.save()

                if elemento['id_impuesto'] != '0':
                    try:
                        mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                        valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                        gasto_servicio.impuesto = mi_impuesto
                        gasto_servicio.valor_total_impuesto = valor_total_impuesto
                        gasto_servicio.save()
                    except:
                        print("Impuesto no encontrado")

            elif (elemento['tipo'] == 'Otro'):

                gasto_otro = GastoOtro(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                       precio_compra_unidad=elemento['costo_unitario'],
                                       unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                gasto_otro.save()

                if elemento['id_impuesto'] != '0':
                    try:
                        mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                        valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                        gasto_otro.impuesto = mi_impuesto
                        gasto_otro.valor_total_impuesto = valor_total_impuesto
                        gasto_otro.save()
                    except:
                        print("Impuesto no encontrado")

            elif (elemento['tipo'] == 'Producto'):
                try:
                    mi_elemento = get_object_or_404(Producto, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras', id_transaccion=nuevo_gasto.id)
                    historia_transaccion.save()

                    gasto_producto = GastoProducto(gasto=nuevo_gasto, producto=mi_elemento,
                                                   precio_compra_unidad=elemento['costo_unitario'],
                                                   unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_producto.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_producto.impuesto = mi_impuesto
                            gasto_producto.valor_total_impuesto = valor_total_impuesto
                            gasto_producto.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Producto no encontrado")

            elif (elemento['tipo'] == 'Medicamento'):
                try:
                    mi_elemento = get_object_or_404(Medicamento, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id)
                    historia_transaccion.save()

                    gasto_medicamento = GastoMedicamento(gasto=nuevo_gasto, medicamento=mi_elemento,
                                                         precio_compra_unidad=elemento['costo_unitario'],
                                                         unidades=elemento['unidades'],
                                                         precio_total=elemento['costo_total'])
                    gasto_medicamento.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_medicamento.impuesto = mi_impuesto
                            gasto_medicamento.valor_total_impuesto = valor_total_impuesto
                            gasto_medicamento.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Medicamento no encontrado")

            elif (elemento['tipo'] == 'Vacuna'):
                try:
                    mi_elemento = get_object_or_404(Vacuna, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id)
                    historia_transaccion.save()

                    gasto_vacuna = GastoVacuna(gasto=nuevo_gasto, vacuna=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_vacuna.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_vacuna.impuesto = mi_impuesto
                            gasto_vacuna.valor_total_impuesto = valor_total_impuesto
                            gasto_vacuna.save()
                        except:
                            print("Impuesto no encontrado")

                except:
                    print("Vacuna no encontrado")

            elif (elemento['tipo'] == 'Desparasitante'):
                try:
                    mi_elemento = get_object_or_404(Desparasitante, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id)
                    historia_transaccion.save()

                    gasto_desparasitante = GastoDesparasitante(gasto=nuevo_gasto, desparasitante=mi_elemento,
                                                               precio_compra_unidad=elemento['costo_unitario'],
                                                               unidades=elemento['unidades'],
                                                               precio_total=elemento['costo_total'])
                    gasto_desparasitante.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_desparasitante.impuesto = mi_impuesto
                            gasto_desparasitante.valor_total_impuesto = valor_total_impuesto
                            gasto_desparasitante.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Desparasitante no encontrado")

            elif (elemento['tipo'] == 'Insumo'):
                try:
                    mi_elemento = get_object_or_404(Insumo, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Insumo',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id)
                    historia_transaccion.save()

                    gasto_insumo = GastoInsumo(gasto=nuevo_gasto, insumo=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_insumo.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_insumo.impuesto = mi_impuesto
                            gasto_insumo.valor_total_impuesto = valor_total_impuesto
                            gasto_insumo.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Insumo no encontrado")

            else:
                try:
                    mi_elemento = get_object_or_404(ElementoCategoriaInventario, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id)
                    historia_transaccion.save()

                    gasto_elemento_categoria = GastoElementoInventario(gasto=nuevo_gasto, elemento=mi_elemento,
                                                                       precio_compra_unidad=elemento['costo_unitario'],
                                                                       unidades=elemento['unidades'],
                                                                       precio_total=elemento['costo_total'])
                    gasto_elemento_categoria.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_elemento_categoria.impuesto = mi_impuesto
                            gasto_elemento_categoria.valor_total_impuesto = valor_total_impuesto
                            gasto_elemento_categoria.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Elemento no encontrado")

        data = {
            'registrado_exitoso': True,
            'id_compra': nuevo_gasto.id
        }
        return JsonResponse(data)

    else:
        print("Prueba GET registrar compra")
        form_registrar_compra = Formulario_registrar_compra()
        lista_categorias = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))

        lista_bodegas = json.dumps(list(Bodega.objects.filter(estado=True).values_list('id', 'nombre')))

        return render(request, 'registrar_gasto_caja.html',{
            'form_registrar_compra': form_registrar_compra,
            'menu_caja': True,
            'lista_categorias': lista_categorias,
            'lista_bodegas': lista_bodegas
        })

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)
