from django.urls import path
from .views import *

urlpatterns = [
    #path('registrar_cita/', CrearCita.as_view(), name="registrar_cita"),
    path('listar_pagos_hoy/', ListarPagosHoy.as_view(), name="listar_pagos_hoy"),
    path('abrir_caja/', abrir_caja, name="abrir_caja"),
    path('cerrar_caja/<int:id_caja>', cerrar_caja, name="cerrar_caja"),
    path('listar_cajas/', ListarCajas.as_view(), name="listar_cajas"),
    path('detalle_caja/<int:id_caja>/', ListarPagosSelected.as_view(), name="detalle_caja"),
    path('registrar_compra_caja/', registrar_compra_caja, name="registrar_compra_caja"),
    path('registrar_pago_caja/', crearPago_caja, name="registrar_pago_caja"),
    path('registrar_gasto_caja/', CrearGastoCaja.as_view(), name="registrar_gasto"),
    path('ajax/registrar_pago_caja/', registrarPago_caja, name="ajax_registrar_pago"),
    path('agregar_caja/', agregar_caja, name="agregar_caja"),
]