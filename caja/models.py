from django.db import models
from django.db import connection
from productos.models  import *
from gastos.models import *
from usuarios.models import Usuario

class Caja(models.Model):
    fecha_inicio = models.DateTimeField(auto_now_add=True)
    fecha_final = models.DateTimeField(null = True)
    estado = models.BooleanField(default = False)
    dinero_inicial = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    dinero_final = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)

class CajaPago(models.Model):
    caja = models.ForeignKey(Caja, on_delete=models.CASCADE)
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)

class CajaCompra(models.Model):
    caja = models.ForeignKey(Caja, on_delete=models.CASCADE)
    compra = models.ForeignKey(Gasto, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)

class CajaAbonoPago(models.Model):
    caja = models.ForeignKey(Caja, on_delete=models.CASCADE)
    abono_pago = models.ForeignKey(CuotaPago, on_delete=models.CASCADE, blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    valor_abonado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE, blank=True, null=True)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')

class CajaAbonoCompra(models.Model):
    caja = models.ForeignKey(Caja, on_delete=models.CASCADE)
    abono_compra = models.ForeignKey(CuotaGasto, on_delete=models.CASCADE, blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    valor_abonado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    gasto = models.ForeignKey(Gasto, on_delete=models.CASCADE, blank=True, null=True)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')
