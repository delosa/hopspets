from django.urls import path
from .views import *

urlpatterns = [
    path('listar_departamentos/', listar_departamento, name="listar_depatamentos"),
    path('listar_municipios/<int:id>/', listar_municipio, name="listar_municipios"),
]