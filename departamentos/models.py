from django.db import models

# Create your models here.

class Departamento(models.Model):
    id_departamento = models.CharField(max_length=100, verbose_name='Codigo Departamento', primary_key=True)
    departamento = models.CharField(max_length=100, verbose_name='Departamento', blank=True)

    def __str__(self):
        return self.departamento

class Municipios(models.Model):
    municipio = models.CharField(max_length=100, verbose_name='Municipio', blank=True)
    estado = models.CharField(max_length=1, verbose_name="Estado", default='1')
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)

    def __str__(self):
        return self.municipio