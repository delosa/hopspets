from .models import *
from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

# Create your views here.
def listar_departamento(request):
    id_cliente = request.GET.get('', '')
    mi_cliente = get_object_or_404(Departamento, pk=id_cliente)
    mi_cliente.delete()
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)

def listar_municipio(request,id):
    id_departamento = request.GET.get('id', '')
    print(id_departamento)
    print(id)
    mis_municipios = Municipios.objects.filter(departamento=id_departamento)
    data = {
        'municipios': serializers.serialize('json', mis_municipios)
    }
    return JsonResponse(data)