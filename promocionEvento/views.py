from django.shortcuts import render,get_object_or_404,redirect,render_to_response
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

class SubirPromocionEvento(SuccessMessageMixin, CreateView):
	model = PromocionEvento 
	form_class = CrearPromocionEventoForm
	template_name = "subir_promocion_evento.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(SubirPromocionEvento, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return '/promocionEvento/listar_promo_evento/'

	def form_valid(self, form):
		promo = form.instance
		self.object = form.save()
		messages.success(self.request, "Se ha guardado su imagen exitosamente")
		return super(SubirPromocionEvento, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, "Error al registrar su imagen, por favor revise los datos")
		return super(SubirPromocionEvento, self).form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super(SubirPromocionEvento, self).get_context_data(**kwargs)
		context['promocionEvento'] = True
		return context


class ListarPromocionesEventos(ListView):
    model = PromocionEvento
    template_name = "listar_promo_event.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPromocionesEventos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPromocionesEventos, self).get_context_data(**kwargs)
        context['promocionEvento'] = True

        promocionEvento = PromocionEvento.objects.all()

        context['promocionEvento'] = promocionEvento
        
        return context

def detallePromo(request, id_promo):
	promo = PromocionEvento.objects.get(id=id_promo)

	return render(request, 'detalle_promocion_evento.html', {
		'promo': promo,
	})