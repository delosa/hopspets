from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from .forms import *

urlpatterns = [
	path('subida_promocion_evento/', SubirPromocionEvento.as_view(), name="subida_promocion_evento"),
	path('listar_promo_evento/', ListarPromocionesEventos.as_view(), name="listar_promo_evento"),
	path('detalle_promo_evento/<int:id_promo>/', detallePromo, name="detalle_promo_evento")
]