from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404

class CrearPromocionEventoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearPromocionEventoForm, self).__init__(*args, **kwargs)


        self.fields['fecha_subida'].initial = datetime.now()
        self.fields['fecha_subida'].label = "Fecha de Subida"
        self.fields['titulo_promo_eve'].required = True
        self.fields['titulo_promo_eve'].label = "Título de la Promocion u Evento"
        self.fields['descripcion_promo_eve'].widget.attrs = {'rows': '5', 'placeholder':'Describa de que se trata la Promocion'}
        self.fields['descripcion_promo_eve'].label = "Descripción"
        self.fields['categoria_promo_eve'].required = True
        self.fields['categoria_promo_eve'].label = "Categoría"
        self.fields['promocion_evento'].required = True
        self.fields['promocion_evento'].label = 'Promocion o Evento'
        self.fields['promocion_evento'].widget.attrs = {'multiple': 'true'}

    class Meta:
        model = PromocionEvento
        fields = (
            'fecha_subida',
            'titulo_promo_eve',
            'descripcion_promo_eve',
            'categoria_promo_eve',
            'promocion_evento',
        )

        widgets = {
            'descripcion_promo_eve': forms.Textarea(),
            'fecha_subida': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'promocion_evento': forms.FileInput()
        }