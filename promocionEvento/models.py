# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from sorl.thumbnail import ImageField, get_thumbnail
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re

def get_upload_to(instance, filename):
    return 'upload/%s/%s' % ('PromocionEvento', filename)

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')

class PromocionEvento(models.Model):
    fecha_subida = models.DateTimeField()
    titulo_promo_eve = models.CharField(max_length=30, verbose_name="titulo_promo_eve")
    descripcion_promo_eve = models.CharField(max_length=100, verbose_name="descripcion_promo_eve")
    categoria_promo_eve = models.CharField(max_length=30, verbose_name="categoria_promo_eve")
    promocion_evento = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True, verbose_name="promocion_evento")

    def save(self, *args, **kwargs):
        if self.promocion_evento:
            if self.promocion_evento.url.endswith('.png') or self.promocion_evento.url.endswith('.PNG'):
                print("Es un png")
                self.promocion_evento = self.promocion_evento
            else:
                self.promocion_evento = self.compressImage(self.promocion_evento)
        super(PromocionEvento, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()

            if imageTemproary.width > 1028 or imageTemproary.height > 1028:
                maxsize = (1028, 1028)
                imageTemproary.thumbnail(maxsize, Image.ANTIALIAS)

            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        return self.titulo_promo_eve