from django.shortcuts import render,get_object_or_404,redirect,render_to_response
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

class SubirVideoTutorial(SuccessMessageMixin, CreateView):
	model = VideosTutoriales 
	form_class = CrearVideoTutorialForm
	template_name = "subir_video_tutorial.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(SubirVideoTutorial, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return '/videosTutoriales/listar_videos_tutoriales/'

	def form_valid(self, form):
		video = form.instance
		self.object = form.save()
		messages.success(self.request, "Se ha guardado el video exitosamente")
		return super(SubirVideoTutorial, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
		return super(SubirVideoTutorial, self).form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super(SubirVideoTutorial, self).get_context_data(**kwargs)
		context['video'] = True
		return context


class ListarVideosTutoriales(ListView):
    model = VideosTutoriales
    template_name = "listar_videos_tutoriales.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarVideosTutoriales, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarVideosTutoriales, self).get_context_data(**kwargs)
        context['videosTutoriales'] = True

        videosTutoriales = VideosTutoriales.objects.all()

        context['videosTutoriales'] = videosTutoriales
        
        return context

class VerVideos(ListView):
	model = VideosTutoriales
	template_name = "ver_videos.html"
	
	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(VerVideos, self).dispatch(request, *args, **kwargs)
		
	def get_context_data(self, **kwargs):
		context = super(VerVideos, self).get_context_data(**kwargs)

		titulo = self.kwargs['criterio_busqueda']

		video = VideosTutoriales.objects.filter(titulo_video=titulo)

		context['video'] = video
		
		return context

def detalleVideo(request, id_video):
    video = VideosTutoriales.objects.get(id=id_video)

    return render (request, 'detalle_video.html', {
        'video': video,
    })