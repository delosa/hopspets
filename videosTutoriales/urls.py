from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from .forms import *

urlpatterns = [
	path('subida_video_tutorial/', SubirVideoTutorial.as_view(), name="subida_video_tutorial"),
	path('listar_videos_tutoriales/', ListarVideosTutoriales.as_view(), name="listar_videos_tutoriales"),
	path('ver_videos/<str:titulo_video>/', VerVideos.as_view(), name="ver_videos"),
	path('detalle_video/<int:id_video>/', detalleVideo, name="detalle_video"),
]