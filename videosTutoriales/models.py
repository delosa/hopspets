# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re

def get_upload_to(instance, filename):
    return 'upload/%s/%s' % ('VideosTutoriales', filename)

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.zip','.tar','.mp4', '.mp3']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Extension del archivo no valida')


class VideosTutoriales(models.Model):
    fecha_subida = models.DateTimeField()
    titulo_video = models.CharField(max_length=30, verbose_name="titulo_video")
    descripcion_video = models.CharField(max_length=100, verbose_name="descripcion_video")
    categoria_video = models.CharField(max_length=30, verbose_name="categoria_video")
    video_tutorial = models.FileField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True, verbose_name="video_tutorial")

    def nombre_archivo(self):
        if len(str(self.video_tutorial).split('/')) == 2:
            return str(self.video_tutorial).split('/')[1]
        elif len(str(self.video_tutorial).split('/')) == 1:
            return str(self.video_tutorial).split('/')[0]
        else:
            return str(self.video_tutorial)