from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404

class CrearVideoTutorialForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearVideoTutorialForm, self).__init__(*args, **kwargs)


        self.fields['fecha_subida'].initial = datetime.now()
        self.fields['fecha_subida'].label = "Fecha de Subida"
        self.fields['titulo_video'].required = True
        self.fields['titulo_video'].label = "Título del Video"
        self.fields['descripcion_video'].widget.attrs = {'rows': '5', 'placeholder':'Describa de que se trata el video'}
        self.fields['descripcion_video'].label = "Descripción del Video"
        self.fields['categoria_video'].required = True
        self.fields['categoria_video'].label = "Categoría del Video"
        self.fields['video_tutorial'].required = True
        self.fields['video_tutorial'].label = 'Video Tutorial'
        self.fields['video_tutorial'].widget.attrs = {'multiple': 'true'}

    class Meta:
        model = VideosTutoriales
        fields = (
            'fecha_subida',
            'titulo_video',
            'descripcion_video',
            'categoria_video',
            'video_tutorial',
        )

        widgets = {
            'descripcion_video': forms.Textarea(),
            'fecha_subida': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }