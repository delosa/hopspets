from django.apps import AppConfig


class VideosTutorialesConfig(AppConfig):
    name = 'videosTutoriales'