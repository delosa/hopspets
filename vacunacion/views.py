from django.shortcuts import render
from django.views.generic import *
from .forms import *
from .models import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from mascotas.models import Mascota
from django.shortcuts import get_object_or_404,redirect
import datetime
from dateutil.relativedelta import relativedelta
from productos.models import HistorialInventario
from django.http import JsonResponse
from django.core import serializers
import json
import math
from gastos.models import GastoVacuna
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from insumos.models import Insumo, InsumoVacunacion
from decimal import Decimal
from django.db.models import Q
from usuarios.models import Usuario
from itertools import chain
from django.db.models import Count
from django.core.serializers.json import DjangoJSONEncoder


class CrearVacuna(SuccessMessageMixin, CreateView):
    model = Vacuna
    form_class = CrearVacunaForm
    template_name = "registrar_vacuna.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡La vacuna se agregó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearVacuna, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        vacuna = form.instance

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro',tipo_elemento='Vacuna',responsable=responsable)
        historia_transaccion.save()

        vacuna.save()
        historia_transaccion.id_elemento = vacuna.id
        historia_transaccion.save()

        return super(CrearVacuna, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearVacuna, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_inventario'] = True
        return context


class ListarVacunas(ListView):
    model = Vacuna
    template_name = "listar_vacunas.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarVacunas, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarVacunas, self).get_context_data(**kwargs)
        context['menu_vacunas'] = True
        return context


class EditarVacuna(SuccessMessageMixin, UpdateView):
    model = Vacuna
    form_class = CrearVacunaForm
    template_name = "registrar_vacuna.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡La vacuna fue modificada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVacuna, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        vacuna = form.instance

        mi_vacuna = get_object_or_404(Vacuna, id=vacuna.id)

        if (mi_vacuna.cantidad != vacuna.cantidad) or (mi_vacuna.cantidad_unidad != vacuna.cantidad_unidad):
            cantidad_nueva = vacuna.cantidad
            vacuna.cantidad_total_unidades = cantidad_nueva * vacuna.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',tipo_elemento='Vacuna', id_elemento=vacuna.id,responsable=responsable)
        historia_transaccion.save()

        self.object = form.save()
        return super(EditarVacuna, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarVacuna, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        return context


class DetalleVacuna(DetailView):
    model = Vacuna
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleVacuna, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleVacuna, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'vacuna'

        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        compras = GastoVacuna.objects.filter(vacuna_id=self.kwargs['pk'])

        context['compras'] = compras

        mi_elemento = Vacuna.objects.get(id=self.kwargs['pk'])

        if mi_elemento.cantidad_unidad != 0 and mi_elemento.cantidad_por_unidad:
            unidades_enteras = math.floor(float(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))
            cantidad_no_entera = round((float(mi_elemento.cantidad_total_unidades) % int(mi_elemento.cantidad_unidad)),1)
        else:
            unidades_enteras = mi_elemento.cantidad
            cantidad_no_entera = 0

        if cantidad_no_entera == 0:
            if mi_elemento.cantidad_por_unidad:
                context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                    mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida
            else:
                context['disponibilidad'] = str(unidades_enteras) + " unidades"
        else:
            context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida + " y 1 unidad de " + str(
                cantidad_no_entera) + " " + mi_elemento.unidad_medida

        return context


class EditarVacunacion(SuccessMessageMixin, UpdateView):
    model = Vacunacion
    form_class = EditarVacunacionForm
    template_name = "editar_vacunacion.html"
    success_message = "¡Vacunación editada correctamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVacunacion, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_vacunacion = get_object_or_404(Vacunacion, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(mi_vacunacion.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarVacunacion, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_vacunacion = get_object_or_404(Vacunacion, pk=self.kwargs['pk'])
        context['mascota'] = mi_vacunacion.mascota
        return context


class EditarVacunacionAgenda(SuccessMessageMixin, UpdateView):
    model = Vacunacion
    form_class = EditarVacunacionForm
    template_name = "editar_vacunacion.html"
    success_url = reverse_lazy('listar_citas')
    success_message = "¡Vacunación editada correctamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVacunacionAgenda, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarVacunacionAgenda, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_vacunacion = get_object_or_404(Vacunacion, pk=self.kwargs['pk'])
        context['mascota'] = mi_vacunacion.mascota
        return context


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


def aplicar_vacunacion(request, id_vacunacion):
    if request.method == 'POST':
        print("Prueba POST registrar compra")
        req = json.loads(request.body.decode('utf-8'))
        id_vacunacion = str(req['id_vacunacion'])

        mi_vacunacion = get_object_or_404(Vacunacion, pk=id_vacunacion)

        costo_vacunacion = 0

        for vacuna in req['vacunas_agregadas']:
            mi_vacuna = get_object_or_404(Vacuna, pk=vacuna['id_vacuna'])

            cantidad = str(vacuna['cantidad_requerida'])
            unidad = str(vacuna['unidades'])

            aplicacion_vacunacion = AplicacionVacunacion(vacuna=mi_vacuna, vacunacion=mi_vacunacion, cantidad=cantidad, unidad=unidad)
            aplicacion_vacunacion.save()

            if unidad == 'Unidad(es)':

                costo_vacunacion = costo_vacunacion + (float(cantidad) * int(mi_vacuna.precio_venta))

                mi_vacuna.cantidad = float(mi_vacuna.cantidad) - float(cantidad)

                mi_vacuna.cantidad_total_unidades = float(mi_vacuna.cantidad_total_unidades) - ( float(cantidad) * float(mi_vacuna.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad) + " Unidad(es)."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion,
                                                           tipo_elemento='Vacuna',
                                                           id_elemento=mi_vacuna.id, tipo_transaccion='Ventas')
                historia_transaccion.save()
            else:

                nuevo_valor = math.floor( float(cantidad) / float(mi_vacuna.cantidad_unidad) )

                costo_vacunacion = costo_vacunacion + (float(cantidad) * int(mi_vacuna.precio_por_unidad))

                mi_vacuna.cantidad_total_unidades = float(mi_vacuna.cantidad_total_unidades) - float(cantidad)

                mi_vacuna.cantidad = math.floor( float(mi_vacuna.cantidad_total_unidades) / float(mi_vacuna.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad) + " " + mi_vacuna.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion,
                                                           tipo_elemento='Vacuna',
                                                           id_elemento=mi_vacuna.id, tipo_transaccion='Ventas')
                historia_transaccion.save()

            mi_vacuna.save()

        mi_vacunacion.estado_aplicacion = 'Aplicada'
        mi_vacunacion.costo = costo_vacunacion
        mi_vacunacion.save()

        for Item in req['insumos_seleccionados']:
            insumo_select = get_object_or_404(Insumo, pk=Item[0])
            if Item[10] == True:
                insumo_select.cantidad_total_unidades = float(
                    insumo_select.cantidad_total_unidades) - float(Item[11])
                insumo_select.cantidad = math.floor(
                    float(insumo_select.cantidad_total_unidades) / float(
                        insumo_select.cantidad_unidad))
                historiatratamiento = InsumoVacunacion(insumo=insumo_select, vacunacion=mi_vacunacion,
                                                     cantidad_requerida=Item[11], unidad_medida="fraccion")
                historiatratamiento.save()
                insumo_select.save()
            else:

                insumo_select.cantidad = int(insumo_select.cantidad) - int(Item[11])
                insumo_select.cantidad_total_unidades = float(
                    insumo_select.cantidad_total_unidades) - (float(Item[11]) * float(
                    insumo_select.cantidad_unidad))
                historiatratamiento = InsumoVacunacion(insumo=insumo_select, vacunacion=mi_vacunacion,
                                                     cantidad_requerida=Item[11], unidad_medida="unidad")
                historiatratamiento.save()
                insumo_select.save()

        data = {
            'id_mascota': mi_vacunacion.mascota.id,
        }
        return JsonResponse(data)

    else:
        insumos = json.dumps(list(Insumo.objects.filter(estado='Activo',bodega=1).values_list('id', 'nombre', 'codigo', 'cantidad', 'unidad_medida','cantidad_unidad', 'cantidad_total_unidades','codigo_inventario', 'cantidad_por_unidad','fecha_vencimiento','fecha_vencimiento','estado')),cls=DjangoJSONEncoder)

        insumos_por_codigo = json.dumps(list(chain(
            Insumo.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
                codigo_inventario__isnull=True).values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Insumo.objects.filter(estado='Activo', codigo_inventario__isnull=True,bodega=1).values_list('nombre',
                                                                                                    'codigo_inventario',
                                                                                                    'id', 'estado'))),
            cls=DjangoJSONEncoder)

        insumos_repetidos = []
        for element in list(
                Insumo.objects.filter(bodega=1).values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                insumos_repetidos.append(element['codigo_inventario'])

        return render(request, 'aplicar_vacunacion.html',{
            'mi_vacunacion': get_object_or_404(Vacunacion, pk=id_vacunacion),
            'mascotas': True,
            'insumos': insumos,
            'insumos_por_codigo': insumos_por_codigo,
            'codigos_insumos': insumos_repetidos
        })


def obtener_vacunas(request):

    all_vacunas = Vacuna.objects.filter(estado='Activa',bodega=1).exclude(tipo_uso='Comercial')

    elementos_por_codigo = chain(
        Vacuna.objects.distinct('codigo_inventario').filter(estado='Activa',bodega=1).exclude(
            codigo_inventario__isnull=True).exclude(tipo_uso='Comercial'),
        Vacuna.objects.filter(codigo_inventario__isnull=True, estado='Activa',bodega=1).exclude(tipo_uso='Comercial'))
    elementos_repetidos = []
    for element in list(
            Vacuna.objects.filter(bodega=1).exclude(tipo_uso='Comercial').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
        if element['cantidad'] > 1:
            elementos_repetidos.append(element['codigo_inventario'])
    data = {
        'all_vacunas': serializers.serialize('json', all_vacunas),
        'elementos_por_codigo': serializers.serialize('json', elementos_por_codigo),
        'codigos_elementos': elementos_repetidos
    }
    return JsonResponse(data)


def registrar_vacunacion(request, id_mascota):
    if request.method == 'POST':
        print("Prueba POST registrar vacunacion")
        form = CrearVacunacionForm()
        mi_mascota = get_object_or_404(Mascota, pk=id_mascota)

        req = json.loads(request.body.decode('utf-8'))
        fecha = datetime.datetime.strptime(str(req['fecha']), '%d/%m/%Y %H:%M')
        veterinario = str(req['veterinario'])
        descripcion = str(req['descripcion'])
        observaciones = str(req['observaciones'])

        mi_veterinario = get_object_or_404(Veterinario, pk=veterinario)

        nueva_vacunacion = Vacunacion(fecha=fecha, observaciones=observaciones, veterinario=mi_veterinario, mascota=mi_mascota, descripcion=descripcion)
        nueva_vacunacion.save()

        if(req['repetir_vacunacion']):
            for vacunacion in req['elementos_seleccionados']:

                mi_fecha = datetime.datetime.strptime(str(vacunacion['fecha']), '%d/%m/%Y %H:%M')
                mi_descripcion = str(vacunacion['descripcion'])

                nueva_vacunacion = Vacunacion(fecha=mi_fecha, veterinario=mi_veterinario, mascota=mi_mascota, descripcion=mi_descripcion)
                nueva_vacunacion.save()

        if (nueva_vacunacion.mascota.cliente.notificaciones_wpp) and  (len(nueva_vacunacion.mascota.cliente.celular) != 10 or not nueva_vacunacion.mascota.cliente.celular.isdigit()):
            messages.error(request, 'Debe ingresar un numero de celular valido para que se le notifique al propietario de la mascota por whatsapp')

        return render(request, 'registrar_vacunacion.html', {
            'form': form,
            'mascota': mi_mascota,
            'mascotas': True,
        })

    else:
        print("Prueba GET registrar vacunacion")
        form = CrearVacunacionForm()
        mi_mascota = get_object_or_404(Mascota, pk=id_mascota)

        return render(request, 'registrar_vacunacion.html', {
            'form': form,
            'mascota': mi_mascota,
            'mascotas': True,
        })


def eliminar_vacunacion(request):
    id_vacunacion = request.GET.get('id', '')
    mi_vacunacion = get_object_or_404(Vacunacion, pk=id_vacunacion)
    id_mascota = mi_vacunacion.mascota.id
    if mi_vacunacion.estado_aplicacion == 'Pendiente' and mi_vacunacion.estado == 'Activa':
        mi_vacunacion.delete()
    data = {
        'eliminacion': True,
        'id_mascota': id_mascota
    }
    return JsonResponse(data)


class DetalleVacunacion(DetailView):
    model = Vacunacion
    template_name = "detalle_vacunacion.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleVacunacion, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleVacunacion, self).get_context_data(**kwargs)
        context['mascotas'] = True
        aplicaciones = AplicacionVacunacion.objects.filter(vacunacion=self.kwargs['pk'])
        if aplicaciones:
            context['aplicaciones'] = aplicaciones

        insumos_utilizados = InsumoVacunacion.objects.filter(vacunacion=self.kwargs['pk'])
        if insumos_utilizados:
            context['insumos_utilizados'] = insumos_utilizados

        return context

def eliminar_vacuna(request):
    id_vacuna = request.GET.get('id', '')
    vacuna = get_object_or_404(Vacuna, pk=id_vacuna)
    vacuna.delete()
    messages.success(request, 'Vacuna eliminada con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)

def inactivar_vacuna(request):
    id_vacuna = request.GET.get('id', '')
    vacuna = get_object_or_404(Vacuna, pk=id_vacuna)
    vacuna.estado = 'Inactiva'
    vacuna.save()
    messages.success(request, 'Vacuna eliminada con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)

def registrar_cita_vacunacion(request,fecha):
    if request.method == "GET":
        return render(request,'registrar_vacunacion_agenda.html',{'citas':True,'fecha':fecha,'form':CrearVacunacionFormAgenda()})
    if request.method == "POST":
        print("IN POST")
        req = json.loads(request.body.decode('utf-8'))
        if (req['repetir_vacunacion']):
            mascota_new = get_object_or_404(Mascota, pk=req['mascota'])
            veterinario_new = get_object_or_404(Veterinario, pk= req['veterinario'])
            fecha = datetime.datetime.strptime(req['fecha'], '%d/%m/%Y %H:%M')
            new_desparasitacion = Vacunacion(fecha = fecha,observaciones = req['observaciones'],veterinario=veterinario_new,mascota = mascota_new,descripcion= req['descripcion'])
            new_desparasitacion.save()
            for vacunacion_in in req['elementos_seleccionados']:
                fecha_in = datetime.datetime.strptime(vacunacion_in['fecha'], '%d/%m/%Y %H:%M')
                new_vacunacion_in_array = Vacunacion(fecha = fecha_in,veterinario=veterinario_new,mascota = mascota_new,descripcion= vacunacion_in['descripcion'])
                new_vacunacion_in_array.save()
        else:
            mascota_new = get_object_or_404(Mascota, pk=req['mascota'])
            veterinario_new = get_object_or_404(Veterinario, pk= req['veterinario'])
            fecha = datetime.datetime.strptime(req['fecha'], '%d/%m/%Y %H:%M')
            new_vacunacion = Vacunacion(fecha = fecha,observaciones = req['observaciones'],veterinario=veterinario_new,mascota = mascota_new,descripcion= req['descripcion'])
            new_vacunacion.save()
        return redirect('/citas/listar_citas')