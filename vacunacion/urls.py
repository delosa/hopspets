from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_vacuna/', CrearVacuna.as_view(), name="registrar_vacuna"),
    path('listar_vacunas/', ListarVacunas.as_view(), name="listar_vacunas"),
    path('editar_vacuna/<int:pk>/', EditarVacuna.as_view(), name="editar_vacuna"),
    #path('registrar_vacunacion/<int:id_mascota>/', CrearVacunacion.as_view(), name="registrar_vacunacion"),
    path('editar_vacunacion/<int:pk>/', EditarVacunacion.as_view(), name="editar_vacunacion"),
    path('detalle_vacuna/<int:pk>/', DetalleVacuna.as_view(), name='detalle_vacuna'),
    path('aplicar_vacunacion/<int:id_vacunacion>/', aplicar_vacunacion, name="aplicar_vacunacion"),
    path('ajax/obtener_vacunas/', obtener_vacunas, name="obtener_vacunas"),
    path('registrar_vacunacion/<int:id_mascota>/', registrar_vacunacion, name="registrar_vacunacion"),
    path('eliminar_vacunacion/', eliminar_vacunacion, name="eliminar_vacunacion"),
    path('detalle_vacunacion/<int:pk>/', DetalleVacunacion.as_view(), name='detalle_vacunacion'),
    path('eliminar_vacuna/', eliminar_vacuna, name="eliminar_vacuna"),
    path('inactivar_vacuna/', inactivar_vacuna, name="inactivar_vacuna"),
    path('registrar_cita_vacunacion/<str:fecha>/', registrar_cita_vacunacion, name="registrar_cita_vacunacion"),
    path('editar_vacunacion_agenda/<int:pk>/', EditarVacunacionAgenda.as_view(), name="editar_vacunacion_agenda"),
]