function generarPdfFactura(data){

    var imgLogoVet= "data:image/png;base64,"+data.datos_veterinaria.logo_veterinaria.substring(2);
    var rowConstructor;

    for (i = 0; i < data.elementos_vendidos.length; i++) {

        rowConstructor += "[{text: '"+data.elementos_vendidos[i].cantidad+"', fontSize: 12,border: [false, true, true, true],margin: [15, 10]},{ text: '"+data.elementos_vendidos[i].descripcion+"', fontSize: 12, border: [true, true, true, true],margin: [15, 10]},{ text: '"+data.elementos_vendidos[i].precio_unidad+"', fontSize: 12, border: [true, true, true, true],margin: [15, 10]},{text: '"+data.elementos_vendidos[i].precio_total+"', fontSize: 12, border: [true, true, true, true],margin: [15, 10]}],"
        
    }


    var pdfDocumentHistoria = {
        info: {
            title: 'Recibo de pago',
            author: 'Hops Pets software veterinario'
        },
        content: [
            {
                columns: [
                    {
                        width: "50%",
                        stack: [                                   
                            {
                                table: {
                                    // headers are automatically repeated if the table spans over multiple pages
                                    // you can declare how many rows should be treated as headers
                                    //headerRows: 1,
                                    widths: ["auto", "auto"],
                                    body: [
                                        [ 
                                            { 
                                                text:[{text: "RECIBO DE PAGO "+ data.numero_recibo, fontSize: 17, bold: true, color:"#0091d1"}], 
                                                border: [false, false, false, false],
                                                margin: [0, 0, 0, 10],
                                                colSpan: 2
                                                
                                            },
                                            {}
                                        ],
                                        [ 
                                            { 
                                                text: data.datos_veterinaria.nombre_veterinaria, 
                                                fontSize: 11, 
                                                bold: true,
                                                border: [false, false, false, false],
                                                margin: [0, 0, 0, 0],
                                                colSpan: 2
                                            },
                                            {}
                                        ],
                                        [ 
                                            { 
                                                text: "Fecha:",
                                                fontSize: 11,
                                                bold: true,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }, 
                                            { 
                                                text: data.fecha_venta,
                                                fontSize: 11,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }
                                        ],
                                        [ 
                                            { 
                                                text: "Nit:",
                                                fontSize: 11,
                                                bold: true,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }, 
                                            { 
                                                text: data.datos_veterinaria.nit, 
                                                fontSize: 11,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }
                                        ],
                                        [ 
                                            { 
                                                text: "Dir:",
                                                fontSize: 11,
                                                bold: true,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }, 
                                            { 
                                                text: data.datos_veterinaria.direccion, 
                                                fontSize: 11,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }
                                        ],
                                        [ 
                                            { 
                                                text: "Tels:",
                                                fontSize: 11,
                                                bold: true,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }, 
                                            { 
                                                text: data.datos_veterinaria.celular, 
                                                fontSize: 11,
                                                border: [false, false, false, false],
                                                margin: [0, -3, 0, 0]
                                                
                                            }
                                        ]                                                                                                                        
                                    ]
                                },
                            }
                        ]                    
                    },
                    {
                        width: "50%",
                        image: 'logoVeterinaria',
                        fit: [200, 200],
                        width: 200,
                        margin:[0,-40,-30,0],
                        alignment: 'right'
                    }
                ],
                
            },
            {
                table: {
                    // headers are automatically repeated if the table spans over multiple pages
                    // you can declare how many rows should be treated as headers
                    //headerRows: 1,
                    widths: ["*", "*"],
                    margin: [0, 20, 0, 0],
                    body: [
                        [ 
                            { 
                                text: "Recibo para", 
                                fontSize: 12, 
                                border: [true, true, true, true],
                                fillColor: '#eeeeee',
                                margin: [15, 10]
                                
                            },
                            { 
                                text: [{text:"Dir: ", bold: true},{text: data.direccion_cliente}], 
                                fontSize: 12, 
                                border: [true, true, true, true],
                                margin: [15, 10]
                                
                            },
                        ],
                        [ 
                            { 
                                text: data.nombre_cliente, 
                                fontSize: 12,
                                bold: true,
                                border: [true, true, true, true],
                                margin: [15, 10]
                                
                            },
                            { 
                                text: [{text:data.tipo_documento_cliente+": ", bold: true},{text: data.numero_documento_cliente}], 
                                fontSize: 12, 
                                border: [true, true, true, true],
                                margin: [15, 10]
                                
                            }

                        ]               					
                    ]
                },                        
            },
            {
                table: {
                    // headers are automatically repeated if the table spans over multiple pages
                    // you can declare how many rows should be treated as headers
                    headerRows: 4,
                    widths: ["*", "*", "*", "*"],
                    margin: [0, 20, 0, 0],
                    body: [
                        [ 
                            { 
                                text: "Cant", 
                                fontSize: 12, 
                                border: [false, false, true, true],
                                fillColor: '#eeeeee',
                                margin: [15, 10]
                                
                            },
                            { 
                                text:"Descripción",
                                fontSize: 12, 
                                border: [true, true, true, true],
                                margin: [15, 10]
                                
                            },
                            { 
                                text:"Precio Unidad",
                                fontSize: 12, 
                                border: [true, true, true, true],
                                margin: [15, 10]
                                
                            },
                            { 
                                text:"Precio total",
                                fontSize: 12, 
                                border: [true, true, true, true],
                                margin: [15, 10]
                                
                            },                                                        
                        ],            					
                    ]
                },                        
            }                        
        ],
        images:{
            logoVeterinaria: imgLogoVet,
        }

    };

    pdfMake.createPdf(pdfDocumentHistoria).open();
}