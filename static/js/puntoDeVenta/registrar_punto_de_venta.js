var app_punto_venta = new Vue({
    delimiters: ['[[', ']]'],
    el: '#app_punto_venta',
    data: {
        lista_elementos: {},
        all_lista_elementos: {},
        nombreCategoria: ''
    },
    filters: {
        formatoMoneda: function (valor) {
            var numero = Number(valor);

            return numero.toLocaleString();
        }
    },
});

var funcionFiltro = function(tipo_elemento) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: '/productos/ajax/filtrar_tipo_producto/',
            data: {
                'tipo': tipo_elemento
            },
            dataType: 'json',
            success: function (data) {
                elementos = JSON.parse(data.lista_elementos);
                //console.log("elementos: ", elementos);
                categorias = JSON.parse(data.lista_categorias);
                bodegas = JSON.parse(data.bodegas);
                //console.log("categorias: ", categorias);
                elementos.forEach(element => {
                    disponibilidad = "";

                    if(element.fields.cantidad != 0){
                        disponibilidad = element.fields.cantidad + " unidades";
                    }

                    if(element.fields.cantidad_por_unidad){
                        if(element.fields.cantidad != 0){
                            disponibilidad += " (" + element.fields.cantidad_unidad + " " + element.fields.unidad_medida + ')';
                        }
                        residuo = (parseFloat(element.fields.cantidad_total_unidades) % parseInt(element.fields.cantidad_unidad)).toFixed(1);

                        if(residuo != 0){
                            if(element.fields.cantidad != 0){
                                disponibilidad += " y ";
                            }
                            disponibilidad += "1 unidad ("+residuo+ " " + element.fields.unidad_medida + ')';
                        }
                    }

                    if(disponibilidad == ""){
                        disponibilidad= "0 unidades"
                    }

                    element.fields.disponibilidad = disponibilidad;
                });
                respuesta={
                    elementos: elementos,
                    categorias: categorias,
                    bodegas: bodegas
                }
                resolve(respuesta);
            }
        });
    });
}

$('.cliente_punto').click(function(){
  $('#modal-client').modal('show');
});

$(document).ready(function()
{
  $("#barras").focus();
  $("#barras").select();
});

function Cerrar_punto_alert(id) {
    swal({
            title: "Estas seguro que desea Cerrar la punto ?",
            text: "Una vez se cierra la punto no puede volver a abrirse hasta el dia de mañana.",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No, cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Si, cerrar punto!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '/puntoDeVenta/cerrar_punto/'+id,
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        window.location.replace("/puntoDeVenta/abrir_punto_de_venta/");
                    }
                });
            }
        });
}

$("#contenedor_productos").hide();
$("#contenedor_medicamentos").hide();
$("#contenedor_vacunas").hide();
$("#contenedor_desparasitantes").hide();
$("#contenedor_otros").hide();
$("#contenedor_servicios").hide();
$("#contenedor_nuevas_categorias").hide();
$("#contenedor_categorias_nuevas").hide();

function Inventario(tipo_producto){
    if(tipo_producto=="Producto") {
        funcionFiltro('productos').then(function (data) {
            app_punto_venta.lista_elementos = data.elementos;
            app_punto_venta.all_lista_elementos = data.elementos;

            if ($('#buscar_inventario').val() != '') {
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos.filter(filtrarPorNombreYCodigo);
            }else{
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos;
            }
        }, function (err) {
            console.log(err); // Error: "It broke"
        });
        $("#contenedor_productos").show();
        $("#contenedor_medicamentos").hide();
        $("#contenedor_vacunas").hide();
        $("#contenedor_desparasitantes").hide();
        $("#contenedor_otros").hide();
        $("#sin_productos").hide();
        $("#contenedor_nuevas_categorias").hide();
        $("#contenedor_categorias_nuevas").hide();
        $("#contenedor_servicios").hide();
        $('.categoria-select').removeClass('categoria-select');
        $('.categoria .producto').addClass('categoria-select');
    }
    else if(tipo_producto=="Medicamento"){
        funcionFiltro('medicamentos').then(function (data) {
            app_punto_venta.lista_elementos = data.elementos;
            app_punto_venta.all_lista_elementos = data.elementos;

            if ($('#buscar_inventario').val() != '') {
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos.filter(filtrarPorNombreYCodigo);
            }else{
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos;
            }
        }, function (err) {
            console.log(err); // Error: "It broke"
        });
        $("#contenedor_productos").hide();
        $("#contenedor_medicamentos").show();
        $("#contenedor_vacunas").hide();
        $("#contenedor_desparasitantes").hide();
        $("#contenedor_otros").hide();
        $("#sin_productos").hide();
        $("#contenedor_nuevas_categorias").hide();
        $("#contenedor_categorias_nuevas").hide();
        $("#contenedor_servicios").hide();
        $('.categoria-select').removeClass('categoria-select');
        $('.categoria .medicamento').addClass('categoria-select');
    }
    else if(tipo_producto=="Vacuna"){
        funcionFiltro('vacunas').then(function (data) {
            app_punto_venta.lista_elementos = data.elementos;
            app_punto_venta.all_lista_elementos = data.elementos;

            if ($('#buscar_inventario').val() != '') {
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos.filter(filtrarPorNombreYCodigo);
            }else{
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos;
            }
        }, function (err) {
            console.log(err); // Error: "It broke"
        });
        $("#contenedor_productos").hide();
        $("#contenedor_medicamentos").hide();
        $("#contenedor_vacunas").show();
        $("#contenedor_desparasitantes").hide();
        $("#contenedor_otros").hide();
        $("#sin_productos").hide();
        $("#contenedor_nuevas_categorias").hide();
        $("#contenedor_categorias_nuevas").hide();
        $("#contenedor_servicios").hide();
        $('.categoria-select').removeClass('categoria-select');
        $('.categoria .vacuna').addClass('categoria-select');
    }
    else if(tipo_producto=="Desparasitante"){
        funcionFiltro('desparasitantes').then(function (data) {
            app_punto_venta.lista_elementos = data.elementos;
            app_punto_venta.all_lista_elementos = data.elementos;

            if ($('#buscar_inventario').val() != '') {
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos.filter(filtrarPorNombreYCodigo);
            }else{
                app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos;
            }
        }, function (err) {
            console.log(err); // Error: "It broke"
        });
        $("#contenedor_productos").hide();
        $("#contenedor_medicamentos").hide();
        $("#contenedor_vacunas").hide();
        $("#contenedor_desparasitantes").show();
        $("#contenedor_otros").hide();
        $("#sin_productos").hide();
        $("#contenedor_nuevas_categorias").hide();
        $("#contenedor_categorias_nuevas").hide();
        $("#contenedor_servicios").hide();
        $('.categoria-select').removeClass('categoria-select');
        $('.categoria .desparasitante').addClass('categoria-select');
    }
    else if(tipo_producto=="Otro"){
        $("#contenedor_productos").hide();
        $("#contenedor_medicamentos").hide();
        $("#contenedor_vacunas").hide();
        $("#contenedor_desparasitantes").hide();
        $("#contenedor_otros").show();
        $("#sin_productos").hide();
        $("#contenedor_nuevas_categorias").hide();
        $("#contenedor_categorias_nuevas").hide();
        $("#contenedor_servicios").hide();
        $('.categoria-select').removeClass('categoria-select');
        $('.categoria .otro').addClass('categoria-select');
    }
    else if(tipo_producto=="Nuevas_Categorias"){
      $("#contenedor_productos").hide();
      $("#contenedor_medicamentos").hide();
      $("#contenedor_vacunas").hide();
      $("#contenedor_desparasitantes").hide();
      $("#contenedor_otros").hide();
      $("#sin_productos").hide();
      $("#contenedor_nuevas_categorias").show();
      $("#contenedor_categorias_nuevas").hide();
      $("#contenedor_servicios").hide();
      $('.categoria-select').removeClass('categoria-select');
      $('.categoria .nuevas_categorias').addClass('categoria-select');
    }
    else if(tipo_producto=="Servicios"){
      $("#contenedor_productos").hide();
      $("#contenedor_medicamentos").hide();
      $("#contenedor_vacunas").hide();
      $("#contenedor_desparasitantes").hide();
      $("#contenedor_otros").hide();
      $("#sin_productos").hide();
      $("#contenedor_nuevas_categorias").hide();
      $("#contenedor_categorias_nuevas").hide();
      $("#contenedor_servicios").show();
      $('.categoria-select').removeClass('categoria-select');
      $('.categoria .servicios').addClass('categoria-select');
    }
}

$('.categorias_nuevas a').click(function(){
    $('.categoria-select').removeClass('categoria-select');
    $(this).addClass('categoria-select');
    let $categoria = $('.categoria-select').data('tipo');
    app_punto_venta.nombreCategoria = $categoria;
    funcionFiltro($('.categoria-select').data('id')).then(function (data) {
        app_punto_venta.lista_elementos = data.elementos;
        app_punto_venta.all_lista_elementos = data.elementos;

        if ($('#buscar_inventario').val() != '') {
            app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos.filter(filtrarPorNombreYCodigo);
        }else{
            app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos;
        }
    }, function (err) {
        console.log(err); // Error: "It broke"
    });
    $("#contenedor_nuevas_categorias").hide();
    $("#contenedor_categorias_nuevas").show();
});

$('.servicios').click(function(){
  let $cliente = $('#datos_cliente').find('.nombre-completo').data('cliente');

  if($cliente){
    $.ajax({
      url: '/productos/ajax/validate_username/',
      data: {
        'id_cliente': $cliente
      },
      dataType: 'json',
      success: function (data) {
        if (data.is_taken) {
            let $citas_estetica_cliente = JSON.parse(data.citas_estetica);
            let $mascotas_cliente = JSON.parse(data.mascotas_cliente);
            let $tipos_baño = JSON.parse(data.tipos_baño);
            let $tratamientos_cliente = JSON.parse(data.tratamientos_cliente);
            let $tratamientos = JSON.parse(data.tratamientos);
            let $desparasitaciones_cliente = JSON.parse(data.desparasitaciones_cliente);
            let $desparasitantes = JSON.parse(data.desparasitantes);
            let $vacunaciones_cliente = JSON.parse(data.vacunaciones_cliente);
            let $vacunas = JSON.parse(data.vacunas);
            let $guarderias_cliente = JSON.parse(data.guarderias_cliente);

            $("#contenedor_servicios .row").empty();

            $citas_estetica_cliente.forEach(function (estetica) {
                estetica.fields.estado = true;
                var fecha = String(estetica.fields.fecha);
                fecha = fecha.slice(0, 10);
                estetica.fields.fecha = fecha;
                $mascotas_cliente.forEach(function (mascota) {
                    if(estetica.fields.mascota == mascota.pk){
                        estetica.fields.nombre_mascota = mascota.fields.nombre
                    }
                });
                $tipos_baño.forEach(function (baño) {
                    if(estetica.fields.tipo_estetica == baño.pk){
                        estetica.fields.nombre_tipo_estetica = baño.fields.nombre
                    }
                });

                let $img;
                let $nombre;
                let $fecha;

                let $div = $('<div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">');
                let $a = $('<a href="#" data-id="' + estetica.pk + '" data-tipo="estetica" data-nombre="Estética_' + estetica.fields.nombre_tipo_estetica + '" data-precio="' + estetica.fields.valor_total + '" data-cantidad_disponible="1" style="color:#000;">');
                let $div2 = $('<div class="card-box-pdv-pago text-center">');

                $img = $('<img width="180" height="150" src="/static/img/producto_sin_foto.jpg" style="margin-bottom:10px;">');
                let $servicio = $('<p style="font-weight: bolder;font-size: 25px;color:#0A6FF1;">').append('Estética');
                $nombre = $('<p style="font-weight: bolder;font-size: 15px;">').append(estetica.fields.nombre_tipo_estetica);
                let $precio = $('<p style="font-size: 20px;">').append('$' + estetica.fields.valor_total);
                $fecha = $('<p style="font-size: 10px;">').append('Fecha: ' + estetica.fields.fecha);
                let $mascota = $('<p style="font-size: 15px;">').append(estetica.fields.nombre_mascota);

                $div2.append($img);
                $div2.append($servicio);
                $div2.append($nombre);
                $div2.append($precio);
                $div2.append($fecha);
                $div2.append($mascota);
                $a.append($div2);
                $div.append($a);
                $($("#contenedor_servicios .row")).append($div);
            });

            $tratamientos_cliente.forEach(function (tratamiento) {
                tratamiento.estado = true;
                var fecha = String(tratamiento.fecha);
                fecha = fecha.slice(0, 10);
                tratamiento.fecha = fecha;
                $mascotas_cliente.forEach(function (mascota) {
                    if(tratamiento.mascota == mascota.pk){
                        tratamiento.nombre_mascota = mascota.fields.nombre
                    }
                });
                $tratamientos.forEach(function (elemento) {
                    if(tratamiento.tratamiento == elemento.pk){
                        tratamiento.nombre_tratamiento = elemento.fields.nombre
                        tratamiento.costo = elemento.fields.precio
                    }
                });

                let $img;
                let $nombre;
                let $fecha;

                let $div = $('<div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">');
                let $a = $('<a href="#" data-id="' + tratamiento.id + '" data-tipo="' + tratamiento.nombre_tratamiento + '" data-nombre="' + tratamiento.motivo_cita_nombre + '" data-precio="' + tratamiento.costo + '" data-cantidad_disponible="1" data-id_motivo_area="' + tratamiento.motivo_cita + '" style="color:#000;">');
                let $div2 = $('<div class="card-box-pdv-pago text-center">');

                $img = $('<img width="180" height="150" src="/static/img/producto_sin_foto.jpg" style="margin-bottom:10px;">');
                let $servicio = $('<p style="font-weight: bolder;font-size: 25px;color:#0A6FF1;">').append('Cita Médica');
                $nombre = $('<p style="font-weight: bolder;font-size: 15px;">').append(tratamiento.motivo_cita_nombre);
                let $precio = $('<p style="font-size: 20px;">').append('$' + tratamiento.costo);
                $fecha = $('<p style="font-size: 10px;">').append('Fecha: ' + tratamiento.fecha);
                let $mascota = $('<p style="font-size: 15px;">').append(tratamiento.nombre_mascota);

                $div2.append($img);
                $div2.append($servicio);
                $div2.append($nombre);
                $div2.append($precio);
                $div2.append($fecha);
                $div2.append($mascota);
                $a.append($div2);
                $div.append($a);
                $($("#contenedor_servicios .row")).append($div);
            });

            $desparasitaciones_cliente.forEach(function (desparasitacion) {
                desparasitacion.fields.estado = true;
                var fecha = String(desparasitacion.fields.fecha);
                fecha = fecha.slice(0, 10);
                desparasitacion.fields.fecha = fecha;
                $mascotas_cliente.forEach(function (mascota) {
                    if(desparasitacion.fields.mascota == mascota.pk){
                        desparasitacion.fields.nombre_mascota = mascota.fields.nombre
                    }
                });
                $desparasitantes.forEach(function (desparasitante) {
                    if(desparasitacion.fields.desparasitante == desparasitante.pk){
                        desparasitacion.fields.nombre_desparasitante = desparasitante.fields.nombre
                    }
                });

                let $img;
                let $nombre;
                let $fecha;

                let $div = $('<div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">');
                let $a = $('<a href="#" data-id="' + desparasitacion.pk + '" data-tipo="desparasitacion" data-nombre="Desparasitación_' + desparasitacion.fields.descripcion + '" data-precio="' + desparasitacion.fields.costo + '" data-cantidad_disponible="1" style="color:#000;">');
                let $div2 = $('<div class="card-box-pdv-pago text-center">');

                $img = $('<img width="180" height="150" src="/static/img/producto_sin_foto.jpg" style="margin-bottom:10px;">');
                let $servicio = $('<p style="font-weight: bolder;font-size: 25px;color:#0A6FF1;">').append('Desparasitación');
                $nombre = $('<p style="font-weight: bolder;font-size: 15px;">').append(desparasitacion.fields.descripcion);
                let $precio = $('<p style="font-size: 20px;">').append('$' + desparasitacion.fields.costo);
                $fecha = $('<p style="font-size: 10px;">').append('Fecha: ' + desparasitacion.fields.fecha);
                let $mascota = $('<p style="font-size: 15px;">').append(desparasitacion.fields.nombre_mascota);

                $div2.append($img);
                $div2.append($servicio);
                $div2.append($nombre);
                $div2.append($precio);
                $div2.append($fecha);
                $div2.append($mascota);
                $a.append($div2);
                $div.append($a);
                $($("#contenedor_servicios .row")).append($div);
            });

            $vacunaciones_cliente.forEach(function (vacunacion) {
                vacunacion.fields.estado = true;
                var fecha = String(vacunacion.fields.fecha);
                fecha = fecha.slice(0, 10);
                vacunacion.fields.fecha = fecha;
                $mascotas_cliente.forEach(function (mascota) {
                    if(vacunacion.fields.mascota == mascota.pk){
                        vacunacion.fields.nombre_mascota = mascota.fields.nombre
                    }
                });
                $vacunas.forEach(function (vacuna) {
                    if(vacunacion.fields.vacuna == vacuna.pk){
                        vacunacion.fields.nombre_vacuna = vacuna.fields.nombre
                    }
                });

                let $img;
                let $nombre;
                let $fecha;

                let $div = $('<div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">');
                let $a = $('<a href="#" data-id="' + vacunacion.pk + '" data-tipo="vacunacion" data-nombre="Vacunación_' + vacunacion.fields.descripcion + '" data-precio="' + vacunacion.fields.costo + '" data-cantidad_disponible="1" style="color:#000;">');
                let $div2 = $('<div class="card-box-pdv-pago text-center">');

                $img = $('<img width="180" height="150" src="/static/img/producto_sin_foto.jpg" style="margin-bottom:10px;">');
                let $servicio = $('<p style="font-weight: bolder;font-size: 25px;color:#0A6FF1;">').append('Vacunación');
                $nombre = $('<p style="font-weight: bolder;font-size: 15px;">').append(vacunacion.fields.descripcion);
                let $precio = $('<p style="font-size: 20px;">').append('$' + vacunacion.fields.costo);
                $fecha = $('<p style="font-size: 10px;">').append('Fecha: ' + vacunacion.fields.fecha);
                let $mascota = $('<p style="font-size: 15px;">').append(vacunacion.fields.nombre_mascota);

                $div2.append($img);
                $div2.append($servicio);
                $div2.append($nombre);
                $div2.append($precio);
                $div2.append($fecha);
                $div2.append($mascota);
                $a.append($div2);
                $div.append($a);
                $($("#contenedor_servicios .row")).append($div);
            });

            $guarderias_cliente.forEach(function (guarderia) {
                guarderia.fields.estado = true;

                var fecha_inicio = String(guarderia.fields.fecha_inicio);
                fecha_inicio = fecha_inicio.slice(0, 10);
                guarderia.fields.fecha_inicio = fecha_inicio;

                var fecha_fin = String(guarderia.fields.fecha_fin);
                fecha_fin = fecha_fin.slice(0, 10);
                guarderia.fields.fecha_fin = fecha_fin;

                $mascotas_cliente.forEach(function (mascota) {
                    if(guarderia.fields.mascota == mascota.pk){
                        guarderia.fields.nombre_mascota = mascota.fields.nombre
                    }
                });

                let $img;
                let $fecha_inicio;
                let $fecha_final;

                let $div = $('<div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">');
                let $div2 = $('<div class="card-box-pdv-pago text-center">');

                $img = $('<img width="180" height="150" src="/static/img/producto_sin_foto.jpg" style="margin-bottom:10px;">');
                let $servicio = $('<p style="font-weight: bolder;font-size: 25px;color:#0A6FF1;">').append('Guardería');
                $fecha_inicio = $('<p style="font-size: 10px;">').append('Fecha Inicio: ' + guarderia.fields.fecha_inicio);
                $fecha_final = $('<p style="font-size: 10px;">').append('Fecha Final: ' + guarderia.fields.fecha_fin);
                let $mascota = $('<p style="font-size: 15px;">').append(guarderia.fields.nombre_mascota);
                let $des_precio = $('<label>').append('Ingrese el costo');
                let $precio = $('<p style="font-size: 20px;">').append('<input class="form-control" id="precio_guarderia" type="number">');
                let $a = $('<a href="#" data-id="' + guarderia.pk + '" data-tipo="guarderia" data-nombre="Guardería" data-precio="0"  data-cantidad_disponible="1,0" style="color:#fff;" class="btn btn-primary m-r-20">');

                $div2.append($img);
                $div2.append($servicio);
                $div2.append($fecha_inicio);
                $div2.append($fecha_final);
                $div2.append($mascota);
                $div2.append($des_precio);
                $div2.append($precio);
                $a.append('<i class="fa fa-check-circle-o" aria-hidden="true"></i>Agregar');
                $div2.append($a);
                $div.append($div2);
                $($("#contenedor_servicios .row")).append($div);
            });

        }
      }
  });
  }
  else{
    $('#modal_servicios').modal('show');
  }
});
var contenedor = $('.contenedor a.product-select');
var $subtotal = $('#precio_total');
var $descuento_total = $('#descuento_total');
let unidades = 1;
var $carrito_total = $("#carrito_total");

$('.contenedor').on('click', 'a', function(){
    $('.product-select').removeClass('product-select');
    $(this).addClass('product-select');
    if($(this).data('cantidad_por_unidad_producto') == "True" ||  $(this).data('cantidad_por_unidad_producto') == true){
      $('#modal_producto').modal('show');
    }
    else {
      let precio = 0;
      let $nombre;
      let $cantidad_disponible;

      if($(this).data('tipo')=='Otros'){
        precio = $('#precio_otro').val();
        $nombre = $('#nombre_otro').val();
        $cantidad_disponible = 1;
      }else {
        if($(this).data('tipo')=='guarderia'){
          precio = $('#precio_guarderia').val();
          $nombre = $(this).data('nombre');
          $cantidad_disponible = 1;
        }
        else{
          precio = $(this).data('precio');
          $nombre = $(this).data('nombre');
          $cantidad_disponible = parseInt($(this).data('cantidad_disponible'));
        }
      }

      let $cantidad_carrito = $('.unidad').data('unidad');

      $nombre_class = $nombre.trim().replace(/\s/g, '_')
      let $tipo = $("." + $(this).data('tipo') + "[data-nombre='" + $nombre_class + "']");
      let $motivo_area = 0;

      if($(this).data('tipo') == "Area" ){
        $motivo_area = $(this).data('id_motivo_area');
      }

      let $div = $('<div class="product-car ' + $(this).data('tipo') +'" data-id=' + $(this).data('id') + ' data-tipo=' + $(this).data('tipo') + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + '  data-id_motivo_area=' + $(this).data('id_motivo_area') + '>');
      let $div_boot_10 = $('<div class="col-md-10">');
      let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
      let $div_uni_prod = $('<div class="unidades_productos">');
      let $unidad = $('<i>');
      let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
      let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
      let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
      let $precio_product = $('<p class="precio">').append(precio);

      let $desc = $('<div class="descuento">');
      let $comple = $('<i>');
      let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

      if($tipo.length>0){
        if($cantidad_carrito < $cantidad_disponible){
          $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
          $unidades = parseInt($tipo.find(".unidad").html())+1;
          $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
          $('.unidad').data('unidad', $unidades);
        }
        else{
          $('#modal_cantidad').modal('show');
          precio = 0;
        }
      }
      else{

        if($cantidad_disponible > 0){
          $comple.append($comple_2);
          $desc.append($comple);
          $div_precio_prod.append($precio_product);
          $unidad.append($unidad_font);
          $div_uni_prod.append($unidad);
          $div_boot_10.append($div_name_prod);
          $div_boot_10.append($div_uni_prod);
          $div_boot_10.append($desc);
          $div_boot_2.append($div_precio_prod);
          $div.append($div_boot_10);
          $div.append($div_boot_2);
          $($carrito_total).before($div);
        }
        else{
          $('#modal_cantidad').modal('show');
          precio = 0;
        }
      }
      let $total = Number($subtotal.data('subtotal')) + Number(precio);
      $carrito_total.find('#precio_total h3').html($total);
      $subtotal.data('subtotal', $total);
    }
});

$('.escoger-venta a').click(function(){
  let precio = 0;
  let $nombre;
  let $cantidad_disponible = parseInt($('.contenedor a.product-select').data('cantidad_disponible'));
  let $cantidad_carrito = $('.unidad').data('unidad');

  if($(this).data('tipo_venta') == 'unidad-select'){


    if($('.contenedor a.product-select').data('tipo')=='Otros'){
      precio = $('#precio_otro').val();
      $nombre = $('#nombre_otro').val();
    }else {
      if($('.contenedor a.product-select').data('tipo')=='guarderia'){
        precio = $('#precio_guarderia').val();
        $nombre = $('.contenedor a.product-select').data('nombre');
      }
      else{
        precio = $('.contenedor a.product-select').data('precio');
        $nombre = $('.contenedor a.product-select').data('nombre');
      }
    }


    $nombre = $nombre == undefined ? '' : $nombre.trim();
    $nombre_class = $nombre.replace(/\s/g, '_');
    let $tipo = $("." + $('.contenedor a.product-select').data('tipo') + "[data-nombre='" + $nombre_class + "']");

    let $div = $('<div class="product-car ' + $('.contenedor a.product-select').data('tipo') +'" data-id=' + $('.contenedor a.product-select').data('id') + ' data-tipo=' + $('.contenedor a.product-select').data('tipo') + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + ' data-granel="false" data-medida_producto="' + $('.contenedor a.product-select').data('unidad_medida_producto') +'" data-cant_unidad_disponible="' + parseInt($('.contenedor a.product-select').data('cantidad_unidad')) +'">');
    let $div_boot_10 = $('<div class="col-md-10">');
    let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
    let $div_uni_prod = $('<div class="unidades_productos">');
    let $unidad = $('<i>');
    let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
    let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
    let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
    let $precio_product = $('<p class="precio">').append(precio);

    let $desc = $('<div class="descuento">');
    let $comple = $('<i>');
    let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

    if($tipo.length>0){
      if($cantidad_carrito < $cantidad_disponible){
        $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
        $unidades = parseInt($tipo.find(".unidad").html())+1;
        $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
        $('.unidad').data('unidad', $unidades);
      }
      else{
        $('#modal_cantidad').modal('show');
        precio = 0;
      }
    }
    else{
      if($cantidad_disponible > 0){
        $comple.append($comple_2);
        $desc.append($comple);
        $div_precio_prod.append($precio_product);
        $unidad.append($unidad_font);
        $div_uni_prod.append($unidad);
        $div_boot_10.append($div_name_prod);
        $div_boot_10.append($div_uni_prod);
        $div_boot_10.append($desc);
        $div_boot_2.append($div_precio_prod);
        $div.append($div_boot_10);
        $div.append($div_boot_2);
        $($carrito_total).before($div);
      }
      else{
        $('#modal_cantidad').modal('show');
        precio = 0;
      }
    }
    let $total = Number($subtotal.data('subtotal')) + Number(precio);
    $carrito_total.find('#precio_total h3').html($total);
    $subtotal.data('subtotal', $total);
  }
  else{
    precio = $('.contenedor a.product-select').data('precio_medida');
    $nombre = $('.contenedor a.product-select').data('nombre');

    $nombre = $nombre == undefined ? '' : $nombre.trim();
    $nombre_class = $nombre.replace(/\s/g, '_');
    let $tipo = $("." + $('.contenedor a.product-select').data('tipo') + "[data-nombre='" + $nombre_class + "']");

    let $cantidad_disponible_unidad = parseInt($('.contenedor a.product-select').data('cantidad_unidad')); //total del granel

    let $div = $('<div class="product-car ' + $('.contenedor a.product-select').data('tipo') +'" data-id=' + $('.contenedor a.product-select').data('id') + ' data-tipo=' + $('.contenedor a.product-select').data('tipo') + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + ' data-granel="true" data-medida_producto="' + $('.contenedor a.product-select').data('unidad_medida_producto') +'" data-cant_unidad_disponible="' + $cantidad_disponible_unidad +'">');
    let $div_boot_10 = $('<div class="col-md-10">');
    let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
    let $div_uni_prod = $('<div class="unidades_productos">');
    let $unidad = $('<i>');
    let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 ' + $('.contenedor a.product-select').data('unidad_medida_producto') + ' en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/' + $('.contenedor a.product-select').data('unidad_medida_producto') + '</h6>');
    let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
    let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
    let $precio_product = $('<p class="precio">').append(precio);

    let $desc = $('<div class="descuento">');
    let $comple = $('<i>');
    let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

    if($tipo.length>0){

      if($cantidad_carrito < $cantidad_disponible_unidad){
        $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
        $unidades = parseInt($tipo.find(".unidad").html())+1;
        $tipo.find(".unidad").html($unidades + " " + $('.contenedor a.product-select').data('unidad_medida_producto') + " en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/" + $('.contenedor a.product-select').data('unidad_medida_producto') + "");
        $('.unidad').data('unidad', $unidades);
      }
      else{
        $('#modal_cantidad').modal('show');
        precio = 0;
      }
    }
    else{
      if($cantidad_disponible_unidad > 0){
        $comple.append($comple_2);
        $desc.append($comple);
        $div_precio_prod.append($precio_product);
        $unidad.append($unidad_font);
        $div_uni_prod.append($unidad);
        $div_boot_10.append($div_name_prod);
        $div_boot_10.append($div_uni_prod);
        $div_boot_10.append($desc);
        $div_boot_2.append($div_precio_prod);
        $div.append($div_boot_10);
        $div.append($div_boot_2);
        $($carrito_total).before($div);
      }
      else{
        $('#modal_cantidad').modal('show');
        precio = 0;
      }
    }
    let $total = Number($subtotal.data('subtotal')) + Number(precio);
    $carrito_total.find('#precio_total h3').html($total);
    $subtotal.data('subtotal', $total);
  }
  $("#modal_producto").modal('hide');//ocultamos el modal
  $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
});


$('.numpad button').click(function(){
    if($(this).data('mode')){
        $('.mode-button.selected-mode').removeClass('selected-mode');
        $(this).addClass('selected-mode');
    }

    let $selected_mode = $(".selected-mode");
    let $cambiar = $(".product-car.selected [data-unidad]");
    let $cambiar_precio = $(".product-car.selected [data-precio]");//precio que esta al lado de unidad
    let $cambiar_descuento = $(".product-car.selected [data-descuento]");
    let $tipo = $(".product-car.selected");
    let $precio_antes = $tipo.find(".precio").html();
    let $cant_disp = $(".product-car.selected").data('cantidad_total_disponible');
    let $cant_unidad_disponible = $(".product-car.selected").data('cant_unidad_disponible');

    if ($(this).hasClass('number-char') && $selected_mode.length > 0) {
        if($('.product-car.selected data-'+ $selected_mode.data('mode') +'')){
            if($selected_mode.data('mode') == "unidad"){
              let $unidad = 0;

              $unidad = $cambiar.data('unidad') + '' + $(this).val() + '';

              if($unidad <= $cant_disp || $unidad <= $cant_unidad_disponible){
                if($(".product-car.selected").data('granel') == true){
                  $cambiar.html($unidad + " " + $(".product-car.selected").data('medida_producto') + " en $<span class='precio_unidad' data-precio='" + $cambiar_precio.data('precio') + "'>" + $cambiar_precio.data('precio') + "</span>/" + $(".product-car.selected").data('medida_producto') + "");
                }
                else{
                  $cambiar.html($unidad + " Unidad(es) en $<span class='precio_unidad' data-precio='" + $cambiar_precio.data('precio') + "'>" + $cambiar_precio.data('precio') + "</span>/Unidad(es)");
                }

                $cambiar.data('unidad', $unidad);
                $tipo.find(".precio").html((parseFloat($tipo.find(".precio_unidad").html())*parseFloat($unidad)).toFixed(2));
                let $act_precio = Number($subtotal.data('subtotal')) + Number($tipo.find(".precio").html());
                $act_precio -= Number($precio_antes);
                $act_precio = $act_precio.toFixed(2);
                $carrito_total.find('#precio_total h3').html($act_precio);
                $subtotal.data('subtotal', $act_precio);
              }
              else{
                $('#modal_cantidad').modal('show');
              }

            }
            if($selected_mode.data('mode') == "precio"){
                let $precio;
                if($cambiar_precio.data('precio') == $(".product-car.selected [data-precio_unidad]").data('precio_unidad')){
                    $precio = $(this).val();
                }
                else {
                    $precio = $cambiar_precio.data('precio') + '' + $(this).val() + '';
                }
                if($(".product-car.selected").data('granel') == true){
                  $cambiar.html($cambiar.data('unidad') + " " + $(".product-car.selected").data('medida_producto') + " en $<span class='precio_unidad' data-precio='" + $precio + "'>" + $precio + "</span>/" + $(".product-car.selected").data('medida_producto') + "");
                }
                else{
                  $cambiar.html($cambiar.data('unidad') + " Unidad(es) en $<span class='precio_unidad' data-precio='" + $precio + "'>" + $precio + "</span>/Unidad(es)");
                }

                $cambiar.data('precio', $precio);
                $tipo.find(".precio").html(parseFloat($cambiar.data('precio'))*parseFloat($cambiar.data('unidad')));
                let $act_precio = Number($subtotal.data('subtotal')) + Number($tipo.find(".precio").html());
                $act_precio -= Number($precio_antes);
                $carrito_total.find('#precio_total h3').html($act_precio);
                $subtotal.data('subtotal', $act_precio);
            }
            let $precio_total = parseFloat($('.product-car.selected .precio_productos').data('precio_unidad')) * parseFloat($cambiar.data('unidad'));
            let $descuento_antes = $cambiar_descuento.data('descuento');
            let $descuento_total_antes = $descuento_total.data('descuento_total');
            if($selected_mode.data('mode') == "descuento"){
              let $descuento;
              if($cambiar_descuento.data('descuento') == 0){
                $descuento = $(this).val();
              }else{
                $descuento = $cambiar_descuento.data('descuento') + '' + $(this).val() + '';
              }
              $cambiar_descuento.html($descuento);
              $cambiar_descuento.data('descuento', $descuento);
              $tipo.find(".precio").html(parseFloat($precio_total)-parseFloat($cambiar_descuento.data('descuento')));
              let $act_precio = Number($subtotal.data('subtotal')) + Number($tipo.find(".precio").html());
              $act_precio -= Number($precio_antes);
              $carrito_total.find('#precio_total h3').html($act_precio);
              $subtotal.data('subtotal', $act_precio);


              let $total_descuento = Number($descuento) + Number($descuento_total_antes);
              $total_descuento -= Number($descuento_antes);
              $descuento_total.data('descuento_total', $total_descuento);
              $descuento_total.html($total_descuento);
            }
        }
    }
    if($(this).data('borrar')){
      if($cambiar.data('unidad')==0){
        let $act_precio = Number($subtotal.data('subtotal')) - Number($precio_antes);
        $carrito_total.find('#precio_total h3').html($act_precio);
        $subtotal.data('subtotal', $act_precio);
        $('.product-car.selected').remove();
      }
      let $unidad_cambio = 0;
      if($selected_mode.data('mode') == 'unidad'){
          if($cambiar.data('unidad')>10){
            $unidad_cambio = parseInt($cambiar.data('unidad')/10);
          }
          if($(".product-car.selected").data('granel') == true){
            $cambiar.html($unidad_cambio + " " + $(".product-car.selected").data('medida_producto') + " en $<span class='precio_unidad' data-precio='" + $cambiar_precio.data('precio') + "'>" + $cambiar_precio.data('precio') + "</span>/" + $(".product-car.selected").data('medida_producto') + "");
          }
          else{
            $cambiar.html($unidad_cambio + " Unidad(es) en $<span class='precio_unidad' data-precio='" + $cambiar_precio.data('precio') + "'>" + $cambiar_precio.data('precio') + "</span>/Unidad(es)");
          }

          $cambiar.data('unidad', $unidad_cambio);
          $tipo.find(".precio").html(parseFloat($cambiar_precio.data('precio'))*parseFloat($cambiar.data('unidad')));
          let $act_precio = Number($subtotal.data('subtotal')) + Number($tipo.find(".precio").html());
          $act_precio -= Number($precio_antes);
          $carrito_total.find('#precio_total h3').html($act_precio);
          $subtotal.data('subtotal', $act_precio);
        }
        else {
            let $precio_cambio = 0;
            if($selected_mode.data('mode') == 'precio'){
                if($cambiar_precio.data('precio')>10){
                    $precio_cambio = parseInt($cambiar_precio.data('precio')/10);
                }
                if($(".product-car.selected").data('granel') == true){
                  $cambiar.html($cambiar.data('unidad') + " " + $(".product-car.selected").data('medida_producto') + " en $<span class='precio_unidad' data-precio='" + $precio_cambio + "'>" + $precio_cambio + "</span>/" + $(".product-car.selected").data('medida_producto') + "");
                }
                else{
                  $cambiar.html($cambiar.data('unidad') + " Unidad(es) en $<span class='precio_unidad' data-precio='" + $precio_cambio + "'>" + $precio_cambio + "</span>/Unidad(es)");
                }

                $cambiar.data('precio', $precio_cambio);
                $tipo.find(".precio").html(parseFloat($cambiar.data('precio'))*parseFloat($cambiar.data('unidad')));
                let $act_precio = Number($subtotal.data('subtotal')) + Number($tipo.find(".precio").html());
                $act_precio -= Number($precio_antes);
                $carrito_total.find('#precio_total h3').html($act_precio);
                $subtotal.data('subtotal', $act_precio);
            }
            else{
              let $descuento_cambio = 0;

              let $descuento_total_antes = $descuento_total.data('descuento_total');
              let $descuento_antes_cambio = $cambiar_descuento.data('descuento');

              let $precio_total = parseFloat($('.product-car.selected .precio_productos').data('precio_unidad')) * parseFloat($cambiar.data('unidad'));
              if($selected_mode.data('mode') == 'descuento'){
                  if($cambiar_descuento.data('descuento')>10){
                      $descuento_cambio = parseInt($cambiar_descuento.data('descuento')/10);
                  }
                  $cambiar_descuento.html($descuento_cambio);
                  $cambiar_descuento.data('descuento', $descuento_cambio);
                  $tipo.find(".precio").html(parseFloat($precio_total)-parseFloat($cambiar_descuento.data('descuento')));
                  let $act_precio = Number($subtotal.data('subtotal')) + Number($tipo.find(".precio").html());
                  $act_precio -= Number($precio_antes);
                  $carrito_total.find('#precio_total h3').html($act_precio);
                  $subtotal.data('subtotal', $act_precio);


                  let $total_descuento = Number($descuento_total_antes) - Number($descuento_antes_cambio);
                  $total_descuento += Number($descuento_cambio);
                  $descuento_total.data('descuento_total', $total_descuento);
                  $descuento_total.html($total_descuento);
              }
            }
        }
    }
});

$(document).ready(function(){
    $('.card-box-pdv-carrito').on('click', '.product-car', function(){
        $('.product-car.selected').removeClass('selected');
        $(this).addClass('selected');
    });

    funcionFiltro('productos').then(function (data) {
        app_punto_venta.lista_elementos = data.elementos;
        app_punto_venta.all_lista_elementos = data.elementos;
    }, function (err) {
        console.log(err); // Error: "It broke"
    });
});

$('#generar_pago a').click(function(){
    let $categoria;

    var productos = new Array();
    let $tipo;
    let $granel;
    let $id_motivo = 0;

    $('.product-car').each(function(){
      if($(this).data('tipo') == 'Producto' || $(this).data('tipo') == 'Medicamentos' || $(this).data('tipo') == 'Vacunas' || $(this).data('tipo') == 'Desparasitantes' || $(this).data('tipo') == 'Otros'){
        $categoria = 'Existente';
      }
      else{
        if($(this).data('tipo') == 'estetica' || $(this).data('tipo') == 'Area' || $(this).data('tipo') == 'Motivo' || $(this).data('tipo') == 'vacunacion' || $(this).data('tipo') == 'desparasitacion' || $(this).data('tipo') == 'guarderia'){
          $categoria = 'Servicios';
          if($(this).data('tipo') == 'Area'){
            $id_motivo = parseInt($(this).data('id_motivo_area'));
          }
        }
        else{
          $categoria = 'Nueva';
        }
      }

      if($(this).data('tipo') == 'Otros'){
        $tipo = $(this).data('nombre');
      }
      else{
        $tipo = $(this).data('tipo');
      }
      if($(this).data('granel') == true){
        $granel = true;
      }
      else{
        $granel = false;
      }

      productos.push({'id': $(this).data('id'),
                      'tipo': $tipo,
                      'granel': $granel,
                      'id_motivo': $id_motivo,
                      'precio_total': $(this).find('.precio').html(),
                      'unidad': $(this).find('.unidad').data('unidad'),
                      'categoria': $categoria
                    });
    });
    let $cliente = 0;

    if($('#datos_cliente').find('.nombre-completo').data('cliente')){
      $cliente = $('#datos_cliente').find('.nombre-completo').data('cliente');

    }
    var $precio_total = $('#precio_total h3').html();
    var $descuento_total = $('#descuento_total').data('descuento_total');
    if($precio_total > 0) {
      $.ajax({
        url: '/puntoDeVenta/guardar_pago/',
        data: {
            'precio_total': $precio_total,
            'descuento_total': $descuento_total,
            'productos[]': JSON.stringify(productos),
            'cliente': $cliente
        },
        dataType: 'json',
        success: function (data) {
            window.location.replace("/puntoDeVenta/generar_pago/" + data.total_pagar + "/" + data.cliente + "/");
        }
      });
    }
    else{
      $("#modal-info").modal('show');
    }

});

$('.eleccion_cliente a').click(function(){
  let $cliente;
  $cliente = $('#id_cliente').val();
  $('.selectpicker').selectpicker();
  $('#id_cliente').selectpicker();

  $($("#datos_cliente")).empty();

  $.ajax({
    url: '/puntoDeVenta/registro_cliente/',
    data: {
      'cliente': $cliente
    }
  }).done(function(data){
    let $div = $('<div class="nombre-completo" data-cliente="' + data.cliente_id + '">');
    let $p_titulo_nombre = $('<p>').append('<strong>Nombre Completo</strong>');
    let $p_nombre = $('<p style="color:#0A6FF1;margin-top: -13px;">').append(data.nombres + " " + data.apellidos);
    let $p_titulo_documento = $('<p>').append('<strong>Documento</strong>');
    let $p_documento = $('<p style="color:#0A6FF1;margin-top: -13px;">').append(data.tipo_documento + " " + data.numero_documento);
    let $p_titulo_ciudad = $('<p>').append('<strong>Ciudad</strong>');
    let $p_ciudad = $('<p style="color:#0A6FF1;margin-top: -13px;">').append(data.ciudad);

    $div.append($p_titulo_nombre);
    $div.append($p_nombre);
    $div.append($p_titulo_documento);
    $div.append($p_documento);
    $div.append($p_titulo_ciudad);
    $div.append($p_ciudad);

    $($("#datos_cliente")).append($div);
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
       console.log(jqXHR);
  });


  $("#modal-client").modal('hide');//ocultamos el modal
  $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
});

$('.clientes-pendientes a').click(function(){
  $.ajax({
    url: '/productos/ajax/validate_username_debe/',
    data: {
      'entro': true
    },
    dataType: 'json',
    success: function (data) {
      if (data.is_taken) {
          $("#modal-client").modal('hide');//ocultamos el modal
          $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
          $("#modal_cliente_deben").modal('show');
          $($(".select-client-deben")).remove();
          let $clientes = JSON.parse(data.clientes);
          let $div = $('<div class="select-client-deben col-lg-12">');
          let $select = $('<select name="cliente" class="form-control selectpicker" data-live-search="true" id="id_cliente_deben" tabindex="-1" aria-hidden="true">');
          let $option;
          $option = $('<option value="0">----------------</option>');
          $select.append($option);
          $clientes.forEach(element => {
            $option = $('<option value="' + element.id + '" data-tokens="' + element.nombre + ' ' + element.documento + '">' + element.nombre + ' ' + element.documento + '</option>');

            $select.append($option);
          });

          $div.append($select);
          $($(".vis_client")).append($div);

          $('.selectpicker').selectpicker();
          $('#id_cliente_deben').selectpicker();
      }
    }
  });
  $("#modal_cliente_deben").modal('hide');//ocultamos el modal
  $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
});

$('.eleccion_cliente_deben a').click(function(){
  let $cliente;
  $cliente = $('#id_cliente_deben').val();

  $($("#datos_cliente")).empty();

  $.ajax({
    url: '/puntoDeVenta/registro_cliente/',
    data: {
      'cliente': $cliente
    }
  }).done(function(data){
    let $div = $('<div class="nombre-completo" data-cliente="' + data.cliente_id + '">');
    let $p_titulo_nombre = $('<p>').append('<strong>Nombre Completo</strong>');
    let $p_nombre = $('<p style="color:#0A6FF1;margin-top: -13px;">').append(data.nombres + " " + data.apellidos);
    let $p_titulo_documento = $('<p>').append('<strong>Documento</strong>');
    let $p_documento = $('<p style="color:#0A6FF1;margin-top: -13px;">').append(data.tipo_documento + " " + data.numero_documento);
    let $p_titulo_ciudad = $('<p>').append('<strong>Ciudad</strong>');
    let $p_ciudad = $('<p style="color:#0A6FF1;margin-top: -13px;">').append(data.ciudad);

    $div.append($p_titulo_nombre);
    $div.append($p_nombre);
    $div.append($p_titulo_documento);
    $div.append($p_documento);
    $div.append($p_titulo_ciudad);
    $div.append($p_ciudad);

    $($("#datos_cliente")).append($div);
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
       console.log(jqXHR);
  });

  $("#modal_cliente_deben").modal('hide');//ocultamos el modal
  $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
});

function filtrarPorNombreYCodigo(obj) {
    var codigo_elemento_inventario = ''

    if(obj.model == 'productos.producto'){
        codigo_elemento_inventario = obj.fields.codigo
    }else{
        codigo_elemento_inventario = obj.fields.codigo_inventario
    }

    if(codigo_elemento_inventario == null){
        codigo_elemento_inventario = ''
    }

    if(codigo_elemento_inventario.toLowerCase().includes($('#buscar_inventario').val().toLowerCase()) || obj.fields.nombre.toLowerCase().includes($('#buscar_inventario').val().toLowerCase())){
        return obj;
    }
}

$('#buscar_inventario').keyup(function () {
    if ($(this).val() != '') {
        app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos.filter(filtrarPorNombreYCodigo);
    }else{
        app_punto_venta.lista_elementos = app_punto_venta.all_lista_elementos;
    }
});

$.fn.delayPasteKeyUp = function(fn, ms)
{
  var timer = 0;
  $(this).on("propertychange input", function()
  {
    clearTimeout(timer);
    timer = setTimeout(fn, ms);
  });
};

//la utilizamos
$(document).ready(function()
{
 $("#barras").delayPasteKeyUp(function(){
   console.log($("#barras").val());
   console.log('entre');
   $.ajax({
     url: '/puntoDeVenta/buscar_producto/',
     data: {
       'codigo_barras': $("#barras").val()
     },
     dataType: 'json',
     success: function (data) {
       console.log(data);
       let $productos = JSON.parse(data.productos);
       let $medicamentos = JSON.parse(data.medicamentos);
       let $vacunas = JSON.parse(data.vacunas);
       let $desparasitantes = JSON.parse(data.desparasitantes);
       let $nuevas_categorias = JSON.parse(data.nuevas_categorias);

       if($productos){
         console.log($productos);
         $productos.forEach(element => {
           if(element.fields.cantidad_por_unidad == true){
             $('#modal_producto').modal('show');
           }
           else {
             let precio = 0;
             let $nombre;
             let $cantidad_disponible;

             precio = element.fields.precio_venta;
             $nombre = element.fields.nombre;
             $cantidad_disponible = parseInt(element.fields.cantidad);

             let $cantidad_carrito = $('.unidad').data('unidad');

             $nombre_class = $nombre.trim().replace(/\s/g, '_')
             let $tipo = $(".Producto [data-nombre='" + $nombre_class + "']");
             let $motivo_area = 0;

             let $div = $('<div class="product-car ' + $tipo +'" data-id=' + element.pk + ' data-tipo=' + $tipo + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + '  data-id_motivo_area=NoTiene>');
             let $div_boot_10 = $('<div class="col-md-10">');
             let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
             let $div_uni_prod = $('<div class="unidades_productos">');
             let $unidad = $('<i>');
             let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
             let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
             let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
             let $precio_product = $('<p class="precio">').append(precio);

             let $desc = $('<div class="descuento">');
             let $comple = $('<i>');
             let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

             if($tipo.length>0){
               if($cantidad_carrito < $cantidad_disponible){
                 $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
                 $unidades = parseInt($tipo.find(".unidad").html())+1;
                 $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
                 $('.unidad').data('unidad', $unidades);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             else{

               if($cantidad_disponible > 0){
                 $comple.append($comple_2);
                 $desc.append($comple);
                 $div_precio_prod.append($precio_product);
                 $unidad.append($unidad_font);
                 $div_uni_prod.append($unidad);
                 $div_boot_10.append($div_name_prod);
                 $div_boot_10.append($div_uni_prod);
                 $div_boot_10.append($desc);
                 $div_boot_2.append($div_precio_prod);
                 $div.append($div_boot_10);
                 $div.append($div_boot_2);
                 $($carrito_total).before($div);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             let $total = Number($subtotal.data('subtotal')) + Number(precio);
             $carrito_total.find('#precio_total h3').html($total);
             $subtotal.data('subtotal', $total);
           }
         });
       }
       if($medicamentos){
         $medicamentos.forEach(element => {
           if(element.fields.cantidad_por_unidad == true){
             $('#modal_producto').modal('show');
           }
           else {
             let precio = 0;
             let $nombre;
             let $cantidad_disponible;

             precio = element.fields.precio_venta;
             $nombre = element.fields.nombre;
             $cantidad_disponible = parseInt(element.fields.cantidad);

             let $cantidad_carrito = $('.unidad').data('unidad');

             $nombre_class = $nombre.trim().replace(/\s/g, '_')
             let $tipo = $(".Producto [data-nombre='" + $nombre_class + "']");
             let $motivo_area = 0;

             let $div = $('<div class="product-car ' + $tipo +'" data-id=' + element.pk + ' data-tipo=' + $tipo + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + '  data-id_motivo_area=NoTiene>');
             let $div_boot_10 = $('<div class="col-md-10">');
             let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
             let $div_uni_prod = $('<div class="unidades_productos">');
             let $unidad = $('<i>');
             let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
             let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
             let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
             let $precio_product = $('<p class="precio">').append(precio);

             let $desc = $('<div class="descuento">');
             let $comple = $('<i>');
             let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

             if($tipo.length>0){
               if($cantidad_carrito < $cantidad_disponible){
                 $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
                 $unidades = parseInt($tipo.find(".unidad").html())+1;
                 $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
                 $('.unidad').data('unidad', $unidades);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             else{

               if($cantidad_disponible > 0){
                 $comple.append($comple_2);
                 $desc.append($comple);
                 $div_precio_prod.append($precio_product);
                 $unidad.append($unidad_font);
                 $div_uni_prod.append($unidad);
                 $div_boot_10.append($div_name_prod);
                 $div_boot_10.append($div_uni_prod);
                 $div_boot_10.append($desc);
                 $div_boot_2.append($div_precio_prod);
                 $div.append($div_boot_10);
                 $div.append($div_boot_2);
                 $($carrito_total).before($div);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             let $total = Number($subtotal.data('subtotal')) + Number(precio);
             $carrito_total.find('#precio_total h3').html($total);
             $subtotal.data('subtotal', $total);
           }
         });
       }
       if($vacunas){
         $vacunas.forEach(element => {
           if(element.fields.cantidad_por_unidad == true){
             $('#modal_producto').modal('show');
           }
           else {
             let precio = 0;
             let $nombre;
             let $cantidad_disponible;

             precio = element.fields.precio_venta;
             $nombre = element.fields.nombre;
             $cantidad_disponible = parseInt(element.fields.cantidad);

             let $cantidad_carrito = $('.unidad').data('unidad');

             $nombre_class = $nombre.trim().replace(/\s/g, '_')
             let $tipo = $(".Producto [data-nombre='" + $nombre_class + "']");
             let $motivo_area = 0;

             let $div = $('<div class="product-car ' + $tipo +'" data-id=' + element.pk + ' data-tipo=' + $tipo + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + '  data-id_motivo_area=NoTiene>');
             let $div_boot_10 = $('<div class="col-md-10">');
             let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
             let $div_uni_prod = $('<div class="unidades_productos">');
             let $unidad = $('<i>');
             let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
             let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
             let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
             let $precio_product = $('<p class="precio">').append(precio);

             let $desc = $('<div class="descuento">');
             let $comple = $('<i>');
             let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

             if($tipo.length>0){
               if($cantidad_carrito < $cantidad_disponible){
                 $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
                 $unidades = parseInt($tipo.find(".unidad").html())+1;
                 $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
                 $('.unidad').data('unidad', $unidades);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             else{

               if($cantidad_disponible > 0){
                 $comple.append($comple_2);
                 $desc.append($comple);
                 $div_precio_prod.append($precio_product);
                 $unidad.append($unidad_font);
                 $div_uni_prod.append($unidad);
                 $div_boot_10.append($div_name_prod);
                 $div_boot_10.append($div_uni_prod);
                 $div_boot_10.append($desc);
                 $div_boot_2.append($div_precio_prod);
                 $div.append($div_boot_10);
                 $div.append($div_boot_2);
                 $($carrito_total).before($div);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             let $total = Number($subtotal.data('subtotal')) + Number(precio);
             $carrito_total.find('#precio_total h3').html($total);
             $subtotal.data('subtotal', $total);
           }
         });
       }
       if($desparasitantes){
         $desparasitantes.forEach(element => {
           if(element.fields.cantidad_por_unidad == true){
             $('#modal_producto').modal('show');
           }
           else {
             let precio = 0;
             let $nombre;
             let $cantidad_disponible;

             precio = element.fields.precio_venta;
             $nombre = element.fields.nombre;
             $cantidad_disponible = parseInt(element.fields.cantidad);

             let $cantidad_carrito = $('.unidad').data('unidad');

             $nombre_class = $nombre.trim().replace(/\s/g, '_')
             let $tipo = $(".Producto [data-nombre='" + $nombre_class + "']");
             let $motivo_area = 0;

             let $div = $('<div class="product-car ' + $tipo +'" data-id=' + element.pk + ' data-tipo=' + $tipo + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + '  data-id_motivo_area=NoTiene>');
             let $div_boot_10 = $('<div class="col-md-10">');
             let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
             let $div_uni_prod = $('<div class="unidades_productos">');
             let $unidad = $('<i>');
             let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
             let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
             let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
             let $precio_product = $('<p class="precio">').append(precio);

             let $desc = $('<div class="descuento">');
             let $comple = $('<i>');
             let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

             if($tipo.length>0){
               if($cantidad_carrito < $cantidad_disponible){
                 $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
                 $unidades = parseInt($tipo.find(".unidad").html())+1;
                 $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
                 $('.unidad').data('unidad', $unidades);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             else{

               if($cantidad_disponible > 0){
                 $comple.append($comple_2);
                 $desc.append($comple);
                 $div_precio_prod.append($precio_product);
                 $unidad.append($unidad_font);
                 $div_uni_prod.append($unidad);
                 $div_boot_10.append($div_name_prod);
                 $div_boot_10.append($div_uni_prod);
                 $div_boot_10.append($desc);
                 $div_boot_2.append($div_precio_prod);
                 $div.append($div_boot_10);
                 $div.append($div_boot_2);
                 $($carrito_total).before($div);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             let $total = Number($subtotal.data('subtotal')) + Number(precio);
             $carrito_total.find('#precio_total h3').html($total);
             $subtotal.data('subtotal', $total);
           }
         });
       }
       if($nuevas_categorias){
         $nuevas_categorias.forEach(element => {
           if(element.fields.cantidad_por_unidad == true){
             $('#modal_producto').modal('show');
           }
           else {
             let precio = 0;
             let $nombre;
             let $cantidad_disponible;

             precio = element.fields.precio_venta;
             $nombre = element.fields.nombre;
             $cantidad_disponible = parseInt(element.fields.cantidad);

             let $cantidad_carrito = $('.unidad').data('unidad');

             $nombre_class = $nombre.trim().replace(/\s/g, '_')
             let $tipo = $(".Producto [data-nombre='" + $nombre_class + "']");
             let $motivo_area = 0;

             let $div = $('<div class="product-car ' + $tipo +'" data-id=' + element.pk + ' data-tipo=' + $tipo + ' data-nombre=' + $nombre_class + ' data-cantidad_total_disponible=' + $cantidad_disponible + '  data-id_motivo_area=NoTiene>');
             let $div_boot_10 = $('<div class="col-md-10">');
             let $div_name_prod = $('<div class="nombre_productos">').append($nombre);
             let $div_uni_prod = $('<div class="unidades_productos">');
             let $unidad = $('<i>');
             let $unidad_font = $('<h6 class ="unidad" data-unidad="1">1 Unidad(es) en $<span class="precio_unidad" data-precio="' + precio + '">' + precio + '</span>/Unidad(es)</h6>');
             let $div_boot_2 = $('<div class="col-md-2" style="padding-left: 0px;">');
             let $div_precio_prod = $('<div class="precio_productos" data-precio_unidad="' + precio + '">');
             let $precio_product = $('<p class="precio">').append(precio);

             let $desc = $('<div class="descuento">');
             let $comple = $('<i>');
             let $comple_2 = $('<h6 class="descuento_total">Tiene un Descuento de <span data-descuento="0">0</span> pesos</h6>');

             if($tipo.length>0){
               if($cantidad_carrito < $cantidad_disponible){
                 $tipo.find(".precio").html(parseFloat($tipo.find(".precio").html())+parseFloat(precio));
                 $unidades = parseInt($tipo.find(".unidad").html())+1;
                 $tipo.find(".unidad").html($unidades + " Unidad(es) en $<span class='precio_unidad' data-precio='" + precio + "'>" + precio + "</span>/Unidad(es)");
                 $('.unidad').data('unidad', $unidades);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             else{

               if($cantidad_disponible > 0){
                 $comple.append($comple_2);
                 $desc.append($comple);
                 $div_precio_prod.append($precio_product);
                 $unidad.append($unidad_font);
                 $div_uni_prod.append($unidad);
                 $div_boot_10.append($div_name_prod);
                 $div_boot_10.append($div_uni_prod);
                 $div_boot_10.append($desc);
                 $div_boot_2.append($div_precio_prod);
                 $div.append($div_boot_10);
                 $div.append($div_boot_2);
                 $($carrito_total).before($div);
               }
               else{
                 $('#modal_cantidad').modal('show');
                 precio = 0;
               }
             }
             let $total = Number($subtotal.data('subtotal')) + Number(precio);
             $carrito_total.find('#precio_total h3').html($total);
             $subtotal.data('subtotal', $total);
           }
         });
       }
     }
   });
   $("#barras").val("");
  }, 200);

});
