
    function AnularPago(id, total, fecha) {
        swal({
            title: "Estas seguro que deseas anular la factura de venta #"+id+" del dia "+fecha+" por valor de $"+total+"?",
            text: "Este cambio es irreversible. El inventario relacionado en la venta será sumado nuevamente en el inventario y las atenciones cobradas quedaran de nuevo pendientes por pago.",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No, cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Si, anular permanentemente!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: '/productos/anular_venta/',
                    data: {
                      'id': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        window.location.replace("/puntoDeVenta/listar_puntos_hoy/");
                    }
                });
            }
        });
    }

    function AnularCompra(id, total, fecha) {
        swal({
            title: "Estas seguro que deseas anular la compra del dia "+fecha+" por valor de $"+total+"?",
            text: "Este cambio es irreversible. El inventario relacionado en la compra será descontado nuevamente en el inventario.",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No, cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Si, anular permanentemente!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: '/gastos/anular_compra/',
                    data: {
                      'id': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        window.location.replace("/puntoDeVenta/listar_puntos_hoy/");
                    }
                });
            }
        });

    }
    function Cerrar_punto_alert(id) {
        swal({
                title: "Estas seguro que desea Cerrar la punto ?",
                text: "Una vez se cierra la punto no puede volver a abrirse hasta el dia de mañana.",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "No, cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Si, cerrar punto!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/puntoDeVenta/cerrar_punto/'+id,
                        data: {
                            'id': id
                        },
                        dataType: 'json',
                        success: function (data) {
                            window.location.replace("/puntoDeVenta/abrir_punto_de_venta/");
                        }
                    });
                }
            });
    }
