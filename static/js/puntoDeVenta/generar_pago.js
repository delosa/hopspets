function Cerrar_punto_alert(id) {
  swal({
    title: "Estas seguro que desea Cerrar la punto ?",
    text: "Una vez se cierra la punto no puede volver a abrirse hasta el dia de mañana.",
    icon: "warning",
    buttons: {
      cancel: {
        text: "No, cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Si, cerrar punto!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }
    },
    dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

          $.ajax({
              url: '/puntoDeVenta/cerrar_punto/'+id,
              data: {
                  'id': id
              },
              dataType: 'json',
              success: function (data) {
                  window.location.replace("/puntoDeVenta/abrir_punto_de_venta/");
              }
          });
      }
    });
}

$(document).ready(function(){
    $('#pago_generado').on('click', '.valor-recibido', function(){
        $('.valor-recibido.selected').removeClass('selected');
        $(this).addClass('selected');

        $('.valor-recibido.selected [data-valor_pagar]').data('valor_pagar', 0);
        $('.valor-recibido.selected [data-valor_pagar]').html("<p>$<span class='costo'>0</span></p>");
    });
});

$('#pago_generado').hide();

$('#generar_tabla a').click(function(){
  $('#pago_generado').show();
  $('#total_pagar').hide();
  $('.informativo').hide();
});

$('#debito a').click(function(){
  $(this).addClass('metodo');
  let $ultima_row = $('#pago_generado table tr:last');
  $ultima_row.find('.metodo_credito').hide();
});

var $restante = 0;
$('.numpago button').click(function(){
  let $valor_pagar = $('.valor-recibido.selected [data-valor_pagar]');
  let $ultima_fila = $('#pago_generado table tr').last();

  if($(this).hasClass('number-char') && $('.selected').length>0){
    let $valor = 0;
    if($valor_pagar){
      if($valor_pagar.data('valor_pagar') == 0 ){
        $valor = $(this).val();
      }
      else {
        $valor = $valor_pagar.data('valor_pagar') + '' + $(this).val() + '';
      }
      $valor_pagar.html("<p>$<span class='costo'>" + $valor + "</span></p>");
      $valor_pagar.data('valor_pagar', $valor);
    }

    let $pagos = $ultima_fila.find('.valor-recibido a').data('valor_pagar');
    let $total_pagar = $ultima_fila.find('.pagar').html();

    if(Number($pagos) > Number($total_pagar)){
      let $cambio = Number($pagos) - Number($total_pagar);
      $ultima_fila.find('.cambio').data('cambio', $cambio);
      $ultima_fila.find('.cambio').html("$" + $cambio);
    }
  }

  if($(this).hasClass('numpad-c-borra')){
    let $valor = 0;
    $valor_pagar.html("<p>$<span class='costo'>" + $valor + "</span></p>");
    $valor_pagar.data('valor_pagar', $valor);

    $ultima_fila.find('.cambio').data('cambio', $valor);
    $ultima_fila.find('.cambio').html("$" + $valor);
  }

  if($(this).data('borrar')){
    let $resta = 0;
    if($valor_pagar.data('valor_pagar') > 10){
      $resta = parseInt($valor_pagar.data('valor_pagar') / 10);
    }
    $valor_pagar.data('valor_pagar', $resta);
    $valor_pagar.html("<p>$<span class='costo'>" + $resta + "</span></p>");

    let $pagos = $valor_pagar.data('valor_pagar');
    let $total_pagar = $('.pagar').html();

    if(Number($resta) > Number($total_pagar)){
      let $cambio = Number($pagos) - Number($total_pagar);
      $ultima_fila.find('.cambio').data('cambio', $cambio);
      $ultima_fila.find('.cambio').html("$" + $cambio);
    }
    else{
      $ultima_fila.find('.cambio').data('cambio', 0);
      $ultima_fila.find('.cambio').html("$0");
    }
    $('.card-box-pdv-pago').find('.informativo').remove();
  }
});

let $pago_total = $('.valor-recibido'); //$('.valor-recibido').data('total_pago');
let $abono = $('.valor-recibido a'); //$('.valor-recibido a').data('valor_pagar');
let $faltante = 0;
$('#pago_generado').on('click', '.agregar', function(e){
  e.preventDefault();
  $('.informativo').hide();
  $row_actual = $(this).parents('tr');
  $faltante = Number($row_actual.find('.valor-recibido').data('total_pago')) - Number($row_actual.find('.valor-recibido a').data('valor_pagar'));

  let $tr = $('<tr class="pagos">');
  let $td1 = $('<td class="text-center valor-pagar">');
  let $content1 = $('<p>').append('$');
  let $content1_1 = $('<span class="pagar">').append($faltante);
  let $td2 = $('<td class="text-center tipo">').append("<select class='form-control'><option value='Efectivo'>Efectivo</option><option value='Tarjeta debito'>Tarjeta débito</option><option value='Consignacion'>Consignación</option><option value='Tarjeta credito'>Tarjeta crédito</option><option value='Otro'>Otro</option></select>");
  let $td4 = $('<td class="text-center valor-recibido" data-total_pago="' + $faltante + '">');
  let $content4 = $('<a data-valor_pagar="0">').append('<p>$<span class="costo">0</span></p>');
  let $td5 = $('<td class="text-center vuelto">').append('<p class="cambio" data-cambio="0">$0</p>');
  let $td6 = $('<td>').append('<a href="#" style="color: #076ff1" title="Agregar" class="action-icon dropdown-toggle agregar"><i class="fa fa-plus-square fa-lg"></i></a><a href="#" style="color: #076ff1" title="Eliminar" class="action-icon dropdown-toggle eliminar"><i class="fa fa-close fa-lg"></i></a>');

  if($row_actual.find('td .cambio').data('cambio') < 1 && $row_actual.find('.valor-recibido a').data('valor_pagar') > 0 && parseInt($row_actual.find('.valor-recibido a').data('valor_pagar')) != parseInt($row_actual.find('.valor-recibido').data('total_pago')) ){
    $content1.append($content1_1);
    $td1.append($content1);
    $td4.append($content4);
    $tr.append($td1);
    $tr.append($td2);
    $tr.append($td4);
    $tr.append($td5);
    $tr.append($td6);

    $('#pago_generado table tbody tr:last td:last').remove();

    $($row_actual).after($tr);
  }
  else{
    if(parseInt($row_actual.find('.valor-recibido a').data('valor_pagar')) == parseInt($row_actual.find('.valor-recibido').data('total_pago'))){
      $('#pago_generado').after($('<div class="informativo"><h1 style="text-align:center;">Ya puede validar su pago</h1></div>'));
    }
    else{
      $('#pago_generado').after($('<div class="informativo"><h1 style="text-align:center;">Debe ingresar montos validos</h1></div>'));
    }
    
  }
});
var $ultima_row = $('#pago_generado table tr:last');
$('#pago_generado').on('click', '.eliminar', function(e){
  let $ultima_row = $('#pago_generado table tr:last');

  if($('#pago_generado table tbody tr').length > 1){
    $ultima_row.remove();
    $('#pago_generado table tbody tr:last').append($('<td>').append('<a href="#" style="color: #076ff1" title="Agregar" class="action-icon dropdown-toggle agregar"><i class="fa fa-plus-square fa-lg"></i></a><a href="#" style="color: #076ff1" title="Eliminar" class="action-icon dropdown-toggle eliminar"><i class="fa fa-close fa-lg"></i></a>'));
  }

});

$('.validar a').on('click', function(){
  let $ultima_row = $('#pago_generado table tr:last');

  if($ultima_row.find('.cambio').data('cambio') > 0 || $ultima_row.find('.valor-recibido').data('total_pago') == $ultima_row.find('.valor-recibido a').data('valor_pagar')){

    var metodos = new Array();
    let $met;
    let $cuota;

    let $cliente = 0;

    if($('#datos_cliente').find('.nombre-completo').data('cliente')){
      $cliente = $('#datos_cliente').find('.nombre-completo').data('cliente');
    }

    $('.pagos').each(function(){
      $met = 'Debito';
      $cuota = 0;

      metodos.push({
        'pago': $(this).find('.valor-recibido a').data('valor_pagar'),
        'tipo': $(this).find('.tipo .form-control').val(),
        'metodo': $met,
        'cuotas': $cuota,
        'cambio': $(this).find('.vuelto .cambio').data('cambio')
      });
    });

    $.ajax({
      url: '/puntoDeVenta/guardar_metodo/',
      data: {
          'precio_total': $('#total_pagar').data('pago'),
          'metodo[]': JSON.stringify(metodos),
          'cliente': $cliente
      },
      dataType: 'json',
      success: function (data) {
          window.location.replace("/puntoDeVenta/validar_pago/" + data.pago + "/" + data.factura + "/");
      }
    });
  }
  else{
    $('#pago_generado').after($('<div class="informativo"><h1 style="text-align:center;">Aún no ha completado su pago</h1><h3 style="text-align:center;">Intente Nuevamente</h3></div>'));
  }

});
