from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_impuesto/', CrearImpuesto.as_view(), name="registrar_impuesto"),
    path('listar_impuesto/', ListarImpuesto.as_view(), name="listar_impuesto"),
    path('editar_impuesto/<int:pk>/', EditarImpuesto.as_view(), name="editar_impuesto"),
    path('eliminar_impuesto/', eliminar_impuesto, name="eliminar_impuesto"),
]