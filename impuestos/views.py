from django.views.generic import *
from .models import Impuesto
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect
from .forms import CrearImpuestoForm, EditarImpuestoForm
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.http import JsonResponse


class CrearImpuesto(SuccessMessageMixin, CreateView):
    model = Impuesto
    form_class = CrearImpuestoForm
    template_name = "registrar_impuesto.html"
    success_url = reverse_lazy('listar_impuesto')
    success_message = "¡Impuesto creado con exito!"
    

    def form_valid(self,form):
        self.object = form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearImpuesto, self).get_context_data(**kwargs)
        context['menu_impuestos'] = True
        return context


class ListarImpuesto (ListView):
    
    model = Impuesto
    template_name = "listar_impuesto.html"

    def get_context_data(self, **kwargs):
        context = super(ListarImpuesto, self).get_context_data(**kwargs)
        context['menu_impuestos'] = True
        return context


class EditarImpuesto(SuccessMessageMixin, UpdateView):

    model = Impuesto
    form_class = EditarImpuestoForm
    template_name = "registrar_impuesto.html"
    success_url = reverse_lazy('listar_impuesto')
    success_message = "¡Impuesto modificado con exito!"

    def dispatch(self, request, *args, **kwargs):
        return super(EditarImpuesto, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarImpuesto, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_impuestos'] = True
        return context


def eliminar_impuesto(request):
    id_impuesto = request.GET.get('id', '')
    mi_impuesto = get_object_or_404(Impuesto, pk=id_impuesto)
    mi_impuesto.delete()
    messages.success(request, '!Impuesto eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)
