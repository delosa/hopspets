from .models import Impuesto
from django import forms


class CrearImpuestoForm(forms.ModelForm):

    class Meta:
        model = Impuesto
        fields = (
            'nombre',
            'porcentaje',
        )

        widgets = {
           'porcentaje': forms.NumberInput(attrs={'required': 'true', 'min': '0', 'max': '100', 'placeholder': 'ej: 19%'}),
        }


class EditarImpuestoForm(forms.ModelForm):

    class Meta:
        model = Impuesto
        
        fields = (
            'nombre',
            'porcentaje',
        )

        widgets = {
            'nombre': forms.TextInput(attrs={'readonly': 'readonly'}),
            'porcentaje': forms.NumberInput(attrs={'required': 'true', 'min': '0', 'max': '100', 'placeholder': 'ej: 19%'}),
        }


