from django.db import models



class Impuesto(models.Model):
    
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    porcentaje = models.DecimalField(max_digits=4, decimal_places=2,  verbose_name='Porcentaje')

    def __str__(self):
        return self.nombre + " (" + str(self.porcentaje) + "%)"
