from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from .forms import *
import zipfile
from mascotas.models import Mascota
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import get_object_or_404, redirect, render,render_to_response
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from datetime import datetime, timedelta
from veteriariaTenant.models import Veterinaria
from usuarios.models import Usuario
from productos.models import Medicamento, HistoriaMedicamento, MedicamentoHospitalizacion, MedicamentoCirugia
from estetica.models import FacturaEstetica
from formulas.models import Formula
from vacunacion.models import Vacunacion, AplicacionVacunacion
from desparasitacion.models import Desparasitacion, AplicacionDesparasitacion
from guarderia.models import Guarderia, DatosGuarderia
from itertools import chain
from django.http import JsonResponse
import json
from django.core import serializers
from django.http import HttpResponse
from django.template import loader
import pytz
from django.utils.timezone import make_aware
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
import math
from decimal import Decimal
from django.core.serializers.json import DjangoJSONEncoder
from insumos.models import Insumo, InsumoHistoria, InsumoHospitalizacion


class CrearCita(SuccessMessageMixin, CreateView):
    model = Cita
    form_class = CrearCitaForm
    template_name = "registrar_cita.html"
    success_url = reverse_lazy('listar_citas')
    success_message = "¡La cita se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearCita, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        cita = form.instance

        if cita.proxima_cita:
            nueva_cita = Cita(mascota=cita.mascota, veterinario=cita.veterinario, fecha=cita.proxima_cita, motivo_cita= cita.motivo_proxima_cita)
            nueva_cita.save()

        self.object = form.save()
        return super(CrearCita, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearCita, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['citas'] = True
        return context


class CrearCitaMascota(SuccessMessageMixin, CreateView):
    model = Cita
    form_class = CrearCitaMascotaForm
    template_name = "registrar_cita_mascota.html"
    success_message = "¡La cita se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearCitaMascota, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_mascota =  get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return '/mascotas/detalle_mascota/' + str(mi_mascota.id)

    def form_valid(self,form):
        cita = form.instance
        cita.mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])

        if cita.proxima_cita:
            nueva_cita = Cita(mascota=cita.mascota, veterinario=cita.veterinario, fecha=cita.proxima_cita, motivo_cita= cita.motivo_proxima_cita)
            nueva_cita.save()

        if (cita.mascota.cliente.notificaciones_wpp) and  (len(cita.mascota.cliente.celular) != 10 or not cita.mascota.cliente.celular.isdigit()):
            messages.error(self.request, 'Debe ingresar un numero de celular valido para que se le notifique al propietario de la mascota por whatsapp')

        self.object = form.save()
        return super(CrearCitaMascota, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearCitaMascota, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        context['citas_mascota'] = Cita.objects.filter(estado='Activa').filter(mascota_id=self.kwargs['id_mascota'])
        return context


class ListarCitas(ListView):
    model = Cita
    template_name = "listar_citas.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarCitas, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarCitas, self).get_context_data(**kwargs)
        context['citas'] = True

        return context

def registrar_citas(request,fecha):
    if request.method == "GET":
        return render(request,'registrar_cita.html',{'fecha':fecha,'form':CrearCitaForm(), 'citas': True})
    if request.method == "POST":
        print("inside POST")
        form_cita = CrearCitaForm(request.POST)
        cita = form_cita.instance           
        form_cita.save()
        if cita.proxima_cita:
            nueva_cita = Cita(mascota=cita.mascota, veterinario=cita.veterinario, fecha=cita.proxima_cita, motivo_cita= cita.motivo_proxima_cita)
            nueva_cita.save()

        return redirect('/citas/listar_citas')

def mostrar_citas(request):
    data = {}
    data['dates'] =[]

    if request.user.usuario.roles == 'Veterinario':
        citas = Cita.objects.filter(veterinario__id=request.user.usuario.id, estado='Activa')
        esteticas = FacturaEstetica.objects.filter(estado_de_atencion='Activa')
        vacunacion = Vacunacion.objects.filter(veterinario__id=request.user.usuario.id, estado_aplicacion='Pendiente')
        desparacitacion = Desparasitacion.objects.filter(veterinario__id=request.user.usuario.id, estado_aplicacion='Pendiente')
        guarderias = Guarderia.objects.filter(estado='Activa')
    else:
        #citas = Cita.objects.filter(estado='Activa')
        citas = Cita.objects.all()
        #esteticas = FacturaEstetica.objects.filter(estado_de_atencion='Activa')
        esteticas = FacturaEstetica.objects.all()
        #vacunacion = Vacunacion.objects.filter(estado_aplicacion='Pendiente')
        vacunacion = Vacunacion.objects.all()
        #desparacitacion = Desparasitacion.objects.filter(estado_aplicacion='Pendiente')
        desparacitacion = Desparasitacion.objects.all()
        #guarderias = Guarderia.objects.filter(estado='Activa')
        guarderias = Guarderia.objects.all()

    for cita in citas:
        x = datetime(cita.fecha.year, cita.fecha.month, cita.fecha.day, cita.fecha.hour,cita.fecha.minute,cita.fecha.second)
        x = x - timedelta(hours=5)
        x_end = x + timedelta(hours=1)
        color = '#00c7ff'
        if cita.estado != 'Activa':
            color = '#4BC241'
        data['dates'].append({'cliente': cita.mascota.cliente.nombres, 'idCliente': cita.mascota.cliente.id,
                              'nroTelefonico': cita.mascota.cliente.telefono,
                              'direccion': cita.mascota.cliente.direccion, 'tipoServicio': '1', 'codigoCita': cita.id,
                              'tipoServicioDes': 'Cita médica', 'codMascota': cita.mascota.id,
                              'nombreMascota': cita.mascota.nombre,
                              'title': cita.mascota.cliente.nombres + ' / ' + cita.mascota.nombre,
                              'start': x.strftime('%Y-%m-%dT%H:%M:%S'), 'end': x_end.strftime('%Y-%m-%dT%H:%M:%S'),
                              'color': color, 'allDay': False, 'descripcion': cita.motivo_cita})

    for estetica in esteticas:
        y = datetime(estetica.fecha.year, estetica.fecha.month, estetica.fecha.day,estetica.fecha.hour, estetica.fecha.minute,estetica.fecha.second)
        y = y - timedelta(hours=5)
        y_end = y + timedelta(hours=1)
        color = '#605fff'
        if estetica.estado_de_atencion != 'Activa':
            color = '#4BC241'
        data['dates'].append({'cliente': estetica.mascota.cliente.nombres, 'idCliente': estetica.mascota.cliente.id,
                              'nroTelefonico': estetica.mascota.cliente.telefono,
                              'direccion': estetica.mascota.cliente.direccion, 'tipoServicio': '2',
                              'codigoCita': estetica.id, 'tipoServicioDes': 'Servicio estética',
                              'codMascota': estetica.mascota.id, 'nombreMascota': estetica.mascota.nombre,
                              'title': estetica.mascota.cliente.nombres + ' / ' + estetica.mascota.nombre,
                              'start': y.strftime("%Y-%m-%dT%H:%M:%S"), 'end': y_end.strftime("%Y-%m-%dT%H:%M:%S"),
                              'color': color, 'allDay': False, 'instrucciones': estetica.otras_instrucciones})

    for cita_vacunas in vacunacion:
        z = datetime(cita_vacunas.fecha.year, cita_vacunas.fecha.month, cita_vacunas.fecha.day,cita_vacunas.fecha.hour, cita_vacunas.fecha.minute,cita_vacunas.fecha.second)
        z = z - timedelta(hours=5)
        z_end = z + timedelta(hours=1)
        color = '#ff7e54'
        if cita_vacunas.estado_aplicacion != 'Pendiente':
            color = '#4BC241'
        data['dates'].append(
            {'cliente': cita_vacunas.mascota.cliente.nombres, 'idCliente': cita_vacunas.mascota.cliente.id,
             'nroTelefonico': cita_vacunas.mascota.cliente.telefono,
             'direccion': cita_vacunas.mascota.cliente.direccion, 'tipoServicio': '3', 'codigoCita': cita_vacunas.id,
             'tipoServicioDes': 'Servicio vacunación', 'codMascota': cita_vacunas.mascota.id,
             'nombreMascota': cita_vacunas.mascota.nombre,
             'title': cita_vacunas.mascota.cliente.nombres + ' / ' + cita_vacunas.mascota.nombre,
             'start': z.strftime("%Y-%m-%dT%H:%M:%S"), 'end': z_end.strftime("%Y-%m-%dT%H:%M:%S"), 'color': color,
             'descripcion': cita_vacunas.descripcion, 'observaciones': cita_vacunas.observaciones})

    for cita_desparacitacion in desparacitacion:
        w = datetime(cita_desparacitacion.fecha.year, cita_desparacitacion.fecha.month, cita_desparacitacion.fecha.day,cita_desparacitacion.fecha.hour, cita_desparacitacion.fecha.minute,cita_desparacitacion.fecha.second)
        w = w - timedelta(hours=5)
        w_end = w + timedelta(hours=1)
        color = '#ff5d86'
        if cita_desparacitacion.estado_aplicacion != 'Pendiente':
            color = '#4BC241'
        data['dates'].append({'cliente': cita_desparacitacion.mascota.cliente.nombres,
                              'idCliente': cita_desparacitacion.mascota.cliente.id,
                              'nroTelefonico': cita_desparacitacion.mascota.cliente.telefono,
                              'direccion': cita_desparacitacion.mascota.cliente.direccion, 'tipoServicio': '4',
                              'codigoCita': cita_desparacitacion.id, 'tipoServicioDes': 'Servicio desparasitación',
                              'nombreMascota': cita_desparacitacion.mascota.nombre,
                              'codMascota': cita_desparacitacion.mascota.id,
                              'title': cita_desparacitacion.mascota.cliente.nombres + ' / ' + cita_desparacitacion.mascota.nombre,
                              'start': w.strftime("%Y-%m-%dT%H:%M:%S"), 'end': w_end.strftime("%Y-%m-%dT%H:%M:%S"),
                              'color': color, 'descripcion': cita_desparacitacion.descripcion,
                              'observaciones': cita_desparacitacion.observaciones})

    for guarderia in guarderias:
        g = datetime(guarderia.fecha_inicio.year, guarderia.fecha_inicio.month, guarderia.fecha_inicio.day,guarderia.fecha_inicio.hour, guarderia.fecha_inicio.minute,guarderia.fecha_inicio.second)
        g = g - timedelta(hours=5)
        t = datetime(guarderia.fecha_fin.year, guarderia.fecha_fin.month, guarderia.fecha_fin.day,guarderia.fecha_fin.hour, guarderia.fecha_fin.minute,guarderia.fecha_fin.second)
        t = t - timedelta(hours=5)
        datos_guarderia = DatosGuarderia.objects.filter(guarderia=guarderia)
        color = '#94919d'
        for datos in datos_guarderia:
            if datos.guarderia == guarderia:
                if datos.representante_retiro:
                    color = '#4BC241'
        #if guarderia.estado != 'Activa':
            #color = '#4BC241'
        data['dates'].append({'cliente': guarderia.mascota.cliente.nombres, 'idCliente': guarderia.mascota.cliente.id,
                              'nroTelefonico': guarderia.mascota.cliente.telefono,
                              'direccion': guarderia.mascota.cliente.direccion, 'tipoServicio': '5',
                              'codigoCita': guarderia.id, 'tipoServicioDes': 'Servicio guardería',
                              'codMascota': guarderia.mascota.id, 'nombreMascota': guarderia.mascota.nombre,
                              'title': guarderia.mascota.cliente.nombres + ' / ' + guarderia.mascota.nombre,
                              'start': g.strftime("%Y-%m-%dT%H:%M:%S"), 'end': t.strftime("%Y-%m-%dT%H:%M:%S"),
                              'color': color, 'observaciones': guarderia.observaciones})
    
    return JsonResponse(data)


class EditarCita(SuccessMessageMixin, UpdateView):
    model = Cita
    form_class = ModificarCitaForm
    template_name = "registrar_cita.html"
    success_message = "¡La cita se editó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarCita, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        cita = get_object_or_404(Cita, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(cita.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarCita, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        cita = get_object_or_404(Cita, pk=self.kwargs['pk'])
        context['mascota'] = cita.mascota
        return context


class EditarCitaAgenda(SuccessMessageMixin, UpdateView):
    model = Cita
    form_class = ModificarCitaForm
    template_name = "registrar_cita.html"
    success_url = reverse_lazy('listar_citas')
    success_message = "¡La cita se editó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarCitaAgenda, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarCitaAgenda, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        cita = get_object_or_404(Cita, pk=self.kwargs['pk'])
        context['mascota'] = cita.mascota
        return context


class CrearHistoriaClinica(SuccessMessageMixin, CreateView):
    model = HistoriaClinica
    form_class = CrearHistoriaClinicaForm
    template_name = "registrar_historia_clinica.html"
    success_message = "¡La Historia se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearHistoriaClinica, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        cita = get_object_or_404(Cita, pk=self.kwargs['id_cita'])
        return '/mascotas/detalle_mascota/' + str(cita.mascota.id)

    def get_form(self, form_class=None):
        cita = get_object_or_404(Cita, pk=self.kwargs['id_cita'])
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(id_mascota=cita.mascota.id, **self.get_form_kwargs())

    def form_valid(self, form):
        historia = form.instance

        cita = get_object_or_404(Cita, pk=self.kwargs['id_cita'])

        historia.cita = cita

        historia.mascota = get_object_or_404(Mascota, pk=cita.mascota.id)

        historia.creada_por = get_object_or_404(Usuario, id=self.request.user.id)

        if historia.peso != '':
            mascota = get_object_or_404(Mascota, pk=cita.mascota.id)
            mascota.peso = historia.peso
            mascota.save()

        mi_mascota = get_object_or_404(Mascota, pk=cita.mascota.id)

        if historia.senales != '':
            mi_mascota.senales = historia.senales

        if historia.estado_reproductivo != '':
            mi_mascota.estado_reproductivo = historia.estado_reproductivo

        if historia.numero_partos != '':
            mi_mascota.numero_partos = historia.numero_partos

        mi_mascota.save()

        cita.estado = 'Atendida'
        cita.save()

        historia = form.save()

        files = self.request.FILES.getlist('archivo_zip')

        for f in files:
            print(f.name)
            documento = Documento(file=f, historia=historia)
            documento.save()

        return super(CrearHistoriaClinica, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearHistoriaClinica, self).get_context_data(**kwargs)
        context['mascotas'] = True
        cita = get_object_or_404(Cita, pk=self.kwargs['id_cita'])
        context['cita'] = cita
        context['mascota'] = cita.mascota
        #context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return context


class ListarHistoriasClinicas(ListView):
    model = HistoriaClinica
    template_name = "listar_historias_clinicas.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarHistoriasClinicas, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(ListarHistoriasClinicas, self).get_queryset()

        queryset = queryset.filter(mascota_id=self.kwargs['id_mascota'])

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListarHistoriasClinicas, self).get_context_data(**kwargs)
        context['citas'] = True
        context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        context['mis_citas'] = Cita.objects.filter(mascota_id=self.kwargs['id_mascota'])
        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        historias = HistoriaClinica.objects.all().values('cita')
        citas = Cita.objects.filter(mascota=self.kwargs['id_mascota'],fecha__date__lte=datetime.now().date()).exclude(id__in=historias)

        if citas:
            context['agregar_historia'] = True
        else:
            context['agregar_historia'] = False

        return context


class AgregarOtraHistoria(SuccessMessageMixin, CreateView):
    model = HistoriaClinica
    form_class = AgregarOtraHistoriaForm
    template_name = "agregar_otra_historia.html"
    success_message = "¡La historia clínica se agregó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AgregarOtraHistoria, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        historia = get_object_or_404(HistoriaClinica, pk=self.kwargs['id_historia'])
        return '/mascotas/detalle_mascota/' + str(historia.mascota.id)

    def form_valid(self, form):
        historia = form.instance

        historia.motivo = 'Tratamiento'

        historia_anterior = get_object_or_404(HistoriaClinica, pk=self.kwargs['id_historia'])

        historia.mascota = historia_anterior.mascota
        historia.cita = historia_anterior.cita

        historia.creada_por = get_object_or_404(Usuario, id=self.request.user.id)

        historia = form.save()

        files = self.request.FILES.getlist('archivo_zip')

        for f in files:
            print(f.name)
            documento = Documento(file=f, historia=historia)
            documento.save()

        return super(AgregarOtraHistoria, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AgregarOtraHistoria, self).get_context_data(**kwargs)
        context['mascotas'] = True
        historia = get_object_or_404(HistoriaClinica, pk=self.kwargs['id_historia'])
        context['mascota'] = get_object_or_404(Mascota, pk=historia.mascota.id)
        return context


class EditarHistoria(SuccessMessageMixin, UpdateView):
    model = HistoriaClinica
    form_class = EditarHistoriaClinicaForm
    template_name = "editar_historia_clinica.html"
    success_message = "¡La historia clínica se modificó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarHistoria, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        historia = get_object_or_404(HistoriaClinica, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(historia.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarHistoria, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_historia = HistoriaClinica.objects.get(pk=self.kwargs['pk'])
        context['historia'] = mi_historia

        if mi_historia.frecuencia_cardiaca or mi_historia.frecuencia_respiratoria or mi_historia.temperatura or mi_historia.tiempo_llenado_capilar or mi_historia.pulso or mi_historia.peso or mi_historia.actitud or mi_historia.estado_corporal or mi_historia.comentarios_examen_clinico:
            context['examen_clinico'] = True
        else:
            context['examen_clinico'] = False

        if mi_historia.listado_problemas:
            context['listado_problemas'] = True
        else:
            context['listado_problemas'] = False

        if mi_historia.diagnostico_diferencial:
            context['diagnostico_diferencial'] = True
        else:
            context['diagnostico_diferencial'] = False

        if mi_historia.diagnostico_presuntivo:
            context['diagnostico_presuntivo'] = True
        else:
            context['diagnostico_presuntivo'] = False

        if mi_historia.examenes_laboratorio:
            context['examenes_laboratorio'] = True
        else:
            context['examenes_laboratorio'] = False

        if mi_historia.imagenes_diagnosticas:
            context['imagenes_diagnosticas'] = True
        else:
            context['imagenes_diagnosticas'] = False

        if mi_historia.diagnostico_definitivo:
            context['diagnostico_definitivo'] = True
        else:
            context['diagnostico_definitivo'] = False

        if mi_historia.observaciones_tratamiento:
            context['tratamiento'] = True
        else:
            context['tratamiento'] = False

        if mi_historia.pronostico:
            context['pronostico'] = True
        else:
            context['pronostico'] = False

        if mi_historia.notas:
            context['notas'] = True
        else:
            context['notas'] = False

        motivos_option = list(Tratamiento.objects.all().values_list('id', 'nombre', 'nombre', 'precio').order_by('nombre'))
        
        motivo_area = list(MotivoArea.objects.filter(historia_clinica_id= mi_historia.id).values_list('id', 'historia_clinica', 'area_consulta', 'estado'))
        array_areas_consulta = []

        """for motivo in motivos_option:
            for motivo_a in motivo_area:
                if motivo_a.area_consulta.id == motivo[0]:
                    motivo[2] = 'True'
                else:
                    motivo[2] = 'False'"""

        """for motivo in motivo_area:
            tratamiento_in = Tratamiento.objects.filter(pk=motivo.area_consulta_id).values_list('id', 'nombre', 'nombre', 'precio')
            for element in tratamiento_in:
                array_areas_consulta.append(element)"""

        print("motivos_option:")
        print(motivos_option)
        print("motivo_area:")
        print(motivo_area)

        historia_medicamento = HistoriaMedicamento.objects.filter(historia_clinica_id= mi_historia.id)
        array_medicamentos = []
        for medicamento in historia_medicamento:
            medicamento_in = Medicamento.objects.filter(pk=medicamento.medicamento_id).values_list('id', 'nombre', 'codigo', 'cantidad','unidad_medida','cantidad_unidad','cantidad_total_unidades', 'codigo_inventario','cantidad_por_unidad')
            for element in medicamento_in:
                array_elements = []
                array_elements.append(element[0])
                array_elements.append(element[1])
                array_elements.append(element[2])
                array_elements.append(element[3])
                array_elements.append(element[4])
                array_elements.append(element[5])
                array_elements.append(element[6])
                array_elements.append(element[7])
                array_elements.append(element[8])
                array_elements.append(medicamento.cantidad)
                array_elements.append(medicamento.unidad_medida)
                array_medicamentos.append(array_elements)
            
        if motivos_option:
            context['motivos_options'] = json.dumps(list(motivos_option), cls=DecimalEncoder)
        else:
            context['motivos_options'] = "null"
        if motivo_area:
            context['motivo_area'] = json.dumps(list(motivo_area), cls=DecimalEncoder)
        else:
            context['motivo_area'] = "null"

        if array_medicamentos:
            context['historia_medicamento'] =json.dumps(list(array_medicamentos), cls=DecimalEncoder)
        else:
            context['historia_medicamento'] = "null"
        
        if mi_historia.descripcion_inyectologia or motivo_area or historia_medicamento:
            context['area_consulta'] = True
        else:
            context['area_consulta'] = False
        medicamentos = json.dumps(list(Medicamento.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad','unidad_medida','cantidad_unidad','cantidad_total_unidades', 'codigo_inventario','cantidad_por_unidad')), cls=DecimalEncoder)
        context['medicamentos_options'] = medicamentos

        context['medicamentos_aplicados'] = historia_medicamento

        insumos_utilizados = InsumoHistoria.objects.filter(historia=mi_historia.id)

        context['insumos_utilizados'] = insumos_utilizados

        if insumos_utilizados:
            context['bool_insumos_utilizados'] = True
        else:
            context['bool_insumos_utilizados'] = False

        insumos_disponibles = json.dumps(list(Insumo.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad', 'unidad_medida','cantidad_unidad', 'cantidad_total_unidades','codigo_inventario', 'cantidad_por_unidad')),cls=DecimalEncoder)

        context['insumos_disponibles'] = insumos_disponibles
        print(insumos_disponibles)

        hospitalizaciones_historia = Hospitalizacion.objects.filter(historia_clinica=mi_historia)

        if hospitalizaciones_historia:
            context['hay_hospitalizaciones'] = True
        else:
            context['hay_hospitalizaciones'] = False

        cirugia_historia = Cirugia.objects.filter(historia_clinica=mi_historia)

        if cirugia_historia:
            context['cirugia_historia'] = True
        else:
            context['cirugia_historia'] = False

        return context

def actualizar_areas_consulta(request):
    req = json.loads(request.body.decode('utf-8'))
    new_area = req['element']
    historia = get_object_or_404(HistoriaClinica,pk=req['id_historia'])
    tratamiento_select = get_object_or_404(Tratamiento,pk=new_area[0])

    motivo_area = MotivoArea(area_consulta=tratamiento_select,historia_clinica=historia)
    motivo_area.save()

    data = {
            'documento_creado': True
        }
    return JsonResponse(data)

def remover_area_consulta(request):
    req = json.loads(request.body.decode('utf-8'))
    id_historia = req['id_historia']
    historia = get_object_or_404(HistoriaClinica, pk=req['id_historia'])
    areas_viejas = MotivoArea.objects.filter(historia_clinica_id=id_historia,estado='Activa')
    for area_vieja in areas_viejas:
        area_vieja.delete()

    for area in req['nuevas_areas']:
        if area['estado'] != 'Pagada':
            area_agregada = get_object_or_404(Tratamiento, pk=area['id'])
            nueva_area = MotivoArea(area_consulta=area_agregada, historia_clinica=historia)
            nueva_area.save()

    data = {
        'documento_creado': True
    }
    return JsonResponse(data)

def actualizar_medicamentos(request):
    req = json.loads(request.body.decode('utf-8'))
    Item = req['element']
    historia = get_object_or_404(HistoriaClinica,pk=req['id_historia'])
    medicamento_select = get_object_or_404(Medicamento,pk = Item[0])

    if Item[10] == True:
        medicamento_select.cantidad_total_unidades = int(medicamento_select.cantidad_total_unidades) - int(Item[11])
        medicamento_select.cantidad = math.floor( int(medicamento_select.cantidad_total_unidades) / int(medicamento_select.cantidad_unidad))
        historiatratamiento = HistoriaMedicamento(medicamento = medicamento_select, historia_clinica= historia,cantidad=Item[11],unidad_medida= "fraccion")
        print(medicamento_select,medicamento_select.cantidad,medicamento_select.cantidad_total_unidades,medicamento_select.cantidad_unidad)
        print(str(historiatratamiento))
        historiatratamiento.save()
        medicamento_select.save()
    else:
        medicamento_select.cantidad = int(medicamento_select.cantidad) - int(Item[11])
        medicamento_select.cantidad_total_unidades = int(medicamento_select.cantidad_total_unidades) - (int(Item[11]) * int(medicamento_select.cantidad_unidad))
        historiatratamiento = HistoriaMedicamento(medicamento = medicamento_select, historia_clinica= historia,cantidad=Item[11],unidad_medida= "unidad")
        print(medicamento_select,medicamento_select.cantidad,medicamento_select.cantidad_total_unidades,medicamento_select.cantidad_unidad)
        print(str(historiatratamiento))
        historiatratamiento.save()
        medicamento_select.save()

    data = {
            'documento_creado': True
        }
    return JsonResponse(data)

def eliminar_medicamentos(request):
    req = json.loads(request.body.decode('utf-8'))
    Item = req['element']
    historia = get_object_or_404(HistoriaClinica,pk=req['id_historia'])
    medicamento_select = get_object_or_404(Medicamento,pk = Item[0])

    if Item[10] == True:
        medicamento_select.cantidad_total_unidades = int(medicamento_select.cantidad_total_unidades) + int(Item[11])
        medicamento_select.cantidad = math.floor( int(medicamento_select.cantidad_total_unidades) / int(medicamento_select.cantidad_unidad))
        historiamedicamento = HistoriaMedicamento.objects.filter(medicamento = medicamento_select, historia_clinica= historia,cantidad=Item[11],unidad_medida= "fraccion")
        for historiamed  in historiamedicamento:
            historiamed.delete()
        medicamento_select.save()
    else:
        medicamento_select.cantidad = int(medicamento_select.cantidad) + int(Item[11])
        medicamento_select.cantidad_total_unidades = int(medicamento_select.cantidad_total_unidades) + (int(Item[11]) * int(medicamento_select.cantidad_unidad))
        historiamedicamento = HistoriaMedicamento.objects.filter(medicamento = medicamento_select, historia_clinica= historia,cantidad=Item[11],unidad_medida= "unidad")
        for historiamed  in historiamedicamento:
            historiamed.delete()
        medicamento_select.save()

    data = {
            'documento_creado': True
        }
    return JsonResponse(data)
    
@login_required
def EliminarDocumento(request,pk):
    documento = get_object_or_404(Documento, pk=pk)
    id_historia = documento.historia.id
    documento.delete()
    return redirect('/citas/editar_historia/'+str(id_historia))


class AgregarDocumentoHistoria(CreateView):
    model = Documento
    fields = ['file']
    template_name = "agregar_documento_historia.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AgregarDocumentoHistoria, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        historia = get_object_or_404(HistoriaClinica, pk=self.kwargs['id_historia'])
        return '/citas/editar_historia/' + str(historia.id)

    def form_valid(self,form):
        documento = form.instance
        historia = get_object_or_404(HistoriaClinica, pk=self.kwargs['id_historia'])
        documento.historia = historia
        self.object = form.save()
        return super(AgregarDocumentoHistoria, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AgregarDocumentoHistoria, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        return context


class DetalleImpresionHistoria(TemplateView):
    template_name = 'detalle_impresion_historia.html'

    def get_context_data(self, **kwargs):
        context = super(DetalleImpresionHistoria, self).get_context_data(**kwargs)
        mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])

        qs1 = Cita.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado='Atendida')
        qs2 = FacturaEstetica.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_de_atencion='Atendida')
        qs3 = Vacunacion.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_aplicacion='Aplicada')
        qs4 = Desparasitacion.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_aplicacion='Aplicada')
        qs5 = Guarderia.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado='Pagada')
        qs6 = Formula.objects.filter(mascota_id=self.kwargs['id_mascota'])
        qs7 = HistoriaClinica.objects.filter(mascota_id=self.kwargs['id_mascota'])
        qs8 = Hospitalizacion.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota']).order_by('fecha_hospitalizacion')
        qs9 = Cirugia.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota']).order_by('fecha')
        qs10 = SeguimientoHistoria.objects.filter(mascota_id=self.kwargs['id_mascota'])

        formulas_id = []

        for q in qs5:
            q.fecha = q.fecha_inicio

        for q in qs3:
            q.tipo = 'Vacunacion'

        for q in qs4:
            q.tipo = 'Desparasitacion'

        for q in qs6:
            q.formula = 'Formula'
            formulas_id.append(q.pk)

        for q in qs7:
            q.historia = 'Historia'

        for q in qs8:
            q.hospitalizacion = 'Hospitalizacion'
            q.fecha = q.fecha.replace(year=q.fecha_hospitalizacion.year, month=q.fecha_hospitalizacion.month,day=q.fecha_hospitalizacion.day,hour=23,minute=59,second=30)

        for q in qs9:
            q.tipo = 'Cirugia'

        for q in qs10:
            q.seguimiento = 'Seguimiento'

        historial_mascota = sorted(chain(qs1, qs2, qs3, qs4, qs5, qs6, qs7, qs8, qs9, qs10), key=lambda instance: instance.fecha,
                                   reverse=True)

        procedimientos_mascota = MotivoArea.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['historial_mascota'] = historial_mascota
        context['procedimientos_mascota'] = procedimientos_mascota

        procedimientos_id_historias = []

        for procedimiento in procedimientos_mascota:
            procedimientos_id_historias.append(procedimiento.historia_clinica.id)

        context[
            'procedimientos_id_historias'] = procedimientos_id_historias

        medicamentos_aplicados_hospitalizaciones = MedicamentoHospitalizacion.objects.filter(
            hospitalizacion__historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['medicamentos_aplicados_hospitalizaciones'] = medicamentos_aplicados_hospitalizaciones

        medicamentos_aplicados_hospitalizaciones_id_historias = []

        for medicamento_aplicado in medicamentos_aplicados_hospitalizaciones:
            medicamentos_aplicados_hospitalizaciones_id_historias.append(medicamento_aplicado.hospitalizacion.id)

        context[
            'medicamentos_aplicados_hospitalizaciones_id_historias'] = medicamentos_aplicados_hospitalizaciones_id_historias

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        context['mi_mascota'] = mascota

        context['fecha_hoy'] = datetime.now().date()

        context['formulas_id'] = formulas_id

        context['vacunaciones_aplicadas'] = AplicacionVacunacion.objects.filter(
            vacunacion__mascota__id=self.kwargs['id_mascota'])
        context['desparasitaciones_aplicadas'] = AplicacionDesparasitacion.objects.filter(
            desparasitacion__mascota__id=self.kwargs['id_mascota'])

        ########################################### MEDICAMENTOS APLICADOS CIRUGIA #################################

        medicamentos_aplicados_cirugias = MedicamentoCirugia.objects.filter(
            cirugia__historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['medicamentos_aplicados_cirugias'] = medicamentos_aplicados_cirugias

        medicamentos_aplicados_cirugias_id_historias = []

        for medicamento_aplicado in medicamentos_aplicados_cirugias:
            medicamentos_aplicados_cirugias_id_historias.append(medicamento_aplicado.cirugia.id)

        context[
            'medicamentos_aplicados_cirugias_id_historias'] = medicamentos_aplicados_cirugias_id_historias

        ########################################## INYECTOLOGIA ###################################################

        inyectologias = HistoriaMedicamento.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['inyectologias'] = inyectologias

        inyectologias_id_historias = []

        for inyectologia in inyectologias:
            inyectologias_id_historias.append(inyectologia.historia_clinica.id)

        context[
            'inyectologias_id_historias'] = inyectologias_id_historias

        ###########################################################################################################

        return context


class DetalleHistoriaClinica(DetailView):
    model = HistoriaClinica
    template_name = "detalle_historia_clinica.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleHistoriaClinica, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleHistoriaClinica, self).get_context_data(**kwargs)
        context['mascotas'] = True

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        mi_historia = HistoriaClinica.objects.get(pk=self.kwargs['pk'])

        context['mi_historia'] = mi_historia

        archivos_adjuntos_motivo = Documento.objects.filter(historia__id=self.kwargs['pk'],area_historia='Motivo consulta')

        context['archivos_adjuntos_motivo'] = archivos_adjuntos_motivo

        archivos_adjuntos_examen_clinico = Documento.objects.filter(historia__id=self.kwargs['pk'],
                                                            area_historia='Examen clinico')

        context['archivos_adjuntos_examen_clinico'] = archivos_adjuntos_examen_clinico

        archivos_adjuntos_diagnostico_diferencial = Documento.objects.filter(historia__id=self.kwargs['pk'],
                                                            area_historia='Diagnostico diferencial')

        context['archivos_adjuntos_diagnostico_diferencial'] = archivos_adjuntos_diagnostico_diferencial

        archivos_adjuntos_diagnostico_presuntivo = Documento.objects.filter(historia__id=self.kwargs['pk'],
                                                            area_historia='Diagnostico presuntivo')

        context['archivos_adjuntos_diagnostico_presuntivo'] = archivos_adjuntos_diagnostico_presuntivo

        archivos_adjuntos_examenes_laboratorio = Documento.objects.filter(historia__id=self.kwargs['pk'], area_historia='Examenes laboratorio')

        context['archivos_adjuntos_examenes_laboratorio'] = archivos_adjuntos_examenes_laboratorio

        archivos_adjuntos_imagenes_diagnosticas = Documento.objects.filter(historia__id=self.kwargs['pk'],
                                                                            area_historia='Imagenes diagnosticas')

        context['archivos_adjuntos_imagenes_diagnosticas'] = archivos_adjuntos_imagenes_diagnosticas

        if mi_historia.frecuencia_cardiaca or mi_historia.frecuencia_respiratoria or mi_historia.temperatura or mi_historia.tiempo_llenado_capilar or mi_historia.pulso or mi_historia.peso or mi_historia.actitud or mi_historia.estado_corporal or mi_historia.comentarios_examen_clinico:
            context['examen_clinico'] = True
        else:
            context['examen_clinico'] = False
        
        motivo_area = MotivoArea.objects.filter(historia_clinica_id= mi_historia.id)
        array_areas_consulta = []
        for motivo in motivo_area:
            tratamiento_in = Tratamiento.objects.filter(pk=motivo.area_consulta_id).values_list('id', 'nombre', 'descripcion', 'precio')
            for element in tratamiento_in:
                array_areas_consulta.append(element)
        array_replace = []
        for place in array_areas_consulta:
            array_replace.append(place[1])

        

        
        context['motivo_area'] = array_replace


        historia_medicamento = HistoriaMedicamento.objects.filter(historia_clinica_id= mi_historia.id)
        array_medicamentos = []
        for medicamento in historia_medicamento:
            medicamento_in = Medicamento.objects.filter(pk=medicamento.medicamento_id).values_list('id', 'nombre', 'codigo', 'cantidad','unidad_medida','cantidad_unidad','cantidad_total_unidades', 'codigo_inventario','cantidad_por_unidad')
            for element in medicamento_in:
                array_elements = []
                array_elements.append(element[0])
                array_elements.append(element[1])
                array_elements.append(element[2])
                array_elements.append(element[3])
                array_elements.append(element[4])
                array_elements.append(element[5])
                array_elements.append(element[6])
                array_elements.append(element[7])
                array_elements.append(element[8])
                array_elements.append(medicamento.cantidad)
                array_elements.append(medicamento.unidad_medida)
                array_medicamentos.append(array_elements)
        context['historia_medicamento'] =json.dumps(list(array_medicamentos), cls=DecimalEncoder)

        if len(array_medicamentos) > 0:
            context['show_table'] = True
        else:
            context['show_table'] = False

        if len(array_medicamentos) > 0 or len(array_replace) > 0 or mi_historia.descripcion_inyectologia:
            context["show_procedures"] = True
        else:
            context["show_procedures"] = False

        if len(array_medicamentos) > 0:
            context['ver_intectologia'] = True
        else:
            context['ver_intectologia'] = False

        if len(motivo_area) > 0 :
            context['ver_area_consulta'] = True
        else:
            context['ver_area_consulta'] = False
        
        #if array_areas_consulta:
            #context['motivo_area'] = json.dumps(list(array_areas_consulta), cls=DecimalEncoder)
            #context['motivo_area'] =  
        #else:
            #context['motivo_area'] = "null"

        insumos_historia = InsumoHistoria.objects.filter(historia=mi_historia)

        context['insumos_historia'] = insumos_historia

        return context


def eliminar_cita(request):
    id_cita = request.GET.get('id', '')
    mi_cita = get_object_or_404(Cita, pk=id_cita)
    id_mascota = mi_cita.mascota.id
    if mi_cita.estado != 'Atendida':
        mi_cita.delete()
    data = {
        'eliminacion': True,
        'id_mascota': id_mascota
    }
    return JsonResponse(data)


def ajax_crear_documento(request):
    if request.method == "POST":
        file = request.FILES.get('img')  # FILES instead of POST
        
        print("Archivooooo: ")
        print(file)

        if request.POST.get('id_historia'):
            mi_historia = get_object_or_404(HistoriaClinica, pk=request.POST.get('id_historia'))
            nuevo_documento = Documento(file=file, historia=mi_historia)
        else:
            nuevo_documento = Documento(file=file)

        print("tipoooo:")
        print(request.POST.get('tipo'))

        if request.POST.get('tipo') == 'examen_clinico':
            nuevo_documento.area_historia = 'Examen clinico'

        if request.POST.get('tipo') == 'diagnostico_diferencial':
            nuevo_documento.area_historia = 'Diagnostico diferencial'

        if request.POST.get('tipo') == 'diagnostico_presuntivo':
            nuevo_documento.area_historia = 'Diagnostico presuntivo'

        if request.POST.get('tipo') == 'examenes_laboratorio':
            nuevo_documento.area_historia = 'Examenes laboratorio'

        if request.POST.get('tipo') == 'imagenes_diagnosticas':
            nuevo_documento.area_historia = 'Imagenes diagnosticas'

        nuevo_documento.save()

        print("Documento id:")
        print(nuevo_documento.id)

        print("Documento url:")
        print(nuevo_documento.file)

        data = {
            'documento_creado': True,
            'id_documento': nuevo_documento.id,
            'nuevo_documento_file_url': str(nuevo_documento.file)
        }
        return JsonResponse(data)


def eliminar_documento_historia(request):
    id_documento = request.GET.get('id', '')
    mi_documento = get_object_or_404(Documento, pk=id_documento)
    mi_documento.delete()
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


@login_required
def registrarHistoria(request,id_cita):

    if request.method == 'POST':

        req = json.loads(request.body.decode('utf-8'))
        # print(req)
        print("fecha: " + req['fecha'])
        #print("area_consulta: " + req['area_consulta'])

        print("Prueba POST registro de historia")

        cita = get_object_or_404(Cita, pk=id_cita)
        id_macota = cita.mascota.id
        mi_mascota = get_object_or_404(Mascota, pk=id_macota)
        creada_por = get_object_or_404(Usuario, id=request.user.id)
        mi_fecha = datetime.strptime(str(req['fecha']), "%d/%m/%Y %H:%M")

        array_areas_consulta = req['area_consulta_selected']

        if req['area_consulta'] != '':
            motivo_cita = get_object_or_404(MotivoCita, pk= req['motivo_cita'])
            print("Frecuencia cardiaca:")
            print(req['frecuencia_cardiaca'])
            print(req['comentarios_examen_clinico'])
            print(req['bool_hospitalizacion'])
            print(req)

            historia = HistoriaClinica(mascota=mi_mascota,
                                       cita=cita, fecha=mi_fecha,
                                       creada_por=creada_por,
                                       motivo_cita=motivo_cita,
                                       descripcion=req['estado_mascota'],
                                       senales=req['senales'],
                                       estado_reproductivo=req['estado_reproductivo'],
                                       numero_partos=req['numero_partos'],
                                       tipo_alimento=req['tipo_alimento'],
                                       racion_alimento=req['racion'],
                                       vacunacion=req['vacunacion'],
                                       desparasitacion=req['desparasitacion'],
                                       heces=req['heces'],
                                       orina=req['orina'],
                                       frecuencia_cardiaca=req['frecuencia_cardiaca'],
                                       frecuencia_respiratoria=req['frecuencia_respiratoria'],
                                       temperatura=req['temperatura'],
                                       tiempo_llenado_capilar=req['tiempo_llenado_capilar'],
                                       pulso=req['pulso'],
                                       peso=req['peso'],
                                       actitud=req['actitud'],
                                       estado_corporal=req['estado_corporal'],
                                       comentarios_examen_clinico=req['comentarios_examen_clinico'],
                                       sistema_linfatico=req['sistema_linfatico'],
                                       sistema_tegumentario=req['sistema_tegumentario'],
                                       sistema_cardiovascular=req['sistema_cardiovascular'],
                                       sistema_respiratorio=req['sistema_respiratorio'],
                                       sistema_digestivo=req['sistema_digestivo'],
                                       sistema_musculoesqueletico=req['sistema_musculoesqueletico'],
                                       sistema_nervioso=req['sistema_nervioso'],
                                       sistema_urinario=req['sistema_urinario'],
                                       sistema_reproductivo=req['sistema_reproductivo'],
                                       listado_problemas=req['listado_problemas'],
                                       diagnostico_diferencial=req['diagnostico_diferencial'],
                                       diagnostico_presuntivo=req['diagnostico_presuntivo'],
                                       examenes_laboratorio=req['examenes_laboratorio'],
                                       imagenes_diagnosticas=req['imagenes_diagnosticas'],
                                       diagnostico_definitivo=req['diagnostico_definitivo'],
                                       observaciones_tratamiento=req['observaciones_tratamiento'],
                                       pronostico=req['pronostico'],
                                       notas=req['notas'],
                                       descripcion_procedimiento=req['descripcion_procedimiento']
                                       )

            if req['descripcion_inyectologia'] != "": 
                historia.descripcion_inyectologia = req['descripcion_inyectologia']
        
                
            historia.save()

            if req['bool_hospitalizacion'] == True:
                hospitalizacion = Hospitalizacion(
                    historia_clinica=historia,
                    fecha_hospitalizacion=datetime.strptime(str(req['fecha_hospitalizacion']), "%d/%m/%Y"),
                    hora_manana=req['hora_mañana'],
                    temperatura_manana=req['temperatura_mañana'],
                    frecuencia_manana=req['frecuencia_cardiaca_mañana'],
                    mucosas_manana=req['mucosas_mañana'],
                    apetito_manana=req['apetito_mañana'],
                    sed_manana=req['sed_mañana'],
                    estado_animo_manana=req['estado_animo_mañana'],
                    consistencia_fecal_manana=req['consistencia_fecal_mañana'],
                    vomito_manana=req['vomito_mañana'],
                    orina_manana=req['orina_mañana'],
                    pronostico_manana=req['pronostico_mañana'],
                    hora_tarde=req['hora_tarde'],
                    temperatura_tarde=req['temperatura_tarde'],
                    frecuencia_tarde=req['frecuencia_cardiaca_tarde'],
                    mucosas_tarde=req['mucosas_tarde'],
                    apetito_tarde=req['apetito_tarde'],
                    sed_tarde=req['sed_tarde'],
                    estado_animo_tarde=req['estado_animo_tarde'],
                    consistencia_fecal_tarde=req['consistencia_fecal_tarde'],
                    vomito_tarde=req['vomito_tarde'],
                    orina_tarde=req['orina_tarde'],
                    pronostico_tarde=req['pronostico_tarde'],
                    observaciones_manana=req['observaciones_mañana'],
                    observaciones_tarde=req['observaciones_tarde'],
                )
                hospitalizacion.save()
                mi_mascota.estado_hospitalizacion = True
                mi_mascota.save()

                hora_actual = datetime.now().time().strftime("%I:%M%p")

                for aplicacion_medicamento in req['medicamentos_seleccionados_hospitalizacion']:
                    medicamento = get_object_or_404(Medicamento, pk=aplicacion_medicamento['id_medicamento'])
                    tipo_unidad_medida = 'unidad'
                    mi_cantidad = str(aplicacion_medicamento['cantidad_requerida'])
                    unidad = str(aplicacion_medicamento['unidades'])
                    if unidad != 'unidad(es)':
                        tipo_unidad_medida = 'granel'
                    medicamento_hospitalizacion = MedicamentoHospitalizacion(hospitalizacion=hospitalizacion,
                                                                             medicamento=medicamento,
                                                                             responsable=creada_por,
                                                                             cantidad_requerida=mi_cantidad,
                                                                             unidad_medida=medicamento.unidad_medida,
                                                                             tipo_unidad_medida=tipo_unidad_medida,
                                                                             hora=hora_actual)
                    medicamento_hospitalizacion.save()

                    cantidad = float(mi_cantidad)

                    if tipo_unidad_medida == 'unidad':
                        medicamento.cantidad = int(medicamento.cantidad) - cantidad
                        medicamento.cantidad_total_unidades = int(medicamento.cantidad_total_unidades) - (
                                    cantidad * int(medicamento.cantidad_unidad))
                        medicamento.save()
                    elif tipo_unidad_medida == 'granel':
                        medicamento.cantidad_total_unidades = int(medicamento.cantidad_total_unidades) - cantidad
                        medicamento.cantidad = math.floor(
                            int(medicamento.cantidad_total_unidades) / int(medicamento.cantidad_unidad))
                        medicamento.save()

            for element in array_areas_consulta:
                tratamiento_select = get_object_or_404(Tratamiento,pk = element['id'])
                motivo_area = MotivoArea(area_consulta=tratamiento_select,historia_clinica=historia)
                motivo_area.save()

            if req['bool_formato_inyectologia'] == True:
                for medicamento in req['medicamentos_seleccionados_inyectologia']:
                    medicamento_select = get_object_or_404(Medicamento,pk = medicamento['id_medicamento'])
                    cantidad = str(medicamento['cantidad_requerida'])
                    unidad = str(medicamento['unidades'])
                    if unidad != 'unidad(es)':
                        medicamento_select.cantidad_total_unidades = int(medicamento_select.cantidad_total_unidades) - int(cantidad)
                        medicamento_select.cantidad = math.floor( int(medicamento_select.cantidad_total_unidades) / int(medicamento_select.cantidad_unidad))
                        historiatratamiento = HistoriaMedicamento(medicamento = medicamento_select, historia_clinica= historia,cantidad=cantidad,unidad_medida= "fraccion")
                        print(medicamento_select,medicamento_select.cantidad,medicamento_select.cantidad_total_unidades,medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()
                    else:
                        
                        medicamento_select.cantidad = int(medicamento_select.cantidad) - int(cantidad)
                        medicamento_select.cantidad_total_unidades = int(medicamento_select.cantidad_total_unidades) - (int(cantidad) * int(medicamento_select.cantidad_unidad))
                        historiatratamiento = HistoriaMedicamento(medicamento = medicamento_select, historia_clinica= historia,cantidad=cantidad,unidad_medida= "unidad")
                        print(medicamento_select,medicamento_select.cantidad,medicamento_select.cantidad_total_unidades,medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()

            if req['bool_formato_cirugia'] == True:
                cirugia = Cirugia(historia_clinica=historia, procedimiento_quirurgico=req['procedimiento_quirurgico'],
                                  abordaje=req['abordaje'], diagnostico_quirurgico=req['diagnostico_quirurgico'])
                cirugia.save()

                for medicamento in req['medicamentos_seleccionados_cirugia']:
                    medicamento_select = get_object_or_404(Medicamento, pk=medicamento['id_medicamento'])
                    cantidad = str(medicamento['cantidad_requerida'])
                    unidad = str(medicamento['unidades'])
                    if unidad != 'unidad(es)':
                        medicamento_select.cantidad_total_unidades = float(
                            medicamento_select.cantidad_total_unidades) - float(cantidad)
                        medicamento_select.cantidad = math.floor(
                            float(medicamento_select.cantidad_total_unidades) / float(
                                medicamento_select.cantidad_unidad))
                        historiatratamiento = MedicamentoCirugia(medicamento=medicamento_select,cirugia=cirugia, cantidad_requerida=cantidad,unidad_medida="fraccion")
                        print(medicamento_select, medicamento_select.cantidad,
                              medicamento_select.cantidad_total_unidades, medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()
                    else:

                        medicamento_select.cantidad = int(medicamento_select.cantidad) - int(cantidad)
                        medicamento_select.cantidad_total_unidades = float(
                            medicamento_select.cantidad_total_unidades) - (float(cantidad) * float(
                            medicamento_select.cantidad_unidad))
                        historiatratamiento = MedicamentoCirugia(medicamento=medicamento_select,cirugia=cirugia, cantidad_requerida=cantidad,unidad_medida="unidad")
                        print(medicamento_select, medicamento_select.cantidad,
                              medicamento_select.cantidad_total_unidades, medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()

            if req['bool_insumos'] == True:
                for insumo in req['insumos_seleccionados']:
                    insumo_select = get_object_or_404(Insumo, pk=insumo['id_insumo'])
                    cantidad = str(insumo['cantidad_requerida'])
                    unidad = str(insumo['unidades'])

                    if unidad != 'unidad(es)':
                        insumo_select.cantidad_total_unidades = float(
                            insumo_select.cantidad_total_unidades) - float(cantidad)
                        insumo_select.cantidad = math.floor(
                            float(insumo_select.cantidad_total_unidades) / float(
                                insumo_select.cantidad_unidad))
                        historiatratamiento = InsumoHistoria(insumo=insumo_select,historia=historia, cantidad_requerida=cantidad,unidad_medida="fraccion")
                        print(insumo_select, insumo_select.cantidad,
                              insumo_select.cantidad_total_unidades, insumo_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        insumo_select.save()
                    else:

                        insumo_select.cantidad = int(insumo_select.cantidad) - int(cantidad)
                        insumo_select.cantidad_total_unidades = float(
                            insumo_select.cantidad_total_unidades) - (float(cantidad) * float(
                            insumo_select.cantidad_unidad))
                        historiatratamiento = InsumoHistoria(insumo=insumo_select,historia=historia, cantidad_requerida=cantidad,unidad_medida="unidad")
                        print(insumo_select, insumo_select.cantidad,
                              insumo_select.cantidad_total_unidades, insumo_select.cantidad_unidad)
                        print(cantidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        insumo_select.save()

        else:
            motivo_cita = get_object_or_404(MotivoCita, pk= req['motivo_cita'])
            historia = HistoriaClinica(mascota=mi_mascota,
                                       cita=cita, fecha=mi_fecha,
                                       creada_por=creada_por,
                                       descripcion=req['estado_mascota'],
                                       senales=req['senales'],
                                       motivo_cita=motivo_cita,
                                       estado_reproductivo=req['estado_reproductivo'],
                                       numero_partos=req['numero_partos'],
                                       tipo_alimento=req['tipo_alimento'],
                                       racion_alimento=req['racion'],
                                       vacunacion=req['vacunacion'],
                                       desparasitacion=req['desparasitacion'],
                                       heces=req['heces'],
                                       orina=req['orina'],
                                       frecuencia_cardiaca=req['frecuencia_cardiaca'],
                                       frecuencia_respiratoria=req['frecuencia_respiratoria'],
                                       temperatura=req['temperatura'],
                                       tiempo_llenado_capilar=req['tiempo_llenado_capilar'],
                                       pulso=req['pulso'],
                                       peso=req['peso'],
                                       actitud=req['actitud'],
                                       estado_corporal=req['estado_corporal'],
                                       comentarios_examen_clinico=req['comentarios_examen_clinico'],
                                       sistema_linfatico=req['sistema_linfatico'],
                                       sistema_tegumentario=req['sistema_tegumentario'],
                                       sistema_cardiovascular=req['sistema_cardiovascular'],
                                       sistema_respiratorio=req['sistema_respiratorio'],
                                       sistema_digestivo=req['sistema_digestivo'],
                                       sistema_musculoesqueletico=req['sistema_musculoesqueletico'],
                                       sistema_nervioso=req['sistema_nervioso'],
                                       sistema_urinario=req['sistema_urinario'],
                                       sistema_reproductivo=req['sistema_reproductivo'],
                                       listado_problemas=req['listado_problemas'],
                                       diagnostico_diferencial=req['diagnostico_diferencial'],
                                       diagnostico_presuntivo=req['diagnostico_presuntivo'],
                                       examenes_laboratorio=req['examenes_laboratorio'],
                                       imagenes_diagnosticas=req['imagenes_diagnosticas'],
                                       diagnostico_definitivo=req['diagnostico_definitivo'],
                                       observaciones_tratamiento=req['observaciones_tratamiento'],
                                       pronostico=req['pronostico'],
                                       notas=req['notas'],
                                       descripcion_procedimiento=req['descripcion_procedimiento']
                                       )
            if req['descripcion_inyectologia'] != "": 
                historia.descripcion_inyectologia = req['descripcion_inyectologia']

            historia.save()

            if req['bool_hospitalizacion'] == True:
                hospitalizacion = Hospitalizacion(
                    historia_clinica=historia,
                    fecha_hospitalizacion=datetime.strptime(str(req['fecha_hospitalizacion']), "%d/%m/%Y"),
                    hora_manana=req['hora_mañana'],
                    temperatura_manana=req['temperatura_mañana'],
                    frecuencia_manana=req['frecuencia_cardiaca_mañana'],
                    mucosas_manana=req['mucosas_mañana'],
                    apetito_manana=req['apetito_mañana'],
                    sed_manana=req['sed_mañana'],
                    estado_animo_manana=req['estado_animo_mañana'],
                    consistencia_fecal_manana=req['consistencia_fecal_mañana'],
                    vomito_manana=req['vomito_mañana'],
                    orina_manana=req['orina_mañana'],
                    pronostico_manana=req['pronostico_mañana'],
                    hora_tarde=req['hora_tarde'],
                    temperatura_tarde=req['temperatura_tarde'],
                    frecuencia_tarde=req['frecuencia_cardiaca_tarde'],
                    mucosas_tarde=req['mucosas_tarde'],
                    apetito_tarde=req['apetito_tarde'],
                    sed_tarde=req['sed_tarde'],
                    estado_animo_tarde=req['estado_animo_tarde'],
                    consistencia_fecal_tarde=req['consistencia_fecal_tarde'],
                    vomito_tarde=req['vomito_tarde'],
                    orina_tarde=req['orina_tarde'],
                    pronostico_tarde=req['pronostico_tarde'],
                    observaciones_manana=req['observaciones_mañana'],
                    observaciones_tarde=req['observaciones_tarde'],
                )
                hospitalizacion.save()
                mi_mascota.estado_hospitalizacion = True
                mi_mascota.save()

                hora_actual = datetime.now().time().strftime("%I:%M%p")

                for aplicacion_medicamento in req['medicamentos_seleccionados_hospitalizacion']:
                    medicamento = get_object_or_404(Medicamento, pk=aplicacion_medicamento['id_medicamento'])
                    tipo_unidad_medida = 'unidad'
                    mi_cantidad = str(aplicacion_medicamento['cantidad_requerida'])
                    unidad = str(aplicacion_medicamento['unidades'])
                    if unidad != 'unidad(es)':
                        tipo_unidad_medida = 'granel'
                    medicamento_hospitalizacion = MedicamentoHospitalizacion(hospitalizacion=hospitalizacion,
                                                                             medicamento=medicamento,
                                                                             responsable=creada_por,
                                                                             cantidad_requerida=mi_cantidad,
                                                                             unidad_medida=medicamento.unidad_medida,
                                                                             tipo_unidad_medida=tipo_unidad_medida,
                                                                             hora=hora_actual)
                    medicamento_hospitalizacion.save()

                    cantidad = float(mi_cantidad)

                    if tipo_unidad_medida == 'unidad':
                        medicamento.cantidad = int(medicamento.cantidad) - cantidad
                        medicamento.cantidad_total_unidades = int(medicamento.cantidad_total_unidades) - (
                                    cantidad * int(medicamento.cantidad_unidad))
                        medicamento.save()
                    elif tipo_unidad_medida == 'granel':
                        medicamento.cantidad_total_unidades = int(medicamento.cantidad_total_unidades) - cantidad
                        medicamento.cantidad = math.floor(
                            int(medicamento.cantidad_total_unidades) / int(medicamento.cantidad_unidad))
                        medicamento.save()

            for element in array_areas_consulta:
                tratamiento_select = get_object_or_404(Tratamiento,pk = element['id'])
                motivo_area = MotivoArea(area_consulta=tratamiento_select,historia_clinica=historia)
                motivo_area.save()

            if req['bool_formato_inyectologia'] == True:
                for medicamento in req['medicamentos_seleccionados_inyectologia']:
                    medicamento_select = get_object_or_404(Medicamento, pk=medicamento['id_medicamento'])
                    cantidad = str(medicamento['cantidad_requerida'])
                    unidad = str(medicamento['unidades'])
                    if unidad != 'unidad(es)':
                        medicamento_select.cantidad_total_unidades = float(medicamento_select.cantidad_total_unidades) - float(cantidad)
                        medicamento_select.cantidad = math.floor( float(medicamento_select.cantidad_total_unidades) / float(medicamento_select.cantidad_unidad))
                        historiatratamiento = HistoriaMedicamento(medicamento = medicamento_select, historia_clinica= historia,cantidad=cantidad,unidad_medida= "fraccion")
                        print(medicamento_select,medicamento_select.cantidad,medicamento_select.cantidad_total_unidades,medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()
                    else:

                        medicamento_select.cantidad = int(medicamento_select.cantidad) - int(cantidad)
                        medicamento_select.cantidad_total_unidades = float(medicamento_select.cantidad_total_unidades) - (float(cantidad) * float(medicamento_select.cantidad_unidad))
                        historiatratamiento = HistoriaMedicamento(medicamento = medicamento_select, historia_clinica= historia,cantidad=cantidad,unidad_medida= "unidad")
                        print(medicamento_select,medicamento_select.cantidad,medicamento_select.cantidad_total_unidades,medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()     

            if req['bool_formato_cirugia'] == True:
                cirugia = Cirugia(historia_clinica=historia,procedimiento_quirurgico=req['procedimiento_quirurgico'],abordaje=req['abordaje'],diagnostico_quirurgico=req['diagnostico_quirurgico'])
                cirugia.save()

                for medicamento in req['medicamentos_seleccionados_cirugia']:
                    medicamento_select = get_object_or_404(Medicamento, pk=medicamento['id_medicamento'])
                    cantidad = str(medicamento['cantidad_requerida'])
                    unidad = str(medicamento['unidades'])
                    if unidad != 'unidad(es)':
                        medicamento_select.cantidad_total_unidades = float(
                            medicamento_select.cantidad_total_unidades) - float(cantidad)
                        medicamento_select.cantidad = math.floor(
                            float(medicamento_select.cantidad_total_unidades) / float(
                                medicamento_select.cantidad_unidad))
                        historiatratamiento = MedicamentoCirugia(medicamento=medicamento_select,cirugia=cirugia, cantidad_requerida=cantidad,unidad_medida="fraccion")
                        print(medicamento_select, medicamento_select.cantidad,
                              medicamento_select.cantidad_total_unidades, medicamento_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()
                    else:

                        medicamento_select.cantidad = int(medicamento_select.cantidad) - int(cantidad)
                        medicamento_select.cantidad_total_unidades = float(
                            medicamento_select.cantidad_total_unidades) - (float(cantidad) * float(
                            medicamento_select.cantidad_unidad))
                        historiatratamiento = MedicamentoCirugia(medicamento=medicamento_select,cirugia=cirugia, cantidad_requerida=cantidad,unidad_medida="unidad")
                        print(medicamento_select, medicamento_select.cantidad,
                              medicamento_select.cantidad_total_unidades, medicamento_select.cantidad_unidad)
                        print(cantidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        medicamento_select.save()

            if req['bool_insumos'] == True:
                for insumo in req['insumos_seleccionados']:
                    insumo_select = get_object_or_404(Insumo, pk=insumo['id_insumo'])
                    cantidad = str(insumo['cantidad_requerida'])
                    unidad = str(insumo['unidades'])

                    if unidad != 'unidad(es)':
                        insumo_select.cantidad_total_unidades = float(
                            insumo_select.cantidad_total_unidades) - float(cantidad)
                        insumo_select.cantidad = math.floor(
                            float(insumo_select.cantidad_total_unidades) / float(
                                insumo_select.cantidad_unidad))
                        historiatratamiento = InsumoHistoria(insumo=insumo_select,historia=historia, cantidad_requerida=cantidad,unidad_medida="fraccion")
                        print(insumo_select, insumo_select.cantidad,
                              insumo_select.cantidad_total_unidades, insumo_select.cantidad_unidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        insumo_select.save()
                    else:

                        insumo_select.cantidad = int(insumo_select.cantidad) - int(cantidad)
                        insumo_select.cantidad_total_unidades = float(
                            insumo_select.cantidad_total_unidades) - (float(cantidad) * float(
                            insumo_select.cantidad_unidad))
                        historiatratamiento = InsumoHistoria(insumo=insumo_select,historia=historia, cantidad_requerida=cantidad,unidad_medida="unidad")
                        print(insumo_select, insumo_select.cantidad,
                              insumo_select.cantidad_total_unidades, insumo_select.cantidad_unidad)
                        print(cantidad)
                        print(str(historiatratamiento))
                        historiatratamiento.save()
                        insumo_select.save()

        if historia.peso != '':
            mascota = get_object_or_404(Mascota, pk=cita.mascota.id)
            mascota.peso = historia.peso
            mascota.save()

        mi_mascota = get_object_or_404(Mascota, pk=cita.mascota.id)

        if historia.senales != '':
            mi_mascota.senales = historia.senales

        if historia.estado_reproductivo != '':
            mi_mascota.estado_reproductivo = historia.estado_reproductivo

        if historia.numero_partos != '':
            mi_mascota.numero_partos = historia.numero_partos

        mi_mascota.save()

        cita.estado = 'Atendida'
        cita.save()

        for f in req['archivos_motivo_consulta']:
            documento = get_object_or_404(Documento, pk=f['id'])
            documento.historia = historia
            documento.save()

        if req['bool_examen_clinico']:
            for f in req['archivos_examen_clinico']:
                documento = get_object_or_404(Documento, pk=f['id'])
                documento.historia = historia
                documento.save()

        if req['bool_diagnostico_diferencial']:
            for f in req['archivos_diagnostico_diferencial']:
                documento = get_object_or_404(Documento, pk=f['id'])
                documento.historia = historia
                documento.save()

        if req['bool_diagnostico_presuntivo']:
            for f in req['archivos_diagnistico_presuntivo']:
                documento = get_object_or_404(Documento, pk=f['id'])
                documento.historia = historia
                documento.save()

        if req['bool_examenes_laboratorio']:
            for f in req['archivos_examenes_laboratorio']:
                documento = get_object_or_404(Documento, pk=f['id'])
                documento.historia = historia
                documento.save()

        if req['bool_imagenes_diagnosticas']:
            for f in req['archivos_imagenes_diagnosticas']:
                documento = get_object_or_404(Documento, pk=f['id'])
                documento.historia = historia
                documento.save()

        data = {
            'id_mascota': id_macota
        }
        return JsonResponse(data)
    else:

        cita = get_object_or_404(Cita, pk=id_cita)
        id_macota = cita.mascota.id
        form = CrearHistoriaClinicaForm(id_mascota=id_macota)
        medicamentos = json.dumps(list(Medicamento.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad','unidad_medida','cantidad_unidad','cantidad_total_unidades', 'codigo_inventario','cantidad_por_unidad')), cls=DecimalEncoder)
        hospitalizaciones = json.dumps(list(Hospitalizacion.objects.filter(historia_clinica__mascota_id=id_macota).values_list('id', 'fecha_hospitalizacion')),cls=DjangoJSONEncoder)

        motivos_option = list(Tratamiento.objects.all().values_list('id', 'nombre', 'nombre', 'precio').order_by('nombre'))

        insumos = json.dumps(list(Insumo.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad','unidad_medida', 'cantidad_unidad','cantidad_total_unidades', 'codigo_inventario','cantidad_por_unidad')), cls=DecimalEncoder)

        return render(request, 'registrar_historia_clinica.html', {
            'mascotas': True,
            'cita': cita,
            'mascota': cita.mascota,
            'form': form,
            'medicamentos': medicamentos,
            'hospitalizaciones': hospitalizaciones,
            'motivos_options': json.dumps(list(motivos_option), cls=DecimalEncoder),
            'insumos': insumos,
        })


def documentos_historia(request):
    id_historia = request.GET.get('id_historia', None)

    ##################### Documentos adjuntos a la historia ####################

    archivos_adjuntos_motivo = Documento.objects.filter(historia__id=id_historia,
                                                        area_historia='Motivo consulta')

    archivos_adjuntos_examen_clinico = Documento.objects.filter(historia__id=id_historia,
                                                                area_historia='Examen clinico')

    archivos_adjuntos_diagnostico_diferencial = Documento.objects.filter(historia__id=id_historia,
                                                                         area_historia='Diagnostico diferencial')

    archivos_adjuntos_diagnostico_presuntivo = Documento.objects.filter(historia__id=id_historia,
                                                                        area_historia='Diagnostico presuntivo')

    archivos_adjuntos_examenes_laboratorio = Documento.objects.filter(historia__id=id_historia,
                                                                      area_historia='Examenes laboratorio')

    archivos_adjuntos_imagenes_diagnosticas = Documento.objects.filter(historia__id=id_historia,
                                                                       area_historia='Imagenes diagnosticas')

    ########################################################################

    data = {
        'archivos_adjuntos_motivo': serializers.serialize('json', archivos_adjuntos_motivo),
        'archivos_adjuntos_examen_clinico': serializers.serialize('json', archivos_adjuntos_examen_clinico),
        'archivos_adjuntos_diagnostico_diferencial': serializers.serialize('json', archivos_adjuntos_diagnostico_diferencial),
        'archivos_adjuntos_diagnostico_presuntivo': serializers.serialize('json', archivos_adjuntos_diagnostico_presuntivo),
        'archivos_adjuntos_examenes_laboratorio': serializers.serialize('json', archivos_adjuntos_examenes_laboratorio),
        'archivos_adjuntos_imagenes_diagnosticas': serializers.serialize('json', archivos_adjuntos_imagenes_diagnosticas)
    }
    return JsonResponse(data)

def enviar_opciones_tratamiento(request):
    opciones = Tratamiento.objects.all()
    data = {'Tratamientos': serializers.serialize('json', opciones)}
    return JsonResponse(data)

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


class actualizar_hospitalizacion(SuccessMessageMixin, UpdateView):
    model = Hospitalizacion
    form_class = ModificarHospitalizacionForm
    template_name = "actualizar_hospitalizacion.html"
    success_message = "Hospitalización se editó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(actualizar_hospitalizacion, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        hospitalizacion = get_object_or_404(Hospitalizacion, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(hospitalizacion.historia_clinica.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(actualizar_hospitalizacion, self).get_context_data(**kwargs)
        context['mascotas'] = True
        hospitalizacion = get_object_or_404(Hospitalizacion, pk=self.kwargs['pk'])
        context['mascota'] = hospitalizacion.historia_clinica.mascota
        context['array_medicamentos'] = json.dumps(list(Medicamento.objects.all().values_list('id', 'nombre', 'codigo', 'cantidad','precio_venta','cantidad_por_unidad','precio_por_unidad','unidad_medida','cantidad_unidad','cantidad_total_unidades', 'codigo_inventario','estado')), cls=DecimalEncoder)
        context['array_usuarios'] = json.dumps(list(Usuario.objects.all().values_list('id', 'first_name', 'segundo_nombre', 'last_name', 'segundo_apellido', 'roles')))
        context['id_hospitalizacion'] = hospitalizacion.id
        context['medicamentos_aplicados'] = MedicamentoHospitalizacion.objects.filter(hospitalizacion_id=hospitalizacion.id).order_by('id')
        context['insumos'] = json.dumps(list(Insumo.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad', 'unidad_medida','cantidad_unidad', 'cantidad_total_unidades','codigo_inventario', 'cantidad_por_unidad')),cls=DecimalEncoder)
        context['insumos_utilizados'] = InsumoHospitalizacion.objects.filter(hospitalizacion=self.kwargs['pk'])
        return context


def terminar_hospitalizacion(request):
    id_hospitalizacion = request.GET.get('id', '')
    mi_hospitalizacion = get_object_or_404(Hospitalizacion, pk=id_hospitalizacion)
    mi_hospitalizacion.estado = 'Cerrada'
    mi_hospitalizacion.save()

    mi_mascota = get_object_or_404(Mascota, pk=mi_hospitalizacion.historia_clinica.mascota.id)
    mi_mascota.estado_hospitalizacion = False
    mi_mascota.save()

    data = {
        'eliminacion': True,
        'id_mascota': mi_mascota.id
    }
    return JsonResponse(data)


def aplicar_medicamentos_hospitalizacion(request):
    req = json.loads(request.body.decode('utf-8'))

    #print("id_hospitalizacion: " + str(req['id_hospitalizacion']))

    for aplicacion_medicamento in req['medicamentos_agregados']:
        hospitalizacion = get_object_or_404(Hospitalizacion, pk=req['id_hospitalizacion'])
        medicamento = get_object_or_404(Medicamento, pk=aplicacion_medicamento['id_medicamento'])
        responsable = get_object_or_404(Usuario, pk=aplicacion_medicamento['id_responsable'])
        medicamento_hospitalizacion = MedicamentoHospitalizacion(hospitalizacion=hospitalizacion, medicamento=medicamento, responsable=responsable, cantidad_requerida=aplicacion_medicamento['cantidad_requerida'], unidad_medida=aplicacion_medicamento['unidad_medida'], tipo_unidad_medida=aplicacion_medicamento['tipo_unidades'], hora=aplicacion_medicamento['hora'])
        medicamento_hospitalizacion.save()

        cantidad = float(aplicacion_medicamento['cantidad_requerida'])

        if str(aplicacion_medicamento['tipo_unidades']) == 'unidad':
            medicamento.cantidad = float(medicamento.cantidad) - cantidad
            medicamento.cantidad_total_unidades = float(medicamento.cantidad_total_unidades) - (cantidad * float(medicamento.cantidad_unidad))
            medicamento.save()
        elif str(aplicacion_medicamento['tipo_unidades']) == 'granel':
            medicamento.cantidad_total_unidades = float(medicamento.cantidad_total_unidades) - cantidad
            medicamento.cantidad = math.floor(float(medicamento.cantidad_total_unidades) / float(medicamento.cantidad_unidad))
            medicamento.save()

    data = {
        'registrado': True,
    }

    return JsonResponse(data)


def crear_nueva_hospitalizacion(request, id_hospitalizacion_anterior):

    if request.method != 'POST':
        form = NuevaHospitalizacionForm()
        hospitalizacion_anterior = get_object_or_404(Hospitalizacion, pk=id_hospitalizacion_anterior)

        return render(request, 'nueva_hospitalizacion.html', {
            'form': form,
            'mascota': hospitalizacion_anterior.historia_clinica.mascota,
            'array_medicamentos': json.dumps(list(
            Medicamento.objects.all().values_list('id', 'nombre', 'codigo', 'cantidad', 'precio_venta',
                                                  'cantidad_por_unidad', 'precio_por_unidad', 'unidad_medida',
                                                  'cantidad_unidad', 'cantidad_total_unidades', 'codigo_inventario',
                                                  'estado')), cls=DecimalEncoder),
            'array_usuarios': json.dumps(list(
            Usuario.objects.all().values_list('id', 'first_name', 'segundo_nombre', 'last_name', 'segundo_apellido',
                                              'roles'))),
            'id_hospitalizacion_anterior': id_hospitalizacion_anterior,
            'mascotas': True,
            'hospitalizaciones_mascota': json.dumps(list(Hospitalizacion.objects.filter(historia_clinica__mascota_id=hospitalizacion_anterior.historia_clinica.mascota.id).values_list('id', 'fecha_hospitalizacion')),cls=DjangoJSONEncoder),
            'insumos': json.dumps(list(Insumo.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad', 'unidad_medida','cantidad_unidad', 'cantidad_total_unidades','codigo_inventario', 'cantidad_por_unidad')),cls=DecimalEncoder)
        })


def ajax_nueva_hospitalizacion(request):
    req = json.loads(request.body.decode('utf-8'))

    #print("id_hospitalizacion_anterior: " + str(req['id_hospitalizacion_anterior']))
    #print("fecha_hospitalizacion: " + str(req['fecha_hospitalizacion']))

    mi_fecha = datetime.strptime(str(req['fecha_hospitalizacion']), "%d/%m/%Y")

    #print("mi_fecha: "+ str(mi_fecha))

    hospitalizacion_anterior = get_object_or_404(Hospitalizacion, pk=req['id_hospitalizacion_anterior'])
    hospitalizacion_anterior.estado = 'Cerrada'
    hospitalizacion_anterior.save()

    hospitalizacion = Hospitalizacion(fecha_hospitalizacion=mi_fecha,
                                      historia_clinica=hospitalizacion_anterior.historia_clinica,
                                      hora_manana=req['hora_manana'],
                                      hora_tarde=req['hora_tarde'],
                                      temperatura_manana=req['temperatura_manana'],
                                      temperatura_tarde=req['temperatura_tarde'],
                                      frecuencia_manana=req['frecuencia_manana'],
                                      frecuencia_tarde=req['frecuencia_tarde'],
                                      mucosas_manana=req['mucosas_manana'],
                                      mucosas_tarde=req['mucosas_tarde'],
                                      apetito_manana=req['apetito_manana'],
                                      apetito_tarde=req['apetito_tarde'],
                                      sed_manana=req['sed_manana'],
                                      sed_tarde=req['sed_tarde'],
                                      estado_animo_manana=req['estado_animo_manana'],
                                      estado_animo_tarde=req['estado_animo_tarde'],
                                      consistencia_fecal_manana=req['consistencia_fecal_manana'],
                                      consistencia_fecal_tarde=req['consistencia_fecal_tarde'],
                                      vomito_manana=req['vomito_manana'],
                                      vomito_tarde=req['vomito_tarde'],
                                      orina_manana=req['orina_manana'],
                                      orina_tarde=req['orina_tarde'],
                                      pronostico_manana=req['pronostico_manana'],
                                      pronostico_tarde=req['pronostico_tarde'],
                                      observaciones_manana=req['observaciones_manana'],
                                      observaciones_tarde=req['observaciones_tarde'])
    hospitalizacion.save()

    for aplicacion_medicamento in req['medicamentos_agregados']:
        medicamento = get_object_or_404(Medicamento, pk=aplicacion_medicamento['id_medicamento'])
        responsable = get_object_or_404(Usuario, pk=aplicacion_medicamento['id_responsable'])
        medicamento_hospitalizacion = MedicamentoHospitalizacion(hospitalizacion=hospitalizacion, medicamento=medicamento, responsable=responsable, cantidad_requerida=aplicacion_medicamento['cantidad_requerida'], unidad_medida=aplicacion_medicamento['unidad_medida'], tipo_unidad_medida=aplicacion_medicamento['tipo_unidades'], hora=aplicacion_medicamento['hora'])
        medicamento_hospitalizacion.save()

        cantidad = float(aplicacion_medicamento['cantidad_requerida'])

        if str(aplicacion_medicamento['tipo_unidades']) == 'unidad':
            medicamento.cantidad = float(medicamento.cantidad) - cantidad
            medicamento.cantidad_total_unidades = float(medicamento.cantidad_total_unidades) - (cantidad * float(medicamento.cantidad_unidad))
            medicamento.save()
        elif str(aplicacion_medicamento['tipo_unidades']) == 'granel':
            medicamento.cantidad_total_unidades = float(medicamento.cantidad_total_unidades) - cantidad
            medicamento.cantidad = math.floor(float(medicamento.cantidad_total_unidades) / float(medicamento.cantidad_unidad))
            medicamento.save()

    for insumo in req['insumos_seleccionados']:
        insumo_select = get_object_or_404(Insumo, pk=insumo['id_insumo'])
        cantidad = str(insumo['cantidad_requerida'])
        unidad = str(insumo['unidades'])
        if unidad != 'unidad(es)':
            insumo_select.cantidad_total_unidades = float(
                insumo_select.cantidad_total_unidades) - float(cantidad)
            insumo_select.cantidad = math.floor(
                float(insumo_select.cantidad_total_unidades) / float(
                    insumo_select.cantidad_unidad))
            historiatratamiento = InsumoHospitalizacion(insumo=insumo_select, hospitalizacion=hospitalizacion,
                                                   cantidad_requerida=cantidad, unidad_medida="fraccion")
            historiatratamiento.save()
            insumo_select.save()
        else:

            insumo_select.cantidad = int(insumo_select.cantidad) - int(cantidad)
            insumo_select.cantidad_total_unidades = float(
                insumo_select.cantidad_total_unidades) - (float(cantidad) * float(
                insumo_select.cantidad_unidad))
            historiatratamiento = InsumoHospitalizacion(insumo=insumo_select, hospitalizacion=hospitalizacion,
                                                   cantidad_requerida=cantidad, unidad_medida="unidad")
            historiatratamiento.save()
            insumo_select.save()

    data = {
        'registrado': True,
    }

    return JsonResponse(data)


def DetalleCitaAgenda(request,tipo_atencion,id_atencion):

    if tipo_atencion == 'cita_medica':
        atencion = Cita.objects.get(pk=id_atencion)
        titulo_cita = 'Cita medica'
        color_atencion = '#00c7ff'
        datos_guarderia = "null"
    elif tipo_atencion == 'cita_estetica':
        atencion = FacturaEstetica.objects.get(pk=id_atencion)
        titulo_cita = 'Estetica'
        color_atencion = '#605fff'
        datos_guarderia = "null"
    elif tipo_atencion == 'cita_vacunacion':
        atencion = Vacunacion.objects.get(pk=id_atencion)
        titulo_cita = 'Vacunación'
        color_atencion = '#ff7e54'
        datos_guarderia = "null"
    elif tipo_atencion == 'cita_desparasitacion':
        atencion = Desparasitacion.objects.get(pk=id_atencion)
        titulo_cita = 'Desparasitación'
        color_atencion = '#ff5d86'
        datos_guarderia = "null"
    elif tipo_atencion == 'cita_guarderia':
        atencion = Guarderia.objects.get(pk=id_atencion)
        titulo_cita = 'Servicio de guarderia'
        color_atencion = '#94919d'
        datos_guarderia = DatosGuarderia.objects.filter(guarderia=atencion)

    mi_mascota = Mascota.objects.get(pk=atencion.mascota.id)

    if request.method == 'GET':
        return render(request, 'detalle_cita_agenda.html', {
            'tipo_cita': tipo_atencion,
            'id_atencion': id_atencion,
            'atencion': atencion,
            'mi_mascota': mi_mascota,
            'titulo_cita': titulo_cita,
            'color_atencion': color_atencion,
            'datos_guarderia': datos_guarderia,
        })


def agregar_insumos_historia(request):
    req = json.loads(request.body.decode('utf-8'))
    historia = get_object_or_404(HistoriaClinica, pk=req['id_historia'])

    for Item in req['insumos_añadidos']:
        insumo_select = get_object_or_404(Insumo, pk=Item[0])
        if Item[10] == True:
            insumo_select.cantidad_total_unidades = float(
                insumo_select.cantidad_total_unidades) - float(Item[11])
            insumo_select.cantidad = math.floor(
                float(insumo_select.cantidad_total_unidades) / float(
                    insumo_select.cantidad_unidad))
            historiatratamiento = InsumoHistoria(insumo=insumo_select, historia=historia,
                                                 cantidad_requerida=Item[11], unidad_medida="fraccion")
            historiatratamiento.save()
            insumo_select.save()
        else:

            insumo_select.cantidad = int(insumo_select.cantidad) - int(Item[11])
            insumo_select.cantidad_total_unidades = float(
                insumo_select.cantidad_total_unidades) - (float(Item[11]) * float(
                insumo_select.cantidad_unidad))
            historiatratamiento = InsumoHistoria(insumo=insumo_select, historia=historia,
                                                 cantidad_requerida=Item[11], unidad_medida="unidad")
            historiatratamiento.save()
            insumo_select.save()

    data = {
        'documento_creado': True
    }
    return JsonResponse(data)


def agregar_hospitalizacion_historia(request):
    req = json.loads(request.body.decode('utf-8'))
    historia = get_object_or_404(HistoriaClinica, pk=req['id_historia'])

    mi_fecha = datetime.strptime(str(req['fecha_hospitalizacion']), "%d/%m/%Y")

    hospitalizacion = Hospitalizacion(fecha_hospitalizacion=mi_fecha,
                                      historia_clinica=historia,
                                      hora_manana=req['hora_mañana'],
                                      temperatura_manana=req['temperatura_mañana'],
                                      frecuencia_manana=req['frecuencia_cardiaca_mañana'],
                                      mucosas_manana=req['mucosas_mañana'],
                                      apetito_manana=req['apetito_mañana'],
                                      sed_manana=req['sed_mañana'],
                                      estado_animo_manana=req['estado_animo_mañana'],
                                      consistencia_fecal_manana=req['consistencia_fecal_mañana'],
                                      vomito_manana=req['vomito_mañana'],
                                      orina_manana=req['orina_mañana'],
                                      pronostico_manana=req['pronostico_mañana'],
                                      hora_tarde=req['hora_tarde'],
                                      temperatura_tarde=req['temperatura_tarde'],
                                      frecuencia_tarde=req['frecuencia_cardiaca_tarde'],
                                      mucosas_tarde=req['mucosas_tarde'],
                                      apetito_tarde=req['apetito_tarde'],
                                      sed_tarde=req['sed_tarde'],
                                      estado_animo_tarde=req['estado_animo_tarde'],
                                      consistencia_fecal_tarde=req['consistencia_fecal_tarde'],
                                      vomito_tarde=req['vomito_tarde'],
                                      orina_tarde=req['orina_tarde'],
                                      pronostico_tarde=req['pronostico_tarde'],
                                      observaciones_manana=req['observaciones_mañana'],
                                      observaciones_tarde=req['observaciones_tarde'])
    hospitalizacion.save()

    mi_mascota = Mascota.objects.get(pk=historia.mascota.id)

    mi_mascota.estado_hospitalizacion = True
    mi_mascota.save()

    hora_actual = datetime.now().time().strftime("%I:%M%p")

    for aplicacion_medicamento in req['medicamentos_seleccionados_hospitalizacion']:
        medicamento = get_object_or_404(Medicamento, pk=aplicacion_medicamento[0])
        tipo_unidad_medida = 'unidad'
        if aplicacion_medicamento[10] == True:
            tipo_unidad_medida = 'granel'
        medicamento_hospitalizacion = MedicamentoHospitalizacion(hospitalizacion=hospitalizacion,
                                                                 medicamento=medicamento,
                                                                 responsable=historia.creada_por,
                                                                 cantidad_requerida=aplicacion_medicamento[11],
                                                                 unidad_medida=aplicacion_medicamento[4],
                                                                 tipo_unidad_medida=tipo_unidad_medida,
                                                                 hora=hora_actual)
        medicamento_hospitalizacion.save()

        cantidad = float(aplicacion_medicamento[11])

        if tipo_unidad_medida == 'unidad':
            medicamento.cantidad = int(medicamento.cantidad) - cantidad
            medicamento.cantidad_total_unidades = int(medicamento.cantidad_total_unidades) - (
                    cantidad * int(medicamento.cantidad_unidad))
            medicamento.save()
        elif tipo_unidad_medida == 'granel':
            medicamento.cantidad_total_unidades = int(medicamento.cantidad_total_unidades) - cantidad
            medicamento.cantidad = math.floor(
                int(medicamento.cantidad_total_unidades) / int(medicamento.cantidad_unidad))
            medicamento.save()

    data = {
        'documento_creado': True
    }
    return JsonResponse(data)


def agregar_cirugia_historia(request):
    req = json.loads(request.body.decode('utf-8'))
    historia = get_object_or_404(HistoriaClinica, pk=req['id_historia'])

    cirugia = Cirugia(historia_clinica=historia, procedimiento_quirurgico=req['procedimiento_quirurgico'],
                      abordaje=req['abordaje'], diagnostico_quirurgico=req['diagnostico_quirurgico'])
    cirugia.save()

    for Item in req['medicamentos_seleccionados_cirugia']:
        medicamento_select = get_object_or_404(Medicamento, pk=Item[0])
        if Item[10] == True:
            medicamento_select.cantidad_total_unidades = float(
                medicamento_select.cantidad_total_unidades) - float(Item[11])
            medicamento_select.cantidad = math.floor(
                float(medicamento_select.cantidad_total_unidades) / float(
                    medicamento_select.cantidad_unidad))
            historiatratamiento = MedicamentoCirugia(medicamento=medicamento_select, cirugia=cirugia,
                                                     cantidad_requerida=Item[11], unidad_medida="fraccion")
            historiatratamiento.save()
            medicamento_select.save()
        else:

            medicamento_select.cantidad = int(medicamento_select.cantidad) - int(Item[11])
            medicamento_select.cantidad_total_unidades = float(
                medicamento_select.cantidad_total_unidades) - (float(Item[11]) * float(
                medicamento_select.cantidad_unidad))
            historiatratamiento = MedicamentoCirugia(medicamento=medicamento_select, cirugia=cirugia,
                                                     cantidad_requerida=Item[11], unidad_medida="unidad")
            historiatratamiento.save()
            medicamento_select.save()

    data = {
        'documento_creado': True
    }
    return JsonResponse(data)


class PdfHistoria(TemplateView):
    template_name = 'pdf_historia.html'

    def get_context_data(self, **kwargs):
        context = super(PdfHistoria, self).get_context_data(**kwargs)
        mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])

        qs1 = Cita.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado='Atendida')
        qs2 = FacturaEstetica.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_de_atencion='Atendida')
        qs3 = Vacunacion.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_aplicacion='Aplicada')
        qs4 = Desparasitacion.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_aplicacion='Aplicada')
        qs5 = Guarderia.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado='Pagada')
        qs6 = Formula.objects.filter(mascota_id=self.kwargs['id_mascota'])
        qs7 = HistoriaClinica.objects.filter(mascota_id=self.kwargs['id_mascota'])
        qs8 = Hospitalizacion.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota']).order_by('fecha_hospitalizacion')
        qs9 = Cirugia.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota']).order_by('fecha')
        qs10 = SeguimientoHistoria.objects.filter(mascota_id=self.kwargs['id_mascota'])

        formulas_id = []

        for q in qs5:
            q.fecha = q.fecha_inicio

        for q in qs3:
            q.tipo = 'Vacunacion'

        for q in qs4:
            q.tipo = 'Desparasitacion'

        for q in qs6:
            q.formula = 'Formula'
            formulas_id.append(q.pk)

        for q in qs7:
            q.historia = 'Historia'

        for q in qs8:
            q.hospitalizacion = 'Hospitalizacion'
            q.fecha = q.fecha.replace(year=q.fecha_hospitalizacion.year, month=q.fecha_hospitalizacion.month,day=q.fecha_hospitalizacion.day,hour=23,minute=59,second=30)

        for q in qs9:
            q.tipo = 'Cirugia'

        for q in qs10:
            q.seguimiento = 'Seguimiento'

        historial_mascota = sorted(chain(qs1, qs2, qs3, qs4, qs5, qs6, qs7, qs8, qs9, qs10), key=lambda instance: instance.fecha,
                                   reverse=True)

        procedimientos_mascota = MotivoArea.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['historial_mascota'] = historial_mascota
        context['procedimientos_mascota'] = procedimientos_mascota

        procedimientos_id_historias = []

        for procedimiento in procedimientos_mascota:
            procedimientos_id_historias.append(procedimiento.historia_clinica.id)

        context[
            'procedimientos_id_historias'] = procedimientos_id_historias

        medicamentos_aplicados_hospitalizaciones = MedicamentoHospitalizacion.objects.filter(
            hospitalizacion__historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['medicamentos_aplicados_hospitalizaciones'] = medicamentos_aplicados_hospitalizaciones

        medicamentos_aplicados_hospitalizaciones_id_historias = []

        for medicamento_aplicado in medicamentos_aplicados_hospitalizaciones:
            medicamentos_aplicados_hospitalizaciones_id_historias.append(medicamento_aplicado.hospitalizacion.id)

        context[
            'medicamentos_aplicados_hospitalizaciones_id_historias'] = medicamentos_aplicados_hospitalizaciones_id_historias

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        context['mi_mascota'] = mascota

        context['fecha_hoy'] = datetime.now().date()

        context['formulas_id'] = formulas_id

        context['vacunaciones_aplicadas'] = AplicacionVacunacion.objects.filter(
            vacunacion__mascota__id=self.kwargs['id_mascota'])
        context['desparasitaciones_aplicadas'] = AplicacionDesparasitacion.objects.filter(
            desparasitacion__mascota__id=self.kwargs['id_mascota'])

        ########################################### MEDICAMENTOS APLICADOS CIRUGIA #################################

        medicamentos_aplicados_cirugias = MedicamentoCirugia.objects.filter(
            cirugia__historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['medicamentos_aplicados_cirugias'] = medicamentos_aplicados_cirugias

        medicamentos_aplicados_cirugias_id_historias = []

        for medicamento_aplicado in medicamentos_aplicados_cirugias:
            medicamentos_aplicados_cirugias_id_historias.append(medicamento_aplicado.cirugia.id)

        context[
            'medicamentos_aplicados_cirugias_id_historias'] = medicamentos_aplicados_cirugias_id_historias

        ########################################## INYECTOLOGIA ###################################################

        inyectologias = HistoriaMedicamento.objects.filter(historia_clinica__mascota_id=self.kwargs['id_mascota'])

        context['inyectologias'] = inyectologias

        inyectologias_id_historias = []

        for inyectologia in inyectologias:
            inyectologias_id_historias.append(inyectologia.historia_clinica.id)

        context[
            'inyectologias_id_historias'] = inyectologias_id_historias

        ###########################################################################################################

        return context


def aplicar_insumos_hospitalizacion(request):
    req = json.loads(request.body.decode('utf-8'))

    mi_hospitalizacion = get_object_or_404(Hospitalizacion, pk=req['id_hospitalizacion'])

    for insumo in req['insumos_seleccionados']:
        insumo_select = get_object_or_404(Insumo, pk=insumo['id_insumo'])
        cantidad = str(insumo['cantidad_requerida'])
        unidad = str(insumo['unidades'])

        if unidad != 'unidad(es)':
            insumo_select.cantidad_total_unidades = float(
                insumo_select.cantidad_total_unidades) - float(cantidad)
            insumo_select.cantidad = math.floor(
                float(insumo_select.cantidad_total_unidades) / float(
                    insumo_select.cantidad_unidad))
            historiatratamiento = InsumoHospitalizacion(insumo=insumo_select, hospitalizacion=mi_hospitalizacion,
                                                   cantidad_requerida=cantidad, unidad_medida="fraccion")
            historiatratamiento.save()
            insumo_select.save()
        else:

            insumo_select.cantidad = int(insumo_select.cantidad) - int(cantidad)
            insumo_select.cantidad_total_unidades = float(
                insumo_select.cantidad_total_unidades) - (float(cantidad) * float(
                insumo_select.cantidad_unidad))
            historiatratamiento = InsumoHospitalizacion(insumo=insumo_select, hospitalizacion=mi_hospitalizacion,
                                                   cantidad_requerida=cantidad, unidad_medida="unidad")
            historiatratamiento.save()
            insumo_select.save()

    data = {
        'registrado': True,
    }

    return JsonResponse(data)


def agregar_seguimiento_historia(request):
    id_mascota = request.GET.get('id', '')
    comentarios_seguimiento = request.GET.get('comentarios_seguimiento', '')

    mi_mascota = get_object_or_404(Mascota, pk=id_mascota)
    mi_usuario = get_object_or_404(Usuario, id=request.user.id)

    nuevo_seguimiento = SeguimientoHistoria(comentarios=comentarios_seguimiento, mascota=mi_mascota, registrado_por=mi_usuario)
    nuevo_seguimiento.save()

    messages.success(request, 'Seguimiento registrado con exito!')
    data = {
        'modificacion': True
    }
    return JsonResponse(data)


def editar_seguimiento_historia(request):
    id_seguimiento = request.GET.get('id', '')
    comentarios_seguimiento = request.GET.get('comentarios_seguimiento', '')

    mi_seguimiento = get_object_or_404(SeguimientoHistoria, pk=id_seguimiento)
    mi_usuario = get_object_or_404(Usuario, id=request.user.id)

    mi_seguimiento.comentarios = comentarios_seguimiento
    mi_seguimiento.registrado_por = mi_usuario
    mi_seguimiento.fecha = datetime.now()
    mi_seguimiento.save()

    messages.success(request, 'Seguimiento editado con exito!')
    data = {
        'modificacion': True
    }
    return JsonResponse(data)