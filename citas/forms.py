from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404


class CrearCitaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearCitaForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].readonly = True
        self.fields['proxima_cita'].readonly = True
        #self.fields['mascota'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        #self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['proxima_cita'].required = False
        self.fields['motivo_cita'].required = False
        self.fields['proxima_cita'].label = 'Próxima cita'

    class Meta:
        model = Cita
        fields = (
            'mascota',
            'veterinario',
            'fecha',
            'proxima_cita',
            'motivo_cita',
            'motivo_proxima_cita',
        )

        widgets = {
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input','id':'firstdate'}),
            'proxima_cita': forms.DateTimeInput(attrs={'class': 'datetime-input','id':'seconddate'})
        }

    def clean_motivo_proxima_cita(self):
        fecha_proxima_cita = self.cleaned_data['proxima_cita']
        motivo_proxima_cita = self.cleaned_data['motivo_proxima_cita']

        if fecha_proxima_cita != None and motivo_proxima_cita == None:
            raise forms.ValidationError(
                'Si desea programar una proxima cita debe especificar un motivo para ella')

        return motivo_proxima_cita


class CrearCitaMascotaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearCitaMascotaForm, self).__init__(*args, **kwargs)
        self.fields['veterinario'].empty_label = None
        self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['proxima_cita'].required = False
        self.fields['motivo_cita'].required = False
        self.fields['proxima_cita'].label = 'Próxima cita'
        self.fields['motivo_proxima_cita'].label = 'Motivo próxima cita'

    class Meta:
        model = Cita
        fields = (
            'veterinario',
            'fecha',
            'proxima_cita',
            'motivo_cita',
            'motivo_proxima_cita',
        )

        widgets = {
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'proxima_cita': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }

    def clean_motivo_proxima_cita(self):
        fecha_proxima_cita = self.cleaned_data['proxima_cita']
        motivo_proxima_cita = self.cleaned_data['motivo_proxima_cita']

        if fecha_proxima_cita != None and motivo_proxima_cita == None:
            raise forms.ValidationError(
                'Si desea programar una próxima cita debe especificar un motivo para ella')

        return motivo_proxima_cita


class ModificarCitaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarCitaForm, self).__init__(*args, **kwargs)
        self.fields['veterinario'].empty_label = None
        self.fields['fecha'].readonly = True
        self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}

    class Meta:
        model = Cita
        fields = (
            'veterinario',
            'fecha',
            'motivo_cita',
        )

        widgets = {
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input','id':'seconddate'})
        }


class CrearHistoriaClinicaForm(forms.ModelForm):

    def __init__(self, id_mascota, *args, **kwargs):
        super(CrearHistoriaClinicaForm, self).__init__(*args, **kwargs)
        mi_mascota = Mascota.objects.get(id=id_mascota)
        self.fields['descripcion'].widget.attrs = {'rows': '5', 'placeholder':'¿En que estado incial se encuentra la mascota al llegar a la cita?'}
        self.fields['descripcion'].label = 'Estado de la Mascota (Anamnesis)'
        self.fields['archivo_zip'].label = 'Archivos adjuntos'
        self.fields['archivo_zip'].widget.attrs = {'multiple': 'true'}
        self.fields['fecha'].initial = datetime.now()
        self.fields['fecha'].label = 'Fecha y hora'
        self.fields['tratamiento'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tratamiento'].label = 'Área de consulta'
        self.fields['motivo_cita'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['motivo_cita'].label = 'Motivo de cita'
        self.fields['frecuencia_cardiaca'].widget.attrs = {'placeholder': 'Frecuencia cardíaca', 'label': 'Frecuencia cardíaca'}
        self.fields['frecuencia_respiratoria'].widget.attrs = {'placeholder': 'Frecuencia respiratoria'}
        self.fields['temperatura'].widget.attrs = {'placeholder': 'Temperatura'}
        self.fields['tiempo_llenado_capilar'].widget.attrs = {'placeholder': 'Tiempo de llenado capilar'}
        self.fields['pulso'].widget.attrs = {'placeholder': ''}
        self.fields['peso'].widget.attrs = {'placeholder': ''}
        self.fields['actitud'].widget.attrs = {'placeholder': 'Ej: decaido, adormecido, etc...'}
        self.fields['estado_corporal'].widget.attrs = {'placeholder': 'Estado corporal: gordo, flaco...'}
        self.fields['senales'].required = False
        self.fields['estado_reproductivo'].required = False
        self.fields['numero_partos'].required = False
        self.fields['tipo_alimento'].required = False
        self.fields['racion_alimento'].required = False
        self.fields['racion_alimento'].label = 'Ración'
        self.fields['vacunacion'].required = False
        self.fields['vacunacion'].label = 'Vacunación'
        self.fields['desparasitacion'].required = False
        self.fields['desparasitacion'].label = 'Desparasitación'
        self.fields['heces'].required = False
        self.fields['orina'].required = False
        self.fields['frecuencia_cardiaca'].required = False
        self.fields['frecuencia_respiratoria'].required = False
        self.fields['temperatura'].required = False
        self.fields['tiempo_llenado_capilar'].required = False
        self.fields['pulso'].required = False
        self.fields['peso'].required = False
        self.fields['actitud'].required = False
        self.fields['estado_corporal'].required = False
        self.fields['comentarios_examen_clinico'].required = False
        self.fields['listado_problemas'].required = False
        self.fields['diagnostico_diferencial'].required = False
        self.fields['diagnostico_presuntivo'].required = False
        self.fields['examenes_laboratorio'].required = False
        self.fields['imagenes_diagnosticas'].required = False
        self.fields['diagnostico_definitivo'].required = False
        self.fields['observaciones_tratamiento'].required = False
        self.fields['pronostico'].required = False
        self.fields['notas'].required = False
        self.fields['senales'].initial = mi_mascota.senales
        self.fields['estado_reproductivo'].initial = mi_mascota.estado_reproductivo
        self.fields['numero_partos'].initial = mi_mascota.numero_partos
        self.fields['tratamiento'].empty_label = None
        self.fields['motivo_cita'].empty_label = None
        self.fields['descripcion_inyectologia'].required = False
        


    class Meta:
        model = HistoriaClinica
        fields = (
            'motivo_cita',
            'tratamiento',
            'archivo_zip',
            'descripcion',
            'fecha',
            'senales',
            'estado_reproductivo',
            'numero_partos',
            'tipo_alimento',
            'racion_alimento',
            'vacunacion',
            'desparasitacion',
            'heces',
            'orina',
            'frecuencia_cardiaca',
            'frecuencia_respiratoria',
            'temperatura',
            'tiempo_llenado_capilar',
            'pulso',
            'peso',
            'actitud',
            'estado_corporal',
            'comentarios_examen_clinico',
            'listado_problemas',
            'diagnostico_diferencial',
            'descripcion_inyectologia',
            'diagnostico_presuntivo',
            'examenes_laboratorio',
            'imagenes_diagnosticas',
            'diagnostico_definitivo',
            'observaciones_tratamiento',
            'pronostico',
            'notas',
            'sistema_linfatico',
            'sistema_tegumentario',
            'sistema_cardiovascular',
            'sistema_respiratorio',
            'sistema_digestivo',
            'sistema_musculoesqueletico',
            'sistema_nervioso',
            'sistema_urinario',
            'sistema_reproductivo',
        )

        widgets = {
            'descripcion': forms.Textarea(),
            'comentarios_examen_clinico': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'listado_problemas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_diferencial': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'descripcion_inyectologia': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_presuntivo': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'examenes_laboratorio': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'imagenes_diagnosticas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_definitivo': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'observaciones_tratamiento': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'pronostico': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'notas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'sistema_linfatico': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_tegumentario': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_cardiovascular': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_respiratorio': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_digestivo': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_musculoesqueletico': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_nervioso': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_urinario': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_reproductivo': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
        }


class EditarHistoriaClinicaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarHistoriaClinicaForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs = {'rows': '5', 'placeholder':'¿En que estado incial se encuentra la mascota al llegar a la cita?'}
        self.fields['descripcion'].label = 'Estado de la mascota'
        self.fields['archivo_zip'].label = 'Archivos adjuntos'
        self.fields['archivo_zip'].widget.attrs = {'multiple': 'true'}
        self.fields['fecha'].initial = datetime.now()
        self.fields['fecha'].label = 'Fecha y hora'
        self.fields['tratamiento'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tratamiento'].label = 'Área de consulta'
        self.fields['motivo_cita'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['motivo_cita'].label = 'Motivo de cita'
        self.fields['frecuencia_cardiaca'].widget.attrs = {'placeholder': 'Frecuencia cardiaca'}
        self.fields['frecuencia_respiratoria'].widget.attrs = {'placeholder': 'Frecuencia respiratoria'}
        self.fields['temperatura'].widget.attrs = {'placeholder': 'Temperatura'}
        self.fields['tiempo_llenado_capilar'].widget.attrs = {'placeholder': 'Tiempo de llenado capilar'}
        self.fields['pulso'].widget.attrs = {'placeholder': ''}
        self.fields['peso'].widget.attrs = {'placeholder': ''}
        self.fields['actitud'].widget.attrs = {'placeholder': 'Ej: decaido, adormecido, etc...'}
        self.fields['estado_corporal'].widget.attrs = {'placeholder': 'Estado corporal: gordo, flaco...'}
        self.fields['senales'].required = False
        self.fields['estado_reproductivo'].required = False
        self.fields['numero_partos'].required = False
        self.fields['tipo_alimento'].required = False
        self.fields['racion_alimento'].required = False
        self.fields['racion_alimento'].label = 'Ración'
        self.fields['vacunacion'].required = False
        self.fields['desparasitacion'].required = False
        self.fields['heces'].required = False
        self.fields['orina'].required = False
        self.fields['frecuencia_cardiaca'].required = False
        self.fields['frecuencia_respiratoria'].required = False
        self.fields['temperatura'].required = False
        self.fields['tiempo_llenado_capilar'].required = False
        self.fields['pulso'].required = False
        self.fields['peso'].required = False
        self.fields['actitud'].required = False
        self.fields['estado_corporal'].required = False
        self.fields['comentarios_examen_clinico'].required = False
        self.fields['listado_problemas'].required = False
        self.fields['diagnostico_diferencial'].required = False
        self.fields['diagnostico_presuntivo'].required = False
        self.fields['examenes_laboratorio'].required = False
        self.fields['imagenes_diagnosticas'].required = False
        self.fields['diagnostico_definitivo'].required = False
        self.fields['observaciones_tratamiento'].required = False
        self.fields['pronostico'].required = False
        self.fields['notas'].required = False
        self.fields['motivo_cita'].empty_label = None
        self.fields['descripcion_inyectologia'].required = False


    class Meta:
        model = HistoriaClinica
        fields = (
            'tratamiento',
            'motivo_cita',
            'archivo_zip',
            'descripcion',
            'fecha',
            'senales',
            'estado_reproductivo',
            'numero_partos',
            'tipo_alimento',
            'racion_alimento',
            'vacunacion',
            'desparasitacion',
            'heces',
            'orina',
            'frecuencia_cardiaca',
            'frecuencia_respiratoria',
            'temperatura',
            'tiempo_llenado_capilar',
            'pulso',
            'peso',
            'actitud',
            'estado_corporal',
            'comentarios_examen_clinico',
            'sistema_linfatico',
            'sistema_tegumentario',
            'sistema_cardiovascular',
            'sistema_respiratorio',
            'sistema_digestivo',
            'sistema_musculoesqueletico',
            'sistema_nervioso',
            'sistema_urinario',
            'sistema_reproductivo',
            'listado_problemas',
            'diagnostico_diferencial',
            'diagnostico_presuntivo',
            'examenes_laboratorio',
            'imagenes_diagnosticas',
            'diagnostico_definitivo',
            'observaciones_tratamiento',
            'pronostico',
            'notas',
            'descripcion_inyectologia',
            
        )

        widgets = {
            'descripcion': forms.Textarea(),
            'comentarios_examen_clinico': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'listado_problemas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_diferencial': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'descripcion_inyectologia': forms.Textarea(attrs={'rows': '5', 'placeholder':'','id':'id_descripcion_inyectologia'}),
            'diagnostico_presuntivo': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'examenes_laboratorio': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'imagenes_diagnosticas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_definitivo': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'observaciones_tratamiento': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'pronostico': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'notas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'sistema_linfatico': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_tegumentario': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_cardiovascular': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_respiratorio': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_digestivo': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_musculoesqueletico': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_nervioso': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_urinario': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_reproductivo': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
        }


class AgregarOtraHistoriaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AgregarOtraHistoriaForm, self).__init__(*args, **kwargs)
        #self.fields['tratamiento'].empty_label = None
        self.fields['descripcion'].label = 'Observaciones'
        self.fields['fecha'].initial = datetime.now()
        self.fields['archivo_zip'].label = 'Archivo'
        self.fields['tratamiento'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['archivo_zip'].widget.attrs = {'multiple': 'true'}
        self.fields['tratamiento'].label = 'Área de consulta'

    class Meta:
        model = HistoriaClinica
        fields = (
            'tratamiento',
            'descripcion',
            'archivo_zip',
            'fecha',
            'estado',
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '5'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }


class EditarHistoriaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarHistoriaForm, self).__init__(*args, **kwargs)
        #self.fields['tratamiento'].empty_label = None
        self.fields['descripcion'].label = 'Observaciones'
        self.fields['archivo_zip'].label = 'Archivo'
        self.fields['tratamiento'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tratamiento'].label = 'Área de consulta'

    class Meta:
        model = HistoriaClinica
        fields = (
            'cita',
            'tratamiento',
            'descripcion',
            'archivo_zip',
            'fecha',
            'motivo',
            'estado',
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '5'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }


class registrarHistoriaClinicaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(registrarHistoriaClinicaForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs = {'rows': '5', 'placeholder':'¿En que estado incial se encuentra la mascota al llegar a la cita?'}
        self.fields['descripcion'].label = 'Estado de la mascota'
        self.fields['archivo_zip'].label = 'Archivos adjuntos'
        self.fields['archivo_zip'].widget.attrs = {'multiple': 'true'}
        self.fields['fecha'].initial = datetime.now()
        self.fields['fecha'].label = 'Fecha y hora'
        self.fields['tratamiento'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tratamiento'].label = 'Area de consulta'
        self.fields['frecuencia_cardiaca'].widget.attrs = {'placeholder': 'Frecuencia cardiaca'}
        self.fields['frecuencia_respiratoria'].widget.attrs = {'placeholder': 'Frecuencia respiratoria'}
        self.fields['temperatura'].widget.attrs = {'placeholder': 'Temperatura'}
        self.fields['tiempo_llenado_capilar'].widget.attrs = {'placeholder': 'Tiempo de llenado capilar'}
        self.fields['pulso'].widget.attrs = {'placeholder': ''}
        self.fields['peso'].widget.attrs = {'placeholder': ''}
        self.fields['actitud'].widget.attrs = {'placeholder': 'Ej: decaido, adormecido, etc...'}
        self.fields['estado_corporal'].widget.attrs = {'placeholder': 'Estado corporal: gordo, flaco...'}
        self.fields['senales'].required = False
        self.fields['estado_reproductivo'].required = False
        self.fields['numero_partos'].required = False
        self.fields['tipo_alimento'].required = False
        self.fields['racion_alimento'].required = False
        self.fields['vacunacion'].required = False
        self.fields['desparasitacion'].required = False
        self.fields['heces'].required = False
        self.fields['orina'].required = False
        self.fields['frecuencia_cardiaca'].required = False
        self.fields['frecuencia_respiratoria'].required = False
        self.fields['temperatura'].required = False
        self.fields['tiempo_llenado_capilar'].required = False
        self.fields['pulso'].required = False
        self.fields['peso'].required = False
        self.fields['actitud'].required = False
        self.fields['estado_corporal'].required = False
        self.fields['comentarios_examen_clinico'].required = False
        self.fields['listado_problemas'].required = False
        self.fields['diagnostico_diferencial'].required = False
        self.fields['diagnostico_presuntivo'].required = False
        self.fields['examenes_laboratorio'].required = False
        self.fields['imagenes_diagnosticas'].required = False
        self.fields['diagnostico_definitivo'].required = False
        self.fields['observaciones_tratamiento'].required = False
        self.fields['pronostico'].required = False
        self.fields['notas'].required = False


    class Meta:
        model = HistoriaClinica
        fields = (
            'tratamiento',
            'archivo_zip',
            'descripcion',
            'fecha',
            'senales',
            'estado_reproductivo',
            'numero_partos',
            'tipo_alimento',
            'racion_alimento',
            'vacunacion',
            'desparasitacion',
            'heces',
            'orina',
            'frecuencia_cardiaca',
            'frecuencia_respiratoria',
            'temperatura',
            'tiempo_llenado_capilar',
            'pulso',
            'peso',
            'actitud',
            'estado_corporal',
            'comentarios_examen_clinico',
            'listado_problemas',
            'diagnostico_diferencial',
            'diagnostico_presuntivo',
            'examenes_laboratorio',
            'imagenes_diagnosticas',
            'diagnostico_definitivo',
            'observaciones_tratamiento',
            'pronostico',
            'notas',
            'sistema_linfatico',
            'sistema_tegumentario',
            'sistema_cardiovascular',
            'sistema_respiratorio',
            'sistema_digestivo',
            'sistema_musculoesqueletico',
            'sistema_nervioso',
            'sistema_urinario',
            'sistema_reproductivo',
        )

        widgets = {
            'descripcion': forms.Textarea(),
            'comentarios_examen_clinico': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'listado_problemas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_diferencial': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_presuntivo': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'examenes_laboratorio': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'imagenes_diagnosticas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'diagnostico_definitivo': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'observaciones_tratamiento': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'pronostico': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'notas': forms.Textarea(attrs={'rows': '5', 'placeholder':''}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'sistema_linfatico': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_tegumentario': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_cardiovascular': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_respiratorio': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_digestivo': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_musculoesqueletico': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_nervioso': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_urinario': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
            'sistema_reproductivo': forms.Textarea(attrs={'rows': '3', 'placeholder': ''}),
        }


class registrarDocumentoHistoriaForm(forms.ModelForm):

    class Meta:
        model = Documento
        fields = (
            'file',
        )


class ModificarHospitalizacionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarHospitalizacionForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs['placeholder'] = ' '
            if field == 'fecha_hospitalizacion':
                self.fields[field].widget.attrs['readonly'] = True

    class Meta:
        model = Hospitalizacion
        fields = (
            'fecha_hospitalizacion',
            'hora_manana',
            'hora_tarde',
            'temperatura_manana',
            'temperatura_tarde',
            'frecuencia_manana',
            'frecuencia_tarde',
            'mucosas_manana',
            'mucosas_tarde',
            'apetito_manana',
            'apetito_tarde',
            'sed_manana',
            'sed_tarde',
            'estado_animo_manana',
            'estado_animo_tarde',
            'consistencia_fecal_manana',
            'consistencia_fecal_tarde',
            'vomito_manana',
            'vomito_tarde',
            'orina_manana',
            'orina_tarde',
            'pronostico_manana',
            'pronostico_tarde',
            'observaciones_manana',
            'observaciones_tarde',
        )

        widgets = {
            'observaciones_manana': forms.Textarea(attrs={'rows': '8', 'placeholder': ''}),
            'observaciones_tarde': forms.Textarea(attrs={'rows': '8', 'placeholder': ''}),
        }


class NuevaHospitalizacionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(NuevaHospitalizacionForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs['placeholder'] = ' '

    class Meta:
        model = Hospitalizacion
        fields = (
            'fecha_hospitalizacion',
            'hora_manana',
            'hora_tarde',
            'temperatura_manana',
            'temperatura_tarde',
            'frecuencia_manana',
            'frecuencia_tarde',
            'mucosas_manana',
            'mucosas_tarde',
            'apetito_manana',
            'apetito_tarde',
            'sed_manana',
            'sed_tarde',
            'estado_animo_manana',
            'estado_animo_tarde',
            'consistencia_fecal_manana',
            'consistencia_fecal_tarde',
            'vomito_manana',
            'vomito_tarde',
            'orina_manana',
            'orina_tarde',
            'pronostico_manana',
            'pronostico_tarde',
            'observaciones_manana',
            'observaciones_tarde',
        )

        widgets = {
            'observaciones_manana': forms.Textarea(attrs={'rows': '8', 'placeholder': ''}),
            'observaciones_tarde': forms.Textarea(attrs={'rows': '8', 'placeholder': ''}),
        }

    """def clean(self):
        cleaned_data = super(NuevaHospitalizacionForm, self).clean()
        hora_manana = cleaned_data.get("hora_manana")

        if 'AM' in hora_manana :
            pass
        else:
            self._errors['hora_manana'] = ['Debe ingresar una hora de la mañana']

        return hora_manana"""