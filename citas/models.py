from django.db import models
from mascotas.models import Mascota
from usuarios.models import Veterinario, Usuario
from tratamientos.models import Tratamiento
import datetime
from django.db import connection
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile


def get_upload_to(instance, filename):
    return 'upload/%s/%s' % (instance.mascota.cliente.numero_documento, filename)

def get_upload_to_documento(instance, filename):
    tenant_name = connection.tenant.schema_name
    return 'upload/%s/documentos_historias/%s' % (tenant_name, filename)

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.zip','.tar','.pdf','.PDF','.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Extension del archivo no valida')


ESTADOS_CITA = (('Activa','Activa'),('Cancelada','Cancelada'),('Atendida','Atendida'))


class Cita(models.Model):
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    veterinario = models.ForeignKey(Veterinario, on_delete=models.CASCADE, limit_choices_to={'is_active': True})
    fecha = models.DateTimeField()
    proxima_cita = models.DateTimeField(null=True, blank=True)
    motivo_cita = models.CharField(max_length=2000, verbose_name='Motivo cita')
    motivo_proxima_cita = models.CharField(max_length=2000, verbose_name='Motivo próxima cita', null=True, blank=True)
    estado = models.CharField(max_length=10, verbose_name="Estado", choices=ESTADOS_CITA, default='Activa')
    email_sent = models.BooleanField(default = False)

    def __str__(self):
        return self.mascota.nombre + " - " + str(self.fecha.strftime('%b. %d, %Y, %H:%M %p'))


MOTIVOS_HISTORIA = (('Estado inicial','Estado inicial'),('Historia adicional','Historia adicional'))

ESTADOS_HISTORIA = (('Activa','Activa'),('Pagada','Pagada'))


class HistoriaClinica(models.Model):
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    cita = models.ForeignKey(Cita, on_delete=models.CASCADE, blank=True, null=True)
    fecha = models.DateTimeField()
    creada_por = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    motivo = models.CharField(max_length=1000, verbose_name='Motivo', default='Estado inicial')
    descripcion = models.CharField(max_length=10000, verbose_name='Descripción')
    archivo_zip = models.FileField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    tratamiento = models.ForeignKey(Tratamiento, on_delete=models.CASCADE, blank=True, null=True)
    estado = models.CharField(max_length=10, verbose_name="Estado", choices=ESTADOS_HISTORIA, default='Activa')
    costo = models.DecimalField(max_digits=50, decimal_places=0, verbose_name='Costo', default=0)
    senales = models.CharField(max_length=2000, verbose_name='Señales', null=True, blank=True)
    estado_reproductivo = models.CharField(max_length=2000, verbose_name='Estado reproductivo', null=True, blank=True)
    numero_partos = models.CharField(max_length=2000, verbose_name='Número de partos', null=True, blank=True)
    tipo_alimento = models.CharField(max_length=2000, verbose_name='Tipo de alimento', null=True, blank=True)
    racion_alimento = models.CharField(max_length=2000, verbose_name='Ración', null=True, blank=True)
    vacunacion = models.CharField(max_length=2000, verbose_name='Vacunación', null=True, blank=True)
    desparasitacion = models.CharField(max_length=2000, verbose_name='Desparasitación', null=True, blank=True)
    heces = models.CharField(max_length=2000, verbose_name='Heces', null=True, blank=True)
    orina = models.CharField(max_length=2000, verbose_name='Orina', null=True, blank=True)
    frecuencia_cardiaca = models.CharField(max_length=2000, verbose_name='F.C.', null=True, blank=True)
    frecuencia_respiratoria = models.CharField(max_length=2000, verbose_name='F.R.', null=True, blank=True)
    temperatura = models.CharField(max_length=2000, verbose_name='T°', null=True, blank=True)
    tiempo_llenado_capilar = models.CharField(max_length=2000, verbose_name='Tllc', null=True, blank=True)
    pulso = models.CharField(max_length=2000, verbose_name='Pulso', null=True, blank=True)
    peso = models.CharField(max_length=2000, verbose_name='Peso', null=True, blank=True)
    actitud = models.CharField(max_length=2000, verbose_name='Actitud', null=True, blank=True)
    estado_corporal = models.CharField(max_length=2000, verbose_name='C/C', null=True, blank=True)
    comentarios_examen_clinico = models.CharField(max_length=4000, verbose_name='Comentarios', null=True, blank=True)
    listado_problemas = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    diagnostico_diferencial = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    diagnostico_presuntivo = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    examenes_laboratorio = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    imagenes_diagnosticas = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    diagnostico_definitivo = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    observaciones_tratamiento = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    pronostico = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    notas = models.CharField(max_length=4000, verbose_name='Descripción', null=True, blank=True)
    motivo_cita = models.ForeignKey('MotivoCita', on_delete=models.CASCADE,null = True)
    descripcion_inyectologia = models.CharField(max_length=4000, verbose_name='Descripción',null=True, blank=True)
    sistema_linfatico = models.CharField(max_length=4000, verbose_name='Sistema linfatico', null=True, blank=True)
    sistema_tegumentario = models.CharField(max_length=4000, verbose_name='Sistema tegumentario', null=True, blank=True)
    sistema_cardiovascular = models.CharField(max_length=4000, verbose_name='Sistema cardiovascular', null=True, blank=True)
    sistema_respiratorio = models.CharField(max_length=4000, verbose_name='Sistema respiratorio', null=True, blank=True)
    sistema_digestivo = models.CharField(max_length=4000, verbose_name='Sistema digestivo', null=True, blank=True)
    sistema_musculoesqueletico = models.CharField(max_length=4000, verbose_name='Sistema musculoesqueletico', null=True, blank=True)
    sistema_nervioso = models.CharField(max_length=4000, verbose_name='Sistema nervioso', null=True, blank=True)
    sistema_urinario = models.CharField(max_length=4000, verbose_name='Sistema urinario', null=True, blank=True)
    sistema_reproductivo = models.CharField(max_length=4000, verbose_name='Sistema reproductivo', null=True, blank=True)
    descripcion_procedimiento = models.CharField(max_length=4000, verbose_name='Descripcion procedimiento', null=True, blank=True)

    def nombre_archivo(self):
        if len(str(self.archivo_zip).split('/')) == 2:
            return str(self.archivo_zip).split('/')[1]
        elif len(str(self.archivo_zip).split('/')) == 1:
            return str(self.archivo_zip).split('/')[0]
        else:
            return str(self.archivo_zip)


class Documento(models.Model):
    file = models.FileField(upload_to=get_upload_to_documento, verbose_name='Archivo')
    fecha_carga = models.DateTimeField(auto_now_add=True)
    historia = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE, null=True, blank=True)
    area_historia = models.CharField(max_length=500, verbose_name='Area', default='Motivo consulta')

    def save(self, *args, **kwargs):
        #print("Save documento!!!!")
        if self.file.url.endswith('.jpg') or self.file.url.endswith('.JPG') or self.file.url.endswith('.JPEG') or self.file.url.endswith('.jpeg'):
            #print(str(self.file) +": "+ str(self.file.size))
            if self.file.size > 1000000:
                self.file = self.compressImage(self.file)
        super(Documento, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def nombre(self):
        if len(str(self.file.name).split('/')) == 4:
            return str(self.file.name).split('/')[3]
        elif len(str(self.file.name).split('/')) == 3:
            return str(self.file.name).split('/')[2]
        elif len(str(self.file.name).split('/')) == 2:
            return str(self.file.name).split('/')[1]
        elif len(str(self.file.name).split('/')) == 1:
            return str(self.file.name).split('/')[0]
        else:
            return str(self.file.name)

    def __str__(self):
        if len(str(self.file).split('/')) == 2:
            return str(self.file).split('/')[1]
        elif len(str(self.file).split('/')) == 1:
            return str(self.file).split('/')[0]
        else:
            return str(self.file)

class MotivoArea(models.Model):
    area_consulta =  models.ForeignKey(Tratamiento, on_delete=models.CASCADE)
    historia_clinica = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE)
    estado = models.CharField(max_length=10, verbose_name="Estado", choices=ESTADOS_HISTORIA, default='Activa')



class MotivoCita(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', unique=True, error_messages={'unique': "Ya existe un tratamiento con este nombre."})
    descripcion = models.CharField(max_length=2000, verbose_name='Descripción')
    precio = models.CharField(max_length=50, verbose_name='Precio', null=True, blank=True,default = 0)

    def __str__(self):
        return self.nombre


class Hospitalizacion(models.Model):
    fecha_hospitalizacion = models.DateField(null=True, blank=True)
    historia_clinica = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    hora_manana = models.CharField(max_length=2000, null=True, blank=True)
    hora_tarde = models.CharField(max_length=2000, null=True, blank=True)
    temperatura_manana = models.CharField(max_length=2000, null=True, blank=True)
    temperatura_tarde = models.CharField(max_length=2000, null=True, blank=True)
    frecuencia_manana = models.CharField(max_length=2000, null=True, blank=True)
    frecuencia_tarde = models.CharField(max_length=2000, null=True, blank=True)
    mucosas_manana = models.CharField(max_length=2000, null=True, blank=True)
    mucosas_tarde = models.CharField(max_length=2000, null=True, blank=True)
    apetito_manana = models.CharField(max_length=2000, null=True, blank=True)
    apetito_tarde = models.CharField(max_length=2000, null=True, blank=True)
    sed_manana = models.CharField(max_length=2000, null=True, blank=True)
    sed_tarde = models.CharField(max_length=2000, null=True, blank=True)
    estado_animo_manana = models.CharField(max_length=2000, null=True, blank=True)
    estado_animo_tarde = models.CharField(max_length=2000, null=True, blank=True)
    consistencia_fecal_manana = models.CharField(max_length=2000, null=True, blank=True)
    consistencia_fecal_tarde = models.CharField(max_length=2000, null=True, blank=True)
    vomito_manana = models.CharField(max_length=2000, null=True, blank=True)
    vomito_tarde = models.CharField(max_length=2000, null=True, blank=True)
    orina_manana = models.CharField(max_length=2000, null=True, blank=True)
    orina_tarde = models.CharField(max_length=2000, null=True, blank=True)
    pronostico_manana = models.CharField(max_length=2000, null=True, blank=True)
    pronostico_tarde = models.CharField(max_length=2000, null=True, blank=True)
    estado = models.CharField(max_length=400, verbose_name='Estado', default='Activa')
    observaciones_manana = models.CharField(max_length=90000, null=True, blank=True)
    observaciones_tarde = models.CharField(max_length=90000, null=True, blank=True)


class Cirugia(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    historia_clinica = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE)
    procedimiento_quirurgico = models.CharField(max_length=2000, null=True, blank=True)
    abordaje = models.CharField(max_length=2000, null=True, blank=True)
    diagnostico_quirurgico = models.CharField(max_length=2000, null=True, blank=True)


class SeguimientoHistoria(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    registrado_por = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comentarios = models.CharField(max_length=90000)