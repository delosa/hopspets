from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.views.generic import *
from django.contrib.auth.models import User
from .models import *
from django.urls import reverse_lazy
from usuarios.models import Usuario
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.http import JsonResponse
from datetime import datetime, timedelta
import decimal
from veteriariaTenant.models import Veterinaria
from clientes.models import Cliente
from tratamientos.models import Tratamiento
from estetica.models import FacturaEstetica, TipoEstetica
from usuarios.models import Usuario
from productos.models import AbonoPago
from mascotas.models import Mascota
from citas.models import HistoriaClinica, MotivoCita, MotivoArea
from caja.models import *
from desparasitacion.models import Desparasitacion, Desparasitante, AplicacionDesparasitacion
from vacunacion.models import Vacunacion, Vacuna, AplicacionVacunacion
from guarderia.models import Guarderia
from itertools import chain
from gastos.models import GastoProducto, GastoMedicamento
from decimal import Decimal
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
import math
from insumos.models import Insumo
import base64
from django.db.models import Count
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q
from categoriasInventario.models import *
from bodegas.models import Bodega
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger


class CrearProveedor(SuccessMessageMixin, CreateView):
    model = Proveedor
    form_class = CrearProveedorForm
    template_name = "registrar_proveedor.html"
    success_url = reverse_lazy('listar_proveedores')
    success_message = "¡El proveedor se agregó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearProveedor, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        proveedor = form.instance
        self.object = form.save()
        return super(CrearProveedor, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearProveedor, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['proveedores'] = True
        return context


class ListarProveedores(ListView):
    model = Proveedor
    template_name = "listar_proveedores.html"

    def get_queryset(self):
        queryset = super(ListarProveedores, self).get_queryset()
        return queryset.order_by('nombre')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarProveedores, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarProveedores, self).get_context_data(**kwargs)
        context['proveedores'] = True

        proveedores = Proveedor.objects.all().order_by('nombre')

        estados_financieros = []

        for proveedor in proveedores:
            estado_financiero = 'Al día'
            cuotas_pendientes = CuotaGasto.objects.filter(gasto__proveedor=proveedor, estado_pago='Pendiente')

            if cuotas_pendientes:
                estado_financiero = 'En deuda'

            for cuota in cuotas_pendientes:
                if cuota.fecha_limite <= datetime.now().date():
                    estado_financiero = 'Moroso'

            estados_financieros.append(estado_financiero)

        context['proveedor'] = proveedores
        context['estados_financieros'] = estados_financieros
        combined_list = zip(proveedores, estados_financieros)
        context['combined_list'] = combined_list

        return context


class EditarProveedor(SuccessMessageMixin, UpdateView):
    model = Proveedor
    form_class = EditarClienteForm
    template_name = "registrar_proveedor.html"
    success_url = reverse_lazy('listar_proveedores')
    success_message = "¡El proveedor se modificó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarProveedor, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarProveedor, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['proveedores'] = True
        return context

class DetalleProveedor(DetailView):
    model = Proveedor
    template_name = "detalle_proveedor.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleProveedor, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleProveedor, self).get_context_data(**kwargs)
        context['proveedores'] = True
        context['Proveedor'] = True
        gastos_proveedor = Gasto.objects.filter(proveedor=self.kwargs['pk']).order_by('-fecha','-id')
        context['gastos_proveedor'] = gastos_proveedor
        motivos_gastos = []

        abonos = []

        for gasto in gastos_proveedor:
            motivos = []

            if GastoProducto.objects.filter(gasto__id=gasto.id):
                motivos.append("Producto")

            if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                motivos.append("Medicamento")

            if GastoVacuna.objects.filter(gasto__id=gasto.id):
                motivos.append("Vacuna")

            if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                motivos.append("Desparasitante")

            if GastoInsumo.objects.filter(gasto__id=gasto.id):
                motivos.append("Insumo")

            if GastoServicio.objects.filter(gasto__id=gasto.id):
                motivos.append("Servicio")

            if GastoOtro.objects.filter(gasto__id=gasto.id):
                motivos.append("Otro")

            if GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                for gasto in GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                    if not gasto.elemento.categoria.nombre in motivos:
                        motivos.append(gasto.elemento.categoria.nombre)

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos

        estado_financiero = 'Al día'
        cuotas_pendientes = CuotaGasto.objects.filter(gasto__proveedor=self.kwargs['pk'],estado_pago='Pendiente')

        if cuotas_pendientes:
            estado_financiero = 'En deuda'

        for cuota in cuotas_pendientes:
            if cuota.fecha_limite <= datetime.now().date():
                estado_financiero = 'Moroso'

        context['estado_financiero'] = estado_financiero

        return context


class CrearProducto(SuccessMessageMixin, CreateView):
    model = Producto
    form_class = CrearProductoForm
    template_name = "registrar_producto.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡Producto creado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearProducto, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        producto = form.instance
        self.object = form.save()

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro', tipo_elemento='Producto',
                                                   id_elemento=producto.id,responsable=responsable)
        historia_transaccion.save()

        return super(CrearProducto, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearProducto, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_inventario'] = True
        return context


class ListarProductos(ListView):
    model = Producto
    template_name = "listar_productos.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarProductos, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(ListarProductos, self).get_queryset()
        return queryset.order_by('nombre')

    def get_context_data(self, **kwargs):
        context = super(ListarProductos, self).get_context_data(**kwargs)
        context['menu_inventario'] = True

        qs1 = Producto.objects.all()
        qs2 = PaqueteProducto.objects.all()
        qs3 = Vacuna.objects.all()
        qs4 = Desparasitante.objects.all()

        for q in qs1:
            q.tipo_elemento = 'Producto'

        for q in qs2:
            q.tipo_elemento = 'Paquete'

        for q in qs3:
            q.tipo_elemento = 'Vacuna'

        for q in qs4:
            q.tipo_elemento = 'Desparasitante'

        total_productos = sorted(chain(qs1, qs2, qs3, qs4), key=lambda instance: instance.nombre.upper())
        context['total_productos'] = total_productos
        return context


class EditarProducto(SuccessMessageMixin, UpdateView):
    model = Producto
    form_class = EditarProductoForm
    template_name = "registrar_producto.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡Producto modificadó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarProducto, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        producto = form.instance

        mi_producto = get_object_or_404(Producto, id=producto.id)

        if (mi_producto.cantidad != producto.cantidad) or (mi_producto.cantidad_unidad != producto.cantidad_unidad):
            cantidad_nueva = producto.cantidad
            producto.cantidad_total_unidades = cantidad_nueva * producto.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',
                                                   tipo_elemento='Producto', id_elemento=producto.id, responsable=responsable)
        historia_transaccion.save()

        self.object = form.save()
        return super(EditarProducto, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarProducto, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        return context


class RegistrarPago(SuccessMessageMixin, CreateView):
    model = Pago
    form_class = RegistrarPagoProductoForm
    template_name = "registrar_pago.html"
    success_url = reverse_lazy('listar_pagos')
    success_message = "¡El Pago se registró con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RegistrarPago, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        pago = form.instance
        self.object = form.save()
        return super(RegistrarPago, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(RegistrarPago, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_pagos'] = True
        return context


class ListarPagos(ListView):
    model = Pago
    template_name = "listar_pagos.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarPagos, self).get_queryset()

        abonos = []

        if self.kwargs['estado_pago'] == 'pagados':
            lista_pagos = Pago.objects.filter(estado_pago='Pagado').order_by('-fecha', '-id')
            abonos = AbonoPago.objects.all()
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_pagos = Pago.objects.filter(estado_pago='Pendiente').order_by('-fecha', '-id')
            abonos = AbonoPago.objects.filter(pago__estado_pago='Pendiente')
        elif self.kwargs['estado_pago'] == 'todos':
            lista_pagos = Pago.objects.all().order_by('-fecha', '-id')
            abonos = AbonoPago.objects.all()

        hoy = datetime.now().date()

        if self.kwargs['fecha_pago'] == 'hoy':
            lista_pagos = lista_pagos.filter(fecha=hoy)
            abonos = abonos.filter(fecha__date=hoy)
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                if abono.pago in lista_pagos:
                    abonos = abonos.exclude(id=abono.id)
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
                for pago in lista_pagos:
                    if pago.id == abono.pago.id:
                        pago.fecha_abono = abono.fecha
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'semana':
            esta_semana = hoy.isocalendar()[1]
            lista_pagos = lista_pagos.filter(fecha__week=esta_semana, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__week=esta_semana, fecha__date__year=hoy.year)
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                for pago in lista_pagos:
                    if pago.id == abono.pago.id and pago.fecha == abono.fecha.date():
                        if pago.estado_pago == 'Pagado':
                            pago.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'mes':
            lista_pagos = lista_pagos.filter(fecha__month=hoy.month, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__month=hoy.month, fecha__date__year=hoy.year)
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        else:
            lista_pagos = []
            lista_completa = []

        queryset = lista_completa

        return queryset

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagos, self).get_context_data(**kwargs)
        context['menu_pagos'] = True

        abonos = []

        if self.kwargs['estado_pago'] == 'pagados':
            lista_pagos = Pago.objects.filter(estado_pago='Pagado').order_by('-fecha', '-id')
            abonos = AbonoPago.objects.all()
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_pagos = Pago.objects.filter(estado_pago='Pendiente').order_by('-fecha', '-id')
            abonos = AbonoPago.objects.filter(pago__estado_pago='Pendiente')
        elif self.kwargs['estado_pago'] == 'todos':
            lista_pagos = Pago.objects.all().order_by('-fecha', '-id')
            abonos = AbonoPago.objects.all()

        hoy = datetime.now().date()

        if self.kwargs['fecha_pago'] == 'hoy':
            lista_pagos = lista_pagos.filter(fecha=hoy)
            abonos = abonos.filter(fecha__date=hoy)
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                if abono.pago in lista_pagos:
                    abonos = abonos.exclude(id=abono.id)
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
                for pago in lista_pagos:
                    if pago.id == abono.pago.id:
                        pago.fecha_abono = abono.fecha
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'semana':
            esta_semana = hoy.isocalendar()[1]
            lista_pagos = lista_pagos.filter(fecha__week=esta_semana, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__week=esta_semana, fecha__date__year=hoy.year)
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                for pago in lista_pagos:
                    if pago.id == abono.pago.id and pago.fecha == abono.fecha.date():
                        if pago.estado_pago == 'Pagado':
                            pago.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'mes':
            lista_pagos = lista_pagos.filter(fecha__month=hoy.month, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__month=hoy.month, fecha__date__year=hoy.year)
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        else:
            lista_pagos = []
            lista_completa = []

        paginator = Paginator(lista_completa, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            lista_completa = paginator.page(page)
        except PageNotAnInteger:
            lista_completa = paginator.page(1)
        except EmptyPage:
            lista_completa = paginator.page(paginator.num_pages)

        motivos_pagos = []

        for pago in lista_completa:
            motivos = []

            if pago.tipo == 'Abono':
                motivos.append("Abono")
            else:

                if PagoGuarderia.objects.filter(pago__id=pago.id):
                    motivos.append("Guardería")

                if PagoProducto.objects.filter(pago__id=pago.id):
                    motivos.append("Producto")

                if PagoMedicamento.objects.filter(pago__id=pago.id):
                    motivos.append("Medicamento")

                if PagoVacuna.objects.filter(pago__id=pago.id):
                    motivos.append("Vacuna")

                if PagoDesparasitante.objects.filter(pago__id=pago.id):
                    motivos.append("Desparasitante")

                if PagoTratamiento.objects.filter(pago__id=pago.id):
                    motivos.append("Cita médica")

                if PagoEstetica.objects.filter(pago__id=pago.id):
                    motivos.append("Estética")

                if PagoVacunacion.objects.filter(pago__id=pago.id):
                    motivos.append("Vacunación")

                if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                    motivos.append("Desparasitación")

                if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                    motivos.append("Procedimiento")

                if PagoOtro.objects.filter(pago__id=pago.id):
                    motivos.append("Otro")

                if PagoElementoInventario.objects.filter(pago__id=pago.id):
                    for pago in PagoElementoInventario.objects.filter(pago__id=pago.id):
                        if not pago.elemento.categoria.nombre in motivos:
                            motivos.append(pago.elemento.categoria.nombre)

            motivos_pagos.append(motivos)

        context['motivos_pagos'] = motivos_pagos
        context['lista_pagos'] = lista_completa
        context['estado_pago'] = self.kwargs['estado_pago']
        context['fecha_pago'] = self.kwargs['fecha_pago']
        # context['abonos'] = AbonoPago.objects.all()

        return context


class ListarPagosMes(ListView):
    model = Pago
    template_name = "listar_pagos.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarPagosMes, self).get_queryset()

        mes = self.kwargs['mes']
        año = self.kwargs['ano']

        if self.kwargs['estado_pago'] == 'pagados':
            lista_pagos = Pago.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pagado').order_by(
                '-fecha', '-id')
            abonos = AbonoPago.objects.filter(fecha__date__month=mes, fecha__date__year=año).exclude(
                pago__estado_pago='Anulada').order_by('-fecha', '-id')
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                for pago in lista_pagos:
                    if pago.id == abono.pago.id and pago.fecha == abono.fecha.date():
                        if pago.estado_pago == 'Pagado':
                            pago.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_pagos = Pago.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pendiente').order_by(
                '-fecha', '-id')
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            lista_completa = lista_pagos
        elif self.kwargs['estado_pago'] == 'todos':
            lista_pagos = Pago.objects.filter(fecha__month=mes, fecha__year=año).order_by('-fecha', '-id')
            abonos = AbonoPago.objects.filter(fecha__date__month=mes, fecha__date__year=año).order_by('-fecha', '-id')
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                for pago in lista_pagos:
                    if pago.id == abono.pago.id and pago.fecha == abono.fecha.date():
                        if pago.estado_pago == 'Pagado':
                            pago.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)

        queryset = lista_completa

        return queryset

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagosMes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagosMes, self).get_context_data(**kwargs)
        context['menu_pagos'] = True

        mes = self.kwargs['mes']
        año = self.kwargs['ano']

        if self.kwargs['estado_pago'] == 'pagados':
            lista_pagos = Pago.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pagado').order_by(
                '-fecha', '-id')
            abonos = AbonoPago.objects.filter(fecha__date__month=mes, fecha__date__year=año).exclude(
                pago__estado_pago='Anulada').order_by('-fecha', '-id')
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                for pago in lista_pagos:
                    if pago.id == abono.pago.id and pago.fecha == abono.fecha.date():
                        if pago.estado_pago == 'Pagado':
                            pago.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_pagos = Pago.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pendiente').order_by(
                '-fecha', '-id')
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            lista_completa = lista_pagos
        elif self.kwargs['estado_pago'] == 'todos':
            lista_pagos = Pago.objects.filter(fecha__month=mes, fecha__year=año).order_by('-fecha', '-id')
            abonos = AbonoPago.objects.filter(fecha__date__month=mes, fecha__date__year=año).order_by('-fecha', '-id')
            for pago in lista_pagos:
                pago.tipo = 'Pago'
            for abono in abonos:
                for pago in lista_pagos:
                    if pago.id == abono.pago.id and pago.fecha == abono.fecha.date():
                        if pago.estado_pago == 'Pagado':
                            pago.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_pagos, abonos), key=lambda instance: instance.fecha, reverse=True)

        paginator = Paginator(lista_completa, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            lista_completa = paginator.page(page)
        except PageNotAnInteger:
            lista_completa = paginator.page(1)
        except EmptyPage:
            lista_completa = paginator.page(paginator.num_pages)

        motivos_pagos = []

        for pago in lista_completa:
            motivos = []

            if pago.tipo == 'Abono':
                motivos.append("Abono")
            else:
                if PagoGuarderia.objects.filter(pago__id=pago.id):
                    motivos.append("Guardería")

                if PagoProducto.objects.filter(pago__id=pago.id):
                    motivos.append("Producto")

                if PagoMedicamento.objects.filter(pago__id=pago.id):
                    motivos.append("Medicamento")

                if PagoVacuna.objects.filter(pago__id=pago.id):
                    motivos.append("Vacuna")

                if PagoDesparasitante.objects.filter(pago__id=pago.id):
                    motivos.append("Desparasitante")

                if PagoTratamiento.objects.filter(pago__id=pago.id):
                    motivos.append("Cita médica")

                if PagoEstetica.objects.filter(pago__id=pago.id):
                    motivos.append("Estética")

                if PagoVacunacion.objects.filter(pago__id=pago.id):
                    motivos.append("Vacunación")

                if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                    motivos.append("Desparasitación")

                if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                    motivos.append("Procedimiento")

                if PagoOtro.objects.filter(pago__id=pago.id):
                    motivos.append("Otro")

                if PagoElementoInventario.objects.filter(pago__id=pago.id):
                    for pago in PagoElementoInventario.objects.filter(pago__id=pago.id):
                        if not pago.elemento.categoria.nombre in motivos:
                            motivos.append(pago.elemento.categoria.nombre)

            motivos_pagos.append(motivos)

        context['motivos_pagos'] = motivos_pagos
        context['lista_pagos'] = lista_completa
        context['estado_pago'] = self.kwargs['estado_pago']
        context['mes'] = self.kwargs['mes']
        context['ano'] = self.kwargs['ano']
        context['fecha_pago'] = 'mes'
        context['abonos'] = AbonoPago.objects.all()

        return context


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


@login_required
def crearPago(request):
    if request.method == 'POST':

        form_pago = RegistrarPagoForm(request.POST)

        """form_pago_producto = RegistrarPagoProductoForm(request.POST)
        if form_pago.is_valid() and form_pago_producto.is_valid():
            obj_datos_basicos = form_pago.save()
            obj_datos_educacion = form_pago_producto.save()

            return redirect('listar_pagos')
        else:
            print('Por favor verificar los campos en rojo del formulario')"""
        return redirect('listar_pagos')
    else:
        form_pago = RegistrarPagoForm()
        all_productos = Producto.objects.filter(estado=True,bodega=1).exclude(tipo_uso='Interno').distinct('codigo')
        array_productos = json.dumps(list(
            Producto.objects.filter(estado=True,bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'codigo', 'tipo_producto',
                                                             'cantidad', 'precio_venta', 'cantidad_por_unidad',
                                                             'precio_por_unidad', 'cantidad_total_unidades',
                                                             'unidad_medida', 'cantidad_unidad','fecha_vencimiento','fecha_vencimiento')), cls=DjangoJSONEncoder)
        array_tratamientos = json.dumps(list(Tratamiento.objects.all().values_list('id', 'nombre', 'nombre', 'precio')))
        array_motivo_cita = json.dumps(
            list(MotivoCita.objects.all().values_list('id', 'nombre', 'nombre', 'precio')))
        array_area_consulta = json.dumps(
            list(MotivoArea.objects.all().values_list('area_consulta', 'historia_clinica', 'estado')),
            cls=DecimalEncoder)

        array_medicamentos = json.dumps(list(
            Medicamento.objects.filter(estado='Activo',bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad',
                                                                    'precio_venta', 'cantidad_por_unidad',
                                                                    'precio_por_unidad', 'unidad_medida',
                                                                    'cantidad_unidad', 'cantidad_total_unidades',
                                                                    'codigo_inventario','fecha_vencimiento','fecha_vencimiento')), cls=DjangoJSONEncoder)
        array_vacunas = json.dumps(list(
            Vacuna.objects.filter(estado='Activa',bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad', 'precio_venta',
                                                               'cantidad_por_unidad', 'precio_por_unidad',
                                                               'cantidad_total_unidades', 'unidad_medida',
                                                               'cantidad_unidad', 'codigo_inventario','fecha_vencimiento','fecha_vencimiento')), cls=DjangoJSONEncoder)
        array_desparasitante = json.dumps(list(
            Desparasitante.objects.filter(estado='Activo',bodega=1).exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad',
                                                                       'precio_venta', 'cantidad_por_unidad',
                                                                       'precio_por_unidad', 'cantidad_total_unidades',
                                                                       'unidad_medida', 'cantidad_unidad',
                                                                       'codigo_inventario','fecha_vencimiento','fecha_vencimiento')), cls=DjangoJSONEncoder)


        productos_repetidos = []
        for element in list(Producto.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo').annotate(cantidad=Count('codigo'))):
            if element['cantidad'] > 1:
                productos_repetidos.append(element['codigo'])

        productos_por_codigo = json.dumps(list(chain(Producto.objects.distinct('codigo').filter(estado=True,bodega=1).exclude(codigo__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo','id','estado'),Producto.objects.filter(codigo__isnull=True,estado=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre', 'codigo','id','estado'))), cls=DecimalEncoder)

        medicamentos_por_codigo = json.dumps(list(chain(
            Medicamento.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Medicamento.objects.filter(estado='Activo',codigo_inventario__isnull=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario', 'id', 'estado'))),
                                          cls=DecimalEncoder)

        medicamentos_repetidos = []
        for element in list(Medicamento.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                medicamentos_repetidos.append(element['codigo_inventario'])

        vacunas_por_codigo = json.dumps(list(chain(
            Vacuna.objects.distinct('codigo_inventario').filter(estado='Activa',bodega=1).exclude(codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre','codigo_inventario','id', 'estado'),
            Vacuna.objects.filter(estado='Activa',codigo_inventario__isnull=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre','codigo_inventario','id', 'estado'))), cls=DecimalEncoder)

        vacunas_repetidos = []
        for element in list(
                Vacuna.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                vacunas_repetidos.append(element['codigo_inventario'])

        desparasitantes_repetidos = []
        for element in list(
                Desparasitante.objects.filter(bodega=1).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                desparasitantes_repetidos.append(element['codigo_inventario'])

        desparasitantes_por_codigo = json.dumps(list(chain(
            Desparasitante.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
                codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Desparasitante.objects.filter(estado='Activo', codigo_inventario__isnull=True,bodega=1).exclude(tipo_uso='Interno').values_list('nombre',
                                                                                                       'codigo_inventario',
                                                                                                       'id',
                                                                                                       'estado'))),
            cls=DecimalEncoder)

        lista_categorias = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id','nombre')))

        return render(request, 'registrar_pago.html', {
            'form_pago': form_pago,
            'all_productos': all_productos,
            'array': array_productos,
            'array_tratamientos': array_tratamientos,
            'menu_pagos': True,
            'array_medicamentos': array_medicamentos,
            'array_vacunas': array_vacunas,
            'array_desparasitante': array_desparasitante,
            'array_motivo_cita': array_motivo_cita,
            'array_area_consulta': array_area_consulta,
            'array_id_productos': productos_repetidos,
            'productos_por_codigo': productos_por_codigo,
            'medicamentos_por_codigo': medicamentos_por_codigo,
            'array_id_medicamentos': medicamentos_repetidos,
            'vacunas_por_codigo': vacunas_por_codigo,
            'array_id_vacunas': vacunas_repetidos,
            'desparasitantes_por_codigo': desparasitantes_por_codigo,
            'array_id_desparasitantes': desparasitantes_repetidos,
            'lista_categorias': lista_categorias
        })


@login_required
def registrarPago(request):
    req = json.loads(request.body.decode('utf-8'))

    tipo_comprobante = str(req['tipo_comprobante'])
    proveedor_nota_debito = str(req['proveedor_nota_debito'])

    responsable = get_object_or_404(Usuario, id=request.user.id)

    if tipo_comprobante == 'Nota Débito':
        proveedor = get_object_or_404(Proveedor, id=proveedor_nota_debito)
        nuevo_pago = Pago(fecha=req['fecha'], comentarios=req['comentarios'], total_factura=req['total'],
                          total_impuesto=req['impuesto'], sub_total=req['subtotal'], descuento=req['descuento'],
                          responsable=responsable, tipo_comprobante=tipo_comprobante,proveedor_nota_debito=proveedor)
        nuevo_pago.save()
    else:
        if req['cliente'] == 0:
            nuevo_pago = Pago(fecha=req['fecha'], comentarios=req['comentarios'], total_factura=req['total'],
                              total_impuesto=req['impuesto'], sub_total=req['subtotal'], descuento=req['descuento'],
                              responsable=responsable,tipo_comprobante=tipo_comprobante)
            nuevo_pago.save()
        else:
            cliente = get_object_or_404(Cliente, pk=req['cliente'])
            nuevo_pago = Pago(cliente=cliente, fecha=req['fecha'],
                              comentarios=req['comentarios'], total_factura=req['total'], total_impuesto=req['impuesto'],
                              sub_total=req['subtotal'], descuento=req['descuento'], responsable=responsable,tipo_comprobante=tipo_comprobante)
            nuevo_pago.save()

    nuevo_pago.forma_pago = req['forma_pago']

    if req['forma_pago'] == 'Debito':
        nuevo_pago.tipo_pago_debito = req['medio_pago_debito']
        """caja_on = Caja.objects.filter(estado= True)
        if caja_on:
            now = datetime.now()
            x = datetime(now.year, now.month, now.day)
            z = datetime.strptime(nuevo_pago.fecha, "%Y-%m-%d")
            y = x + timedelta(days=1)
            if z >= x and z < y:
                caja_pago = CajaPago(caja = caja_on[0], pago=nuevo_pago)
                caja_pago.save()"""

    if req['forma_pago'] == 'Credito':
        nuevo_pago.tipo_pago_credito = req['medio_pago_credito']
        nuevo_pago.numero_cuotas_credito = req['numero_cuotas']
        nuevo_pago.saldo = req['total']
        nuevo_pago.estado_pago = 'Pendiente'

        for cuota in req['cuotas']:
            mi_cuota = CuotaPago(pago=nuevo_pago, fecha_limite=cuota['fecha'], valor_cuota=cuota['abono'],
                                 saldo_cuota=cuota['abono'])
            if mi_cuota.fecha_limite == nuevo_pago.fecha:
                abono_pago = AbonoPago(pago=nuevo_pago, valor_abonado=mi_cuota.saldo_cuota,
                                       medio_pago=req['medio_pago_credito'])
                abono_pago.save()
                nuevo_pago.saldo = nuevo_pago.saldo - float(mi_cuota.saldo_cuota)
                nuevo_pago.save()
                mi_cuota.valor_pagado = float(mi_cuota.saldo_cuota)
                mi_cuota.saldo_cuota = 0
                mi_cuota.estado_pago = 'Pagada'
                mi_cuota.fecha_pago = mi_cuota.fecha_limite
                mi_cuota.save()
                """caja_on = Caja.objects.filter(estado= True)
                if caja_on:
                    abono_caja_pago = CajaAbonoPago(caja = caja_on[0], abono_pago=mi_cuota)
                    abono_caja_pago.save()"""

            else:
                mi_cuota.save()

    messages.success(request, 'Venta registrada con exito!')

    nuevo_pago.save()

    for producto in req['productos']:

        if producto['tipo'] == 'Producto':
            mi_producto = get_object_or_404(Producto, pk=producto['id'])
            # print(producto)
            cantidad = float(producto['cantidad'])
            pago_producto = PagoProducto(producto=mi_producto, pago=nuevo_pago, cantidad=cantidad,
                                         total=float(producto['costo_total']))

            # print(pago_producto)
            # mi_producto.cantidad = mi_producto.cantidad - cantidad
            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_producto.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_producto.cantidad = float(mi_producto.cantidad) - cantidad

                mi_producto.cantidad_total_unidades = float(mi_producto.cantidad_total_unidades) - (
                            cantidad * float(mi_producto.cantidad_unidad))

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                           id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                # print(mi_producto.cantidad_total_unidades,mi_producto.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_producto.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_producto.cantidad_total_unidades = float(mi_producto.cantidad_total_unidades) - cantidad

                mi_producto.cantidad = math.floor(
                    float(mi_producto.cantidad_total_unidades) / float(mi_producto.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_producto.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                           id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                # print(mi_producto.cantidad_total_unidades,mi_producto.cantidad,"a granel")
            pago_producto.save()
            mi_producto.save()

        elif producto['tipo'] == 'Medicamento':
            mi_medicamento = get_object_or_404(Medicamento, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_medicamento = PagoMedicamento(medicamento=mi_medicamento, pago=nuevo_pago, cantidad=cantidad,
                                               total=float(producto['costo_total']))
            # pago_medicamento.save()
            # mi_medicamento.cantidad = mi_medicamento.cantidad - cantidad
            # mi_medicamento.save()
            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_medicamento.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_medicamento.cantidad = float(mi_medicamento.cantidad) - cantidad

                mi_medicamento.cantidad_total_unidades = float(mi_medicamento.cantidad_total_unidades) - (
                            cantidad * float(mi_medicamento.cantidad_unidad))

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                           id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                # print(mi_medicamento.cantidad_total_unidades,mi_medicamento.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_medicamento.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_medicamento.cantidad_total_unidades = float(mi_medicamento.cantidad_total_unidades) - cantidad

                mi_medicamento.cantidad = math.floor(
                    float(mi_medicamento.cantidad_total_unidades) / float(mi_medicamento.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_medicamento.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                           id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                # print(mi_medicamento.cantidad_total_unidades,mi_medicamento.cantidad,"a granel")

            pago_medicamento.save()
            mi_medicamento.save()

            # historia_transaccion.save()

        elif producto['tipo'] == 'Vacuna':
            mi_vacuna = get_object_or_404(Vacuna, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_vacuna = PagoVacuna(vacuna=mi_vacuna, pago=nuevo_pago, cantidad=cantidad,
                                     total=float(producto['costo_total']))

            # mi_vacuna.cantidad = mi_vacuna.cantidad - cantidad

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_vacuna.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_vacuna.cantidad = float(mi_vacuna.cantidad) - cantidad

                mi_vacuna.cantidad_total_unidades = float(mi_vacuna.cantidad_total_unidades) - (
                            cantidad * float(mi_vacuna.cantidad_unidad))

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                           id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                # print(mi_vacuna.cantidad_total_unidades,mi_vacuna.cantidad,"unidad" )

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_vacuna.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_vacuna.cantidad_total_unidades = float(mi_vacuna.cantidad_total_unidades) - cantidad

                mi_vacuna.cantidad = math.floor(float(mi_vacuna.cantidad_total_unidades) / float(mi_vacuna.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_vacuna.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                           id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                # print(mi_vacuna.cantidad_total_unidades,mi_vacuna.cantidad,"a granel")

            pago_vacuna.save()
            mi_vacuna.save()

        elif producto['tipo'] == 'Desparasitante':
            mi_desparasitante = get_object_or_404(Desparasitante, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_desparasitante = PagoDesparasitante(desparasitante=mi_desparasitante, pago=nuevo_pago,
                                                     cantidad=cantidad,
                                                     total=float(producto['costo_total']))

            # mi_desparasitante.cantidad = mi_desparasitante.cantidad - cantidad

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_desparasitante.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_desparasitante.cantidad = float(mi_desparasitante.cantidad) - cantidad

                mi_desparasitante.cantidad_total_unidades = float(mi_desparasitante.cantidad_total_unidades) - (
                            cantidad * float(mi_desparasitante.cantidad_unidad))

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                           id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                print(mi_desparasitante.cantidad_total_unidades, mi_desparasitante.cantidad, "unidad")

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_desparasitante.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_desparasitante.cantidad_total_unidades = float(mi_desparasitante.cantidad_total_unidades) - cantidad

                mi_desparasitante.cantidad = math.floor(
                    float(mi_desparasitante.cantidad_total_unidades) / float(mi_desparasitante.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_desparasitante.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                           id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                print(mi_desparasitante.cantidad_total_unidades, mi_desparasitante.cantidad, "a granel")

            mi_desparasitante.save()
            pago_desparasitante.save()

        else:
            mi_elemento = get_object_or_404(ElementoCategoriaInventario, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_elemento = PagoElementoInventario(elemento=mi_elemento, pago=nuevo_pago,
                                                     cantidad=cantidad,
                                                     total=float(producto['costo_total']))

            # mi_desparasitante.cantidad = mi_desparasitante.cantidad - cantidad

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_elemento.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_elemento.cantidad = float(mi_elemento.cantidad) - cantidad

                mi_elemento.cantidad_total_unidades = float(mi_elemento.cantidad_total_unidades) - (
                            cantidad * float(mi_elemento.cantidad_unidad))

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                print(mi_elemento.cantidad_total_unidades, mi_elemento.cantidad, "unidad")

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_elemento.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * float(producto['costo_unitario'])

                mi_elemento.cantidad_total_unidades = float(mi_elemento.cantidad_total_unidades) - cantidad

                mi_elemento.cantidad = math.floor(
                    float(mi_elemento.cantidad_total_unidades) / float(mi_elemento.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_elemento.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id,responsable=responsable)
                historia_transaccion.save()
                print(mi_elemento.cantidad_total_unidades, mi_elemento.cantidad, "a granel")

            mi_elemento.save()
            pago_elemento.save()

    for vacunacion in req['vacunaciones']:
        mi_vacunacion = get_object_or_404(Vacunacion, pk=vacunacion['id'])
        pago_vacunacion = PagoVacunacion(vacunacion=mi_vacunacion, pago=nuevo_pago, total=float(vacunacion['costo']))
        pago_vacunacion.save()
        mi_vacunacion.costo = vacunacion['costo']
        mi_vacunacion.estado = 'Pagada'
        mi_vacunacion.save()

    for desparasitacion in req['desparasitaciones']:
        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=desparasitacion['id'])
        pago_desparasitacion = PagoDesparasitacion(desparasitacion=mi_desparasitacion, pago=nuevo_pago,
                                                   total=float(desparasitacion['costo']))
        pago_desparasitacion.save()
        mi_desparasitacion.costo = desparasitacion['costo']
        mi_desparasitacion.estado = 'Pagada'
        mi_desparasitacion.save()

    for estetica in req['esteticas']:
        mi_estetica = get_object_or_404(FacturaEstetica, pk=estetica['id'])
        pago_estetica = PagoEstetica(estetica=mi_estetica, pago=nuevo_pago, total=float(estetica['valor_total']))
        pago_estetica.save()
        mi_estetica.saldo = 0
        mi_estetica.estado = 'Pagada'
        mi_estetica.save()

    for tratamiento in req['tratamientos']:

        if tratamiento['nombre_tratamiento'] == "Motivo Cita":
            mi_tratamiento = get_object_or_404(HistoriaClinica, pk=tratamiento['id'])
            pago_tratamiento = PagoTratamiento(tratamiento=mi_tratamiento, pago=nuevo_pago,
                                               total=float(tratamiento['costo']))
            pago_tratamiento.save()
            mi_tratamiento.costo = tratamiento['costo']
            mi_tratamiento.estado = 'Pagada'
            # print(str(pago_tratamiento),str(mi_tratamiento))
            mi_tratamiento.save()
        if tratamiento['nombre_tratamiento'] == "Area Consulta":
            mi_tratamiento = get_object_or_404(HistoriaClinica, pk=tratamiento['id'])
            mi_area = get_object_or_404(Tratamiento, pk=tratamiento['motivo_cita'])
            motivo_cita = get_object_or_404(MotivoArea, historia_clinica=mi_tratamiento, area_consulta=mi_area)
            pago_area = PagoAreaConsulta(area_consulta=motivo_cita, pago=nuevo_pago, total=float(tratamiento['costo']))
            pago_area.save()
            motivo_cita.estado = 'Pagada'
            motivo_cita.save()
            # print(str(pago_area),str(motivo_cita))

    for guarderia in req['guarderias']:
        mi_guarderia = get_object_or_404(Guarderia, pk=guarderia['id'])
        pago_guarderia = PagoGuarderia(guarderia=mi_guarderia, pago=nuevo_pago, total=float(guarderia['costo']))
        pago_guarderia.save()
        mi_guarderia.estado = 'Pagada'
        mi_guarderia.save()

    for otro in req['otros']:
        pago_otro = PagoOtro(descripcion=otro['nombre'], pago=nuevo_pago, total=float(otro['costo_total']))
        pago_otro.save()

    data = {
        'registrado': True,
        'id_pago': nuevo_pago.id
    }

    return JsonResponse(data)


class DetallePago(DetailView):
    model = Pago
    template_name = "detalle_pago.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetallePago, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetallePago, self).get_context_data(**kwargs)
        context['menu_pagos'] = True
        # print(PagoAreaConsulta.objects.filter(pago = self.kwargs['pk'])[0].area_consulta.area_consulta)
        context['areas_consulta_pago'] = PagoAreaConsulta.objects.filter(pago=self.kwargs['pk'])
        context['productos_pago'] = PagoProducto.objects.filter(pago=self.kwargs['pk'])
        context['medicamentos_pago'] = PagoMedicamento.objects.filter(pago=self.kwargs['pk'])
        context['vacunas_pago'] = PagoVacuna.objects.filter(pago=self.kwargs['pk'])
        context['desparasitantes_pago'] = PagoDesparasitante.objects.filter(pago=self.kwargs['pk'])
        context['elementos_inventario_pago'] = PagoElementoInventario.objects.filter(pago=self.kwargs['pk'])
        context['tratamientos_pago'] = PagoTratamiento.objects.filter(pago=self.kwargs['pk'])
        context['esteticas_pago'] = PagoEstetica.objects.filter(pago=self.kwargs['pk'])
        context['vacunaciones_pago'] = PagoVacunacion.objects.filter(pago=self.kwargs['pk'])
        context['desparasitaciones_pago'] = PagoDesparasitacion.objects.filter(pago=self.kwargs['pk'])
        context['guarderias_pago'] = PagoGuarderia.objects.filter(pago=self.kwargs['pk'])
        context['otros_pago'] = PagoOtro.objects.filter(pago=self.kwargs['pk'])
        context['vacunaciones_aplicadas'] = AplicacionVacunacion.objects.all()
        context['desparasitaciones_aplicadas'] = AplicacionDesparasitacion.objects.all()
        context['abonos'] = AbonoPago.objects.filter(pago=self.kwargs['pk'])
        context['medios_pago'] = MedioPago.objects.filter(pago=self.kwargs['pk'])

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        return context


def validate_username(request):
    id_cliente = request.GET.get('id_cliente', None)
    print(id_cliente)
    citas_estetica_cliente = FacturaEstetica.objects.filter(mascota__cliente__id=id_cliente, estado='Activa',
                                                            estado_de_atencion='Atendida')
    mascotas_cliente = Mascota.objects.filter(cliente__id=id_cliente)
    tipos_baño = TipoEstetica.objects.all()
    tratamientos_cliente = HistoriaClinica.objects.filter(mascota__cliente__id=id_cliente, estado='Activa')
    array_all_history = []
    for element in tratamientos_cliente:
        mascota_in_historia = get_object_or_404(Mascota, pk=element.mascota.pk)
        elements_motivo_area = MotivoArea.objects.filter(historia_clinica=element, estado="Activa")
        # if not elements_motivo_area:
        history_element = {
            "id": element.pk,
            'fecha': str(element.fecha),
            "motivo": element.motivo,
            "mascota": element.mascota.pk,
            "nombre_mascota": mascota_in_historia.nombre,
            "nombre_tratamiento": "Motivo Cita",
            'motivo_cita': element.motivo_cita.pk,
            'motivo_cita_nombre': element.motivo_cita.nombre,
            "costo": element.motivo_cita.precio,
            "estado": element.estado,
        }
        array_all_history.append(history_element)
    tratamientos_cliente_area = HistoriaClinica.objects.filter(mascota__cliente__id=id_cliente)
    for element in tratamientos_cliente_area:
        mascota_in_historia = get_object_or_404(Mascota, pk=element.mascota.pk)
        elements_motivo_area = MotivoArea.objects.filter(historia_clinica=element, estado="Activa")
        if elements_motivo_area:
            for item in elements_motivo_area:
                find_item = get_object_or_404(Tratamiento, pk=item.area_consulta.pk)
                area_consulta = {
                    "id": element.pk,
                    'fecha': str(element.fecha),
                    "motivo": element.motivo,
                    "mascota": element.mascota.pk,
                    "nombre_mascota": mascota_in_historia.nombre,
                    "nombre_tratamiento": "Area Consulta",
                    'motivo_cita': find_item.pk,
                    'motivo_cita_nombre': find_item.nombre,
                    "costo": find_item.precio,
                    "estado": item.estado,
                }
                array_all_history.append(area_consulta)
    tratamientos = Tratamiento.objects.all()
    vacunaciones_cliente = Vacunacion.objects.filter(mascota__cliente__id=id_cliente, estado='Activa',
                                                     estado_aplicacion='Aplicada').order_by('-mascota')
    desparasitaciones_cliente = Desparasitacion.objects.filter(mascota__cliente__id=id_cliente, estado='Activo',
                                                               estado_aplicacion='Aplicada').order_by('-mascota')
    vacunas = Vacuna.objects.all()
    desparasitantes = Desparasitante.objects.all()
    guarderias_cliente = Guarderia.objects.filter(mascota__cliente__id=id_cliente, estado='Activa')
    data = {
        'citas_estetica': serializers.serialize('json', citas_estetica_cliente),
        'mascotas_cliente': serializers.serialize('json', mascotas_cliente),
        'tipos_baño': serializers.serialize('json', tipos_baño),
        # 'tratamientos_cliente': serializers.serialize('json', tratamientos_cliente),
        'tratamientos_cliente': json.dumps(array_all_history, cls=DecimalEncoder),
        'vacunaciones_cliente': serializers.serialize('json', vacunaciones_cliente),
        'desparasitaciones_cliente': serializers.serialize('json', desparasitaciones_cliente),
        'vacunas': serializers.serialize('json', vacunas),
        'desparasitantes': serializers.serialize('json', desparasitantes),
        'tratamientos': serializers.serialize('json', tratamientos),
        'guarderias_cliente': serializers.serialize('json', guarderias_cliente),
        'is_taken': True
    }
    return JsonResponse(data)


class RegistrarCompraProducto(SuccessMessageMixin, CreateView):
    model = CompraProducto
    form_class = RegistrarCompraProductoForm
    template_name = "registrar_compra_producto.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡La compra se registró con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RegistrarCompraProducto, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        compra_producto = form.instance

        mi_producto = get_object_or_404(Producto, pk=self.kwargs['id_producto'])

        compra_producto.producto = mi_producto

        mi_producto.cantidad = mi_producto.cantidad + compra_producto.cantidad

        compra_producto.save()
        mi_producto.save()

        return super(RegistrarCompraProducto, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(RegistrarCompraProducto, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        mi_producto = get_object_or_404(Producto, pk=self.kwargs['id_producto'])
        context['mi_producto'] = mi_producto
        return context


class RegistrarPaqueteProducto(SuccessMessageMixin, CreateView):
    model = PaqueteProducto
    form_class = RegistrarPaqueteProductoForm
    template_name = "registrar_paquete_producto.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡El Paquete se registró con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RegistrarPaqueteProducto, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        paquete_producto = form.instance

        mi_producto = get_object_or_404(Producto, pk=self.kwargs['id_producto'])

        paquete_producto.producto = mi_producto

        paquete_producto.cantidad_disponible = mi_producto.cantidad / paquete_producto.numero_productos_paquete

        paquete_producto.save()

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro', tipo_elemento='Paquete',
                                                   id_elemento=paquete_producto.id)
        historia_transaccion.save()

        return super(RegistrarPaqueteProducto, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(RegistrarPaqueteProducto, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        mi_producto = get_object_or_404(Producto, pk=self.kwargs['id_producto'])
        context['mi_producto'] = mi_producto
        return context


def obtener_elementos_categoria(request):
    id_categoria = request.GET.get('id_categoria', None)

    elementos_categoria_repetidos = []
    for element in list(
            ElementoCategoriaInventario.objects.filter(estado='Activo', categoria=id_categoria).exclude(tipo_uso='Interno').values('codigo_inventario').annotate(
                cantidad=Count('codigo_inventario'))):
        if element['cantidad'] > 1:
            elementos_categoria_repetidos.append(element['codigo_inventario'])

    elementos_categoria_por_codigo = json.dumps(list(chain(
        ElementoCategoriaInventario.objects.filter(estado='Activo', categoria=id_categoria).distinct('codigo_inventario').exclude(
            codigo_inventario__isnull=True).exclude(tipo_uso='Interno').values_list('nombre', 'codigo_inventario',
                                                                                    'id', 'estado', 'precio_venta', 'foto', 'codigo',
                                                                                    'cantidad_por_unidad', 'cantidad_unidad', 'cantidad_unidad', 'unidad_medida', 'precio_por_unidad', 'cantidad'),
        ElementoCategoriaInventario.objects.filter(estado='Activo', categoria=id_categoria, codigo_inventario__isnull=True).exclude(
            tipo_uso='Interno').values_list('nombre',
                                            'codigo_inventario',
                                            'id',
                                            'estado', 'precio_venta', 'foto', 'codigo', 'cantidad_por_unidad', 'cantidad_unidad', 'cantidad_unidad', 'unidad_medida', 'precio_por_unidad', 'cantidad'))),
        cls=DecimalEncoder)

    all_elementos_categoria = json.dumps(list(
            ElementoCategoriaInventario.objects.filter(estado='Activo').exclude(tipo_uso='Interno').values_list('id', 'nombre', 'codigo', 'cantidad',
                                                                       'precio_venta', 'cantidad_por_unidad',
                                                                       'precio_por_unidad', 'cantidad_total_unidades',
                                                                       'unidad_medida', 'cantidad_unidad',
                                                                       'codigo_inventario','fecha_vencimiento','fecha_vencimiento')), cls=DjangoJSONEncoder)

    data = {
        'elementos_categoria_repetidos': elementos_categoria_repetidos,
        'elementos_categoria_por_codigo': elementos_categoria_por_codigo,
        'all_elementos_categoria': all_elementos_categoria
    }
    return JsonResponse(data)

def filtrar_tipo_producto(request):
    tipo_producto = request.GET.get('tipo', None)
    print(tipo_producto)

    if tipo_producto == 'productos':
        qs1 = Producto.objects.filter(estado=True).order_by('nombre')
        qs2 = PaqueteProducto.objects.all().order_by('nombre')

        lista_elementos = sorted(chain(qs1, qs2), key=lambda instance: instance.nombre.upper())

    elif tipo_producto == 'vacunas':
        lista_elementos = Vacuna.objects.filter(estado='Activa').order_by('nombre')

    elif tipo_producto == 'desparasitantes':
        lista_elementos = Desparasitante.objects.filter(estado='Activo').order_by('nombre')

    elif tipo_producto == 'medicamentos':
        lista_elementos = Medicamento.objects.filter(estado='Activo').order_by('nombre')

    elif tipo_producto == 'insumos':
        lista_elementos = Insumo.objects.filter(estado='Activo').order_by('nombre')

    elif tipo_producto.isdigit():
        lista_elementos = ElementoCategoriaInventario.objects.filter(estado='Activo', categoria=tipo_producto).order_by('nombre')

    else:
        qs1 = Producto.objects.filter(estado=True)
        qs2 = PaqueteProducto.objects.all()
        qs3 = Vacuna.objects.filter(estado='Activa')
        qs4 = Desparasitante.objects.filter(estado='Activo')
        qs5 = Medicamento.objects.filter(estado='Activo')
        qs6 = Insumo.objects.filter(estado='Activo')
        qs7 = ElementoCategoriaInventario.objects.filter(estado='Activo')

        for q in qs1:
            q.tipo_elemento = 'Producto'

        for q in qs2:
            q.tipo_elemento = 'Paquete'

        for q in qs3:
            q.tipo_elemento = 'Vacuna'

        for q in qs4:
            q.tipo_elemento = 'Desparasitante'

        for q in qs5:
            q.tipo_elemento = 'Medicamento'

        for q in qs6:
            q.tipo_elemento = 'Insumo'

        lista_elementos = sorted(chain(qs1, qs2, qs3, qs4, qs5, qs6, qs7), key=lambda instance: instance.nombre.upper())

    lista_categorias = CategoriaInventario.objects.all()
    bodegas = Bodega.objects.all()

    data = {
        'lista_elementos': serializers.serialize('json', lista_elementos),
        'lista_categorias': serializers.serialize('json', lista_categorias),
        'bodegas': serializers.serialize('json', bodegas)
    }
    return JsonResponse(data)


class CrearMedicamento(SuccessMessageMixin, CreateView):
    model = Medicamento
    form_class = CrearMedicamentoForm
    template_name = "registrar_medicamento.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡El medicamento se agregó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearMedicamento, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        medicamento = form.instance
        self.object = form.save()

        cantidad_nueva = medicamento.cantidad

        medicamento.cantidad_total_unidades = cantidad_nueva * medicamento.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro', tipo_elemento='Medicamento',
                                                   id_elemento=medicamento.id, responsable=responsable)
        historia_transaccion.save()

        return super(CrearMedicamento, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearMedicamento, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_inventario'] = True
        return context


class EditarMedicamento(SuccessMessageMixin, UpdateView):
    model = Medicamento
    form_class = CrearMedicamentoForm
    template_name = "registrar_medicamento.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡El medicamento fue modificado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarMedicamento, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        medicamento = form.instance

        mi_medicamento = get_object_or_404(Medicamento, id=medicamento.id)

        if (mi_medicamento.cantidad != medicamento.cantidad) or (mi_medicamento.cantidad_unidad != medicamento.cantidad_unidad):
            cantidad_nueva = medicamento.cantidad
            medicamento.cantidad_total_unidades = cantidad_nueva * medicamento.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',
                                                   tipo_elemento='Medicamento', id_elemento=medicamento.id, responsable=responsable)
        historia_transaccion.save()

        self.object = form.save()
        return super(EditarMedicamento, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarMedicamento, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        return context


class DetalleMedicamento(DetailView):
    model = Medicamento
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleMedicamento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleMedicamento, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'medicamento'
        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        compras = GastoMedicamento.objects.filter(medicamento_id=self.kwargs['pk'])

        context['compras'] = compras

        mi_elemento = Medicamento.objects.get(id=self.kwargs['pk'])

        if mi_elemento.cantidad_unidad != 0 and mi_elemento.cantidad_por_unidad:
            unidades_enteras = math.floor(float(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))
            cantidad_no_entera = round((float(mi_elemento.cantidad_total_unidades) % int(mi_elemento.cantidad_unidad)),
                                       1)
        else:
            unidades_enteras = mi_elemento.cantidad
            cantidad_no_entera = 0

        if cantidad_no_entera == 0:
            if mi_elemento.cantidad_por_unidad:
                context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                    mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida
            else:
                context['disponibilidad'] = str(unidades_enteras) + " unidades"
        else:
            context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida + " y 1 unidad de " + str(
                cantidad_no_entera) + " " + mi_elemento.unidad_medida

        return context


class DetalleProducto(DetailView):
    model = Producto
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleProducto, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleProducto, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'producto'
        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        compras = GastoProducto.objects.filter(producto_id=self.kwargs['pk'])

        context['compras'] = compras

        mi_elemento = Producto.objects.get(id=self.kwargs['pk'])

        if mi_elemento.cantidad_unidad != 0 and mi_elemento.cantidad_por_unidad:
            unidades_enteras = math.floor(float(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))
            cantidad_no_entera = round((float(mi_elemento.cantidad_total_unidades) % int(mi_elemento.cantidad_unidad)),
                                       1)
        else:
            unidades_enteras = mi_elemento.cantidad
            cantidad_no_entera = 0

        if cantidad_no_entera == 0:
            if mi_elemento.cantidad_por_unidad:
                context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                    mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida
            else:
                context['disponibilidad'] = str(unidades_enteras) + " unidades"
        else:
            context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida + " y 1 unidad de " + str(
                cantidad_no_entera) + " " + mi_elemento.unidad_medida

        return context


class DetallePaqueteProducto(DetailView):
    model = PaqueteProducto
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetallePaqueteProducto, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetallePaqueteProducto, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'paquete'
        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Paquete',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Paquete',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Paquete',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        return context


class EditarPaqueteProducto(SuccessMessageMixin, UpdateView):
    model = PaqueteProducto
    form_class = EditarPaqueteProductoForm
    template_name = "registrar_paquete_producto.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡El Paquete se modificó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarPaqueteProducto, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        paquete_producto = form.instance
        mi_paquete = get_object_or_404(PaqueteProducto, pk=self.kwargs['pk'])
        paquete_producto.cantidad_disponible = mi_paquete.producto.cantidad / paquete_producto.numero_productos_paquete
        self.object = form.save()

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',
                                                   tipo_elemento='Paquete',
                                                   id_elemento=paquete_producto.id)
        historia_transaccion.save()

        return super(EditarPaqueteProducto, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarPaqueteProducto, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        paquete = get_object_or_404(PaqueteProducto, pk=self.kwargs['pk'])
        context['mi_producto'] = paquete.producto
        return context


def eliminar_producto(request):
    id_producto = request.GET.get('id', '')
    producto = get_object_or_404(Producto, pk=id_producto)
    paquetes_producto = PaqueteProducto.objects.filter(producto=producto)
    for paquete in paquetes_producto:
        paquete.delete()
    producto.delete()
    messages.success(request, 'Producto eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def inactivar_producto(request):
    id_producto = request.GET.get('id', '')
    producto = get_object_or_404(Producto, pk=id_producto)
    producto.estado = False
    producto.save()
    messages.success(request, 'Producto eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def eliminar_paquete(request):
    id_paquete = request.GET.get('id', '')
    paquete = get_object_or_404(PaqueteProducto, pk=id_paquete)
    paquete.delete()
    messages.success(request, 'Paquete eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def eliminar_medicamento(request):
    id_medicamento = request.GET.get('id', '')
    medicamento = get_object_or_404(Medicamento, pk=id_medicamento)
    medicamento.delete()
    messages.success(request, 'Medicamento eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def inactivar_medicamento(request):
    id_medicamento = request.GET.get('id', '')
    medicamento = get_object_or_404(Medicamento, pk=id_medicamento)
    medicamento.estado = 'Inactivo'
    medicamento.save()
    messages.success(request, 'Medicamento eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


@login_required
def abonarPago(request, id_pago):
    valor_abonado = request.GET.get('valor_abonado', None)
    medio_pago = request.GET.get('medio_pago', None)
    if valor_abonado:
        print("Hola desde POST request")
        mi_pago = Pago.objects.get(id=id_pago)
        abono_pago = AbonoPago(pago=mi_pago, valor_abonado=valor_abonado, medio_pago=medio_pago)
        abono_pago.save()
        cuotas_pago = CuotaPago.objects.filter(pago__id=mi_pago.id, estado_pago='Pendiente').order_by('fecha_limite')
        abono_a_saldo = int(valor_abonado)
        print("Abono inicial" + str(abono_a_saldo))
        caja_on = Caja.objects.filter(estado=True)
        if caja_on:
            caja_pago = CajaAbonoPago(caja=caja_on[0], abono_pago=cuotas_pago[0], pago=mi_pago,
                                      valor_abonado=abono_a_saldo, medio_pago=medio_pago)
            caja_pago.save()
        for cuota in cuotas_pago:
            print("Abono inicial" + str(abono_a_saldo))
            if cuota.saldo_cuota > abono_a_saldo:
                if abono_a_saldo > 0:
                    cuota.fecha_pago = datetime.now().date()
                cuota.saldo_cuota = cuota.saldo_cuota - abono_a_saldo
                cuota.valor_pagado = cuota.valor_pagado + abono_a_saldo
                mi_pago.saldo = mi_pago.saldo - abono_a_saldo
                mi_pago.save()
                abono_a_saldo = 0
                cuota.save()

            elif cuota.saldo_cuota <= abono_a_saldo:
                cuota.fecha_pago = datetime.now().date()
                cuota.valor_pagado = cuota.valor_pagado + cuota.saldo_cuota
                mi_pago.saldo = mi_pago.saldo - cuota.saldo_cuota
                mi_pago.save()
                abono_a_saldo = abono_a_saldo - cuota.saldo_cuota
                cuota.saldo_cuota = 0
                cuota.estado_pago = 'Pagada'
                cuota.save()

        if not CuotaPago.objects.filter(pago__id=mi_pago.id, estado_pago='Pendiente'):
            mi_pago.estado_pago = 'Pagado'
            mi_pago.saldo = 0
            mi_pago.save()
        messages.success(request, 'Abono registrado con exito!')
        data = {
            'eliminacion': True
        }
        return JsonResponse(data)
    else:
        mi_pago = Pago.objects.get(id=id_pago)
        cuotas_pago = CuotaPago.objects.filter(pago__id=mi_pago.id).order_by('fecha_limite')
        fecha_actual = datetime.now().date()
        return render(request, 'registrar_abono_pago.html', {
            'menu_pagos': True,
            'mi_pago': mi_pago,
            'cuotas_pago': cuotas_pago,
            'fecha_actual': fecha_actual,
            'cuota_actual': CuotaPago.objects.filter(pago__id=mi_pago.id, estado_pago='Pendiente').order_by(
                'fecha_limite').first(),
        })


def anular_venta(request):
    id_venta = request.GET.get('id', '')
    pago = get_object_or_404(Pago, pk=id_venta)
    pago.estado_pago = 'Anulada'
    pago.save()

    pagos_producto = PagoProducto.objects.filter(pago=pago)
    for pago_producto in pagos_producto:
        elemento_vendido = Producto.objects.get(pk=pago_producto.producto.id)

        if pago_producto.tipo_de_venta == 'a granel':
            elemento_vendido.cantidad_total_unidades = elemento_vendido.cantidad_total_unidades + pago_producto.cantidad
            elemento_vendido.cantidad = math.floor(
                int(elemento_vendido.cantidad_total_unidades) / int(elemento_vendido.cantidad_unidad))
        else:
            elemento_vendido.cantidad = elemento_vendido.cantidad + pago_producto.cantidad
            elemento_vendido.cantidad_total_unidades += pago_producto.cantidad * elemento_vendido.cantidad_unidad

        elemento_vendido.save()

    pagos_medicamento = PagoMedicamento.objects.filter(pago=pago)
    for pago_medicamento in pagos_medicamento:
        elemento_vendido = Medicamento.objects.get(pk=pago_medicamento.medicamento.id)

        if pago_medicamento.tipo_de_venta == 'a granel':
            elemento_vendido.cantidad_total_unidades = elemento_vendido.cantidad_total_unidades + pago_medicamento.cantidad
            elemento_vendido.cantidad = math.floor(
                int(elemento_vendido.cantidad_total_unidades) / int(elemento_vendido.cantidad_unidad))
        else:
            elemento_vendido.cantidad = elemento_vendido.cantidad + pago_medicamento.cantidad
            elemento_vendido.cantidad_total_unidades += pago_medicamento.cantidad * elemento_vendido.cantidad_unidad

        elemento_vendido.save()

    pagos_vacuna = PagoVacuna.objects.filter(pago=pago)
    for pago_vacuna in pagos_vacuna:
        elemento_vendido = Vacuna.objects.get(pk=pago_vacuna.vacuna.id)

        if pago_vacuna.tipo_de_venta == 'a granel':
            elemento_vendido.cantidad_total_unidades = elemento_vendido.cantidad_total_unidades + pago_vacuna.cantidad
            elemento_vendido.cantidad = math.floor(
                int(elemento_vendido.cantidad_total_unidades) / int(elemento_vendido.cantidad_unidad))
        else:
            elemento_vendido.cantidad = elemento_vendido.cantidad + pago_vacuna.cantidad
            elemento_vendido.cantidad_total_unidades += pago_vacuna.cantidad * elemento_vendido.cantidad_unidad

        elemento_vendido.save()

    pagos_desparasitante = PagoDesparasitante.objects.filter(pago=pago)
    for pago_desparasitante in pagos_desparasitante:
        elemento_vendido = Desparasitante.objects.get(pk=pago_desparasitante.desparasitante.id)

        if pago_desparasitante.tipo_de_venta == 'a granel':
            elemento_vendido.cantidad_total_unidades = elemento_vendido.cantidad_total_unidades + pago_desparasitante.cantidad
            elemento_vendido.cantidad = math.floor(
                int(elemento_vendido.cantidad_total_unidades) / int(elemento_vendido.cantidad_unidad))
        else:
            elemento_vendido.cantidad = elemento_vendido.cantidad + pago_desparasitante.cantidad
            elemento_vendido.cantidad_total_unidades += pago_desparasitante.cantidad * elemento_vendido.cantidad_unidad

        elemento_vendido.save()

    pagos_categoria = PagoElementoInventario.objects.filter(pago=pago)
    for pago_categoria in pagos_categoria:
        elemento_vendido = ElementoCategoriaInventario.objects.get(pk=pago_categoria.elemento.id)

        if pago_categoria.tipo_de_venta == 'a granel':
            elemento_vendido.cantidad_total_unidades = elemento_vendido.cantidad_total_unidades + pago_categoria.cantidad
            elemento_vendido.cantidad = math.floor(
                int(elemento_vendido.cantidad_total_unidades) / int(elemento_vendido.cantidad_unidad))
        else:
            elemento_vendido.cantidad = elemento_vendido.cantidad + pago_categoria.cantidad
            elemento_vendido.cantidad_total_unidades += pago_categoria.cantidad * elemento_vendido.cantidad_unidad

        elemento_vendido.save()

    pagos_vacunacion = PagoVacunacion.objects.filter(pago=pago)
    for pago_vacunacion in pagos_vacunacion:
        vacunacion = Vacunacion.objects.get(pk=pago_vacunacion.vacunacion.id)
        vacunacion.estado = "Activa"
        # vacunacion.costo = 0
        vacunacion.save()

    pagos_desparasitacion = PagoDesparasitacion.objects.filter(pago=pago)
    for pago_desparasitacion in pagos_desparasitacion:
        desparasitacion = Desparasitacion.objects.get(pk=pago_desparasitacion.desparasitacion.id)
        desparasitacion.estado = "Activo"
        # desparasitacion.costo = 0
        desparasitacion.save()

    pagos_guarderia = PagoGuarderia.objects.filter(pago=pago)
    for pago_guarderia in pagos_guarderia:
        guarderia = Guarderia.objects.get(pk=pago_guarderia.guarderia.id)
        guarderia.estado = "Activa"
        guarderia.save()

    pagos_estetica = PagoEstetica.objects.filter(pago=pago)
    for pago_estetica in pagos_estetica:
        estetica = FacturaEstetica.objects.get(pk=pago_estetica.estetica.id)
        estetica.estado = "Activa"
        estetica.saldo = estetica.valor_total
        estetica.save()

    pagos_area_consulta = PagoAreaConsulta.objects.filter(pago=pago)
    for pago_area_consulta in pagos_area_consulta:
        motivo_area = MotivoArea.objects.get(pk=pago_area_consulta.area_consulta.id)
        motivo_area.estado = "Activa"
        motivo_area.save()

    pagos_atencion_cita = PagoTratamiento.objects.filter(pago=pago)
    for pago_atencion_cita in pagos_atencion_cita:
        historia = HistoriaClinica.objects.get(pk=pago_atencion_cita.tratamiento.id)
        historia.estado = "Activa"
        historia.costo = 0
        historia.save()

    cuotas_pago = CuotaPago.objects.filter(pago=pago)
    for cuota in cuotas_pago:
        cuota.estado_pago = "Anulada"
        cuota.save()

    messages.success(request, 'Factura anulada con exito!')

    id_cliente = ''
    if pago.cliente:
        id_cliente = pago.cliente.id
    data = {
        'eliminacion': True,
        'id_cliente': id_cliente
    }
    return JsonResponse(data)


def cambiar_categoria(request, tipo_elemento, id_elemento):
    if request.method == 'POST':
        nueva_categoria = request.POST.get("tipo_categoria", "")
        if nueva_categoria != tipo_elemento:
            if tipo_elemento == 'medicamento':
                mi_medicamento = Medicamento.objects.get(pk=id_elemento)
                if HistoriaMedicamento.objects.filter(
                        medicamento=mi_medicamento) or MedicamentoHospitalizacion.objects.filter(
                        medicamento=mi_medicamento):
                    messages.error(request,
                                   'No se puede cambiar la categoria de este medicamento porque ya ha sido aplicado a una mascota')
                elif nueva_categoria == 'insumo' and PagoMedicamento.objects.filter(medicamento=id_elemento):
                    messages.error(request,
                                   'No se puede cambiar la categoria de este medicamento a insumo porque ya ha sido relacionado en una venta')
                else:
                    if nueva_categoria == 'producto':
                        nuevo_producto = Producto(nombre=mi_medicamento.nombre,
                                                  codigo=mi_medicamento.codigo_inventario,
                                                  descripcion=mi_medicamento.descripcion,
                                                  cantidad=mi_medicamento.cantidad,
                                                  precio_venta=mi_medicamento.precio_venta,
                                                  unidad_medida=mi_medicamento.unidad_medida,
                                                  cantidad_unidad=mi_medicamento.cantidad_unidad,
                                                  foto=mi_medicamento.foto,
                                                  cantidad_por_unidad=mi_medicamento.cantidad_por_unidad,
                                                  precio_por_unidad=mi_medicamento.precio_por_unidad,
                                                  cantidad_total_unidades=mi_medicamento.cantidad_total_unidades,
                                                  tipo_uso=mi_medicamento.tipo_uso,
                                                  fecha_vencimiento=mi_medicamento.fecha_vencimiento,
                                                  stock_minimo=mi_medicamento.stock_minimo)
                        nuevo_producto.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Producto'
                            historia.id_elemento = nuevo_producto.id
                            historia.save()

                        for pago_medicamento in PagoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_pago_producto = PagoProducto(producto=nuevo_producto, pago=pago_medicamento.pago,
                                                               cantidad=pago_medicamento.cantidad,
                                                               total=pago_medicamento.total,
                                                               tipo_de_venta=pago_medicamento.tipo_de_venta)
                            nuevo_pago_producto.save()
                            pago_medicamento.delete()

                        for gasto_medicamento in GastoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_gasto_producto = GastoProducto(producto=nuevo_producto, gasto=gasto_medicamento.gasto,
                                                                 precio_compra_unidad=gasto_medicamento.precio_compra_unidad,
                                                                 unidades=gasto_medicamento.unidades,
                                                                 precio_total=gasto_medicamento.precio_total,
                                                                 impuesto=gasto_medicamento.impuesto,
                                                                 valor_total_impuesto=gasto_medicamento.valor_total_impuesto)
                            nuevo_gasto_producto.save()
                            gasto_medicamento.delete()

                        mi_medicamento.delete()
                    elif nueva_categoria == 'desparasitante':
                        nuevo_desparasitante = Desparasitante(nombre=mi_medicamento.nombre,
                                                              codigo_inventario=mi_medicamento.codigo_inventario,
                                                              codigo=mi_medicamento.codigo,
                                                              descripcion=mi_medicamento.descripcion,
                                                              cantidad=mi_medicamento.cantidad,
                                                              precio_venta=mi_medicamento.precio_venta,
                                                              unidad_medida=mi_medicamento.unidad_medida,
                                                              cantidad_unidad=mi_medicamento.cantidad_unidad,
                                                              foto=mi_medicamento.foto,
                                                              cantidad_por_unidad=mi_medicamento.cantidad_por_unidad,
                                                              precio_por_unidad=mi_medicamento.precio_por_unidad,
                                                              cantidad_total_unidades=mi_medicamento.cantidad_total_unidades,
                                                              tipo_uso=mi_medicamento.tipo_uso,
                                                              fecha_vencimiento=mi_medicamento.fecha_vencimiento,
                                                              stock_minimo=mi_medicamento.stock_minimo)
                        nuevo_desparasitante.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Desparasitante'
                            historia.id_elemento = nuevo_desparasitante.id
                            historia.save()

                        for pago_medicamento in PagoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_pago_desparasitante = PagoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                           pago=pago_medicamento.pago,
                                                                           cantidad=pago_medicamento.cantidad,
                                                                           total=pago_medicamento.total,
                                                                           tipo_de_venta=pago_medicamento.tipo_de_venta)
                            nuevo_pago_desparasitante.save()
                            pago_medicamento.delete()

                        for gasto_medicamento in GastoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_gasto_desparasitante = GastoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                             gasto=gasto_medicamento.gasto,
                                                                             precio_compra_unidad=gasto_medicamento.precio_compra_unidad,
                                                                             unidades=gasto_medicamento.unidades,
                                                                             precio_total=gasto_medicamento.precio_total,
                                                                             impuesto=gasto_medicamento.impuesto,
                                                                             valor_total_impuesto=gasto_medicamento.valor_total_impuesto)
                            nuevo_gasto_desparasitante.save()
                            gasto_medicamento.delete()

                        mi_medicamento.delete()
                    elif nueva_categoria == 'vacuna':
                        nueva_vacuna = Vacuna(nombre=mi_medicamento.nombre,
                                              codigo_inventario=mi_medicamento.codigo_inventario,
                                              codigo=mi_medicamento.codigo,
                                              descripcion=mi_medicamento.descripcion,
                                              cantidad=mi_medicamento.cantidad,
                                              precio_venta=mi_medicamento.precio_venta,
                                              unidad_medida=mi_medicamento.unidad_medida,
                                              cantidad_unidad=mi_medicamento.cantidad_unidad,
                                              foto=mi_medicamento.foto,
                                              cantidad_por_unidad=mi_medicamento.cantidad_por_unidad,
                                              precio_por_unidad=mi_medicamento.precio_por_unidad,
                                              cantidad_total_unidades=mi_medicamento.cantidad_total_unidades,
                                              tipo_uso=mi_medicamento.tipo_uso,
                                              fecha_vencimiento=mi_medicamento.fecha_vencimiento,
                                              stock_minimo=mi_medicamento.stock_minimo)
                        nueva_vacuna.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Vacuna'
                            historia.id_elemento = nueva_vacuna.id
                            historia.save()

                        for pago_medicamento in PagoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_pago_vacuna = PagoVacuna(vacuna=nueva_vacuna,
                                                           pago=pago_medicamento.pago,
                                                           cantidad=pago_medicamento.cantidad,
                                                           total=pago_medicamento.total,
                                                           tipo_de_venta=pago_medicamento.tipo_de_venta)
                            nuevo_pago_vacuna.save()
                            pago_medicamento.delete()

                        for gasto_medicamento in GastoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_gasto_vacuna = GastoVacuna(vacuna=nueva_vacuna,
                                                             gasto=gasto_medicamento.gasto,
                                                             precio_compra_unidad=gasto_medicamento.precio_compra_unidad,
                                                             unidades=gasto_medicamento.unidades,
                                                             precio_total=gasto_medicamento.precio_total,
                                                             impuesto=gasto_medicamento.impuesto,
                                                             valor_total_impuesto=gasto_medicamento.valor_total_impuesto)
                            nuevo_gasto_vacuna.save()
                            gasto_medicamento.delete()

                        mi_medicamento.delete()
                    elif nueva_categoria == 'insumo':
                        nuevo_insumo = Insumo(nombre=mi_medicamento.nombre,
                                              codigo_inventario=mi_medicamento.codigo_inventario,
                                              codigo=mi_medicamento.codigo,
                                              descripcion=mi_medicamento.descripcion,
                                              cantidad=mi_medicamento.cantidad,
                                              unidad_medida=mi_medicamento.unidad_medida,
                                              cantidad_unidad=mi_medicamento.cantidad_unidad,
                                              foto=mi_medicamento.foto,
                                              cantidad_por_unidad=mi_medicamento.cantidad_por_unidad,
                                              cantidad_total_unidades=mi_medicamento.cantidad_total_unidades,
                                              fecha_vencimiento=mi_medicamento.fecha_vencimiento,
                                              stock_minimo=mi_medicamento.stock_minimo)
                        nuevo_insumo.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Insumo'
                            historia.id_elemento = nuevo_insumo.id
                            historia.save()

                        for gasto_medicamento in GastoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_gasto_insumo = GastoInsumo(insumo=nuevo_insumo,
                                                             gasto=gasto_medicamento.gasto,
                                                             precio_compra_unidad=gasto_medicamento.precio_compra_unidad,
                                                             unidades=gasto_medicamento.unidades,
                                                             precio_total=gasto_medicamento.precio_total,
                                                             impuesto=gasto_medicamento.impuesto,
                                                             valor_total_impuesto=gasto_medicamento.valor_total_impuesto)
                            nuevo_gasto_insumo.save()
                            gasto_medicamento.delete()

                        mi_medicamento.delete()
                    else:
                        mi_categoria = CategoriaInventario.objects.get(id=nueva_categoria)
                        nuevo_elemento = ElementoCategoriaInventario(nombre=mi_medicamento.nombre,
                                              categoria=mi_categoria,
                                              codigo_inventario=mi_medicamento.codigo_inventario,
                                              codigo=mi_medicamento.codigo,
                                              descripcion=mi_medicamento.descripcion,
                                              cantidad=mi_medicamento.cantidad,
                                              precio_venta=mi_medicamento.precio_venta,
                                              unidad_medida=mi_medicamento.unidad_medida,
                                              cantidad_unidad=mi_medicamento.cantidad_unidad,
                                              foto=mi_medicamento.foto,
                                              cantidad_por_unidad=mi_medicamento.cantidad_por_unidad,
                                              precio_por_unidad=mi_medicamento.precio_por_unidad,
                                              cantidad_total_unidades=mi_medicamento.cantidad_total_unidades,
                                              tipo_uso=mi_medicamento.tipo_uso,
                                              fecha_vencimiento=mi_medicamento.fecha_vencimiento,
                                              stock_minimo=mi_medicamento.stock_minimo)
                        nuevo_elemento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Medicamento',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Categoria'
                            historia.id_elemento = nuevo_elemento.id
                            historia.save()

                        for pago_medicamento in PagoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_pago_elemento = PagoElementoInventario(elemento=nuevo_elemento,
                                                           pago=pago_medicamento.pago,
                                                           cantidad=pago_medicamento.cantidad,
                                                           total=pago_medicamento.total,
                                                           tipo_de_venta=pago_medicamento.tipo_de_venta)
                            nuevo_pago_elemento.save()
                            pago_medicamento.delete()

                        for gasto_medicamento in GastoMedicamento.objects.filter(medicamento=id_elemento):
                            nuevo_gasto_elemento = GastoElementoInventario(elemento=nuevo_elemento,
                                                             gasto=gasto_medicamento.gasto,
                                                             precio_compra_unidad=gasto_medicamento.precio_compra_unidad,
                                                             unidades=gasto_medicamento.unidades,
                                                             precio_total=gasto_medicamento.precio_total,
                                                             impuesto=gasto_medicamento.impuesto,
                                                             valor_total_impuesto=gasto_medicamento.valor_total_impuesto)
                            nuevo_gasto_elemento.save()
                            gasto_medicamento.delete()

                        mi_medicamento.delete()
                    messages.success(request, 'Cambio de categoria exitoso!')
            elif tipo_elemento == 'producto':
                mi_producto = Producto.objects.get(pk=id_elemento)
                if nueva_categoria == 'insumo' and PagoProducto.objects.filter(producto=id_elemento):
                    messages.error(request,
                                   'No se puede cambiar la categoria de este producto a insumo porque ya ha sido relacionado en una venta')
                else:
                    if nueva_categoria == 'vacuna':
                        nueva_vacuna = Vacuna(nombre=mi_producto.nombre,
                                              codigo_inventario=mi_producto.codigo,
                                              descripcion=mi_producto.descripcion,
                                              cantidad=mi_producto.cantidad,
                                              precio_venta=mi_producto.precio_venta,
                                              unidad_medida=mi_producto.unidad_medida,
                                              cantidad_unidad=mi_producto.cantidad_unidad,
                                              foto=mi_producto.foto,
                                              cantidad_por_unidad=mi_producto.cantidad_por_unidad,
                                              precio_por_unidad=mi_producto.precio_por_unidad,
                                              cantidad_total_unidades=mi_producto.cantidad_total_unidades,
                                              tipo_uso=mi_producto.tipo_uso,
                                              fecha_vencimiento=mi_producto.fecha_vencimiento,
                                              stock_minimo=mi_producto.stock_minimo)
                        nueva_vacuna.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Vacuna'
                            historia.id_elemento = nueva_vacuna.id
                            historia.save()

                        for pago_producto in PagoProducto.objects.filter(producto=id_elemento):
                            nuevo_pago_vacuna = PagoVacuna(vacuna=nueva_vacuna, pago=pago_producto.pago,
                                                           cantidad=pago_producto.cantidad, total=pago_producto.total,
                                                           tipo_de_venta=pago_producto.tipo_de_venta)
                            nuevo_pago_vacuna.save()
                            pago_producto.delete()

                        for gasto_producto in GastoProducto.objects.filter(producto=id_elemento):
                            nuevo_gasto_vacuna = GastoVacuna(vacuna=nueva_vacuna, gasto=gasto_producto.gasto,
                                                             precio_compra_unidad=gasto_producto.precio_compra_unidad,
                                                             unidades=gasto_producto.unidades,
                                                             precio_total=gasto_producto.precio_total,
                                                             impuesto=gasto_producto.impuesto,
                                                             valor_total_impuesto=gasto_producto.valor_total_impuesto)
                            nuevo_gasto_vacuna.save()
                            gasto_producto.delete()

                        mi_producto.delete()
                    elif nueva_categoria == 'desparasitante':
                        nuevo_desparasitante = Desparasitante(nombre=mi_producto.nombre,
                                                              codigo_inventario=mi_producto.codigo,
                                                              descripcion=mi_producto.descripcion,
                                                              cantidad=mi_producto.cantidad,
                                                              precio_venta=mi_producto.precio_venta,
                                                              unidad_medida=mi_producto.unidad_medida,
                                                              cantidad_unidad=mi_producto.cantidad_unidad,
                                                              foto=mi_producto.foto,
                                                              cantidad_por_unidad=mi_producto.cantidad_por_unidad,
                                                              precio_por_unidad=mi_producto.precio_por_unidad,
                                                              cantidad_total_unidades=mi_producto.cantidad_total_unidades,
                                                              tipo_uso=mi_producto.tipo_uso,
                                                              fecha_vencimiento=mi_producto.fecha_vencimiento,
                                                              stock_minimo=mi_producto.stock_minimo)
                        nuevo_desparasitante.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Desparasitante'
                            historia.id_elemento = nuevo_desparasitante.id
                            historia.save()

                        for pago_producto in PagoProducto.objects.filter(producto=id_elemento):
                            nuevo_pago_desparasitante = PagoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                           pago=pago_producto.pago,
                                                                           cantidad=pago_producto.cantidad,
                                                                           total=pago_producto.total,
                                                                           tipo_de_venta=pago_producto.tipo_de_venta)
                            nuevo_pago_desparasitante.save()
                            pago_producto.delete()

                        for gasto_producto in GastoProducto.objects.filter(producto=id_elemento):
                            nuevo_gasto_desparasitante = GastoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                             gasto=gasto_producto.gasto,
                                                                             precio_compra_unidad=gasto_producto.precio_compra_unidad,
                                                                             unidades=gasto_producto.unidades,
                                                                             precio_total=gasto_producto.precio_total,
                                                                             impuesto=gasto_producto.impuesto,
                                                                             valor_total_impuesto=gasto_producto.valor_total_impuesto)
                            nuevo_gasto_desparasitante.save()
                            gasto_producto.delete()

                        mi_producto.delete()
                    elif nueva_categoria == 'medicamento':
                        nuevo_medicamento = Medicamento(nombre=mi_producto.nombre,
                                                        codigo_inventario=mi_producto.codigo,
                                                        descripcion=mi_producto.descripcion,
                                                        cantidad=mi_producto.cantidad,
                                                        precio_venta=mi_producto.precio_venta,
                                                        unidad_medida=mi_producto.unidad_medida,
                                                        cantidad_unidad=mi_producto.cantidad_unidad,
                                                        foto=mi_producto.foto,
                                                        cantidad_por_unidad=mi_producto.cantidad_por_unidad,
                                                        precio_por_unidad=mi_producto.precio_por_unidad,
                                                        cantidad_total_unidades=mi_producto.cantidad_total_unidades,
                                                        tipo_uso=mi_producto.tipo_uso,
                                                        fecha_vencimiento=mi_producto.fecha_vencimiento,
                                                        stock_minimo=mi_producto.stock_minimo)
                        nuevo_medicamento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Medicamento'
                            historia.id_elemento = nuevo_medicamento.id
                            historia.save()

                        for pago_producto in PagoProducto.objects.filter(producto=id_elemento):
                            nuevo_pago_medicamento = PagoMedicamento(medicamento=nuevo_medicamento,
                                                                     pago=pago_producto.pago,
                                                                     cantidad=pago_producto.cantidad,
                                                                     total=pago_producto.total,
                                                                     tipo_de_venta=pago_producto.tipo_de_venta)
                            nuevo_pago_medicamento.save()
                            pago_producto.delete()

                        for gasto_producto in GastoProducto.objects.filter(producto=id_elemento):
                            nuevo_gasto_medicamento = GastoMedicamento(medicamento=nuevo_medicamento,
                                                                       gasto=gasto_producto.gasto,
                                                                       precio_compra_unidad=gasto_producto.precio_compra_unidad,
                                                                       unidades=gasto_producto.unidades,
                                                                       precio_total=gasto_producto.precio_total,
                                                                       impuesto=gasto_producto.impuesto,
                                                                       valor_total_impuesto=gasto_producto.valor_total_impuesto)
                            nuevo_gasto_medicamento.save()
                            gasto_producto.delete()

                        mi_producto.delete()
                    elif nueva_categoria == 'insumo':
                        nuevo_insumo = Insumo(nombre=mi_producto.nombre,
                                              codigo_inventario=mi_producto.codigo,
                                              descripcion=mi_producto.descripcion,
                                              cantidad=mi_producto.cantidad,
                                              unidad_medida=mi_producto.unidad_medida,
                                              cantidad_unidad=mi_producto.cantidad_unidad,
                                              foto=mi_producto.foto,
                                              cantidad_por_unidad=mi_producto.cantidad_por_unidad,
                                              cantidad_total_unidades=mi_producto.cantidad_total_unidades,
                                              fecha_vencimiento=mi_producto.fecha_vencimiento,
                                              stock_minimo=mi_producto.stock_minimo)
                        nuevo_insumo.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Insumo'
                            historia.id_elemento = nuevo_insumo.id
                            historia.save()

                        for gasto_producto in GastoProducto.objects.filter(producto=id_elemento):
                            nuevo_gasto_insumo = GastoInsumo(insumo=nuevo_insumo,
                                                             gasto=gasto_producto.gasto,
                                                             precio_compra_unidad=gasto_producto.precio_compra_unidad,
                                                             unidades=gasto_producto.unidades,
                                                             precio_total=gasto_producto.precio_total,
                                                             impuesto=gasto_producto.impuesto,
                                                             valor_total_impuesto=gasto_producto.valor_total_impuesto)
                            nuevo_gasto_insumo.save()
                            gasto_producto.delete()

                        mi_producto.delete()
                    else:
                        mi_categoria = CategoriaInventario.objects.get(id=nueva_categoria)
                        nuevo_elemento = ElementoCategoriaInventario(nombre=mi_producto.nombre,
                                                              categoria=mi_categoria,
                                                              codigo_inventario=mi_producto.codigo,
                                                              descripcion=mi_producto.descripcion,
                                                              cantidad=mi_producto.cantidad,
                                                              precio_venta=mi_producto.precio_venta,
                                                              unidad_medida=mi_producto.unidad_medida,
                                                              cantidad_unidad=mi_producto.cantidad_unidad,
                                                              foto=mi_producto.foto,
                                                              cantidad_por_unidad=mi_producto.cantidad_por_unidad,
                                                              precio_por_unidad=mi_producto.precio_por_unidad,
                                                              cantidad_total_unidades=mi_producto.cantidad_total_unidades,
                                                              tipo_uso=mi_producto.tipo_uso,
                                                              fecha_vencimiento=mi_producto.fecha_vencimiento,
                                                              stock_minimo=mi_producto.stock_minimo)
                        nuevo_elemento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Producto',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Categoria'
                            historia.id_elemento = nuevo_elemento.id
                            historia.save()

                        for pago_producto in PagoProducto.objects.filter(producto=id_elemento):
                            nuevo_pago_elemento = PagoElementoInventario(elemento=nuevo_elemento,
                                                           pago=pago_producto.pago,
                                                           cantidad=pago_producto.cantidad,
                                                           total=pago_producto.total,
                                                           tipo_de_venta=pago_producto.tipo_de_venta)
                            nuevo_pago_elemento.save()
                            pago_producto.delete()

                        for gasto_producto in GastoProducto.objects.filter(producto=id_elemento):
                            nuevo_gasto_elemento = GastoElementoInventario(elemento=nuevo_elemento,
                                                             gasto=gasto_producto.gasto,
                                                             precio_compra_unidad=gasto_producto.precio_compra_unidad,
                                                             unidades=gasto_producto.unidades,
                                                             precio_total=gasto_producto.precio_total,
                                                             impuesto=gasto_producto.impuesto,
                                                             valor_total_impuesto=gasto_producto.valor_total_impuesto)
                            nuevo_gasto_elemento.save()
                            gasto_producto.delete()

                        mi_producto.delete()
                    messages.success(request, 'Cambio de categoria exitoso!')
            elif tipo_elemento == 'vacuna':
                mi_vacuna = Vacuna.objects.get(pk=id_elemento)
                if AplicacionVacunacion.objects.filter(vacuna=mi_vacuna):
                    messages.error(request,
                                   'No se puede cambiar la categoria de esta vacuna porque ya ha sido aplicada a una mascota')
                elif nueva_categoria == 'insumo' and PagoVacuna.objects.filter(vacuna=id_elemento):
                    messages.error(request,
                                   'No se puede cambiar la categoria de esta vacuna a insumo porque ya ha sido relacionada en una venta')
                else:
                    if nueva_categoria == 'producto':
                        nuevo_producto = Producto(nombre=mi_vacuna.nombre,
                                                  codigo=mi_vacuna.codigo_inventario,
                                                  descripcion=mi_vacuna.descripcion,
                                                  cantidad=mi_vacuna.cantidad,
                                                  precio_venta=mi_vacuna.precio_venta,
                                                  unidad_medida=mi_vacuna.unidad_medida,
                                                  cantidad_unidad=mi_vacuna.cantidad_unidad,
                                                  foto=mi_vacuna.foto,
                                                  cantidad_por_unidad=mi_vacuna.cantidad_por_unidad,
                                                  precio_por_unidad=mi_vacuna.precio_por_unidad,
                                                  cantidad_total_unidades=mi_vacuna.cantidad_total_unidades,
                                                  tipo_uso=mi_vacuna.tipo_uso,
                                                  fecha_vencimiento=mi_vacuna.fecha_vencimiento,
                                                  stock_minimo=mi_vacuna.stock_minimo)
                        nuevo_producto.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Producto'
                            historia.id_elemento = nuevo_producto.id
                            historia.save()

                        for pago_vacuna in PagoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_pago_producto = PagoProducto(producto=nuevo_producto, pago=pago_vacuna.pago,
                                                               cantidad=pago_vacuna.cantidad, total=pago_vacuna.total,
                                                               tipo_de_venta=pago_vacuna.tipo_de_venta)
                            nuevo_pago_producto.save()
                            pago_vacuna.delete()

                        for gasto_vacuna in GastoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_gasto_producto = GastoProducto(producto=nuevo_producto, gasto=gasto_vacuna.gasto,
                                                                 precio_compra_unidad=gasto_vacuna.precio_compra_unidad,
                                                                 unidades=gasto_vacuna.unidades,
                                                                 precio_total=gasto_vacuna.precio_total,
                                                                 impuesto=gasto_vacuna.impuesto,
                                                                 valor_total_impuesto=gasto_vacuna.valor_total_impuesto)
                            nuevo_gasto_producto.save()
                            gasto_vacuna.delete()

                        mi_vacuna.delete()
                    elif nueva_categoria == 'desparasitante':
                        nuevo_desparasitante = Desparasitante(nombre=mi_vacuna.nombre,
                                                              codigo_inventario=mi_vacuna.codigo_inventario,
                                                              codigo=mi_vacuna.codigo,
                                                              descripcion=mi_vacuna.descripcion,
                                                              cantidad=mi_vacuna.cantidad,
                                                              precio_venta=mi_vacuna.precio_venta,
                                                              unidad_medida=mi_vacuna.unidad_medida,
                                                              cantidad_unidad=mi_vacuna.cantidad_unidad,
                                                              foto=mi_vacuna.foto,
                                                              cantidad_por_unidad=mi_vacuna.cantidad_por_unidad,
                                                              precio_por_unidad=mi_vacuna.precio_por_unidad,
                                                              cantidad_total_unidades=mi_vacuna.cantidad_total_unidades,
                                                              tipo_uso=mi_vacuna.tipo_uso,
                                                              fecha_vencimiento=mi_vacuna.fecha_vencimiento,
                                                              stock_minimo=mi_vacuna.stock_minimo)
                        nuevo_desparasitante.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Desparasitante'
                            historia.id_elemento = nuevo_desparasitante.id
                            historia.save()

                        for pago_vacuna in PagoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_pago_desparasitante = PagoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                           pago=pago_vacuna.pago,
                                                                           cantidad=pago_vacuna.cantidad,
                                                                           total=pago_vacuna.total,
                                                                           tipo_de_venta=pago_vacuna.tipo_de_venta)
                            nuevo_pago_desparasitante.save()
                            pago_vacuna.delete()

                        for gasto_vacuna in GastoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_gasto_desparasitante = GastoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                             gasto=gasto_vacuna.gasto,
                                                                             precio_compra_unidad=gasto_vacuna.precio_compra_unidad,
                                                                             unidades=gasto_vacuna.unidades,
                                                                             precio_total=gasto_vacuna.precio_total,
                                                                             impuesto=gasto_vacuna.impuesto,
                                                                             valor_total_impuesto=gasto_vacuna.valor_total_impuesto)
                            nuevo_gasto_desparasitante.save()
                            gasto_vacuna.delete()

                        mi_vacuna.delete()
                    elif nueva_categoria == 'medicamento':
                        nuevo_medicamento = Medicamento(nombre=mi_vacuna.nombre,
                                                        codigo_inventario=mi_vacuna.codigo_inventario,
                                                        codigo=mi_vacuna.codigo,
                                                        descripcion=mi_vacuna.descripcion,
                                                        cantidad=mi_vacuna.cantidad,
                                                        precio_venta=mi_vacuna.precio_venta,
                                                        unidad_medida=mi_vacuna.unidad_medida,
                                                        cantidad_unidad=mi_vacuna.cantidad_unidad,
                                                        foto=mi_vacuna.foto,
                                                        cantidad_por_unidad=mi_vacuna.cantidad_por_unidad,
                                                        precio_por_unidad=mi_vacuna.precio_por_unidad,
                                                        cantidad_total_unidades=mi_vacuna.cantidad_total_unidades,
                                                        tipo_uso=mi_vacuna.tipo_uso,
                                                        fecha_vencimiento=mi_vacuna.fecha_vencimiento,
                                                        stock_minimo=mi_vacuna.stock_minimo)
                        nuevo_medicamento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Medicamento'
                            historia.id_elemento = nuevo_medicamento.id
                            historia.save()

                        for pago_vacuna in PagoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_pago_medicamento = PagoMedicamento(medicamento=nuevo_medicamento,
                                                                     pago=pago_vacuna.pago,
                                                                     cantidad=pago_vacuna.cantidad,
                                                                     total=pago_vacuna.total,
                                                                     tipo_de_venta=pago_vacuna.tipo_de_venta)
                            nuevo_pago_medicamento.save()
                            pago_vacuna.delete()

                        for gasto_vacuna in GastoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_gasto_medicamento = GastoMedicamento(medicamento=nuevo_medicamento,
                                                                       gasto=gasto_vacuna.gasto,
                                                                       precio_compra_unidad=gasto_vacuna.precio_compra_unidad,
                                                                       unidades=gasto_vacuna.unidades,
                                                                       precio_total=gasto_vacuna.precio_total,
                                                                       impuesto=gasto_vacuna.impuesto,
                                                                       valor_total_impuesto=gasto_vacuna.valor_total_impuesto)
                            nuevo_gasto_medicamento.save()
                            gasto_vacuna.delete()

                        mi_vacuna.delete()
                    elif nueva_categoria == 'insumo':
                        nuevo_insumo = Insumo(nombre=mi_vacuna.nombre,
                                              codigo_inventario=mi_vacuna.codigo_inventario,
                                              codigo=mi_vacuna.codigo,
                                              descripcion=mi_vacuna.descripcion,
                                              cantidad=mi_vacuna.cantidad,
                                              unidad_medida=mi_vacuna.unidad_medida,
                                              cantidad_unidad=mi_vacuna.cantidad_unidad,
                                              foto=mi_vacuna.foto,
                                              cantidad_por_unidad=mi_vacuna.cantidad_por_unidad,
                                              cantidad_total_unidades=mi_vacuna.cantidad_total_unidades,
                                              fecha_vencimiento=mi_vacuna.fecha_vencimiento,
                                              stock_minimo=mi_vacuna.stock_minimo)
                        nuevo_insumo.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Insumo'
                            historia.id_elemento = nuevo_insumo.id
                            historia.save()

                        for gasto_vacuna in GastoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_gasto_insumo = GastoInsumo(insumo=nuevo_insumo,
                                                             gasto=gasto_vacuna.gasto,
                                                             precio_compra_unidad=gasto_vacuna.precio_compra_unidad,
                                                             unidades=gasto_vacuna.unidades,
                                                             precio_total=gasto_vacuna.precio_total,
                                                             impuesto=gasto_vacuna.impuesto,
                                                             valor_total_impuesto=gasto_vacuna.valor_total_impuesto)
                            nuevo_gasto_insumo.save()
                            gasto_vacuna.delete()

                        mi_vacuna.delete()
                    else:
                        mi_categoria = CategoriaInventario.objects.get(id=nueva_categoria)
                        nuevo_elemento = ElementoCategoriaInventario(nombre=mi_vacuna.nombre,
                                              categoria=mi_categoria,
                                              codigo_inventario=mi_vacuna.codigo_inventario,
                                              codigo=mi_vacuna.codigo,
                                              descripcion=mi_vacuna.descripcion,
                                              cantidad=mi_vacuna.cantidad,
                                              precio_venta=mi_vacuna.precio_venta,
                                              unidad_medida=mi_vacuna.unidad_medida,
                                              cantidad_unidad=mi_vacuna.cantidad_unidad,
                                              foto=mi_vacuna.foto,
                                              cantidad_por_unidad=mi_vacuna.cantidad_por_unidad,
                                              precio_por_unidad=mi_vacuna.precio_por_unidad,
                                              cantidad_total_unidades=mi_vacuna.cantidad_total_unidades,
                                              tipo_uso=mi_vacuna.tipo_uso,
                                              fecha_vencimiento=mi_vacuna.fecha_vencimiento,
                                              stock_minimo=mi_vacuna.stock_minimo)
                        nuevo_elemento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Vacuna',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Categoria'
                            historia.id_elemento = nuevo_elemento.id
                            historia.save()

                        for pago_vacuna in PagoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_pago_elemento = PagoElementoInventario(elemento=nuevo_elemento,
                                                           pago=pago_vacuna.pago,
                                                           cantidad=pago_vacuna.cantidad,
                                                           total=pago_vacuna.total,
                                                           tipo_de_venta=pago_vacuna.tipo_de_venta)
                            nuevo_pago_elemento.save()
                            pago_vacuna.delete()

                        for gasto_vacuna in GastoVacuna.objects.filter(vacuna=id_elemento):
                            nuevo_gasto_elemento = GastoElementoInventario(elemento=nuevo_elemento,
                                                             gasto=gasto_vacuna.gasto,
                                                             precio_compra_unidad=gasto_vacuna.precio_compra_unidad,
                                                             unidades=gasto_vacuna.unidades,
                                                             precio_total=gasto_vacuna.precio_total,
                                                             impuesto=gasto_vacuna.impuesto,
                                                             valor_total_impuesto=gasto_vacuna.valor_total_impuesto)
                            nuevo_gasto_elemento.save()
                            gasto_vacuna.delete()

                        mi_vacuna.delete()
                    messages.success(request, 'Cambio de categoria exitoso!')
            elif tipo_elemento == 'desparasitante':
                mi_desparasitante = Desparasitante.objects.get(pk=id_elemento)

                if AplicacionDesparasitacion.objects.filter(desparasitante=mi_desparasitante):
                    messages.error(request,
                                   'No se puede cambiar la categoria de este desparasitante porque ya ha sido aplicado a una mascota')
                elif nueva_categoria == 'insumo' and PagoDesparasitante.objects.filter(desparasitante=id_elemento):
                    messages.error(request,
                                   'No se puede cambiar la categoria de este desparasitante a insumo porque ya ha sido relacionado en una venta')
                else:
                    if nueva_categoria == 'producto':
                        nuevo_producto = Producto(nombre=mi_desparasitante.nombre,
                                                  codigo=mi_desparasitante.codigo_inventario,
                                                  descripcion=mi_desparasitante.descripcion,
                                                  cantidad=mi_desparasitante.cantidad,
                                                  precio_venta=mi_desparasitante.precio_venta,
                                                  unidad_medida=mi_desparasitante.unidad_medida,
                                                  cantidad_unidad=mi_desparasitante.cantidad_unidad,
                                                  foto=mi_desparasitante.foto,
                                                  cantidad_por_unidad=mi_desparasitante.cantidad_por_unidad,
                                                  precio_por_unidad=mi_desparasitante.precio_por_unidad,
                                                  cantidad_total_unidades=mi_desparasitante.cantidad_total_unidades,
                                                  tipo_uso=mi_desparasitante.tipo_uso,
                                                  fecha_vencimiento=mi_desparasitante.fecha_vencimiento,
                                                  stock_minimo=mi_desparasitante.stock_minimo)
                        nuevo_producto.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Producto'
                            historia.id_elemento = nuevo_producto.id
                            historia.save()

                        for pago_desparasitante in PagoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_pago_producto = PagoProducto(producto=nuevo_producto, pago=pago_desparasitante.pago,
                                                               cantidad=pago_desparasitante.cantidad,
                                                               total=pago_desparasitante.total,
                                                               tipo_de_venta=pago_desparasitante.tipo_de_venta)
                            nuevo_pago_producto.save()
                            pago_desparasitante.delete()

                        for gasto_desparasitante in GastoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_gasto_producto = GastoProducto(producto=nuevo_producto,
                                                                 gasto=gasto_desparasitante.gasto,
                                                                 precio_compra_unidad=gasto_desparasitante.precio_compra_unidad,
                                                                 unidades=gasto_desparasitante.unidades,
                                                                 precio_total=gasto_desparasitante.precio_total,
                                                                 impuesto=gasto_desparasitante.impuesto,
                                                                 valor_total_impuesto=gasto_desparasitante.valor_total_impuesto)
                            nuevo_gasto_producto.save()
                            gasto_desparasitante.delete()

                        mi_desparasitante.delete()
                    elif nueva_categoria == 'vacuna':
                        nueva_vacuna = Vacuna(nombre=mi_desparasitante.nombre,
                                              codigo_inventario=mi_desparasitante.codigo_inventario,
                                              codigo=mi_desparasitante.codigo,
                                              descripcion=mi_desparasitante.descripcion,
                                              cantidad=mi_desparasitante.cantidad,
                                              precio_venta=mi_desparasitante.precio_venta,
                                              unidad_medida=mi_desparasitante.unidad_medida,
                                              cantidad_unidad=mi_desparasitante.cantidad_unidad,
                                              foto=mi_desparasitante.foto,
                                              cantidad_por_unidad=mi_desparasitante.cantidad_por_unidad,
                                              precio_por_unidad=mi_desparasitante.precio_por_unidad,
                                              cantidad_total_unidades=mi_desparasitante.cantidad_total_unidades,
                                              tipo_uso=mi_desparasitante.tipo_uso,
                                              fecha_vencimiento=mi_desparasitante.fecha_vencimiento,
                                              stock_minimo=mi_desparasitante.stock_minimo)
                        nueva_vacuna.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Vacuna'
                            historia.id_elemento = nueva_vacuna.id
                            historia.save()

                        for pago_desparasitante in PagoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_pago_vacuna = PagoVacuna(vacuna=nueva_vacuna,
                                                           pago=pago_desparasitante.pago,
                                                           cantidad=pago_desparasitante.cantidad,
                                                           total=pago_desparasitante.total,
                                                           tipo_de_venta=pago_desparasitante.tipo_de_venta)
                            nuevo_pago_vacuna.save()
                            pago_desparasitante.delete()

                        for gasto_desparasitante in GastoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_gasto_vacuna = GastoVacuna(vacuna=nueva_vacuna,
                                                             gasto=gasto_desparasitante.gasto,
                                                             precio_compra_unidad=gasto_desparasitante.precio_compra_unidad,
                                                             unidades=gasto_desparasitante.unidades,
                                                             precio_total=gasto_desparasitante.precio_total,
                                                             impuesto=gasto_desparasitante.impuesto,
                                                             valor_total_impuesto=gasto_desparasitante.valor_total_impuesto)
                            nuevo_gasto_vacuna.save()
                            gasto_desparasitante.delete()

                        mi_desparasitante.delete()
                    elif nueva_categoria == 'medicamento':
                        nuevo_medicamento = Medicamento(nombre=mi_desparasitante.nombre,
                                                        codigo_inventario=mi_desparasitante.codigo_inventario,
                                                        codigo=mi_desparasitante.codigo,
                                                        descripcion=mi_desparasitante.descripcion,
                                                        cantidad=mi_desparasitante.cantidad,
                                                        precio_venta=mi_desparasitante.precio_venta,
                                                        unidad_medida=mi_desparasitante.unidad_medida,
                                                        cantidad_unidad=mi_desparasitante.cantidad_unidad,
                                                        foto=mi_desparasitante.foto,
                                                        cantidad_por_unidad=mi_desparasitante.cantidad_por_unidad,
                                                        precio_por_unidad=mi_desparasitante.precio_por_unidad,
                                                        cantidad_total_unidades=mi_desparasitante.cantidad_total_unidades,
                                                        tipo_uso=mi_desparasitante.tipo_uso,
                                                        fecha_vencimiento=mi_desparasitante.fecha_vencimiento,
                                                        stock_minimo=mi_desparasitante.stock_minimo)
                        nuevo_medicamento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Medicamento'
                            historia.id_elemento = nuevo_medicamento.id
                            historia.save()

                        for pago_desparasitante in PagoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_pago_medicamento = PagoMedicamento(medicamento=nuevo_medicamento,
                                                                     pago=pago_desparasitante.pago,
                                                                     cantidad=pago_desparasitante.cantidad,
                                                                     total=pago_desparasitante.total,
                                                                     tipo_de_venta=pago_desparasitante.tipo_de_venta)
                            nuevo_pago_medicamento.save()
                            pago_desparasitante.delete()

                        for gasto_desparasitante in GastoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_gasto_medicamento = GastoMedicamento(medicamento=nuevo_medicamento,
                                                                       gasto=gasto_desparasitante.gasto,
                                                                       precio_compra_unidad=gasto_desparasitante.precio_compra_unidad,
                                                                       unidades=gasto_desparasitante.unidades,
                                                                       precio_total=gasto_desparasitante.precio_total,
                                                                       impuesto=gasto_desparasitante.impuesto,
                                                                       valor_total_impuesto=gasto_desparasitante.valor_total_impuesto)
                            nuevo_gasto_medicamento.save()
                            gasto_desparasitante.delete()

                        mi_desparasitante.delete()
                    elif nueva_categoria == 'insumo':
                        nuevo_insumo = Insumo(nombre=mi_desparasitante.nombre,
                                              codigo_inventario=mi_desparasitante.codigo_inventario,
                                              codigo=mi_desparasitante.codigo,
                                              descripcion=mi_desparasitante.descripcion,
                                              cantidad=mi_desparasitante.cantidad,
                                              unidad_medida=mi_desparasitante.unidad_medida,
                                              cantidad_unidad=mi_desparasitante.cantidad_unidad,
                                              foto=mi_desparasitante.foto,
                                              cantidad_por_unidad=mi_desparasitante.cantidad_por_unidad,
                                              cantidad_total_unidades=mi_desparasitante.cantidad_total_unidades,
                                              fecha_vencimiento=mi_desparasitante.fecha_vencimiento,
                                              stock_minimo=mi_desparasitante.stock_minimo)
                        nuevo_insumo.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Insumo'
                            historia.id_elemento = nuevo_insumo.id
                            historia.save()

                        for gasto_desparasitante in GastoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_gasto_insumo = GastoInsumo(insumo=nuevo_insumo,
                                                             gasto=gasto_desparasitante.gasto,
                                                             precio_compra_unidad=gasto_desparasitante.precio_compra_unidad,
                                                             unidades=gasto_desparasitante.unidades,
                                                             precio_total=gasto_desparasitante.precio_total,
                                                             impuesto=gasto_desparasitante.impuesto,
                                                             valor_total_impuesto=gasto_desparasitante.valor_total_impuesto)
                            nuevo_gasto_insumo.save()
                            gasto_desparasitante.delete()

                        mi_desparasitante.delete()
                    else:
                        mi_categoria = CategoriaInventario.objects.get(id=nueva_categoria)
                        nuevo_elemento = ElementoCategoriaInventario(nombre=mi_desparasitante.nombre,
                                              categoria=mi_categoria,
                                              codigo_inventario=mi_desparasitante.codigo_inventario,
                                              codigo=mi_desparasitante.codigo,
                                              descripcion=mi_desparasitante.descripcion,
                                              cantidad=mi_desparasitante.cantidad,
                                              precio_venta=mi_desparasitante.precio_venta,
                                              unidad_medida=mi_desparasitante.unidad_medida,
                                              cantidad_unidad=mi_desparasitante.cantidad_unidad,
                                              foto=mi_desparasitante.foto,
                                              cantidad_por_unidad=mi_desparasitante.cantidad_por_unidad,
                                              precio_por_unidad=mi_desparasitante.precio_por_unidad,
                                              cantidad_total_unidades=mi_desparasitante.cantidad_total_unidades,
                                              tipo_uso=mi_desparasitante.tipo_uso,
                                              fecha_vencimiento=mi_desparasitante.fecha_vencimiento,
                                              stock_minimo=mi_desparasitante.stock_minimo)
                        nuevo_elemento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Categoria'
                            historia.id_elemento = nuevo_elemento.id
                            historia.save()

                        for pago_desparasitante in PagoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_pago_elemento = PagoElementoInventario(elemento=nuevo_elemento,
                                                           pago=pago_desparasitante.pago,
                                                           cantidad=pago_desparasitante.cantidad,
                                                           total=pago_desparasitante.total,
                                                           tipo_de_venta=pago_desparasitante.tipo_de_venta)
                            nuevo_pago_elemento.save()
                            pago_desparasitante.delete()

                        for gasto_desparasitante in GastoDesparasitante.objects.filter(desparasitante=id_elemento):
                            nuevo_gasto_elemento = GastoElementoInventario(elemento=nuevo_elemento,
                                                             gasto=gasto_desparasitante.gasto,
                                                             precio_compra_unidad=gasto_desparasitante.precio_compra_unidad,
                                                             unidades=gasto_desparasitante.unidades,
                                                             precio_total=gasto_desparasitante.precio_total,
                                                             impuesto=gasto_desparasitante.impuesto,
                                                             valor_total_impuesto=gasto_desparasitante.valor_total_impuesto)
                            nuevo_gasto_elemento.save()
                            gasto_desparasitante.delete()

                        mi_desparasitante.delete()

                    messages.success(request, 'Cambio de categoria exitoso!')
            elif tipo_elemento == 'insumo':
                mi_insumo = Insumo.objects.get(pk=id_elemento)
                if nueva_categoria == 'producto':
                    nuevo_producto = Producto(nombre=mi_insumo.nombre,
                                              codigo=mi_insumo.codigo_inventario,
                                              descripcion=mi_insumo.descripcion,
                                              cantidad=mi_insumo.cantidad,
                                              precio_venta=0,
                                              unidad_medida=mi_insumo.unidad_medida,
                                              cantidad_unidad=mi_insumo.cantidad_unidad,
                                              foto=mi_insumo.foto,
                                              cantidad_por_unidad=mi_insumo.cantidad_por_unidad,
                                              precio_por_unidad=0,
                                              cantidad_total_unidades=mi_insumo.cantidad_total_unidades,
                                              fecha_vencimiento=mi_insumo.fecha_vencimiento,
                                              stock_minimo=mi_insumo.stock_minimo)
                    nuevo_producto.save()

                    for historia in HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                                       id_elemento=id_elemento):
                        historia.tipo_elemento = 'Producto'
                        historia.id_elemento = nuevo_producto.id
                        historia.save()

                    for gasto_insumo in GastoInsumo.objects.filter(insumo=id_elemento):
                        nuevo_gasto_producto = GastoProducto(producto=nuevo_producto, gasto=gasto_insumo.gasto,
                                                             precio_compra_unidad=gasto_insumo.precio_compra_unidad,
                                                             unidades=gasto_insumo.unidades,
                                                             precio_total=gasto_insumo.precio_total,
                                                             impuesto=gasto_insumo.impuesto,
                                                             valor_total_impuesto=gasto_insumo.valor_total_impuesto)
                        nuevo_gasto_producto.save()
                        gasto_insumo.delete()

                    mi_insumo.delete()
                elif nueva_categoria == 'vacuna':
                    nueva_vacuna = Vacuna(nombre=mi_insumo.nombre,
                                          codigo_inventario=mi_insumo.codigo_inventario,
                                          codigo=mi_insumo.codigo,
                                          descripcion=mi_insumo.descripcion,
                                          cantidad=mi_insumo.cantidad,
                                          precio_venta=0,
                                          unidad_medida=mi_insumo.unidad_medida,
                                          cantidad_unidad=mi_insumo.cantidad_unidad,
                                          foto=mi_insumo.foto,
                                          cantidad_por_unidad=mi_insumo.cantidad_por_unidad,
                                          precio_por_unidad=0,
                                          cantidad_total_unidades=mi_insumo.cantidad_total_unidades,
                                          fecha_vencimiento=mi_insumo.fecha_vencimiento,
                                          stock_minimo=mi_insumo.stock_minimo)
                    nueva_vacuna.save()

                    for historia in HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                                       id_elemento=id_elemento):
                        historia.tipo_elemento = 'Vacuna'
                        historia.id_elemento = nueva_vacuna.id
                        historia.save()

                    for gasto_insumo in GastoInsumo.objects.filter(insumo=id_elemento):
                        nuevo_gasto_vacuna = GastoVacuna(vacuna=nueva_vacuna,
                                                         gasto=gasto_insumo.gasto,
                                                         precio_compra_unidad=gasto_insumo.precio_compra_unidad,
                                                         unidades=gasto_insumo.unidades,
                                                         precio_total=gasto_insumo.precio_total,
                                                         impuesto=gasto_insumo.impuesto,
                                                         valor_total_impuesto=gasto_insumo.valor_total_impuesto)
                        nuevo_gasto_vacuna.save()
                        gasto_insumo.delete()

                    mi_insumo.delete()
                elif nueva_categoria == 'medicamento':
                    nuevo_medicamento = Medicamento(nombre=mi_insumo.nombre,
                                                    codigo_inventario=mi_insumo.codigo_inventario,
                                                    codigo=mi_insumo.codigo,
                                                    descripcion=mi_insumo.descripcion,
                                                    cantidad=mi_insumo.cantidad,
                                                    precio_venta=0,
                                                    unidad_medida=mi_insumo.unidad_medida,
                                                    cantidad_unidad=mi_insumo.cantidad_unidad,
                                                    foto=mi_insumo.foto,
                                                    cantidad_por_unidad=mi_insumo.cantidad_por_unidad,
                                                    precio_por_unidad=0,
                                                    cantidad_total_unidades=mi_insumo.cantidad_total_unidades,
                                                    fecha_vencimiento=mi_insumo.fecha_vencimiento,
                                                    stock_minimo=mi_insumo.stock_minimo)
                    nuevo_medicamento.save()

                    for historia in HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                                       id_elemento=id_elemento):
                        historia.tipo_elemento = 'Medicamento'
                        historia.id_elemento = nuevo_medicamento.id
                        historia.save()

                    for gasto_insumo in GastoInsumo.objects.filter(insumo=id_elemento):
                        nuevo_gasto_medicamento = GastoMedicamento(medicamento=nuevo_medicamento,
                                                                   gasto=gasto_insumo.gasto,
                                                                   precio_compra_unidad=gasto_insumo.precio_compra_unidad,
                                                                   unidades=gasto_insumo.unidades,
                                                                   precio_total=gasto_insumo.precio_total,
                                                                   impuesto=gasto_insumo.impuesto,
                                                                   valor_total_impuesto=gasto_insumo.valor_total_impuesto)
                        nuevo_gasto_medicamento.save()
                        gasto_insumo.delete()

                    mi_insumo.delete()
                elif nueva_categoria == 'desparasitante':
                    nuevo_desparasitante = Desparasitante(nombre=mi_insumo.nombre,
                                          codigo_inventario=mi_insumo.codigo_inventario,
                                          codigo=mi_insumo.codigo,
                                          descripcion=mi_insumo.descripcion,
                                          cantidad=mi_insumo.cantidad,
                                          precio_venta=0,
                                          unidad_medida=mi_insumo.unidad_medida,
                                          cantidad_unidad=mi_insumo.cantidad_unidad,
                                          foto=mi_insumo.foto,
                                          cantidad_por_unidad=mi_insumo.cantidad_por_unidad,
                                          precio_por_unidad=0,
                                          cantidad_total_unidades=mi_insumo.cantidad_total_unidades,
                                          fecha_vencimiento=mi_insumo.fecha_vencimiento,
                                          stock_minimo=mi_insumo.stock_minimo)
                    nuevo_desparasitante.save()

                    for historia in HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                                       id_elemento=id_elemento):
                        historia.tipo_elemento = 'Desparasitante'
                        historia.id_elemento = nuevo_desparasitante.id
                        historia.save()

                    for gasto_insumo in GastoInsumo.objects.filter(insumo=id_elemento):
                        nuevo_gasto_desparasitante = GastoDesparasitante(desparasitante=nuevo_desparasitante,
                                                         gasto=gasto_insumo.gasto,
                                                         precio_compra_unidad=gasto_insumo.precio_compra_unidad,
                                                         unidades=gasto_insumo.unidades,
                                                         precio_total=gasto_insumo.precio_total,
                                                         impuesto=gasto_insumo.impuesto,
                                                         valor_total_impuesto=gasto_insumo.valor_total_impuesto)
                        nuevo_gasto_desparasitante.save()
                        gasto_insumo.delete()

                    mi_insumo.delete()
                else:
                    mi_categoria = CategoriaInventario.objects.get(id=nueva_categoria)
                    nuevo_elemento = ElementoCategoriaInventario(nombre=mi_insumo.nombre,
                                                                 categoria=mi_categoria,
                                                                 codigo_inventario=mi_insumo.codigo_inventario,
                                                                 codigo=mi_insumo.codigo,
                                                                 descripcion=mi_insumo.descripcion,
                                                                 cantidad=mi_insumo.cantidad,
                                                                 precio_venta=0,
                                                                 unidad_medida=mi_insumo.unidad_medida,
                                                                 cantidad_unidad=mi_insumo.cantidad_unidad,
                                                                 foto=mi_insumo.foto,
                                                                 cantidad_por_unidad=mi_insumo.cantidad_por_unidad,
                                                                 precio_por_unidad=0,
                                                                 cantidad_total_unidades=mi_insumo.cantidad_total_unidades,
                                                                 fecha_vencimiento=mi_insumo.fecha_vencimiento,
                                                                 stock_minimo=mi_insumo.stock_minimo)
                    nuevo_elemento.save()

                    for historia in HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                                       id_elemento=id_elemento):
                        historia.tipo_elemento = 'Categoria'
                        historia.id_elemento = nuevo_elemento.id
                        historia.save()

                    for gasto_insumo in GastoInsumo.objects.filter(insumo=id_elemento):
                        nuevo_gasto_elemento = GastoElementoInventario(elemento=nuevo_elemento,
                                                                       gasto=gasto_insumo.gasto,
                                                                       precio_compra_unidad=gasto_insumo.precio_compra_unidad,
                                                                       unidades=gasto_insumo.unidades,
                                                                       precio_total=gasto_insumo.precio_total,
                                                                       impuesto=gasto_insumo.impuesto,
                                                                       valor_total_impuesto=gasto_insumo.valor_total_impuesto)
                        nuevo_gasto_elemento.save()
                        gasto_insumo.delete()

                    mi_insumo.delete()

                messages.success(request, 'Cambio de categoria exitoso!')
            else:
                mi_elemento = ElementoCategoriaInventario.objects.get(pk=id_elemento)
                if nueva_categoria == 'insumo' and PagoElementoInventario.objects.filter(elemento=id_elemento):
                    messages.error(request,
                                   'No se puede cambiar la categoria de este elemento a insumo porque ya ha sido relacionado en una venta')
                else:
                    if nueva_categoria == 'producto':
                        nuevo_producto = Producto(nombre=mi_elemento.nombre,
                                                  codigo=mi_elemento.codigo_inventario,
                                                  descripcion=mi_elemento.descripcion,
                                                  cantidad=mi_elemento.cantidad,
                                                  precio_venta=mi_elemento.precio_venta,
                                                  unidad_medida=mi_elemento.unidad_medida,
                                                  cantidad_unidad=mi_elemento.cantidad_unidad,
                                                  foto=mi_elemento.foto,
                                                  cantidad_por_unidad=mi_elemento.cantidad_por_unidad,
                                                  precio_por_unidad=mi_elemento.precio_por_unidad,
                                                  cantidad_total_unidades=mi_elemento.cantidad_total_unidades,
                                                  tipo_uso=mi_elemento.tipo_uso,
                                                  fecha_vencimiento=mi_elemento.fecha_vencimiento,
                                                  stock_minimo=mi_elemento.stock_minimo)
                        nuevo_producto.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Producto'
                            historia.id_elemento = nuevo_producto.id
                            historia.save()

                        for pago_elemento in PagoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_pago_producto = PagoProducto(producto=nuevo_producto, pago=pago_elemento.pago,
                                                               cantidad=pago_elemento.cantidad,
                                                               total=pago_elemento.total,
                                                               tipo_de_venta=pago_elemento.tipo_de_venta)
                            nuevo_pago_producto.save()
                            pago_elemento.delete()

                        for gasto_elemento in GastoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_gasto_producto = GastoProducto(producto=nuevo_producto,
                                                                 gasto=gasto_elemento.gasto,
                                                                 precio_compra_unidad=gasto_elemento.precio_compra_unidad,
                                                                 unidades=gasto_elemento.unidades,
                                                                 precio_total=gasto_elemento.precio_total,
                                                                 impuesto=gasto_elemento.impuesto,
                                                                 valor_total_impuesto=gasto_elemento.valor_total_impuesto)
                            nuevo_gasto_producto.save()
                            gasto_elemento.delete()

                        mi_elemento.delete()
                    elif nueva_categoria == 'vacuna':
                        nueva_vacuna = Vacuna(nombre=mi_elemento.nombre,
                                              codigo_inventario=mi_elemento.codigo_inventario,
                                              codigo=mi_elemento.codigo,
                                              descripcion=mi_elemento.descripcion,
                                              cantidad=mi_elemento.cantidad,
                                              precio_venta=mi_elemento.precio_venta,
                                              unidad_medida=mi_elemento.unidad_medida,
                                              cantidad_unidad=mi_elemento.cantidad_unidad,
                                              foto=mi_elemento.foto,
                                              cantidad_por_unidad=mi_elemento.cantidad_por_unidad,
                                              precio_por_unidad=mi_elemento.precio_por_unidad,
                                              cantidad_total_unidades=mi_elemento.cantidad_total_unidades,
                                              tipo_uso=mi_elemento.tipo_uso,
                                              fecha_vencimiento=mi_elemento.fecha_vencimiento,
                                              stock_minimo=mi_elemento.stock_minimo)
                        nueva_vacuna.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Vacuna'
                            historia.id_elemento = nueva_vacuna.id
                            historia.save()

                        for pago_elemento in PagoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_pago_vacuna = PagoVacuna(vacuna=nueva_vacuna,
                                                           pago=pago_elemento.pago,
                                                           cantidad=pago_elemento.cantidad,
                                                           total=pago_elemento.total,
                                                           tipo_de_venta=pago_elemento.tipo_de_venta)
                            nuevo_pago_vacuna.save()
                            pago_elemento.delete()

                        for gasto_elemento in GastoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_gasto_vacuna = GastoVacuna(vacuna=nueva_vacuna,
                                                             gasto=gasto_elemento.gasto,
                                                             precio_compra_unidad=gasto_elemento.precio_compra_unidad,
                                                             unidades=gasto_elemento.unidades,
                                                             precio_total=gasto_elemento.precio_total,
                                                             impuesto=gasto_elemento.impuesto,
                                                             valor_total_impuesto=gasto_elemento.valor_total_impuesto)
                            nuevo_gasto_vacuna.save()
                            gasto_elemento.delete()

                        mi_elemento.delete()
                    elif nueva_categoria == 'medicamento':
                        nuevo_medicamento = Medicamento(nombre=mi_elemento.nombre,
                                                        codigo_inventario=mi_elemento.codigo_inventario,
                                                        codigo=mi_elemento.codigo,
                                                        descripcion=mi_elemento.descripcion,
                                                        cantidad=mi_elemento.cantidad,
                                                        precio_venta=mi_elemento.precio_venta,
                                                        unidad_medida=mi_elemento.unidad_medida,
                                                        cantidad_unidad=mi_elemento.cantidad_unidad,
                                                        foto=mi_elemento.foto,
                                                        cantidad_por_unidad=mi_elemento.cantidad_por_unidad,
                                                        precio_por_unidad=mi_elemento.precio_por_unidad,
                                                        cantidad_total_unidades=mi_elemento.cantidad_total_unidades,
                                                        tipo_uso=mi_elemento.tipo_uso,
                                                        fecha_vencimiento=mi_elemento.fecha_vencimiento,
                                                        stock_minimo=mi_elemento.stock_minimo)
                        nuevo_medicamento.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Medicamento'
                            historia.id_elemento = nuevo_medicamento.id
                            historia.save()

                        for pago_elemento in PagoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_pago_medicamento = PagoMedicamento(medicamento=nuevo_medicamento,
                                                                     pago=pago_elemento.pago,
                                                                     cantidad=pago_elemento.cantidad,
                                                                     total=pago_elemento.total,
                                                                     tipo_de_venta=pago_elemento.tipo_de_venta)
                            nuevo_pago_medicamento.save()
                            pago_elemento.delete()

                        for gasto_elemento in GastoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_gasto_medicamento = GastoMedicamento(medicamento=nuevo_medicamento,
                                                                       gasto=gasto_elemento.gasto,
                                                                       precio_compra_unidad=gasto_elemento.precio_compra_unidad,
                                                                       unidades=gasto_elemento.unidades,
                                                                       precio_total=gasto_elemento.precio_total,
                                                                       impuesto=gasto_elemento.impuesto,
                                                                       valor_total_impuesto=gasto_elemento.valor_total_impuesto)
                            nuevo_gasto_medicamento.save()
                            gasto_elemento.delete()

                        mi_elemento.delete()
                    elif nueva_categoria == 'insumo':
                        nuevo_insumo = Insumo(nombre=mi_elemento.nombre,
                                              codigo_inventario=mi_elemento.codigo_inventario,
                                              codigo=mi_elemento.codigo,
                                              descripcion=mi_elemento.descripcion,
                                              cantidad=mi_elemento.cantidad,
                                              unidad_medida=mi_elemento.unidad_medida,
                                              cantidad_unidad=mi_elemento.cantidad_unidad,
                                              foto=mi_elemento.foto,
                                              cantidad_por_unidad=mi_elemento.cantidad_por_unidad,
                                              cantidad_total_unidades=mi_elemento.cantidad_total_unidades,
                                              fecha_vencimiento=mi_elemento.fecha_vencimiento,
                                              stock_minimo=mi_elemento.stock_minimo)
                        nuevo_insumo.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Insumo'
                            historia.id_elemento = nuevo_insumo.id
                            historia.save()

                        for gasto_elemento in GastoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_gasto_insumo = GastoInsumo(insumo=nuevo_insumo,
                                                             gasto=gasto_elemento.gasto,
                                                             precio_compra_unidad=gasto_elemento.precio_compra_unidad,
                                                             unidades=gasto_elemento.unidades,
                                                             precio_total=gasto_elemento.precio_total,
                                                             impuesto=gasto_elemento.impuesto,
                                                             valor_total_impuesto=gasto_elemento.valor_total_impuesto)
                            nuevo_gasto_insumo.save()
                            gasto_elemento.delete()

                        mi_elemento.delete()
                    elif nueva_categoria == 'desparasitante':
                        nuevo_desparasitante = Desparasitante(nombre=mi_elemento.nombre,
                                                              codigo_inventario=mi_elemento.codigo_inventario,
                                                              codigo=mi_elemento.codigo,
                                                              descripcion=mi_elemento.descripcion,
                                                              cantidad=mi_elemento.cantidad,
                                                              precio_venta=mi_elemento.precio_venta,
                                                              unidad_medida=mi_elemento.unidad_medida,
                                                              cantidad_unidad=mi_elemento.cantidad_unidad,
                                                              foto=mi_elemento.foto,
                                                              cantidad_por_unidad=mi_elemento.cantidad_por_unidad,
                                                              precio_por_unidad=mi_elemento.precio_por_unidad,
                                                              cantidad_total_unidades=mi_elemento.cantidad_total_unidades,
                                                              tipo_uso=mi_elemento.tipo_uso,
                                                              fecha_vencimiento=mi_elemento.fecha_vencimiento,
                                                              stock_minimo=mi_elemento.stock_minimo)
                        nuevo_desparasitante.save()

                        for historia in HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                                           id_elemento=id_elemento):
                            historia.tipo_elemento = 'Desparasitante'
                            historia.id_elemento = nuevo_desparasitante.id
                            historia.save()

                        for pago_elemento in PagoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_pago_desparasitante = PagoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                           pago=pago_elemento.pago,
                                                                           cantidad=pago_elemento.cantidad,
                                                                           total=pago_elemento.total,
                                                                           tipo_de_venta=pago_elemento.tipo_de_venta)
                            nuevo_pago_desparasitante.save()
                            pago_elemento.delete()

                        for gasto_elemento in GastoElementoInventario.objects.filter(elemento=id_elemento):
                            nuevo_gasto_desparasitante = GastoDesparasitante(desparasitante=nuevo_desparasitante,
                                                                             gasto=gasto_elemento.gasto,
                                                                             precio_compra_unidad=gasto_elemento.precio_compra_unidad,
                                                                             unidades=gasto_elemento.unidades,
                                                                             precio_total=gasto_elemento.precio_total,
                                                                             impuesto=gasto_elemento.impuesto,
                                                                             valor_total_impuesto=gasto_elemento.valor_total_impuesto)
                            nuevo_gasto_desparasitante.save()
                            gasto_elemento.delete()

                        mi_elemento.delete()
                    else:
                        mi_categoria = CategoriaInventario.objects.get(id=nueva_categoria)
                        mi_elemento.categoria = mi_categoria
                        mi_elemento.save()
                    messages.success(request, 'Cambio de categoria exitoso!')

        return redirect('listar_productos')
    else:
        return render(request, 'cambiar_categoria_inventario.html', {
            'tipo_elemento': tipo_elemento,
            'menu_inventario': True,
            'categorias': json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))
        })


def datos_impresion_venta(request):
    id_venta = request.GET.get('id_venta', None)

    venta = Pago.objects.get(pk=id_venta)
    veterinaria = Veterinaria.objects.first()

    logo_veterinaria = ""

    if veterinaria.foto:
        with open("media/" + str(veterinaria.foto), "rb") as pdf_file:
            encoded_string = base64.b64encode(pdf_file.read())
            logo_veterinaria = encoded_string

    datos_veterinaria = {'nombre_veterinaria': veterinaria.nombre,
                         'nit': veterinaria.nit,
                         'direccion': veterinaria.direccion,
                         'ciudad': veterinaria.ciudad,
                         'telefono': veterinaria.telefono,
                         'celular': veterinaria.celular,
                         'logo_veterinaria': str(logo_veterinaria)}

    elementos_vendidos = []

    for pago_producto in PagoProducto.objects.filter(pago_id=id_venta):
        item = {"cantidad": str(pago_producto.cantidad),
                "descripcion": pago_producto.producto.nombre,
                "precio_unidad": str(pago_producto.producto.precio_venta),
                "precio_total": str(pago_producto.total)}
        elementos_vendidos.append(item)

    for pago_medicamento in PagoMedicamento.objects.filter(pago_id=id_venta):
        item = {"cantidad": str(pago_medicamento.cantidad),
                "descripcion": pago_medicamento.medicamento.nombre,
                "precio_unidad": str(pago_medicamento.medicamento.precio_venta),
                "precio_total": str(pago_medicamento.total)}
        elementos_vendidos.append(item)

    for pago_vacuna in PagoVacuna.objects.filter(pago_id=id_venta):
        item = {"cantidad": str(pago_vacuna.cantidad),
                "descripcion": pago_vacuna.vacuna.nombre,
                "precio_unidad": str(pago_vacuna.vacuna.precio_venta),
                "precio_total": str(pago_vacuna.total)}
        elementos_vendidos.append(item)

    for pago_desparasitante in PagoDesparasitante.objects.filter(pago_id=id_venta):
        item = {"cantidad": str(pago_desparasitante.cantidad),
                "descripcion": pago_desparasitante.desparasitante.nombre,
                "precio_unidad": str(pago_desparasitante.desparasitante.precio_venta),
                "precio_total": str(pago_desparasitante.total)}
        elementos_vendidos.append(item)

    for pago_cita in PagoTratamiento.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": "Cita médica - " + pago_cita.tratamiento.motivo_cita.nombre,
                "precio_unidad": "-",
                "precio_total": str(pago_cita.total)}
        elementos_vendidos.append(item)

    for pago_procedimiento in PagoAreaConsulta.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": "Cita médica - " + pago_procedimiento.area_consulta.area_consulta.nombre,
                "precio_unidad": "-",
                "precio_total": str(pago_procedimiento.total)}
        elementos_vendidos.append(item)

    for pago_estetica in PagoEstetica.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": pago_estetica.estetica.tipo_estetica,
                "precio_unidad": "-",
                "precio_total": str(pago_estetica.total)}
        elementos_vendidos.append(item)

    for pago_vacunacion in PagoVacunacion.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": pago_vacunacion.vacunacion.descripcion,
                "precio_unidad": "-",
                "precio_total": str(pago_vacunacion.total)}
        elementos_vendidos.append(item)

    for pago_desparasitacion in PagoDesparasitacion.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": pago_desparasitacion.desparasitacion.descripcion,
                "precio_unidad": "-",
                "precio_total": str(pago_desparasitacion.total)}
        elementos_vendidos.append(item)

    for pago_guarderia in PagoGuarderia.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": pago_guarderia.guarderia.fecha_inicio + " - " + pago_guarderia.guarderia.fecha_fin,
                "precio_unidad": "-",
                "precio_total": str(pago_guarderia.total)}
        elementos_vendidos.append(item)

    for pago_otro in PagoOtro.objects.filter(pago_id=id_venta):
        item = {"cantidad": "-",
                "descripcion": pago_otro.descripcion,
                "precio_unidad": "-",
                "precio_total": str(pago_otro.total)}
        elementos_vendidos.append(item)

    nombre_cliente = ''
    tipo_documento_cliente = ''
    numero_documento_cliente = ''
    direccion_cliente = ''

    if venta.cliente:
        nombre_cliente = venta.cliente.nombres + " " + venta.cliente.apellidos
        tipo_documento_cliente = venta.cliente.tipo_documento
        numero_documento_cliente = venta.cliente.numero_documento
        direccion_cliente = venta.cliente.direccion

    numero_recibo = ''

    if venta.id < 10:
        numero_recibo = "#0000" + str(venta.id)
    elif venta.id < 100:
        numero_recibo = "#000" + str(venta.id)
    elif venta.id < 1000:
        numero_recibo = "#00" + str(venta.id)
    elif venta.id < 10000:
        numero_recibo = "#0" + str(venta.id)
    elif venta.id < 100000:
        numero_recibo = "#" + str(venta.id)

    datos_impresion = {'elementos_vendidos': elementos_vendidos,
                       'numero_recibo': numero_recibo,
                       'total': str(venta.total_factura),
                       'sub_total': str(venta.sub_total),
                       'descuento': str(venta.descuento),
                       'fecha_venta': str(venta.fecha.strftime("%d de %B de %Y")),
                       'nombre_cliente': nombre_cliente,
                       'tipo_documento_cliente': tipo_documento_cliente,
                       'numero_documento_cliente': numero_documento_cliente,
                       'direccion_cliente': direccion_cliente,
                       'datos_veterinaria': datos_veterinaria}

    data = {
        'datos_veterinaria': serializers.serialize('json', [veterinaria]),
        'datos_venta': json.dumps(datos_impresion)
    }
    return JsonResponse(data)


def obtener_medicamentos(request):

    all_medicamentos = Medicamento.objects.filter(estado='Activo',bodega=1).exclude(tipo_uso='Comercial').exclude(Q(cantidad=0) & Q(cantidad_por_unidad=False)).exclude(Q(cantidad_total_unidades=0) & Q(cantidad_por_unidad=True))

    elementos_por_codigo = chain(
        Medicamento.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
            codigo_inventario__isnull=True).exclude(tipo_uso='Comercial'),
        Medicamento.objects.filter(codigo_inventario__isnull=True, estado='Activo',bodega=1).exclude(tipo_uso='Comercial'))
    elementos_repetidos = []
    for element in list(
            Medicamento.objects.filter(bodega=1).exclude(tipo_uso='Comercial').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
        if element['cantidad'] > 1:
            elementos_repetidos.append(element['codigo_inventario'])
    data = {
        'all_medicamentos': serializers.serialize('json', all_medicamentos),
        'elementos_por_codigo': serializers.serialize('json', elementos_por_codigo),
        'codigos_elementos': elementos_repetidos
    }
    return JsonResponse(data)

def validate_username_debe(request):
    entro = request.GET.get('entro', None)
    clientes = Cliente.objects.all()

    clientes_mascotas = []
    for cliente in clientes:
        citas_estetica_cliente = FacturaEstetica.objects.filter(mascota__cliente=cliente, estado='Activa', estado_de_atencion='Atendida')
        tratamientos_cliente = HistoriaClinica.objects.filter(mascota__cliente=cliente, estado='Activa')
        vacunaciones_cliente = Vacunacion.objects.filter(mascota__cliente=cliente, estado='Activa',estado_aplicacion='Aplicada').order_by('-mascota')
        desparasitaciones_cliente = Desparasitacion.objects.filter(mascota__cliente=cliente, estado='Activo',estado_aplicacion='Aplicada').order_by('-mascota')
        guarderias_cliente = Guarderia.objects.filter(mascota__cliente=cliente, estado='Activa')

        if citas_estetica_cliente or tratamientos_cliente or vacunaciones_cliente or desparasitaciones_cliente or guarderias_cliente:
            clientes_deben = {
                'id': int(float(cliente.id)),
                'nombre': cliente.nombres + " " + cliente.apellidos,
                'documento': cliente.tipo_documento + " " + cliente.numero_documento,
                'ciudad': cliente.ciudad,
            }
            clientes_mascotas.append(clientes_deben)

    data = {
        'clientes': json.dumps(clientes_mascotas, cls=DecimalEncoder),
        'is_taken': True
    }
    return JsonResponse(data)