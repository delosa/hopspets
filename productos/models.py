from django.db import models
from clientes.models import Cliente
from sorl.thumbnail import ImageField, get_thumbnail
from citas.models import HistoriaClinica, MotivoArea, Hospitalizacion, Cirugia
from estetica.models import FacturaEstetica
from vacunacion.models import Vacunacion, Vacuna
from desparasitacion.models import Desparasitacion, Desparasitante
from guarderia.models import Guarderia
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from usuarios.models import Usuario
from categoriasInventario.models import ElementoCategoriaInventario
from bodegas.models import Bodega


class PagoProducto(models.Model):
    producto = models.ForeignKey('Producto', on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1,)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    tipo_de_venta = models.CharField(max_length=500, verbose_name='Tipo venta', blank=True, null=True)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoMedicamento(models.Model):
    medicamento = models.ForeignKey('Medicamento', on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1,)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    tipo_de_venta = models.CharField(max_length=500, verbose_name='Tipo venta', blank=True, null=True)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoVacuna(models.Model):
    vacuna = models.ForeignKey(Vacuna, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1,)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    tipo_de_venta = models.CharField(max_length=500, verbose_name='Tipo venta', blank=True, null=True)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoDesparasitante(models.Model):
    desparasitante = models.ForeignKey(Desparasitante, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1,)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    tipo_de_venta = models.CharField(max_length=500, verbose_name='Tipo venta', blank=True, null=True)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoElementoInventario(models.Model):
    elemento = models.ForeignKey(ElementoCategoriaInventario, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    tipo_de_venta = models.CharField(max_length=500, verbose_name='Tipo venta', blank=True, null=True)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)

class PagoTratamiento(models.Model):
    tratamiento = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)

class PagoAreaConsulta(models.Model):
    area_consulta = models.ForeignKey(MotivoArea, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)

class PagoEstetica(models.Model):
    estetica = models.ForeignKey(FacturaEstetica, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoVacunacion(models.Model):
    vacunacion = models.ForeignKey(Vacunacion, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoDesparasitacion(models.Model):
    desparasitacion = models.ForeignKey(Desparasitacion, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoGuarderia(models.Model):
    guarderia = models.ForeignKey(Guarderia, on_delete=models.CASCADE)
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)


class PagoOtro(models.Model):
    descripcion = models.CharField(max_length=300, verbose_name='Descripcion')
    pago = models.ForeignKey('Pago', on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=50, decimal_places=0)
    total_punto = models.ForeignKey('TotalFacturaPunto', on_delete=models.CASCADE, blank=True, null=True)

class TotalFacturaPunto(models.Model):
    total_factura = models.DecimalField(max_digits=50, decimal_places=0)
    punto_abierto = models.IntegerField(default=0)

TIPOS_DE_COMPROBANTE = (('Venta', 'Venta'), ('Nota Débito', 'Nota Débito'))

class Pago(models.Model):
    fecha_registro = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha = models.DateField()
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, blank=True, null=True)
    direccion = models.CharField(max_length=100, null=True, blank=True)
    comentarios = models.TextField()
    total_factura = models.DecimalField(max_digits=50, decimal_places=0)
    total_impuesto = models.DecimalField(max_digits=50, decimal_places=0)
    sub_total = models.DecimalField(max_digits=50, decimal_places=0)
    descuento = models.DecimalField(max_digits=50, decimal_places=0)
    forma_pago = models.CharField(max_length=400, verbose_name='Forma pago', default='Debito')
    tipo_pago_debito = models.CharField(max_length=400, verbose_name='Tipo pago', blank=True, null=True)
    tipo_pago_credito = models.CharField(max_length=400, verbose_name='Tipo pago credito', blank=True, null=True)
    numero_cuotas_credito = models.CharField(max_length=400, verbose_name='Número de cuotas', blank=True, null=True)
    saldo = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    estado_pago = models.CharField(max_length=400, verbose_name='Estado', default='Pagado')
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    tipo_comprobante = models.CharField(max_length=100, verbose_name="Tipo de comprobante", choices=TIPOS_DE_COMPROBANTE, default='Venta')
    proveedor_nota_debito = models.ForeignKey('Proveedor', on_delete=models.CASCADE, blank=True, null=True)
    total_factura_punto = models.ForeignKey(TotalFacturaPunto, on_delete=models.CASCADE, blank=True, null=True)
    cambio = models.DecimalField(max_digits=50, decimal_places=0, blank=True, null=True)

    def valor_pagado(self):
        return self.total_factura - self.saldo

    def __str__(self):
        return '%s %s' % (self.fecha, self.cliente)


TIPOS_DE_DOCUMENTO = (('CC', 'Cédula de Ciudadania'), ('NI', 'Nit'), ('RU', 'Rut'))
TIPOS_DE_TERCERO = (('Proveedor', 'Proveedor'), ('Empleado', 'Colaborador'))


class Proveedor(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    ciudad = models.CharField(max_length=200, verbose_name='Ciudad')
    direccion = models.CharField(max_length=200, verbose_name='Dirección')
    telefono = models.CharField(max_length=200, verbose_name='Teléfono')
    celular = models.CharField(max_length=200, verbose_name='Celular')
    email = models.CharField(max_length=200, verbose_name='Email')
    tipo_documento = models.CharField(max_length=2, verbose_name="Tipo de documento", choices=TIPOS_DE_DOCUMENTO)
    numero_documento = models.CharField(max_length=20, verbose_name='Número de documento', unique=True, error_messages={'unique': "Ya existe un proveedor con este número de documento."})
    estado = models.BooleanField(default=True)
    tipo_tercero = models.CharField(max_length=100, verbose_name="Tipo de tercero",choices=TIPOS_DE_TERCERO, default='Proveedor')

    def __str__(self):
        return self.nombre


TIPOS_DE_PRODUCTO = (('Usable', 'Usable'), ('Consumible', 'Consumible'))


def get_upload_to(instance, filename):
    return 'upload/productos/%s/%s' % (instance.nombre, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')

UNIDADES_MEDIDA = (('mililitros','Mililitros (ml)'),('litros','Litros (l)'),('gramos','Gramos (g)'),('miligramos','Miligramos (mg)'),('kilogramos','Kilogramos (kg)'),('libras','Libras (lb)'))

TIPOS_DE_USO = (('Interno', 'Interno'), ('Comercial', 'Comercial'),('Interno/Comercial', 'Interno/Comercial'))

class Producto(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    codigo = models.CharField(max_length=50, verbose_name='Código', blank=True, null=True)
    descripcion = models.CharField(max_length=300, verbose_name='Descripción', null=True, blank=True)
    tipo_producto = models.CharField(max_length=50, verbose_name='Tipo de producto', choices=TIPOS_DE_PRODUCTO, default='Usable')
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    precio_venta = models.CharField(max_length=50, verbose_name='Precio de venta')
    estado = models.BooleanField(default=True)
    unidad_medida = models.CharField(max_length=100, verbose_name='Unidad de medida', choices=UNIDADES_MEDIDA, default='ml')
    cantidad_unidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad por unidad', default=0)
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    cantidad_por_unidad = models.BooleanField(default=False)
    precio_por_unidad = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    cantidad_total_unidades = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad total unidades', default=0)
    tipo_uso = models.CharField(max_length=50, verbose_name='Tipo de uso', choices=TIPOS_DE_USO, default='Interno/Comercial')
    fecha_vencimiento = models.DateField(blank=True, null=True)
    stock_minimo = models.PositiveIntegerField(verbose_name='Stock minimo', default=1)
    bodega = models.ForeignKey(Bodega, on_delete=models.CASCADE, default=1)
    precio_compra = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    """def save(self, *args, **kwargs):
        if self.foto:
            self.foto = get_thumbnail(self.foto, '500x600', quality=99, format='JPEG').name
        super(Producto, self).save(*args, **kwargs)"""

    def save(self, *args, **kwargs):
        if self.cantidad_total_unidades == 0:
            self.cantidad_total_unidades = float(self.cantidad) * float(self.cantidad_unidad)
        if self.foto:
            if self.foto.url.endswith('.png') or self.foto.url.endswith('.PNG'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(Producto, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()

            if imageTemproary.width > 1028 or imageTemproary.height > 1028:
                maxsize = (1028, 1028)
                imageTemproary.thumbnail(maxsize, Image.ANTIALIAS)

            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        return self.nombre


class CompraProducto(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    costo = models.DecimalField(max_digits=50, decimal_places=0)
    cantidad = models.IntegerField(verbose_name='Cantidad')
    fecha_compra = models.DateTimeField(auto_now_add=True)


class PaqueteProducto(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    numero_productos_paquete = models.IntegerField(verbose_name='Número de productos por paquete')
    cantidad_disponible = models.IntegerField(verbose_name='Cantidad disponible')
    precio_venta = models.CharField(max_length=50, verbose_name='Precio de venta')
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    tipo_paquete = models.CharField(max_length=100, verbose_name='Tipo de paquete')


ESTADOS = (('Activo','Activo'),('Inactivo','Inactivo'))
UNIDADES_MEDIDA = (('ml','Mililitros (ml)'),('l','Litros (l)'),('g','Gramos (g)'),('mg','Miligramos (mg)'),('kg','Kilogramos (kg)'), ('tabs', 'Tabletas (tabs)'))


class Medicamento(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    codigo = models.CharField(max_length=50, verbose_name='Número de lote', blank=True, null=True)
    codigo_inventario = models.CharField(max_length=50, verbose_name='Código', blank=True, null=True)
    descripcion = models.CharField(max_length=800, verbose_name='Descripción', null=True, blank=True)
    estado = models.CharField(max_length=20, verbose_name="Estado", choices=ESTADOS, default='Activo')
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    precio_venta = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    unidad_medida = models.CharField(max_length=100, verbose_name='Unidad de medida', choices=UNIDADES_MEDIDA, default='ml')
    cantidad_unidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad por unidad', default=0)
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    cantidad_por_unidad = models.BooleanField(default=False)
    precio_por_unidad = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    cantidad_total_unidades = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad total unidades', default=0)
    tipo_uso = models.CharField(max_length=50, verbose_name='Tipo de uso', choices=TIPOS_DE_USO, default='Interno/Comercial')
    fecha_vencimiento = models.DateField(blank=True, null=True)
    stock_minimo = models.PositiveIntegerField(verbose_name='Stock minimo', default=1)
    bodega = models.ForeignKey(Bodega, on_delete=models.CASCADE, default=1)
    precio_compra = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    def save(self, *args, **kwargs):
        if self.cantidad_total_unidades == 0:
            self.cantidad_total_unidades = float(self.cantidad) * float(self.cantidad_unidad)
        if self.foto:
            if self.foto.url.endswith('.png'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(Medicamento, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        if self.codigo:
            return self.nombre + " - " + self.codigo
        else:
            return self.nombre


class HistorialInventario(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    nombre_transaccion = models.CharField(max_length=100, verbose_name='Nombre')
    tipo_elemento = models.CharField(max_length=100, verbose_name='Tipo de elemento')
    id_elemento = models.IntegerField(verbose_name='Id elemento', null=True, blank=True)
    tipo_transaccion = models.CharField(max_length=100, verbose_name='Tipo de transacción', default='Inventario')
    id_transaccion = models.IntegerField(verbose_name='Id transacción', default=0)
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Responsable")


class CuotaPago(models.Model):
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)
    fecha_limite = models.DateField()
    fecha_pago = models.DateField(blank=True, null=True)
    valor_pagado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    valor_cuota = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    estado_pago = models.CharField(max_length=400, verbose_name='Estado', default='Pendiente')
    saldo_cuota = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    def __str__(self):
        return '%s %s' % (self.fecha, self.cliente)

class HistoriaMedicamento(models.Model):
    medicamento =  models.ForeignKey(Medicamento , on_delete=models.CASCADE)
    historia_clinica = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=100, verbose_name='Unidad de medida', choices=UNIDADES_MEDIDA, default='ml')


class MedicamentoHospitalizacion(models.Model):
    hospitalizacion = models.ForeignKey(Hospitalizacion, on_delete=models.CASCADE)
    medicamento = models.ForeignKey(Medicamento, on_delete=models.CASCADE)
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    cantidad_requerida = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=200, verbose_name='Unidad de medida')
    tipo_unidad_medida = models.CharField(max_length=200, verbose_name='Tipo de unidad')
    hora = models.CharField(max_length=200, verbose_name='Hora')


class MedicamentoCirugia(models.Model):
    cirugia = models.ForeignKey(Cirugia, on_delete=models.CASCADE)
    medicamento = models.ForeignKey(Medicamento, on_delete=models.CASCADE)
    cantidad_requerida = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=200, verbose_name='Unidad de medida')
    tipo_unidad_medida = models.CharField(max_length=200, verbose_name='Tipo de unidad')


class AbonoPago(models.Model):
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    valor_abonado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')


class MedioPago(models.Model):
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)
    valor_pagado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    dinero_recibido = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    cambio = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')
