from .models import *
from django import forms
from vacunacion.models import Vacuna
from desparasitacion.models import Desparasitante
import datetime
from insumos.models import Insumo


class CrearProveedorForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearProveedorForm, self).__init__(*args, **kwargs)
        self.fields['ciudad'].required = False
        self.fields['direccion'].required = False
        self.fields['email'].required = False
        self.fields['telefono'].required = False
        self.fields['celular'].required = False
        

    class Meta:
        model = Proveedor
        fields = (
            'nombre',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'email',
            'tipo_documento',
            'numero_documento',
            'tipo_tercero'
        )

        widgets = {
            'email': forms.EmailInput()
        }

    def clean(self):

        cleaned_data = super(CrearProveedorForm, self).clean()
        numero_documento = cleaned_data.get("numero_documento")


        try:
            if Proveedor.objects.filter(numero_documento=numero_documento):
                self._errors['numero_documento'] = [
                    'Ya existe un proveedor con este número de documento']
        except:
            pass


class EditarClienteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarClienteForm, self).__init__(*args, **kwargs)
        self.fields['ciudad'].required = False
        self.fields['direccion'].required = False
        self.fields['email'].required = False
        self.fields['telefono'].required = False
        self.fields['celular'].required = False

    class Meta:
        model = Proveedor
        fields = (
            'nombre',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'email',
            'tipo_documento',
            'numero_documento'
        )

        widgets = {
            'email': forms.EmailInput()
        }


class CrearProductoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearProductoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['cantidad_por_unidad'].label = 'Especificar cantidad por unidad'
        self.fields['precio_venta'].initial = '0'
        self.fields['cantidad'].initial = '1'
        self.fields['cantidad_unidad'].initial = '100'
        self.fields['codigo'].label = 'Código de barras'

    class Meta:
        model = Producto
        fields = (
            'nombre',
            'codigo',
            'descripcion',
            'tipo_producto',
            'precio_venta',
            'cantidad',
            'foto',
            'cantidad_por_unidad',
            'unidad_medida',
            'cantidad_unidad',
            'precio_por_unidad',
            'fecha_vencimiento',
            'tipo_uso',
            'stock_minimo',
            'bodega',
            'precio_compra'
        )

        widgets = {
            'precio_venta': forms.NumberInput(
                attrs={'max_length': '11', 'min': '0'}),
            'descripcion': forms.Textarea(attrs={'rows': '10'}),
            'foto': forms.FileInput(),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}),
            'codigo': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'fecha_vencimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'precio_compra': forms.NumberInput(attrs={'max_length': '11', 'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(CrearProductoForm, self).clean()
        codigo = cleaned_data.get("codigo")
        nombre = cleaned_data.get("nombre")
        fecha_vencimiento = cleaned_data.get("fecha_vencimiento")
        cantidad_por_unidad = cleaned_data.get("cantidad_por_unidad")
        cantidad_unidad = cleaned_data.get("cantidad_unidad")

        producto_instance = self.instance

        if codigo != None:
            try:
                if Medicamento.objects.filter(codigo_inventario=codigo) or Vacuna.objects.filter(codigo_inventario=codigo) or Desparasitante.objects.filter(codigo_inventario=codigo) or Insumo.objects.filter(codigo_inventario=codigo):
                    self._errors['codigo'] = [
                        'Ya existe un elemento del inventario con este codigo']
                elif Producto.objects.filter(codigo=codigo):
                    for producto in Producto.objects.filter(codigo=codigo):
                        if producto.nombre != nombre:
                            self._errors['codigo'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                            self._errors['nombre'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                        elif producto.fecha_vencimiento == fecha_vencimiento:
                            self._errors['fecha_vencimiento'] = [
                                'Este producto ya se encuentra registrado en el inventario con la misma fecha de vencimiento']
            except:
                pass

        if cantidad_por_unidad == True and cantidad_unidad < 1:
            self._errors['cantidad_unidad'] = [
                'La cantidad no puede ser menor que uno']


class EditarProductoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarProductoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['cantidad_por_unidad'].label = 'Especificar cantidad por unidad'
        self.fields['codigo'].label = 'Código de barras'

    class Meta:
        model = Producto
        fields = (
            'nombre',
            'codigo',
            'descripcion',
            'tipo_producto',
            'precio_venta',
            'cantidad',
            'foto',
            'cantidad_por_unidad',
            'unidad_medida',
            'cantidad_unidad',
            'precio_por_unidad',
            'fecha_vencimiento',
            'tipo_uso',
            'stock_minimo',
            'bodega',
            'precio_compra'
        )

        widgets = {
            'precio_venta': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'}),
            'descripcion': forms.Textarea(attrs={'rows': '10'}),
            'foto': forms.FileInput(),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}),
            'codigo': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'fecha_vencimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'precio_compra': forms.NumberInput(attrs={'max_length': '11', 'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(EditarProductoForm, self).clean()
        codigo = cleaned_data.get("codigo")
        nombre = cleaned_data.get("nombre")
        fecha_vencimiento = cleaned_data.get("fecha_vencimiento")
        cantidad_por_unidad = cleaned_data.get("cantidad_por_unidad")
        cantidad_unidad = cleaned_data.get("cantidad_unidad")

        producto_instance = self.instance

        if codigo != None:
            try:
                if Medicamento.objects.filter(codigo_inventario=codigo) or Vacuna.objects.filter(codigo_inventario=codigo) or Desparasitante.objects.filter(codigo_inventario=codigo) or Insumo.objects.filter(codigo_inventario=codigo):
                    self._errors['codigo'] = [
                        'Ya existe un elemento del inventario con este codigo']
                elif Producto.objects.filter(codigo=codigo).exclude(id=producto_instance.id):
                    for producto in Producto.objects.filter(codigo=codigo).exclude(id=producto_instance.id):
                        if producto.nombre != nombre:
                            self._errors['codigo'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                            self._errors['nombre'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                        elif producto.fecha_vencimiento == fecha_vencimiento:
                            self._errors['fecha_vencimiento'] = [
                                'Este producto ya se encuentra registrado en el inventario con la misma fecha de vencimiento']
            except:
                pass

        if cantidad_por_unidad == True and cantidad_unidad < 1:
            self._errors['cantidad_unidad'] = [
                'La cantidad no puede ser menor que uno']


class RegistrarPagoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegistrarPagoForm, self).__init__(*args, **kwargs)
        self.fields['cliente'].widget.attrs = {'class': 'form-control'}
        self.fields['cliente'].label = 'Tercero'
        self.fields['proveedor_nota_debito'].label = 'Tercero'
        self.fields['proveedor_nota_debito'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}

    class Meta:
        model = Pago
        fields = (
            'fecha',
            'cliente',
            'comentarios',
            'total_factura',
            'total_impuesto',
            'sub_total',
            'descuento',
            'tipo_comprobante',
            'proveedor_nota_debito'
        )

        widgets = {
            'fecha': forms.DateInput(attrs={'class': 'form-control fechaPago'}, format='%m/%d/%Y')
        }


class RegistrarPagoProductoForm(forms.ModelForm):

    class Meta:
        model = PagoProducto
        fields = (
            'producto',
            'cantidad'
        )


class RegistrarCompraProductoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegistrarCompraProductoForm, self).__init__(*args, **kwargs)
        self.fields['proveedor'].empty_label = None
        self.fields['proveedor'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}

    class Meta:
        model = CompraProducto
        fields = (
            'proveedor',
            'cantidad',
            'costo',
        )

        widgets = {
            'costo': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'}),
            'cantidad': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'})
        }


class RegistrarPaqueteProductoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegistrarPaqueteProductoForm, self).__init__(*args, **kwargs)
        self.fields['tipo_paquete'].widget.attrs = {'placeholder': 'ej: Caja, Bolsa...'}
        self.fields['numero_productos_paquete'].widget.attrs = {'placeholder': 'Cantidad de unidades en el paquete'}
        self.fields['precio_venta'].widget.attrs = {'placeholder': 'Precio de venta del paquete'}
        self.fields['nombre'].widget.attrs = {'placeholder': 'Nombre del paquete','class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}
        self.fields['numero_productos_paquete'].label = 'Unidades por paquete'

    class Meta:
        model = PaqueteProducto
        fields = (
            'nombre',
            'tipo_paquete',
            'numero_productos_paquete',
            'precio_venta',
        )

        widgets = {
            'precio_venta': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'}),
            'numero_productos_paquete': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'}),
            
        }


class EditarPaqueteProductoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarPaqueteProductoForm, self).__init__(*args, **kwargs)
        self.fields['tipo_paquete'].widget.attrs = {'placeholder': 'ej: Caja, Bolsa...'}
        self.fields['numero_productos_paquete'].widget.attrs = {'placeholder': 'Cantidad de unidades en el paquete'}
        self.fields['precio_venta'].widget.attrs = {'placeholder': 'Precio de venta del paquete'}
        self.fields['nombre'].widget.attrs = {'placeholder': 'Nombre del paquete','class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}

    class Meta:
        model = PaqueteProducto
        fields = (
            'numero_productos_paquete',
            'nombre',
            'tipo_paquete',
            'precio_venta',
        )

        widgets = {
            'precio_venta': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'}),
            'numero_productos_paquete': forms.NumberInput(
                attrs={'max_length': '11', 'min': '1'}),
            
        }


class CrearMedicamentoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearMedicamentoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['cantidad'].label = 'Unidades disponibles'
        self.fields['foto'].label = 'Imagen'
        self.fields['cantidad_por_unidad'].label = 'Especificar cantidad por unidad'
        self.fields['precio_venta'].initial = '0'
        self.fields['cantidad'].initial = '1'
        self.fields['cantidad_unidad'].initial = '100'
        self.fields['codigo_inventario'].label = 'Código de barras'

    class Meta:
        model = Medicamento
        fields = (
            'nombre',
            'codigo',
            'codigo_inventario',
            'descripcion',
            'cantidad',
            'precio_venta',
            'foto',
            'cantidad_por_unidad',
            'unidad_medida',
            'cantidad_unidad',
            'precio_por_unidad',
            'fecha_vencimiento',
            'tipo_uso',
            'stock_minimo',
            'bodega',
            'precio_compra'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '10'}),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}),
            'codigo': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'codigo_inventario': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'fecha_vencimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'precio_compra': forms.NumberInput(attrs={'max_length': '11', 'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(CrearMedicamentoForm, self).clean()
        codigo = cleaned_data.get("codigo_inventario")
        nombre = cleaned_data.get("nombre")
        fecha_vencimiento = cleaned_data.get("fecha_vencimiento")
        cantidad_por_unidad = cleaned_data.get("cantidad_por_unidad")
        cantidad_unidad = cleaned_data.get("cantidad_unidad")

        medicamento_instance = self.instance

        if codigo != None:
            try:
                if Producto.objects.filter(codigo=codigo) or Vacuna.objects.filter(
                        codigo_inventario=codigo) or Desparasitante.objects.filter(codigo_inventario=codigo) or Insumo.objects.filter(codigo_inventario=codigo):
                    self._errors['codigo_inventario'] = [
                        'Ya existe un elemento del inventario con este codigo']
                elif Medicamento.objects.filter(codigo_inventario=codigo).exclude(id=medicamento_instance.id):
                    for medicamento in Medicamento.objects.filter(codigo_inventario=codigo).exclude(id=medicamento_instance.id):
                        if medicamento.nombre != nombre:
                            self._errors['codigo_inventario'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                            self._errors['nombre'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                        elif medicamento.fecha_vencimiento == fecha_vencimiento:
                            self._errors['fecha_vencimiento'] = [
                                'Este medicamento ya se encuentra registrado en el inventario con la misma fecha de vencimiento']
            except:
                pass

        if cantidad_por_unidad == True and cantidad_unidad < 1:
            self._errors['cantidad_unidad'] = [
                'La cantidad no puede ser menor que uno']