Django==2.1.7
django-bootstrap3==11.0.0
django-tenants==2.1.0
Pillow==5.4.1
psycopg2==2.7.7
psycopg2-binary==2.7.7
python-dateutil==2.8.0
pytz==2018.9
six==1.12.0
sorl-thumbnail==12.5.0
