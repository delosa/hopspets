from django.views.generic import *
from django.shortcuts import render, redirect, get_object_or_404
from .models import Tenant, Domain
from django.urls import reverse_lazy
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from usuarios.models import Administrador 
from usuarios.forms import CrearAdministradorForm
from django.core.mail import send_mail
from django.db import connection
from veteriariaTenant.models import Veterinaria
from usuarios.models import Administrador
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.http import JsonResponse


class TenantCreateView(SuccessMessageMixin, CreateView):
    model = Tenant
    form_class = CrearVeterinariaForm
    template_name = "registrar_tenant.html"
    success_url = reverse_lazy('listar_tenants')
    success_message = "¡La veterinaria fue creada exitosamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TenantCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        tenant_registrado = form.instance
        tenant_registrado.schema_name = tenant_registrado.nombre_tenant
        self.object = form.save()
        dominio_tenant = Domain(domain=self.object.nombre_tenant+'.localhost',
                                is_primary=True,
                                tenant=tenant_registrado
                                )
        dominio_tenant.save()

        connection.set_tenant(tenant_registrado)

        veterinaria_cliente = Veterinaria(nit=tenant_registrado.codigo_tenant, nombre=tenant_registrado.nombre_veterinaria,
                                  ciudad=tenant_registrado.ciudad, direccion=tenant_registrado.direccion,
                                  telefono=tenant_registrado.telefono, celular=tenant_registrado.celular)

        veterinaria_cliente.save()

        connection.set_schema_to_public()

        return super(TenantCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TenantCreateView, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['veterinarias'] = True
        return context


class ListarTenants(ListView):
    model = Tenant
    template_name = "listar_tenants.html"

    def get_queryset(self):
        queryset = super(ListarTenants, self).get_queryset()
        return queryset.order_by('nombre_tenant')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarTenants, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarTenants, self).get_context_data(**kwargs)
        context['veterinarias'] = True
        return context


class EditarVeterinaria(SuccessMessageMixin, UpdateView):
    model = Tenant
    form_class = EditarVeterinariaForm
    template_name = "registrar_tenant.html"
    success_url = reverse_lazy('listar_tenants')
    success_message = "¡La veterinaria fue modificada exitosamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVeterinaria, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        tenant_editado = form.instance
        self.object = form.save()

        connection.set_tenant(tenant_editado)

        print(tenant_editado.id)

        veterinaria_cliente = Veterinaria.objects.all()[0]

        print(veterinaria_cliente.nombre)

        veterinaria_cliente.nombre = tenant_editado.nombre_veterinaria
        veterinaria_cliente.nit = tenant_editado.codigo_tenant
        veterinaria_cliente.ciudad = tenant_editado.ciudad
        veterinaria_cliente.direccion = tenant_editado.direccion
        veterinaria_cliente.telefono = tenant_editado.telefono
        veterinaria_cliente.celular = tenant_editado.celular
        veterinaria_cliente.save()
        connection.set_schema_to_public()

        return super(EditarVeterinaria, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarVeterinaria, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['veterinarias'] = True
        return context


def index(request):
    return redirect('listar_tenants')


def crearAdministrador(request, id_tenant):
    tenant = get_object_or_404(Tenant, id=id_tenant)

    if request.method == 'POST':
        connection.set_tenant(tenant)
        formAdministrador = Formulario_crear_administrador_tenant(request.POST)
        if formAdministrador.is_valid():

            administrador = formAdministrador.instance
            administrador.username = administrador.email
            administrador.roles = "Administrador"
            contra = administrador.first_name[0] + administrador.numero_documento_identificacion + administrador.last_name[0]
            administrador.set_password(contra)
            administrador.is_active = True
            administrador.save()
            connection.set_schema_to_public()
            tenant.tiene_administrador = True
            tenant.save()

            # Enviar email al administrador creado
            send_mail('Bienvenido a Hopspet ' + tenant.nombre_veterinaria,
                      'Buen día \n \n' +
                      'Sus datos para el inicio de sesión en la plataforma Hopspet: ' + '\n\n URL: http://' + tenant.nombre_tenant + '.hopspet.com \n' +
                      'Usuario: ' + administrador.email + '\nContraseña: ' + contra + '\n\n Muchas gracias por tu atención,',
                      'soporte@hopspet.com',
                      [administrador.email],
                      fail_silently=True)

            return redirect('listar_tenants')

    elif request.method == 'GET':
        print("HOLA DESDE GETTTTT!")
        formAdministrador = Formulario_crear_administrador_tenant()
        return render(request, 'registrar_administrador_tenant.html',
                      {'form': formAdministrador,
                        'veterinarias': True
                       })


def DetalleAdministradorCliente(request, id_tenant):
    tenant = get_object_or_404(Tenant, id=id_tenant)
    nombreVeterinaria = tenant.nombre_veterinaria

    connection.set_tenant(tenant)
    administrador_cliente = Administrador.objects.all()[0]
    connection.set_schema_to_public()

    return render(request, 'detalle_administrador_cliente.html',{'object': administrador_cliente,'nombreVeterinaria': nombreVeterinaria, 'tenant': tenant})


def inactivar_tenant(request):
    id_tenant = request.GET.get('id', '')
    mi_tenant = get_object_or_404(Tenant, pk=id_tenant)
    mi_tenant.estado = False
    mi_tenant.save()
    mi_dominio = get_object_or_404(Domain, tenant_id=id_tenant)
    mi_dominio.delete()
    messages.success(request, 'Veterinaria inactivada con exito')
    data = {
        'modificacion': True
    }
    return JsonResponse(data)


def activar_tenant(request):
    id_tenant = request.GET.get('id', '')
    mi_tenant = get_object_or_404(Tenant, pk=id_tenant)
    mi_tenant.estado = True
    mi_tenant.save()
    dominio_tenant = Domain(domain=mi_tenant.nombre_tenant + '.localhost',
                            is_primary=True,
                            tenant=mi_tenant
                            )
    dominio_tenant.save()
    messages.success(request, 'Veterinaria inactivada con exito')
    data = {
        'modificacion': True
    }
    return JsonResponse(data)

def DetalleTerminosCondicionesCliente(request, id_tenant):
    vete = Tenant.objects.filter(id=id_tenant)

    #datos = TerminosCondiciones.objects.filter(veterinaria=vete[0])

    #return render(request, 'detalle_terminos.html',{'datos': datos})

    return render(request, 'detalle_terminos.html')