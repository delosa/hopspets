from django.db import models
from django_tenants.models import TenantMixin, DomainMixin
from django.core.validators import RegexValidator
import re


re_alpha = re.compile('[^\W\d_]+$', re.UNICODE)
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores numericos')
alpha = RegexValidator(re_alpha, 'Solamente se reciben caracteres de A-Z')


TIPO_PLAN=(('Pago Completo', 'Pago Completo'), ('Plan Mensual', 'Plan Mensual'), ('Plan Trimestral', 'Plan Trimestral'), ('Plan Semestral', 'Plan Semestral'), ('Plan Anual', 'Plan Anual'))
EST_FINANCIERO = (('Al Día', 'Al Día'), ('En Mora', 'En Mora'))

class Tenant(TenantMixin):
    nombre_tenant = models.CharField(max_length=100)
    codigo_tenant = models.CharField(max_length=100, verbose_name="Nit veterinaria")
    nombre_veterinaria = models.CharField(max_length=100, verbose_name="Nombre veterinaria", validators=[alpha])
    estado = models.BooleanField(default=True)
    ciudad = models.CharField(blank=True, null=True, max_length=100, verbose_name='Ciudad')
    direccion = models.CharField(blank=True, null=True, max_length=100, verbose_name='Direccion')
    telefono = models.CharField(blank=True, null=True, max_length=100, verbose_name='Telefono')
    celular = models.CharField(blank=True, null=True, max_length=100, verbose_name='Celular')
    fecha_fundacion = models.DateField(blank=True, null=True, verbose_name="Fecha de fundacion")
    tiene_administrador = models.BooleanField(default=False)
    tipo_plan = models.CharField(blank=True, null=True, max_length=100, verbose_name="Tipo de Plan", choices=TIPO_PLAN)
    valor_plan = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    cuota_pago = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    estado_financiero = models.CharField(blank=True, null=True, max_length=100, verbose_name='Estado Financiero', choices=EST_FINANCIERO)
    fecha_corte= models.DateField(blank=True, null=True, verbose_name="Fecha de Corte")
    tienda_online = models.BooleanField(default=False)

    auto_create_schema = True

    class Meta:
        ordering = ["nombre_veterinaria"]
        verbose_name_plural = "Veterinarias"
        verbose_name = "Veterinaria"
        unique_together = ('nombre_tenant', 'codigo_tenant')

    def __str__(self):
        return self.schema_name


class Domain(DomainMixin):
    pass

""" class TerminosCondiciones(models.Model):
    fecha_registro = models.DateField(verbose_name="Fecha de Aceptacion")
    revisado = models.BooleanField(default=False)
    cliente_id = models.IntegerField(default=0)
    veterinaria = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    observaciones = models.CharField(max_length=1000, verbose_name="Observaciones") """