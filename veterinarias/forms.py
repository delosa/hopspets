from .models import *
from django import forms
from usuarios.models import Administrador
from datetime import datetime


class CrearVeterinariaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearVeterinariaForm, self).__init__(*args, **kwargs)
        self.fields['fecha_fundacion'].initial = datetime.now()
        self.fields['fecha_fundacion'].label = 'Fecha de registro'
        self.fields['tienda_online'].label = 'Tiene Tienda Online?'
        self.fields['tienda_online'].required = False

    class Meta:
        model = Tenant
        fields = (
            'nombre_tenant',
            'codigo_tenant',
            'nombre_veterinaria',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'fecha_fundacion',
            'tipo_plan',
            'valor_plan',
            'cuota_pago',
            'estado_financiero',
            'fecha_corte',
            'tienda_online'
        )

        widgets = {
            'fecha_fundacion': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'fecha_corte': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'valor_plan': forms.NumberInput(attrs={'min': '0'}),
            'cuota_pago': forms.NumberInput(attrs={'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(CrearVeterinariaForm, self).clean()
        nombre_tenant = cleaned_data.get("nombre_tenant")
        codigo_tenant = cleaned_data.get("codigo_tenant")

        try:
            if Tenant.objects.filter(nombre_tenant=nombre_tenant):
                self._errors['nombre_tenant'] = [
                    'Ya existe un tenant con este nombre']

            if Tenant.objects.filter(codigo_tenant=codigo_tenant):
                self._errors['codigo_tenant'] = [
                    'Ya existe una veterinaria con este Nit']
        except:
            pass


class EditarVeterinariaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarVeterinariaForm, self).__init__(*args, **kwargs)
        self.fields['nombre_tenant'].disabled = True
        self.fields['fecha_fundacion'].label = 'Fecha de registro'
        self.fields['tienda_online'].label = 'Tiene Tienda Online?'
        self.fields['tienda_online'].required = False

    class Meta:
        model = Tenant
        fields = (
            'nombre_tenant',
            'codigo_tenant',
            'nombre_veterinaria',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'fecha_fundacion',
            'tipo_plan',
            'valor_plan',
            'cuota_pago',
            'estado_financiero',
            'fecha_corte',
            'tienda_online'
        )

        widgets = {
            'fecha_fundacion': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'fecha_corte': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'valor_plan': forms.NumberInput(attrs={'min': '0'}),
            'cuota_pago': forms.NumberInput(attrs={'min': '0'}),
        }


class Formulario_crear_administrador_tenant(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(Formulario_crear_administrador_tenant, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['email'].label = 'Correo Electronico'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['segundo_nombre'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['email'].required = True
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Administrador
        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'email',
            'telefono',
            'celular',
            'sexo',
            'direccion'
        )

        widgets = {
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'email': forms.EmailInput(),
            'direccion': forms.TextInput(attrs={'max_length': '100'})

        }