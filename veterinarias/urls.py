from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_veterinaria/', TenantCreateView.as_view(), name="registrar_tenant"),
    path('listar_veterinarias/', ListarTenants.as_view(), name="listar_tenants"),
    path('editar_veterinaria/<int:pk>/', EditarVeterinaria.as_view(), name="editar_veterinaria"),
    path('crear_administrador_tenant/<int:id_tenant>/', crearAdministrador, name="crear_administrador_tenant"),
    path('detalle_administrador_tenant/<int:id_tenant>/', DetalleAdministradorCliente, name='detalle_administrador_tenant'),
    path('detalle_terminos/<int:id_tenant>/', DetalleTerminosCondicionesCliente, name='detalle_terminos'),
    path('inactivar_tenant/', inactivar_tenant, name="inactivar_tenant"),
    path('activar_tenant/', activar_tenant, name="activar_tenant"),
]