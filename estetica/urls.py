from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_tipo_estetica/', CrearTipoEstetica.as_view(), name="registrar_tipo_estetica"),
    path('listar_tipos_estetica/', ListarTipoEstetica.as_view(), name="listar_tipos_estetica"),
    path('editar_tipo_estetica/<int:pk>/', EditarTipoEstetica.as_view(), name="editar_tipo_estetica"),
    path('registrar_tipo_corte/', CrearTipoCorte.as_view(), name="registrar_tipo_corte"),
    path('listar_descripcion_estetica/', ListarTipoCorte.as_view(), name="listar_tipos_corte"),
    path('editar_tipo_corte/<int:pk>/', EditarTipoCorte.as_view(), name="editar_tipo_corte"),
    #path('registrar_factura_estetica/', CrearFacturaEstetica.as_view(), name="registrar_factura_estetica"),
    #path('listar_facturas_estetica/', ListarFacturaEstetica.as_view(), name="listar_facturas_estetica"),
    path('editar_factura_estetica/<int:pk>/', EditarFacturaEstetica.as_view(), name="editar_factura_estetica"),
    path('registrar_factura_estetica_mascota/<int:id_mascota>/', CrearFacturaEsteticaMascota.as_view(), name="registrar_factura_estetica_mascota"),
    path('pagar_factura_estetica/<int:pk>/', PagarFacturaEstetica.as_view(), name="pagar_factura_estetica"),
    path('eliminar_estetica/', eliminar_estetica, name="eliminar_estetica"),
    path('registrar_estetica_agenda/<str:fecha>/', registrar_cita_estetica, name="registrar_cita_estetica"),
    path('atender_estetica/', atendercita, name="atender_cita_estetica"),
    path('eliminar_tipo_corte/', eliminar_tipo_corte, name="eliminar_tipo_corte"),
    path('eliminar_tipo_estetica/', eliminar_tipo_estetica, name="eliminar_tipo_estetica"),
    path('editar_factura_estetica_agenda/<int:pk>/', EditarFacturaEsteticaAgenda.as_view(), name="editar_factura_estetica_agenda"),
    path('pdf_estetica/<int:id_mascota>/<str:fecha>/', PdfEstetica.as_view(), name="pdf_estetica"),
    path('detalle_impresion_estetica/<int:id_mascota>/<str:fecha>/', DetalleImpresionEstetica.as_view(), name="detalle_impresion_estetica"),
]