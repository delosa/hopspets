from django.db import models
from mascotas.models import Mascota
from usuarios.models import Usuario


class TipoEstetica(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Tipo de Estética', unique=True, error_messages={'unique': "Ya existe un tipo de estetica con este nombre."})
    valor_estetica = models.CharField(max_length=20, verbose_name='Valor de la Estética')
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class TipoCorte(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Descripción de Estética', unique=True, error_messages={'unique': "Ya existe una Descripción de Estética con este nombre."})
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


ESTADOS_FACTURA = (('Activa','Activa'),('Pagada','Pagada'))

ESTADOS_CITA = (('Activa','Activa'),('Cancelada','Cancelada'),('Atendida','Atendida'))

class FacturaEstetica(models.Model):
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    tipo_estetica = models.ForeignKey(TipoEstetica, on_delete=models.CASCADE, limit_choices_to={})
    tipo_corte = models.ForeignKey(TipoCorte, on_delete=models.CASCADE, blank=True, null=True)
    otras_instrucciones = models.CharField(max_length=2000, verbose_name='Otras instrucciones')
    valor_transporte = models.CharField(max_length=20, verbose_name='Valor transporte')
    valor_otros = models.CharField(max_length=20, verbose_name='Valor otros')
    valor_total = models.DecimalField(max_digits=50, decimal_places=0)
    saldo = models.CharField(max_length=10, verbose_name='Saldo', default='0')
    estado = models.CharField(max_length=10, verbose_name="Estado", choices=ESTADOS_FACTURA, default='Activa')
    estado_de_atencion = models.CharField(max_length=10, verbose_name="Estado Cita", choices=ESTADOS_CITA, default='Activa')
    fecha = models.DateTimeField()
    email_sent = models.BooleanField(default = False)
    veterinario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True, limit_choices_to={'is_active': True}, verbose_name="Responsable")
