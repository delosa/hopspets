from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect,render
from .forms import *
from django.core import serializers
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from mascotas.models import Mascota
from decimal import Decimal
from django.http import JsonResponse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from veteriariaTenant.models import Veterinaria
from datetime import datetime


############################ TIPOS DE ESTETICA ###########################


class CrearTipoEstetica(SuccessMessageMixin, CreateView):
    model = TipoEstetica
    form_class = TipoEsteticaForm
    template_name = "registrar_tipo_estetica.html"
    success_url = reverse_lazy('listar_tipos_estetica')
    success_message = "¡Tipo de Estética creado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearTipoEstetica, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        tipo_estetica = form.instance
        self.object = form.save()
        return super(CrearTipoEstetica, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearTipoEstetica, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['tipos_estetica'] = True
        return context


class ListarTipoEstetica(ListView):
    model = TipoEstetica
    template_name = "listar_tipo_estetica.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarTipoEstetica, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarTipoEstetica, self).get_context_data(**kwargs)
        context['tipos_estetica'] = True
        return context


class EditarTipoEstetica(SuccessMessageMixin, UpdateView):
    model = TipoEstetica
    form_class = TipoEsteticaForm
    template_name = "registrar_tipo_estetica.html"
    success_url = reverse_lazy('listar_tipos_estetica')
    success_message = "¡Tipo de estética editada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarTipoEstetica, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarTipoEstetica, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['tipos_estetica'] = True
        return context

############################ TIPOS DE CORTES (Descripcion de Estetica)###########################


class CrearTipoCorte(SuccessMessageMixin, CreateView):
    model = TipoCorte
    fields = ['nombre']
    template_name = "registrar_tipo_corte.html"
    success_url = reverse_lazy('listar_tipos_corte')
    success_message = "¡Descripción de Estetica Creado con Éxito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearTipoCorte, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        tipo_corte = form.instance
        self.object = form.save()
        return super(CrearTipoCorte, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearTipoCorte, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['tipos_corte'] = True
        return context


class ListarTipoCorte(ListView):
    model = TipoCorte
    template_name = "listar_tipo_corte.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarTipoCorte, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarTipoCorte, self).get_context_data(**kwargs)
        context['tipos_corte'] = True
        return context


class EditarTipoCorte(SuccessMessageMixin, UpdateView):
    model = TipoCorte
    fields = ['nombre']
    template_name = "registrar_tipo_corte.html"
    success_url = reverse_lazy('listar_tipos_corte')
    success_message = "¡La Descripción de la Estética fue editada con Éxito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarTipoCorte, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarTipoCorte, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['tipos_corte'] = True
        return context


############################ FACTURA ESTETICA ###########################


class CrearFacturaEstetica(SuccessMessageMixin, CreateView):
    model = FacturaEstetica
    form_class = FacturaEsteticaForm
    template_name = "registrar_factura_estetica.html"
    success_url = reverse_lazy('listar_facturas_estetica')
    success_message = "¡Factura creada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearFacturaEstetica, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        factura_estetica = form.instance
        factura_estetica.saldo = factura_estetica.valor_total
        self.object = form.save()
        return super(CrearFacturaEstetica, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearFacturaEstetica, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['factura_estetica'] = True
        context['all_tipos_estetica'] = serializers.serialize("json", TipoEstetica.objects.all())
        return context


class CrearFacturaEsteticaMascota(SuccessMessageMixin, CreateView):
    model = FacturaEstetica
    form_class = FacturaEsteticaMascotaForm
    template_name = "registrar_factura_estetica_mascota.html"
    success_url = reverse_lazy('listar_facturas_estetica')
    success_message = "¡Factura creada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearFacturaEsteticaMascota, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_mascota =  get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return '/mascotas/detalle_mascota/' + str(mi_mascota.id)

    def form_valid(self,form):
        factura_estetica = form.instance
        factura_estetica.mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        factura_estetica.saldo = factura_estetica.valor_total
        self.object = form.save()

        if (factura_estetica.mascota.cliente.notificaciones_wpp) and (len(factura_estetica.mascota.cliente.celular) != 10 or not factura_estetica.mascota.cliente.celular.isdigit()):
            messages.error(self.request, 'Debe ingresar un numero de celular valido para que se le notifique al propietario de la mascota por whatsapp')

        return super(CrearFacturaEsteticaMascota, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearFacturaEsteticaMascota, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        context['all_tipos_estetica'] = serializers.serialize("json", TipoEstetica.objects.all())
        context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        context['esteticas_mascota'] = FacturaEstetica.objects.filter(estado='Activa').filter(mascota_id=self.kwargs['id_mascota'])
        return context


class ListarFacturaEstetica(ListView):
    model = FacturaEstetica
    template_name = "listar_factura_estetica.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarFacturaEstetica, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarFacturaEstetica, self).get_context_data(**kwargs)
        context['factura_estetica'] = True
        return context


class EditarFacturaEstetica(SuccessMessageMixin, UpdateView):
    model = FacturaEstetica
    form_class = FacturaEsteticaForm
    template_name = "registrar_factura_estetica.html"
    success_url = reverse_lazy('listar_facturas_estetica')
    success_message = "¡Cita de estetica modificada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarFacturaEstetica, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_estetica =  get_object_or_404(FacturaEstetica, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(mi_estetica.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarFacturaEstetica, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        context['all_tipos_estetica'] = serializers.serialize("json", TipoEstetica.objects.all())
        return context


class EditarFacturaEsteticaAgenda(SuccessMessageMixin, UpdateView):
    model = FacturaEstetica
    form_class = FacturaEsteticaForm
    template_name = "registrar_factura_estetica.html"
    success_url = reverse_lazy('listar_citas')
    success_message = "¡Cita de estetica modificada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarFacturaEsteticaAgenda, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarFacturaEsteticaAgenda, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        context['all_tipos_estetica'] = serializers.serialize("json", TipoEstetica.objects.all())
        return context


####################### PAGAR FACTURA ESTETICA ########################

class PagarFacturaEstetica(SuccessMessageMixin, UpdateView):
    model = FacturaEstetica
    form_class = PagarFacturaEsteticaForm
    template_name = "pagar_factura_estetica.html"
    success_url = reverse_lazy('listar_facturas_estetica')
    success_message = "¡Factura actualizada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PagarFacturaEstetica, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        factura_estetica = form.instance
        self.object = form.save()
        return super(PagarFacturaEstetica, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PagarFacturaEstetica, self).get_context_data(**kwargs)
        context['factura_estetica'] = True
        context['factura'] = get_object_or_404(FacturaEstetica, pk=self.kwargs['pk'])
        return context


def eliminar_estetica(request):
    id_estetica = request.GET.get('id', '')
    mi_estetica = get_object_or_404(FacturaEstetica, pk=id_estetica)
    id_mascota = mi_estetica.mascota.id
    if mi_estetica.estado == 'Activa':
        mi_estetica.delete()
    data = {
        'eliminacion': True,
        'id_mascota': id_mascota
    }
    return JsonResponse(data)

def registrar_cita_estetica(request,fecha):
    if request.method == "GET":
        return render(request,'registrar_estetica_agenda.html',{'citas': True,'fecha':fecha,'form':FacturaEsteticaForm(),'all_tipos_estetica':serializers.serialize("json", TipoEstetica.objects.all())})
    if request.method == "POST":
        form_cita = FacturaEsteticaForm(request.POST)
        form_cita.save()
        return redirect('/citas/listar_citas')

def atendercita(request):
    id_estetica = request.GET.get('id', '')
    mi_estetica = get_object_or_404(FacturaEstetica, pk=id_estetica)
    id_mascota = mi_estetica.mascota.id
    if mi_estetica.estado_de_atencion == 'Activa':
        mi_estetica.estado_de_atencion = 'Atendida'
        mi_estetica.save()
    data = {
        'eliminacion': True,
        'id_mascota': id_mascota
    }
    return JsonResponse(data)


def eliminar_tipo_corte(request):
    id_tipo_corte = request.GET.get('id', '')
    mi_tipo_corte = get_object_or_404(TipoCorte, pk=id_tipo_corte)
    mi_tipo_corte.delete()
    messages.success(request, 'La Descripción de la Estética fue eliminada con éxito!')
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)


def eliminar_tipo_estetica(request):
    id_tipo_estetica = request.GET.get('id', '')
    mi_tipo_estetica = get_object_or_404(TipoEstetica, pk=id_tipo_estetica)
    if not FacturaEstetica.objects.filter(tipo_estetica=id_tipo_estetica):
        mi_tipo_estetica.delete()
        messages.success(request, 'Tipo de estetica eliminado con exito!')
        data = {
            'eliminacion': True,
        }
    else:
        messages.error(request, 'No se puede eliminar porque hay citas de estetica relacionadas a este tipo de estetica')
        data = {
            'eliminacion': False,
        }
    return JsonResponse(data)


class PdfEstetica(TemplateView):
    template_name = 'pdf_estetica.html'

    def get_context_data(self, **kwargs):
        context = super(PdfEstetica, self).get_context_data(**kwargs)
        mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        fecha = self.kwargs['fecha']

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        esteticas = FacturaEstetica.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_de_atencion='Atendida').filter(fecha=fecha)
        
        for estetica in esteticas:
            context['estetica'] = estetica

        context['fecha_hoy'] = datetime.now().date()
        context['mi_mascota'] = mascota

        return context

class DetalleImpresionEstetica(TemplateView):
    template_name = 'detalle_impresion_estetica.html'

    def get_context_data(self, **kwargs):
        context = super(DetalleImpresionEstetica, self).get_context_data(**kwargs)
        mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        fecha = self.kwargs['fecha']

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        esteticas = FacturaEstetica.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(estado_de_atencion='Atendida').filter(fecha=fecha)
        
        for estetica in esteticas:
            context['estetica'] = estetica

        context['fecha_hoy'] = datetime.now().date()
        context['mi_mascota'] = mascota

        return context