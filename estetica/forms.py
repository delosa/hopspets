from .models import *
from django import forms
from clientes.models import Cliente
from decimal import Decimal


class TipoEsteticaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TipoEsteticaForm, self).__init__(*args, **kwargs)
        self.fields['valor_estetica'].label = 'Valor de la Estética'

    class Meta:
        model = TipoEstetica
        fields = (
            'nombre',
            'valor_estetica'
        )

        widgets = {
            'valor_estetica': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'})
        }


class FacturaEsteticaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FacturaEsteticaForm, self).__init__(*args, **kwargs)
        #self.fields['tipo_estetica'].empty_label = None
        self.fields['valor_total'].readonly = True
        self.fields['otras_instrucciones'].required = False
        self.fields['valor_otros'].initial = 0
        self.fields['valor_transporte'].initial = 0
        #self.fields['mascota'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        #self.fields['tipo_estetica'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        #self.fields['tipo_corte'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tipo_corte'].label = 'Descripción de Estética'
        self.fields['tipo_estetica'].label = 'Tipo de Estética'
        self.fields['fecha'].label = 'Fecha y hora'
        self.fields['otras_instrucciones'].widget.attrs = {'rows':'5', 'placeholder': 'Ej: Patas rasuradas, glandulas, con moños, uñas y oidos...'}
        self.fields['valor_otros'].label = 'Costos adicionales'
        #self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}

    class Meta:
        model = FacturaEstetica
        fields = (
            'mascota',
            'fecha',
            'tipo_estetica',
            'tipo_corte',
            'otras_instrucciones',
            'valor_transporte',
            'valor_otros',
            'valor_total',
            'veterinario'
        )

        widgets = {
            'valor_transporte': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'valor_otros': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'valor_total': forms.NumberInput(attrs={'required': 'true','readonly': 'true', 'max_length': '11', 'min': '0'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'otras_instrucciones': forms.Textarea()

        }




class FacturaEsteticaMascotaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FacturaEsteticaMascotaForm, self).__init__(*args, **kwargs)
        self.fields['tipo_estetica'].empty_label = None
        self.fields['valor_total'].readonly = True
        self.fields['otras_instrucciones'].required = False
        self.fields['valor_otros'].initial = 0
        self.fields['valor_transporte'].initial = 0
        self.fields['tipo_estetica'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tipo_corte'].label = 'Descripción de Estética'
        self.fields['tipo_corte'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['tipo_estetica'].label = 'Tipo de Estética'
        self.fields['fecha'].label = 'Fecha y hora'
        self.fields['otras_instrucciones'].widget.attrs = {'rows': '5',
                                                           'placeholder': 'Ej: Patas rasuradas, glandulas, con moños, uñas y oidos...'}
        self.fields['valor_otros'].label = 'Costos adicionales'
        self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}

    class Meta:
        model = FacturaEstetica
        fields = (
            'fecha',
            'tipo_estetica',
            'tipo_corte',
            'otras_instrucciones',
            'valor_transporte',
            'valor_otros',
            'valor_total',
            'veterinario'
        )

        widgets = {
            'valor_transporte': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'valor_otros': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'valor_total': forms.NumberInput(attrs={'required': 'true','readonly': 'true', 'max_length': '11', 'min': '0'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'otras_instrucciones': forms.Textarea()
        }


class PagarFacturaEsteticaForm(forms.ModelForm):
    valor_pago = forms.DecimalField();

    def __init__(self, *args, **kwargs):
        super(PagarFacturaEsteticaForm, self).__init__(*args, **kwargs)
        self.fields['mascota'].empty_label = None
        self.fields['tipo_estetica'].empty_label = None
        self.fields['tipo_corte'].empty_label = None
        self.fields['valor_total'].readonly = True
        self.fields['otras_instrucciones'].initial = 'Ninguna'
        self.fields['valor_otros'].initial = 0
        self.fields['valor_transporte'].initial = 0
        self.fields['mascota'].disabled = True
        self.fields['tipo_estetica'].disabled = True
        self.fields['tipo_corte'].disabled = True
        self.fields['otras_instrucciones'].disabled = True
        self.fields['valor_transporte'].disabled = True
        self.fields['valor_otros'].disabled = True
        self.fields['saldo'].disabled = True
        self.fields['fecha'].disabled = True
        self.fields['mascota'].label = 'Nombre mascota'
        self.fields['valor_pago'].label = 'Valor a pagar'
        self.fields['valor_otros'].label = 'Costos adicionales'
        self.fields['tipo_estetica'].label = 'Tipo de Estética'
        self.fields['fecha'].label = 'Fecha y hora'
        self.fields['otras_instrucciones'].required = False

    class Meta:
        model = FacturaEstetica
        fields = (
            'fecha',
            'mascota',
            'tipo_estetica',
            'tipo_corte',
            'otras_instrucciones',
            'valor_transporte',
            'valor_otros',
            'valor_total',
            'saldo'
        )

        widgets = {
            'valor_transporte': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'valor_otros': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'valor_total': forms.NumberInput(attrs={'required': 'true','readonly': 'true', 'max_length': '11', 'min': '0'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'otras_instrucciones': forms.Textarea(attrs={'rows': '5', 'required': 'false'})
        }

    def clean_valor_pago(self):
        valor_pago = self.cleaned_data['valor_pago']
        valor_pago = Decimal(valor_pago)
        saldo = self.cleaned_data['saldo']
        saldo = Decimal(saldo)
        if valor_pago > saldo:
            raise forms.ValidationError(
                'El valor a pagar no debe ser mayor al saldo de la factura')

        nuevo_saldo = saldo - valor_pago
        self.instance.saldo = nuevo_saldo

        if nuevo_saldo == 0:
            self.instance.estado = 'Pagada'

        return valor_pago
