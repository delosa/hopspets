from django.views.generic import *
from django.contrib.auth.models import User
from .models import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect, render
from .forms import CrearGastoForm, EditarGastoForm, Formulario_registrar_compra
from productos.models import Proveedor, Producto, Medicamento
from django.core import serializers
from django.http import JsonResponse
from vacunacion.models import Vacuna
from usuarios.models import Usuario
from desparasitacion.models import Desparasitante
from impuestos.models import Impuesto
import json
from datetime import datetime, timedelta
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from veteriariaTenant.models import Veterinaria
from productos.models import HistorialInventario
from django.contrib.messages.views import SuccessMessageMixin
from caja.models import *
from django.contrib import messages
from insumos.models import Insumo
from itertools import chain
from django.db.models import Count
from categoriasInventario.models import *
from bodegas.models import Bodega
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger


class CrearGasto(SuccessMessageMixin, CreateView):
    model = Gasto
    form_class = CrearGastoForm
    template_name = "registrar_gasto.html"
    success_url = reverse_lazy('listar_gasto')
    success_message = "¡Gasto ingresado con exito!"

    def form_valid(self, form):
        self.object = form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearGasto, self).get_context_data(**kwargs)
        context['gastos'] = True
        return context


class ListarGasto(ListView):
    model = Gasto
    template_name = "listar_gastos.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarGasto, self).get_queryset()

        abonos = []

        if self.kwargs['estado_pago'] == 'pagados':
            lista_gastos = Gasto.objects.filter(estado_pago='Pagado').order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.all()
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_gastos = Gasto.objects.filter(estado_pago='Pendiente').order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.filter(gasto__estado_pago='Pendiente')
        elif self.kwargs['estado_pago'] == 'todos':
            lista_gastos = Gasto.objects.all().order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.all()

        hoy = datetime.now().date()

        if self.kwargs['fecha_pago'] == 'hoy':
            lista_gastos = lista_gastos.filter(fecha=hoy)
            abonos = abonos.filter(fecha__date=hoy)
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                if abono.gasto in lista_gastos:
                    abonos = abonos.exclude(id=abono.id)
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id:
                        gasto.fecha_abono = abono.fecha
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'semana':
            esta_semana = hoy.isocalendar()[1]
            lista_gastos = lista_gastos.filter(fecha__week=esta_semana, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__week=esta_semana, fecha__date__year=hoy.year)
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id and gasto.fecha == abono.fecha.date():
                        if gasto.estado_pago == 'Pagado':
                            gasto.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'mes':
            lista_gastos = lista_gastos.filter(fecha__month=hoy.month, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__month=hoy.month, fecha__date__year=hoy.year)
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        else:
            lista_gastos = []
            lista_completa = []

        queryset = lista_completa

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListarGasto, self).get_context_data(**kwargs)
        context['gastos'] = True

        abonos = []

        if self.kwargs['estado_pago'] == 'pagados':
            lista_gastos = Gasto.objects.filter(estado_pago='Pagado').order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.all()
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_gastos = Gasto.objects.filter(estado_pago='Pendiente').order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.filter(gasto__estado_pago='Pendiente')
        elif self.kwargs['estado_pago'] == 'todos':
            lista_gastos = Gasto.objects.all().order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.all()

        hoy = datetime.now().date()

        if self.kwargs['fecha_pago'] == 'hoy':
            lista_gastos = lista_gastos.filter(fecha=hoy)
            abonos = abonos.filter(fecha__date=hoy)
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                if abono.gasto in lista_gastos:
                    abonos = abonos.exclude(id=abono.id)
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id:
                        gasto.fecha_abono = abono.fecha
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'semana':
            esta_semana = hoy.isocalendar()[1]
            lista_gastos = lista_gastos.filter(fecha__week=esta_semana, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__week=esta_semana, fecha__date__year=hoy.year)
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id and gasto.fecha == abono.fecha.date():
                        if gasto.estado_pago == 'Pagado':
                            gasto.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['fecha_pago'] == 'mes':
            lista_gastos = lista_gastos.filter(fecha__month=hoy.month, fecha__year=hoy.year)
            abonos = abonos.filter(fecha__date__month=hoy.month, fecha__date__year=hoy.year)
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        else:
            lista_gastos = []
            lista_completa = []

        paginator = Paginator(lista_completa, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            lista_completa = paginator.page(page)
        except PageNotAnInteger:
            lista_completa = paginator.page(1)
        except EmptyPage:
            lista_completa = paginator.page(paginator.num_pages)

        motivos_gastos = []

        for gasto in lista_completa:
            motivos = []

            if gasto.tipo == 'Abono':
                motivos.append("Abono")
            else:
                if GastoProducto.objects.filter(gasto__id=gasto.id):
                    motivos.append("Producto")

                if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                    motivos.append("Medicamento")

                if GastoVacuna.objects.filter(gasto__id=gasto.id):
                    motivos.append("Vacuna")

                if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                    motivos.append("Desparasitante")

                if GastoInsumo.objects.filter(gasto__id=gasto.id):
                    motivos.append("Insumo")

                if GastoServicio.objects.filter(gasto__id=gasto.id):
                    motivos.append("Servicio")

                if GastoOtro.objects.filter(gasto__id=gasto.id):
                    motivos.append("Otro")

                if GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                    for gasto in GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                        if not gasto.elemento.categoria.nombre in motivos:
                            motivos.append(gasto.elemento.categoria.nombre)

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos
        context['lista_gastos'] = lista_completa
        context['estado_pago'] = self.kwargs['estado_pago']
        context['fecha_pago'] = self.kwargs['fecha_pago']
        context['abonos'] = AbonoGasto.objects.all()

        return context


class ListarGastoMes(ListView):
    model = Gasto
    template_name = "listar_gastos.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarGastoMes, self).get_queryset()

        mes = self.kwargs['mes']
        año = self.kwargs['ano']

        if self.kwargs['estado_pago'] == 'pagados':
            lista_gastos = Gasto.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pagado').order_by(
                '-fecha', '-id')
            abonos = AbonoGasto.objects.filter(fecha__date__month=mes, fecha__date__year=año).exclude(
                gasto__estado_pago='Anulada').order_by('-fecha', '-id')
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id and gasto.fecha == abono.fecha.date():
                        if gasto.estado_pago == 'Pagado':
                            gasto.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_gastos = Gasto.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pendiente').order_by(
                '-fecha', '-id')
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            lista_completa = lista_gastos
        elif self.kwargs['estado_pago'] == 'todos':
            lista_gastos = Gasto.objects.filter(fecha__month=mes, fecha__year=año).order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.filter(fecha__date__month=mes, fecha__date__year=año).order_by('-fecha', '-id')
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id and gasto.fecha == abono.fecha.date():
                        if gasto.estado_pago == 'Pagado':
                            gasto.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)

        queryset = lista_completa

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListarGastoMes, self).get_context_data(**kwargs)
        context['gastos'] = True

        mes = self.kwargs['mes']
        año = self.kwargs['ano']

        if self.kwargs['estado_pago'] == 'pagados':
            lista_gastos = Gasto.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pagado').order_by(
                '-fecha', '-id')
            abonos = AbonoGasto.objects.filter(fecha__date__month=mes, fecha__date__year=año).exclude(
                gasto__estado_pago='Anulada').order_by('-fecha', '-id')
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id and gasto.fecha == abono.fecha.date():
                        if gasto.estado_pago == 'Pagado':
                            gasto.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)
        elif self.kwargs['estado_pago'] == 'pendientes':
            lista_gastos = Gasto.objects.filter(fecha__month=mes, fecha__year=año, estado_pago='Pendiente').order_by(
                '-fecha', '-id')
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            lista_completa = lista_gastos
        elif self.kwargs['estado_pago'] == 'todos':
            lista_gastos = Gasto.objects.filter(fecha__month=mes, fecha__year=año).order_by('-fecha', '-id')
            abonos = AbonoGasto.objects.filter(fecha__date__month=mes, fecha__date__year=año).order_by('-fecha', '-id')
            for gasto in lista_gastos:
                gasto.tipo = 'Gasto'
            for abono in abonos:
                for gasto in lista_gastos:
                    if gasto.id == abono.gasto.id and gasto.fecha == abono.fecha.date():
                        if gasto.estado_pago == 'Pagado':
                            gasto.fecha_abono = abono.fecha.date()
                        abonos = abonos.exclude(id=abono.id)
            for abono in abonos:
                abono.tipo = 'Abono'
                abono.fecha = abono.fecha.date()
            lista_completa = sorted(chain(lista_gastos, abonos), key=lambda instance: instance.fecha, reverse=True)

        paginator = Paginator(lista_completa, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            lista_completa = paginator.page(page)
        except PageNotAnInteger:
            lista_completa = paginator.page(1)
        except EmptyPage:
            lista_completa = paginator.page(paginator.num_pages)

        motivos_gastos = []

        for gasto in lista_completa:
            motivos = []
            if gasto.tipo == 'Abono':
                motivos.append("Abono")
            else:
                if GastoProducto.objects.filter(gasto__id=gasto.id):
                    motivos.append("Producto")

                if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                    motivos.append("Medicamento")

                if GastoVacuna.objects.filter(gasto__id=gasto.id):
                    motivos.append("Vacuna")

                if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                    motivos.append("Desparasitante")

                if GastoInsumo.objects.filter(gasto__id=gasto.id):
                    motivos.append("Insumo")

                if GastoServicio.objects.filter(gasto__id=gasto.id):
                    motivos.append("Servicio")

                if GastoOtro.objects.filter(gasto__id=gasto.id):
                    motivos.append("Otro")

                if GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                    for gasto in GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                        if not gasto.elemento.categoria.nombre in motivos:
                            motivos.append(gasto.elemento.categoria.nombre)

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos
        context['lista_gastos'] = lista_completa
        context['estado_pago'] = self.kwargs['estado_pago']
        context['mes'] = self.kwargs['mes']
        context['ano'] = self.kwargs['ano']
        context['fecha_pago'] = 'mes'

        return context


class EditarGasto(SuccessMessageMixin, UpdateView):
    model = Gasto
    form_class = EditarGastoForm
    template_name = "registrar_gasto.html"
    success_url = reverse_lazy('listar_gasto')
    success_message = "¡Gasto editado con exito!"

    def dispatch(self, request, *args, **kwargs):
        return super(EditarGasto, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarGasto, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['gastos'] = True
        return context


class EliminarGasto(SuccessMessageMixin, DeleteView):
    model = Gasto
    template_name = "gasto_confirm_delete.html"
    success_url = reverse_lazy('listar_gasto')
    success_message = "¡El gasto se eliminó con exito!"


def registrar_compra(request):
    if request.method == 'POST':
        print("Prueba POST registrar compra")
        form_registrar_compra = Formulario_registrar_compra()
        req = json.loads(request.body.decode('utf-8'))
        fecha = datetime.strptime(str(req['fecha']), "%Y-%m-%d")
        comentarios = str(req['comentarios'])
        subtotal = float(req['subtotal'])
        descuento = float(req['descuento'])
        total_impuesto = float(req['total_impuesto'])
        total = float(req['total'])
        numero_comprobante = str(req['numero_comprobante'])
        tipo_comprobante = str(req['tipo_comprobante'])
        cliente_nota_credito = str(req['cliente_nota_credito'])

        responsable = get_object_or_404(Usuario, id=request.user.id)

        if tipo_comprobante == 'Nota Crédito':
            cliente = get_object_or_404(Cliente, id=cliente_nota_credito)
            nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                sub_total=subtotal, descuento=descuento, responsable=responsable,
                                numero_comprobante=numero_comprobante, tipo_comprobante=tipo_comprobante,cliente_nota_credito=cliente)
            nuevo_gasto.save()
        else:
            if req['proveedor']:
                id_proveedor = str(req['proveedor'])
                proveedor = get_object_or_404(Proveedor, pk=id_proveedor)

                nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                    sub_total=subtotal, descuento=descuento, proveedor=proveedor, responsable=responsable,numero_comprobante=numero_comprobante,tipo_comprobante=tipo_comprobante)
                nuevo_gasto.save()


            else:
                nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                    sub_total=subtotal, descuento=descuento, responsable=responsable,numero_comprobante=numero_comprobante,tipo_comprobante=tipo_comprobante)
                nuevo_gasto.save()

        nuevo_gasto.forma_pago = req['forma_pago']

        if req['forma_pago'] == 'Debito':
            nuevo_gasto.tipo_pago_debito = req['medio_pago_debito']
            """caja_on = Caja.objects.filter(estado= True)
            if caja_on:
                now = datetime.now()
                x = datetime(now.year, now.month, now.day)
                z = datetime(nuevo_gasto.fecha.year,nuevo_gasto.fecha.month,nuevo_gasto.fecha.day)
                y = x + timedelta(days=1)
                if z >= x and z < y:
                    caja_compra = CajaCompra(caja = caja_on[0], compra=nuevo_gasto)
                    caja_compra.save()"""

        if req['forma_pago'] == 'Credito':
            nuevo_gasto.numero_cuotas_credito = req['numero_cuotas']
            nuevo_gasto.saldo = int(req['total'])
            nuevo_gasto.estado_pago = 'Pendiente'

            for cuota in req['cuotas']:
                mi_cuota = CuotaGasto(gasto=nuevo_gasto, fecha_limite=cuota['fecha'], valor_cuota=cuota['abono'],
                                      saldo_cuota=cuota['abono'])
                fecha_comparar_init = datetime.strptime(mi_cuota.fecha_limite, "%Y-%m-%d")
                fecha_comparar = datetime(nuevo_gasto.fecha.year, nuevo_gasto.fecha.month, nuevo_gasto.fecha.day)
                fecha_limite_comparar = datetime(fecha_comparar_init.year, fecha_comparar_init.month,
                                                 fecha_comparar_init.day)

                if fecha_limite_comparar == fecha_comparar:
                    abono_gasto = AbonoGasto(gasto=nuevo_gasto, valor_abonado=mi_cuota.saldo_cuota,
                                             medio_pago=req['medio_pago_credito'])
                    abono_gasto.save()
                    nuevo_gasto.saldo = nuevo_gasto.saldo - int(mi_cuota.saldo_cuota)
                    nuevo_gasto.save()
                    mi_cuota.valor_pagado = int(mi_cuota.saldo_cuota)
                    mi_cuota.saldo_cuota = 0
                    mi_cuota.estado_pago = 'Pagada'
                    mi_cuota.fecha_pago = mi_cuota.fecha_limite
                    mi_cuota.save()
                    """caja_on = Caja.objects.filter(estado= True)
                    if caja_on:
                        caja_compra = CajaAbonoCompra(caja = caja_on[0], abono_compra=mi_cuota)
                        caja_compra.save()"""


                else:
                    mi_cuota.save()

        messages.success(request, 'Compra registrada con exito!')

        nuevo_gasto.save()

        for elemento in req['elementos_agregados']:

            if (elemento['tipo'] == 'Servicio'):

                gasto_servicio = GastoServicio(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                               precio_compra_unidad=float(elemento['costo_unitario']),
                                               unidades=elemento['unidades'], precio_total=float(elemento['costo_total']))
                gasto_servicio.save()

                if elemento['id_impuesto'] != '0':
                    try:
                        mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                        valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                        gasto_servicio.impuesto = mi_impuesto
                        gasto_servicio.valor_total_impuesto = valor_total_impuesto
                        gasto_servicio.save()
                    except:
                        print("Impuesto no encontrado")


            elif (elemento['tipo'] == 'Otro'):

                gasto_otro = GastoOtro(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                       precio_compra_unidad=elemento['costo_unitario'],
                                       unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                gasto_otro.save()

                if elemento['id_impuesto'] != '0':
                    try:
                        mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                        valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                        gasto_otro.impuesto = mi_impuesto
                        gasto_otro.valor_total_impuesto = valor_total_impuesto
                        gasto_otro.save()
                    except:
                        print("Impuesto no encontrado")

            elif (elemento['tipo'] == 'Producto'):
                try:
                    mi_elemento = get_object_or_404(Producto, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (
                                int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id, responsable=responsable)
                    historia_transaccion.save()

                    gasto_producto = GastoProducto(gasto=nuevo_gasto, producto=mi_elemento,
                                                   precio_compra_unidad=elemento['costo_unitario'],
                                                   unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_producto.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_producto.impuesto = mi_impuesto
                            gasto_producto.valor_total_impuesto = valor_total_impuesto
                            gasto_producto.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Producto no encontrado")

            elif (elemento['tipo'] == 'Medicamento'):
                try:
                    mi_elemento = get_object_or_404(Medicamento, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (
                                int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id, responsable=responsable)
                    historia_transaccion.save()

                    gasto_medicamento = GastoMedicamento(gasto=nuevo_gasto, medicamento=mi_elemento,
                                                         precio_compra_unidad=elemento['costo_unitario'],
                                                         unidades=elemento['unidades'],
                                                         precio_total=elemento['costo_total'])
                    gasto_medicamento.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_medicamento.impuesto = mi_impuesto
                            gasto_medicamento.valor_total_impuesto = valor_total_impuesto
                            gasto_medicamento.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Medicamento no encontrado")

            elif (elemento['tipo'] == 'Vacuna'):
                try:
                    mi_elemento = get_object_or_404(Vacuna, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (
                                int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id, responsable=responsable)
                    historia_transaccion.save()

                    gasto_vacuna = GastoVacuna(gasto=nuevo_gasto, vacuna=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_vacuna.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_vacuna.impuesto = mi_impuesto
                            gasto_vacuna.valor_total_impuesto = valor_total_impuesto
                            gasto_vacuna.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Vacuna no encontrado")

            elif (elemento['tipo'] == 'Desparasitante'):
                try:
                    mi_elemento = get_object_or_404(Desparasitante, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (
                                int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion,
                                                               tipo_elemento='Desparasitante',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id, responsable=responsable)
                    historia_transaccion.save()

                    gasto_desparasitante = GastoDesparasitante(gasto=nuevo_gasto, desparasitante=mi_elemento,
                                                               precio_compra_unidad=elemento['costo_unitario'],
                                                               unidades=elemento['unidades'],
                                                               precio_total=elemento['costo_total'])
                    gasto_desparasitante.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_desparasitante.impuesto = mi_impuesto
                            gasto_desparasitante.valor_total_impuesto = valor_total_impuesto
                            gasto_desparasitante.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Desparasitante no encontrado")

            elif (elemento['tipo'] == 'Insumo'):
                try:
                    mi_elemento = get_object_or_404(Insumo, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (
                                int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Insumo',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id, responsable=responsable)
                    historia_transaccion.save()

                    gasto_insumo = GastoInsumo(gasto=nuevo_gasto, insumo=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_insumo.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_insumo.impuesto = mi_impuesto
                            gasto_insumo.valor_total_impuesto = valor_total_impuesto
                            gasto_insumo.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Insumo no encontrado")

            else:
                try:
                    mi_elemento = get_object_or_404(ElementoCategoriaInventario, pk=elemento['id_elemento'])
                    mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                    mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (
                            int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                    mi_elemento.save()

                    descripcion = "Compra de " + elemento['unidades'] + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion,
                                                               tipo_elemento='Categoria',
                                                               id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                               id_transaccion=nuevo_gasto.id, responsable=responsable)
                    historia_transaccion.save()

                    gasto_elemento_categoria = GastoElementoInventario(gasto=nuevo_gasto, elemento=mi_elemento,
                                                                       precio_compra_unidad=elemento['costo_unitario'],
                                                                       unidades=elemento['unidades'],
                                                                       precio_total=elemento['costo_total'])
                    gasto_elemento_categoria.save()

                    if elemento['id_impuesto'] != '0':
                        try:
                            mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                            valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                            gasto_elemento_categoria.impuesto = mi_impuesto
                            gasto_elemento_categoria.valor_total_impuesto = valor_total_impuesto
                            gasto_elemento_categoria.save()
                        except:
                            print("Impuesto no encontrado")
                except:
                    print("Elemento no encontrado")


        data = {
            'registrado_exitoso': True,
        }
        return JsonResponse(data)

    else:
        print("Prueba GET registrar compra")
        form_registrar_compra = Formulario_registrar_compra()

        lista_categorias = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))

        lista_bodegas = json.dumps(list(Bodega.objects.filter(estado=True).values_list('id', 'nombre')))

        return render(request, 'registrar_compra.html', {
            'form_registrar_compra': form_registrar_compra,
            'gastos': True,
            'lista_categorias': lista_categorias,
            'lista_bodegas': lista_bodegas
        })


def obtener_impuestos(request):
    tipo_producto = request.GET.get('tipo', '')
    print(tipo_producto)

    impuestos = Impuesto.objects.all()

    data = {
        'impuestos': serializers.serialize('json', impuestos),
    }
    return JsonResponse(data)


def obtener_inventario(request):
    tipo_elemento = request.GET.get('tipo_elemento', None)
    print(tipo_elemento)

    if tipo_elemento == 'Producto':
        lista_elementos = Producto.objects.filter(estado=True).order_by('nombre')
        elementos_por_codigo = chain(Producto.objects.distinct('codigo').filter(estado=True).exclude(codigo__isnull=True),Producto.objects.filter(codigo__isnull=True, estado=True))
        elementos_repetidos = []
        for element in list(Producto.objects.values('codigo').annotate(cantidad=Count('codigo'))):
            if element['cantidad'] > 1:
                elementos_repetidos.append(element['codigo'])

    elif tipo_elemento == 'Medicamento':
        lista_elementos = Medicamento.objects.filter(estado='Activo').order_by('nombre')
        elementos_por_codigo = chain(
            Medicamento.objects.distinct('codigo_inventario').filter(estado='Activo').exclude(codigo_inventario__isnull=True),
            Medicamento.objects.filter(codigo_inventario__isnull=True, estado='Activo'))
        elementos_repetidos = []
        for element in list(Medicamento.objects.values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                elementos_repetidos.append(element['codigo_inventario'])

    elif tipo_elemento == 'Vacuna':
        lista_elementos = Vacuna.objects.filter(estado='Activa').order_by('nombre')
        elementos_por_codigo = chain(
            Vacuna.objects.distinct('codigo_inventario').filter(estado='Activa').exclude(
                codigo_inventario__isnull=True),
            Vacuna.objects.filter(codigo_inventario__isnull=True, estado='Activa'))
        elementos_repetidos = []
        for element in list(
                Vacuna.objects.values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                elementos_repetidos.append(element['codigo_inventario'])

    elif tipo_elemento == 'Desparasitante':
        lista_elementos = Desparasitante.objects.filter(estado='Activo').order_by('nombre')
        elementos_por_codigo = chain(
            Desparasitante.objects.distinct('codigo_inventario').filter(estado='Activo').exclude(
                codigo_inventario__isnull=True),
            Desparasitante.objects.filter(codigo_inventario__isnull=True, estado='Activo'))
        elementos_repetidos = []
        for element in list(
                Desparasitante.objects.values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                elementos_repetidos.append(element['codigo_inventario'])

    elif tipo_elemento == 'Insumo':
        lista_elementos = Insumo.objects.filter(estado='Activo').order_by('nombre')
        elementos_por_codigo = chain(
            Insumo.objects.distinct('codigo_inventario').filter(estado='Activo').exclude(
                codigo_inventario__isnull=True),
            Insumo.objects.filter(codigo_inventario__isnull=True, estado='Activo'))
        elementos_repetidos = []
        for element in list(
                Insumo.objects.values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                elementos_repetidos.append(element['codigo_inventario'])

    else:
        lista_elementos = ElementoCategoriaInventario.objects.filter(categoria_id=tipo_elemento,estado='Activo').order_by('nombre')
        elementos_por_codigo = chain(
            ElementoCategoriaInventario.objects.distinct('codigo_inventario').filter(categoria_id=tipo_elemento,estado='Activo').exclude(
                codigo_inventario__isnull=True),
            ElementoCategoriaInventario.objects.filter(categoria_id=tipo_elemento,codigo_inventario__isnull=True, estado='Activo'))
        elementos_repetidos = []
        for element in list(
                ElementoCategoriaInventario.objects.filter(categoria_id=tipo_elemento).values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                elementos_repetidos.append(element['codigo_inventario'])


    data = {
        'lista_elementos': serializers.serialize('json', lista_elementos),
        'elementos_por_codigo': serializers.serialize('json', elementos_por_codigo),
        'codigos_elementos': elementos_repetidos
    }
    return JsonResponse(data)


class DetalleCompra(DetailView):
    model = Gasto
    template_name = "detalle_compra.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleCompra, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleCompra, self).get_context_data(**kwargs)
        context['gastos'] = True
        context['gastos_producto'] = GastoProducto.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_medicamento'] = GastoMedicamento.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_vacuna'] = GastoVacuna.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_desparasitante'] = GastoDesparasitante.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_insumo'] = GastoInsumo.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_elementos_inventario'] = GastoElementoInventario.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_servicio'] = GastoServicio.objects.filter(gasto=self.kwargs['pk'])
        context['gastos_otro'] = GastoOtro.objects.filter(gasto=self.kwargs['pk'])

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        context['abonos'] = AbonoGasto.objects.filter(gasto=self.kwargs['pk'])

        return context


@login_required
def abonarGasto(request, id_gasto):
    valor_abonado = request.GET.get('valor_abonado', None);
    medio_pago = request.GET.get('medio_pago', None)
    if valor_abonado:
        print("Hola desde POST request")
        mi_gasto = Gasto.objects.get(id=id_gasto)

        abono_gasto = AbonoGasto(gasto=mi_gasto, valor_abonado=valor_abonado, medio_pago=medio_pago)
        abono_gasto.save()

        cuotas_gasto = CuotaGasto.objects.filter(gasto__id=mi_gasto.id, estado_pago='Pendiente').order_by(
            'fecha_limite')
        abono_a_saldo = int(valor_abonado)
        print("Abono inicial" + str(abono_a_saldo))
        caja_on = Caja.objects.filter(estado=True)
        if caja_on:
            caja_compra = CajaAbonoCompra(caja=caja_on[0], abono_compra=cuotas_gasto[0], gasto=mi_gasto,
                                          valor_abonado=abono_a_saldo, medio_pago=medio_pago)
            caja_compra.save()
        for cuota in cuotas_gasto:
            print("Abono inicial" + str(abono_a_saldo))
            if cuota.saldo_cuota > abono_a_saldo:
                if abono_a_saldo > 0:
                    cuota.fecha_pago = datetime.now().date()
                cuota.saldo_cuota = cuota.saldo_cuota - abono_a_saldo
                cuota.valor_pagado = cuota.valor_pagado + abono_a_saldo
                mi_gasto.saldo = mi_gasto.saldo - abono_a_saldo
                mi_gasto.save()
                abono_a_saldo = 0
                cuota.save()
            elif cuota.saldo_cuota <= abono_a_saldo:
                cuota.fecha_pago = datetime.now().date()
                cuota.valor_pagado = cuota.valor_pagado + cuota.saldo_cuota
                mi_gasto.saldo = mi_gasto.saldo - cuota.saldo_cuota
                mi_gasto.save()
                abono_a_saldo = abono_a_saldo - cuota.saldo_cuota
                cuota.saldo_cuota = 0
                cuota.estado_pago = 'Pagada'
                cuota.save()

        if not CuotaGasto.objects.filter(gasto__id=mi_gasto.id, estado_pago='Pendiente'):
            mi_gasto.estado_pago = 'Pagado'
            mi_gasto.saldo = 0
            mi_gasto.save()

        messages.success(request, 'Abono registrado con exito!')
        data = {
            'eliminacion': True
        }
        return JsonResponse(data)
    else:
        mi_gasto = Gasto.objects.get(id=id_gasto)
        cuotas_gasto = CuotaGasto.objects.filter(gasto__id=mi_gasto.id).order_by('fecha_limite')
        fecha_actual = datetime.now().date()
        return render(request, 'registrar_abono_gasto.html', {
            'gastos': True,
            'mi_gasto': mi_gasto,
            'cuotas_gasto': cuotas_gasto,
            'fecha_actual': fecha_actual,
            'cuota_actual': CuotaGasto.objects.filter(gasto__id=mi_gasto.id, estado_pago='Pendiente').order_by(
                'fecha_limite').first(),
        })


def anular_compra(request):
    id_compra = request.GET.get('id', '')
    compra = get_object_or_404(Gasto, pk=id_compra)
    compra.estado_pago = 'Anulada'
    compra.save()

    compras_producto = GastoProducto.objects.filter(gasto=compra)
    for compra_producto in compras_producto:
        elemento_comprado = Producto.objects.get(pk=compra_producto.producto.id)
        elemento_comprado.cantidad = elemento_comprado.cantidad - compra_producto.unidades
        elemento_comprado.cantidad_total_unidades -= compra_producto.unidades * elemento_comprado.cantidad_unidad

        elemento_comprado.save()

    compras_medicamento = GastoMedicamento.objects.filter(gasto=compra)
    for compra_medicamento in compras_medicamento:
        elemento_comprado = Medicamento.objects.get(pk=compra_medicamento.medicamento.id)
        elemento_comprado.cantidad = elemento_comprado.cantidad - compra_medicamento.unidades
        elemento_comprado.cantidad_total_unidades -= compra_medicamento.unidades * elemento_comprado.cantidad_unidad

        elemento_comprado.save()

    compras_vacuna = GastoVacuna.objects.filter(gasto=compra)
    for compra_vacuna in compras_vacuna:
        elemento_comprado = Vacuna.objects.get(pk=compra_vacuna.vacuna.id)
        elemento_comprado.cantidad = elemento_comprado.cantidad - compra_vacuna.unidades
        elemento_comprado.cantidad_total_unidades -= compra_vacuna.unidades * elemento_comprado.cantidad_unidad

        elemento_comprado.save()

    compras_desparasitante = GastoDesparasitante.objects.filter(gasto=compra)
    for compra_desparasitante in compras_desparasitante:
        elemento_comprado = Desparasitante.objects.get(pk=compra_desparasitante.desparasitante.id)
        elemento_comprado.cantidad = elemento_comprado.cantidad - compra_desparasitante.unidades
        elemento_comprado.cantidad_total_unidades -= compra_desparasitante.unidades * elemento_comprado.cantidad_unidad

        elemento_comprado.save()

    compras_categoria = GastoElementoInventario.objects.filter(gasto=compra)
    for compra_categoria in compras_categoria:
        elemento_comprado = ElementoCategoriaInventario.objects.get(pk=compra_categoria.elemento.id)
        elemento_comprado.cantidad = elemento_comprado.cantidad - compra_categoria.unidades
        elemento_comprado.cantidad_total_unidades -= compra_categoria.unidades * elemento_comprado.cantidad_unidad

        elemento_comprado.save()

    compras_insumo = GastoInsumo.objects.filter(gasto=compra)
    for compra_insumo in compras_insumo:
        elemento_comprado = Insumo.objects.get(pk=compra_insumo.insumo.id)
        elemento_comprado.cantidad = elemento_comprado.cantidad - compra_insumo.unidades
        elemento_comprado.cantidad_total_unidades -= compra_insumo.unidades * elemento_comprado.cantidad_unidad

        elemento_comprado.save()

    cuotas_compra = CuotaGasto.objects.filter(gasto=compra)
    for cuota in cuotas_compra:
        cuota.estado_pago = "Anulada"
        cuota.save()

    messages.success(request, 'Factura anulada con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)