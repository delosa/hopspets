from django.db import models
from clientes.models import Cliente
from impuestos.models import Impuesto
from productos.models import Producto
from productos.models import Proveedor
from productos.models import Medicamento
from vacunacion.models import Vacuna
from desparasitacion.models import Desparasitante
from insumos.models import Insumo
from usuarios.models import Usuario
from categoriasInventario.models import ElementoCategoriaInventario


class GastoProducto(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoMedicamento(models.Model):
    medicamento = models.ForeignKey(Medicamento, on_delete=models.CASCADE)
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoVacuna(models.Model):
    vacuna = models.ForeignKey(Vacuna, on_delete=models.CASCADE)
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoDesparasitante(models.Model):
    desparasitante = models.ForeignKey(Desparasitante, on_delete=models.CASCADE)
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoElementoInventario(models.Model):
    elemento = models.ForeignKey(ElementoCategoriaInventario, on_delete=models.CASCADE)
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoInsumo(models.Model):
    insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=0, default=2)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoServicio(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


class GastoOtro(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    gasto = models.ForeignKey('Gasto', on_delete=models.CASCADE)
    precio_compra_unidad = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    unidades = models.IntegerField()
    precio_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    impuesto = models.ForeignKey(Impuesto, on_delete=models.CASCADE, blank=True, null=True)
    valor_total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    def total_con_impuesto(self):
        return self.precio_total + self.valor_total_impuesto


TIPOS_DE_COMPROBANTE = (('Compra', 'Factura de Compra'), ('Gasto', 'Gasto'), ('Nota Crédito', 'Nota Crédito'))

class Gasto(models.Model):
    valor_total = models.DecimalField(max_digits=50, decimal_places=2, verbose_name='Valor', default=0)
    fecha = models.DateField(verbose_name='Fecha')
    comentarios = models.TextField(blank=True, null=True)
    total_impuesto = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    sub_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    descuento = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE, blank=True, null=True)
    forma_pago = models.CharField(max_length=400, verbose_name='Forma pago', default='Debito')
    tipo_pago_debito = models.CharField(max_length=400, verbose_name='Tipo pago', blank=True, null=True)
    numero_cuotas_credito = models.CharField(max_length=400, verbose_name='Número de cuotas', blank=True, null=True)
    saldo = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    estado_pago = models.CharField(max_length=400, verbose_name='Estado', default='Pagado')
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    numero_comprobante = models.CharField(max_length=100, verbose_name='N° comprobante', blank=True, null=True)
    tipo_comprobante = models.CharField(max_length=100, verbose_name="Tipo de comprobante", choices=TIPOS_DE_COMPROBANTE,default='Compra')
    cliente_nota_credito = models.ForeignKey(Cliente, on_delete=models.CASCADE, blank=True, null=True)

    def valor_pagado(self):
        return self.valor_total - self.saldo

    def __str__(self):
        return self.fecha


class CuotaGasto(models.Model):
    gasto = models.ForeignKey(Gasto, on_delete=models.CASCADE)
    fecha_limite = models.DateField()
    fecha_pago = models.DateField(blank=True, null=True)
    valor_pagado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    valor_cuota = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    estado_pago = models.CharField(max_length=400, verbose_name='Estado', default='Pendiente')
    saldo_cuota = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    def __str__(self):
        return '%s %s' % (self.fecha_limite, self.saldo_cuota)


class AbonoGasto(models.Model):
    gasto = models.ForeignKey(Gasto, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    valor_abonado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')