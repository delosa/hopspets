from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_gasto/', CrearGasto.as_view(), name="registrar_gasto"),
    path('listar_gasto/<str:estado_pago>/<str:fecha_pago>/', ListarGasto.as_view(), name="listar_gasto"),
    path('editar_gasto/<int:pk>/', EditarGasto.as_view(), name="editar_gasto"),
    path('eliminar_gasto/<int:pk>/', EliminarGasto.as_view(), name="eliminar_gasto"),
    path('registrar_compra/', registrar_compra, name="registrar_compra"),
    path('ajax/obtener_impuestos/', obtener_impuestos, name="obtener_impuestos"),
    path('ajax/obtener_inventario/', obtener_inventario, name="obtener_inventario"),
    path('detalle_compra/<int:pk>/', DetalleCompra.as_view(), name="detalle_compra"),
    path('abonar_gasto/<int:id_gasto>/', abonarGasto, name="abonar_gasto"),
    path('anular_compra/', anular_compra, name="anular_compra"),
    path('listar_gasto_mes/<str:estado_pago>/<str:mes>/<str:ano>/', ListarGastoMes.as_view(), name="listar_gasto_mes"),
]