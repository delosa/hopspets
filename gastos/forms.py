from .models import Gasto
from django import forms
from datetime import datetime


class CrearGastoForm(forms.ModelForm):

    class Meta:
        model = Gasto
        fields = (
            'fecha',
        )

        widgets = {
            'fecha': forms.DateInput(attrs={'class': 'form-control fechaGasto'})
        }


class EditarGastoForm(forms.ModelForm):

    class Meta:
        model = Gasto
        
        fields = (
            'fecha',
        )

        widgets = {
            'fecha': forms.DateInput(attrs={'class': 'form-control fechaGasto'})
        }


class Formulario_registrar_compra(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(Formulario_registrar_compra, self).__init__(*args, **kwargs)
        #self.fields['fecha'].initial = datetime.now()
        self.fields['proveedor'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['numero_comprobante'].widget.attrs = {'placeholder': ''}
        self.fields['proveedor'].label = 'Tercero'
        self.fields['cliente_nota_credito'].label = 'Tercero'
        self.fields['tipo_comprobante'].label = 'Comprobante de Egreso'

    class Meta:
        model = Gasto
        fields = (
            'fecha',
            'comentarios',
            'valor_total',
            'sub_total',
            'total_impuesto',
            'proveedor',
            'numero_comprobante',
            'tipo_comprobante',
            'cliente_nota_credito'
        )

        widgets = {
            'fecha': forms.DateInput(attrs={'class': 'form-control fechaCompra'}, format='%m/%d/%Y')
        }