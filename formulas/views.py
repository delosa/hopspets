from django.views.generic import *
from .models import *
from .forms import *
from django.urls import reverse_lazy
from mascotas.models import Mascota
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from  veteriariaTenant.models import Veterinaria
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
import base64
import json
from django.http import JsonResponse


class CrearFormula(SuccessMessageMixin, CreateView):
    model = Formula
    form_class = CrearFormulaForm
    template_name = "registrar_formula.html"
    success_message = "¡Fórmula creada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearFormula, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_mascota =  get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return '/mascotas/detalle_mascota/' + str(mi_mascota.id)

    def form_valid(self,form):
        formula = form.instance
        formula.mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])

        self.object = form.save()
        return super(CrearFormula, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearFormula, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        context['formulas'] = Formula.objects.filter(mascota = self.kwargs['id_mascota'])
        return context


class EditarFormula(SuccessMessageMixin, UpdateView):
    model = Formula
    form_class = CrearFormulaForm
    template_name = "registrar_formula.html"
    success_message = "¡Fórmula editada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarFormula, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_formula =  get_object_or_404(Formula, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(mi_formula.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarFormula, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_formula = get_object_or_404(Formula, pk=self.kwargs['pk'])
        context['mascota'] = get_object_or_404(Mascota, pk=mi_formula.mascota.id)
        context['formulas'] = Formula.objects.filter(mascota=mi_formula.mascota.id)
        return context


class DetalleFormula(DetailView):
    model = Formula
    template_name = "detalle_formula.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleFormula, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleFormula, self).get_context_data(**kwargs)
        context['mascotas'] = True

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        return context


def datos_impresion_formula(request):
    id_formula = request.GET.get('id_formula', None)

    formula = Formula.objects.get(pk=id_formula)
    veterinaria = Veterinaria.objects.first()

    logo_veterinaria = ""

    if veterinaria.foto:
        with open("media/" + str(veterinaria.foto), "rb") as pdf_file:
            encoded_string = base64.b64encode(pdf_file.read())
            logo_veterinaria = encoded_string

    datos_veterinaria = {'nombre_veterinaria':veterinaria.nombre,
                         'nit':veterinaria.nit,
                         'direccion':veterinaria.direccion,
                         'ciudad':veterinaria.ciudad,
                         'telefono':veterinaria.telefono,
                         'celular':veterinaria.celular,
                         'logo_veterinaria': str(logo_veterinaria)}

    datos_impresion = {'descripcion': formula.descripcion,
                       'nombre_veterinario': formula.veterinario.first_name,
                       'numero_tarjeta_veterinario': str(formula.veterinario.no_tarjeta),
                       'telefono_veterinario': str(formula.veterinario.telefono),
                       'celular_veterinario': str(formula.veterinario.celular),
                       'fecha_formula': str(formula.fecha.date()),
                       'id_formula': str(formula.id),
                       'nombre_mascota': formula.mascota.nombre,
                       'numero_carnet_mascota': formula.mascota.numero_carnet,
                       'numero_chip_mascota': formula.mascota.numero_micro_chip,
                       'raza_mascota': formula.mascota.raza.nombre + " ("+formula.mascota.raza.tipo_mascota.nombre+")",
                       'datos_veterinaria': datos_veterinaria}

    data = {
        'datos_impresion': json.dumps(datos_impresion)
    }
    return JsonResponse(data)