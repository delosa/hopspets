from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_formula/<int:id_mascota>/', CrearFormula.as_view(), name="registrar_formula"),
    path('editar_formula/<int:pk>/', EditarFormula.as_view(), name="editar_formula"),
    path('detalle_formula/<int:pk>/', DetalleFormula.as_view(), name="detalle_formula"),
    path('ajax/datos_impresion_formula/', datos_impresion_formula, name="datos_impresion_formula"),
]