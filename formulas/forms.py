from .models import *
from django import forms
from datetime import datetime


class CrearFormulaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearFormulaForm, self).__init__(*args, **kwargs)
        self.fields['veterinario'].empty_label = None
        self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['descripcion'].required = True
        self.fields['fecha'].initial = datetime.now()

    class Meta:
        model = Formula
        fields = (
            'veterinario',
            'descripcion',
            'fecha',
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '20', 'placeholder': '', 'maxrows': '20'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }
