from django.db import models
from mascotas.models import Mascota
from usuarios.models import Veterinario


class Formula(models.Model):
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    veterinario = models.ForeignKey(Veterinario, on_delete=models.CASCADE, limit_choices_to={'is_active': True})
    fecha = models.DateTimeField()
    descripcion = models.CharField(max_length=2000, verbose_name='Descripción')

    def __str__(self):
        return self.mascota.nombre + " - " + str(self.fecha.strftime('%b. %d, %Y, %H:%M %p'))
