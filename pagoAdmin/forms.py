from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404

class CrearPagoAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearPagoAdminForm, self).__init__(*args, **kwargs)
        self.fields['fecha_pago'].initial = datetime.now()
        self.fields['adjunto'].label = "Adjunto Comprobante"

    class Meta:
        model = PagoAdmin
        fields = (
            'veterinaria',
            'fecha_pago',
            'medio_pago',
            'valor_pagado',
            'adjunto',
        )

        widgets = {
            'fecha_pago': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'adjunto': forms.FileInput()
        }

class EditarPagoAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditarPagoAdminForm, self).__init__(*args, **kwargs)
        self.fields['veterinaria'].disabled = True
        self.fields['adjunto'].label = "Adjunto Comprobante"

    class Meta:
        model = PagoAdmin
        fields = (
            'fecha_pago',
            'veterinaria',
            'fecha_pago',
            'medio_pago',
            'valor_pagado',
            'adjunto',
        )

        widgets = {
            'fecha_pago': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'adjunto': forms.FileInput()
        }