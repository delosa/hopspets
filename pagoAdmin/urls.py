from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from .forms import *

urlpatterns = [
    path('listar_pago_admin/<str:fecha_filtro>/', ListarPagoAdmin.as_view(), name="listar_pago_admin"),
    path('listar_pago_admin_mes/<str:mes>/<str:ano>/', ListarPagoAdminMes.as_view(), name="listar_pago_admin_mes"),
	path('agregar_pago/<int:id_user>/', AgregarPago.as_view(), name="agregar_pago"),
    path('editar_pago_admin/<int:pk>/', EditarPago.as_view(),name="editar_pago_admin"),
]