from django.shortcuts import render,get_object_or_404,redirect, render_to_response
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth.models import User
from datetime import datetime

class ListarPagoAdmin(ListView):
	model = PagoAdmin
	template_name = "listar_pago_admin.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(ListarPagoAdmin,self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ListarPagoAdmin, self).get_context_data(**kwargs)
		context['pago'] = True

		pagos = PagoAdmin.objects.all()

		hoy = datetime.now().date()

		if self.kwargs['fecha_filtro'] == 'hoy':
			pagos = pagos.filter(fecha_pago=hoy)
		elif self.kwargs['fecha_filtro'] == 'semana':
			esta_semana = hoy.isocalendar()[1]
			pagos = pagos.filter(fecha_pago__week=esta_semana, fecha_pago__year=hoy.year)
		elif self.kwargs['fecha_filtro'] == 'mes':
			pagos = pagos.filter(fecha_pago__month=hoy.month, fecha_pago__year=hoy.year)
		else:
			pagos = PagoAdmin.objects.all()
            
		context['pagos'] = pagos
		context['fecha_filtro'] = self.kwargs['fecha_filtro']

		return context

class AgregarPago(SuccessMessageMixin, CreateView):
    model = PagoAdmin
    form_class = CrearPagoAdminForm
    template_name = "agregar_pago.html"
    success_message = "¡Se registro el pago con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AgregarPago, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/pagoAdmin/listar_pago_admin/hoy/'

    def form_valid(self, form):
        pago = form.instance
        pago.responsable = get_object_or_404(UsuarioPublico, id=self.kwargs['id_user'])
        self.object = form.save()
        messages.success(self.request, "Se ha registrado el Pago Exitosamente")
        return super(AgregarPago, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el pago, por favor revise los datos")
        return super(AgregarPago, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(AgregarPago, self).get_context_data(**kwargs)
        context['pago'] = True
        context['modificar'] = False
        context['usuario_actual'] = get_object_or_404(UsuarioPublico, id=self.kwargs['id_user'])
        return context

class EditarPago(SuccessMessageMixin, UpdateView):
    model = PagoAdmin
    form_class = EditarPagoAdminForm
    template_name = "agregar_pago.html"
    success_message = "Se edito el pago con exito"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarPago, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/pagoAdmin/listar_pago_admin/hoy/'

    def form_valid(self, form):
        pago = form.instance
        self.object = form.save()
        return super(EditarPago, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarPago, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['pagos'] = True
        return context

class ListarPagoAdminMes(ListView):
    model = PagoAdmin
    template_name = "listar_pago_admin.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagoAdminMes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagoAdminMes, self).get_context_data(**kwargs)
        context['pago'] = True
        mes = self.kwargs['mes']
        ano = self.kwargs['ano']
        pagos = PagoAdmin.objects.all()
        pagos_admin = pagos.filter(fecha_pago__month=mes, fecha_pago__year=ano)

        context['pagos'] = pagos_admin
        context['mes'] = mes
        context['ano'] = ano
        context['fecha_filtro'] = 'mes'

        return context