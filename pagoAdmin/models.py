from django.db import models
from django.core.validators import RegexValidator
import sys
import re
from io import BytesIO
from django.contrib.auth.models import User
from usuariosPublicos.models import UsuarioPublico
from veterinarias.models import Tenant

def get_upload_to(instance, filename):
    return 'upload/%s/%s' % ('PagoAdmin', filename)

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1] 
    valid_extensions = ['.zip','.tar','.rar' ,'.pdf','.PDF','.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')

MEDIO_PAGO = (('Efectivo','Efectivo'),('Consignación','Consignación'),('Transferencia','Transferencia'))

class PagoAdmin(models.Model):
    fecha_pago = models.DateField(verbose_name="Fecha de Pago")
    veterinaria = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    medio_pago = models.CharField(max_length=100, verbose_name="Medio de Pago",choices=MEDIO_PAGO)
    valor_pagado = models.DecimalField(max_digits=50, decimal_places=0,verbose_name="Valor Pagado")
    comprobante = models.CharField(max_length=100, verbose_name='Comprobante',null=True, blank=True)
    responsable = models.ForeignKey(UsuarioPublico, on_delete=models.CASCADE, verbose_name="Responsable", null=True, blank=True)
    responsable_cliente = models.CharField(max_length=100, verbose_name="Responsable Cliente", null=True, blank=True)
    adjunto = models.FileField(upload_to=get_upload_to, validators=[validate_file_extension])

    def __str__(self):
        return self.veterinaria

    def archivo_pres(self):
        if len(str(self.adjunto).split('/')) == 2:
            return str(self.adjunto).split('/')[1]
        elif len(str(self.adjunto).split('/')) == 1:
            return str(self.adjunto).split('/')[0]
        else:
            return str(self.adjunto)
