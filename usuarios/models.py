# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.contrib.auth.models import Group
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re


alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Unicamente se aceptan caracteres alfanumericos.')
alpha = RegexValidator(r'^[a-zA-Z]*$', 'Unicamente se aceptan caracteres alfanumericos.')
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores númericos')


def get_upload_to(instance, filename):
    return 'upload/%s/%s' % (instance.numero_documento_identificacion, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')

# Create your models here.


class Usuario(User):
    IDENTIFICACION_SEXO = (('MASCULINO', 'Masculino'), ('FEMENINO', 'Femenino'))
    IDENTIFICACION_CHOICES = (
        ('CC', 'Cédula de ciudadanía'), ('PAS', 'Pasaporte'), ('CE', 'Cedula extranjera'))

    tipo_documento_identificacion = models.CharField(max_length=20, verbose_name="tipo de identificación",
                                                     choices=IDENTIFICACION_CHOICES)
    numero_documento_identificacion = models.CharField(max_length=20,
                                                       verbose_name="número de documento",
                                                       validators=[numeric])
    telefono = models.CharField(max_length=20, verbose_name="Télefono", null=True, blank=True)
    celular = models.CharField(max_length=20, verbose_name="Celular", null=True, blank=True)
    segundo_nombre = models.CharField(max_length=100, verbose_name="segundo nombre", blank=True)
    segundo_apellido = models.CharField(max_length=100, verbose_name="primer apellido")
    sexo = models.CharField(max_length=20, verbose_name="Sexo", choices=IDENTIFICACION_SEXO, null=True, blank=True)
    direccion = models.CharField(max_length=100, verbose_name="Dirección")
    roles = models.CharField(max_length=100, verbose_name="roles", null=True, blank=True, default="No asignado")
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    
    
    

    def save(self, *args, **kwargs):
        if self.foto:
            if self.foto.url.endswith('.png') or self.foto.url.endswith('.PNG'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(Usuario, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        return '%s %s (%s)' % (self.first_name, self.last_name, self.roles)


class Administrador(Usuario):
    pass


class Asistente(Usuario):
    pass


class Veterinario(Usuario):
    especializacion = models.CharField(max_length=100, verbose_name="Especialización")
    no_tarjeta = models.CharField(max_length=20, verbose_name="Número de tarjeta",
                                  validators=[numeric])

class PostEMail(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True)
