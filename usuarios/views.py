from django.db.models import Sum
from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DetailView
from .forms import CrearVeterinarioForm, CrearAdministradorForm, ModificarAdministradorForm, ModificarVeterinarioForm, CrearAsistenteForm, ModificarAsistenteForm
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from citas.models import Cita, HistoriaClinica,MotivoCita, Hospitalizacion
from mascotas.models import Mascota
from usuarios.models import Veterinario, Administrador, Asistente, PostEMail, Usuario
from gastos.models import Gasto
from datetime import datetime, timedelta
from veterinarias.models import Tenant
from estetica.models import FacturaEstetica
from productos.models import Pago
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import redirect
from vacunacion.models import Vacunacion
from desparasitacion.models import Desparasitacion
from guarderia.models import Guarderia
from django.core import serializers
from django.http import JsonResponse
from django.core.mail import send_mail
from django.db import connection
from threading import Timer
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import PasswordChangeForm
from citas.models import Documento
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from gastos.models import CuotaGasto
from productos.models import CuotaPago
from citas.models import Documento
from veteriariaTenant.models import Veterinaria
from hopsPets.utilities import *
from productos.models import Producto, Medicamento
from vacunacion.models import Vacuna
from desparasitacion.models import Desparasitante
from insumos.models import Insumo
from django.db.models import F
from itertools import chain
from categoriasInventario.models import ElementoCategoriaInventario
from imagenesActualizacion.models import *
from promocionEvento.models import *
from pres.models import *


class Index(TemplateView):

    template_name = "./index.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):

        return super(Index, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        #fix_roles_user()
        context = super(Index, self).get_context_data(**kwargs)
        context['inicio'] = True
        context['total_citas_atendidas'] = Cita.objects.filter(estado='Atendida').count()
        context['total_mascotas'] = Mascota.objects.all().count()
        context['total_veterinarios'] = Veterinario.objects.all().count()
        context['hospitalizaciones_activas'] = Hospitalizacion.objects.filter(estado='Activa')
        """ veterinaria = Veterinaria.objects.all()
        datos = Tenant.objects.filter(nombre_veterinaria=veterinaria[0].nombre)
        term = TerminosCondiciones.objects.filter(veterinaria=datos[0])
        context['terminos_condiciones'] = term """

        if self.request.user.usuario.roles == 'Administrador':
            context['imagen_actualizacion'] = ImagenActualizacion.objects.last()
            context['promocion_evento'] = PromocionEvento.objects.last()

        if self.request.user.usuario.roles == 'Veterinario':
            citas_hoy = Cita.objects.filter(fecha__date=datetime.now().date(), veterinario__id=self.request.user.usuario.id)
            vacunaciones_hoy = Vacunacion.objects.filter(fecha__date=datetime.now().date(), veterinario__id=self.request.user.usuario.id)
            desparasitaciones_hoy = Desparasitacion.objects.filter(fecha__date=datetime.now().date(), veterinario__id=self.request.user.usuario.id)
        else:
            citas_hoy = Cita.objects.filter(fecha__date=datetime.now().date())
            vacunaciones_hoy = Vacunacion.objects.filter(fecha__date=datetime.now().date())
            desparasitaciones_hoy = Desparasitacion.objects.filter(fecha__date=datetime.now().date())

        context['citas_hoy'] = citas_hoy
        esteticas_hoy = FacturaEstetica.objects.filter(fecha__date=datetime.now().date())
        context['esteticas_hoy'] = esteticas_hoy
        context['vacunaciones_hoy'] = vacunaciones_hoy
        context['desparasitaciones_hoy'] = desparasitaciones_hoy
        guarderias_hoy = Guarderia.objects.filter(fecha_inicio__date=datetime.now().date())
        context['guarderias_hoy'] = guarderias_hoy

        ingresos_diarios_pagos = Pago.objects.filter(estado_pago='Pagado', fecha=datetime.now().date()).aggregate(Sum('total_factura'))['total_factura__sum']
        ingresos_diarios_pagos_pendientes = Pago.objects.filter(estado_pago='Pendiente', fecha=datetime.now().date())

        if ingresos_diarios_pagos == None:
            ingresos_diarios_pagos = 0

        for pago_pendiente in ingresos_diarios_pagos_pendientes:
            ingresos_diarios_pagos += pago_pendiente.total_factura - pago_pendiente.saldo

        context['ingresos_diarios'] = ingresos_diarios_pagos

        #############INGRESOS MENSUALES#####################

        ingresos_mensuales_pagos = Pago.objects.filter(estado_pago='Pagado', fecha__month=datetime.now().date().month, fecha__year=datetime.now().date().year).aggregate(Sum('total_factura'))['total_factura__sum']
        ingresos_mensuales_pagos_pendientes = Pago.objects.filter(estado_pago='Pendiente', fecha__month=datetime.now().date().month, fecha__year=datetime.now().date().year)

        if ingresos_mensuales_pagos == None:
            ingresos_mensuales_pagos = 0

        for pago_pendiente in ingresos_mensuales_pagos_pendientes:
            ingresos_mensuales_pagos += pago_pendiente.total_factura - pago_pendiente.saldo

        context['ingresos_mensuales'] = ingresos_mensuales_pagos

        ####################################################

        egresos_diarios_pagados = Gasto.objects.filter(estado_pago='Pagado',fecha=datetime.now().date()).aggregate(Sum('valor_total'))['valor_total__sum']
        egresos_diarios_pendientes = Gasto.objects.filter(estado_pago='Pendiente',fecha=datetime.now().date())

        if egresos_diarios_pagados == None:
            egresos_diarios_pagados = 0

        for pago_egreso_pendiente in egresos_diarios_pendientes:
            egresos_diarios_pagados += pago_egreso_pendiente.valor_total - pago_egreso_pendiente.saldo

        context['egresos_diarios'] = egresos_diarios_pagados

        ##############EGRESOS MENSUALES###############

        egresos_mensuales_pagados = Gasto.objects.filter(estado_pago='Pagado', fecha__month=datetime.now().date().month, fecha__year=datetime.now().date().year).aggregate(Sum('valor_total'))['valor_total__sum']
        egresos_mensuales_pendientes = Gasto.objects.filter(estado_pago='Pendiente', fecha__month=datetime.now().date().month, fecha__year=datetime.now().date().year)

        if egresos_mensuales_pagados == None:
            egresos_mensuales_pagados = 0

        for pago_egreso_pendiente in egresos_mensuales_pendientes:
            egresos_mensuales_pagados += pago_egreso_pendiente.valor_total - pago_egreso_pendiente.saldo

        context['egresos_mensuales'] = egresos_mensuales_pagados

        ##############################################

        cuentas_por_pagar = CuotaGasto.objects.filter(estado_pago='Pendiente')

        valor_cuentas_por_pagar = 0

        for cuenta in cuentas_por_pagar:
            valor_cuentas_por_pagar += cuenta.saldo_cuota

        context['valor_cuentas_por_pagar'] = valor_cuentas_por_pagar

        cuentas_por_cobrar = CuotaPago.objects.filter(estado_pago='Pendiente')

        valor_cuentas_por_cobrar = 0

        for cuenta in cuentas_por_cobrar:
            valor_cuentas_por_cobrar += cuenta.saldo_cuota

        context['valor_cuentas_por_cobrar'] = valor_cuentas_por_cobrar

        context['cuentas_por_pagar'] = cuentas_por_pagar.filter(fecha_limite__month__lte=datetime.now().date().month).distinct('gasto')
        context['cuentas_por_cobrar'] = cuentas_por_cobrar.filter(fecha_limite__month__lte=datetime.now().date().month).distinct('pago')

        return context


def notificaciones(request):
    #print("compresion de imagenes como documentos!")
    #documentos = Documento.objects.all()
    #for documento in documentos:
    #    documento.save()

    send_email_notification(request)

    if request.user.usuario.roles == 'Veterinario':
        citas_hoy = Cita.objects.filter(fecha__date=datetime.now().date(), estado='Activa',veterinario__id=request.user.usuario.id)
        esteticas_hoy = FacturaEstetica.objects.filter(fecha__date=datetime.now().date(), estado_de_atencion='Activa')
        vacunaciones_hoy = Vacunacion.objects.filter(fecha__date=datetime.now().date(), estado_aplicacion='Pendiente',veterinario__id=request.user.usuario.id)
        desparasitaciones_hoy = Desparasitacion.objects.filter(fecha__date=datetime.now().date(), estado_aplicacion='Pendiente', veterinario__id=request.user.usuario.id)
        guarderias_hoy = Guarderia.objects.filter(fecha_inicio__date=datetime.now().date(), estado='Activa')

    else:
        citas_hoy = Cita.objects.filter(fecha__date=datetime.now().date(), estado='Activa')
        esteticas_hoy = FacturaEstetica.objects.filter(fecha__date=datetime.now().date(), estado_de_atencion='Activa')
        vacunaciones_hoy = Vacunacion.objects.filter(fecha__date=datetime.now().date(), estado_aplicacion='Pendiente')
        desparasitaciones_hoy = Desparasitacion.objects.filter(fecha__date=datetime.now().date(),estado_aplicacion='Pendiente')
        guarderias_hoy = Guarderia.objects.filter(fecha_inicio__date=datetime.now().date(), estado='Activa')

    numero_notificaciones_hoy = 0
    numero_notificaciones_hoy += len(citas_hoy)
    numero_notificaciones_hoy += len(esteticas_hoy)
    numero_notificaciones_hoy += len(vacunaciones_hoy)
    numero_notificaciones_hoy += len(desparasitaciones_hoy)
    numero_notificaciones_hoy += len(guarderias_hoy)

    mascotas = Mascota.objects.all()

    cumpleaños_hoy = Mascota.objects.filter(fecha_nacimiento__month=datetime.now().date().month, fecha_nacimiento__day=datetime.now().date().day)

    una_semana = datetime.now() + timedelta(days=7)

    proximos_cumpleaños = Mascota.objects.filter(fecha_nacimiento__month__gte=datetime.now().date().month, fecha_nacimiento__day__gt=datetime.now().date().day, fecha_nacimiento__month__lte=una_semana.date().month, fecha_nacimiento__day__lte=una_semana.date().day)
    soluciones_pres = SolucionPRES.objects.filter(estado="Gestionando", revisado=False)

    inventario_stock = chain(Producto.objects.filter(estado=True, cantidad__lte=F('stock_minimo')),
                             Medicamento.objects.filter(estado='Activo', cantidad__lte=F('stock_minimo')),
                             Vacuna.objects.filter(estado='Activa', cantidad__lte=F('stock_minimo')),
                             Desparasitante.objects.filter(estado='Activo', cantidad__lte=F('stock_minimo')),
                             Insumo.objects.filter(estado='Activo', cantidad__lte=F('stock_minimo')),
                             ElementoCategoriaInventario.objects.filter(estado='Activo', cantidad__lte=F('stock_minimo')))

    quince_dias = datetime.now() + timedelta(days=15)

    inventario_vencido = sorted(chain(Producto.objects.filter(estado=True, fecha_vencimiento__lte=quince_dias.date()),
                               Medicamento.objects.filter(estado='Activo', fecha_vencimiento__lte=quince_dias.date()),
                               Vacuna.objects.filter(estado='Activa', fecha_vencimiento__lte=quince_dias.date()),
                               Desparasitante.objects.filter(estado='Activo', fecha_vencimiento__lte=quince_dias.date()),
                               Insumo.objects.filter(estado='Activo', fecha_vencimiento__lte=quince_dias.date()),
                               ElementoCategoriaInventario.objects.filter(estado='Activo', fecha_vencimiento__lte=quince_dias.date())), key=lambda instance: instance.fecha_vencimiento)

    data = {
        'numero_notificaciones_hoy': numero_notificaciones_hoy,
        'citas_hoy': serializers.serialize('json', citas_hoy),
        'esteticas_hoy': serializers.serialize('json', esteticas_hoy),
        'vacunaciones_hoy': serializers.serialize('json', vacunaciones_hoy),
        'desparasitaciones_hoy': serializers.serialize('json', desparasitaciones_hoy),
        'guarderias_hoy': serializers.serialize('json', guarderias_hoy),
        'mascotas': serializers.serialize('json', mascotas),
        'cumpleaños_hoy': serializers.serialize('json', cumpleaños_hoy),
        'proximos_cumpleaños': serializers.serialize('json', proximos_cumpleaños),
        'fecha_hoy': datetime.today().strftime('%Y-%m-%d'),
        'inventario_stock': serializers.serialize('json', inventario_stock),
        'inventario_vencido': serializers.serialize('json', inventario_vencido),
        'soluciones_pres': serializers.serialize('json', soluciones_pres),
    }

    documentos_sin_historia = Documento.objects.filter(historia__isnull=True)

    for documento in documentos_sin_historia:
        documento.delete()

    return JsonResponse(data)


class CrearAdministrador(SuccessMessageMixin, CreateView):
    
    model = Administrador
    form_class = CrearAdministradorForm
    template_name = "crear-administrador.html"
    success_url = reverse_lazy('listar_administradores')
    success_msg = "El usuario fue creado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearAdministrador, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        administrador = form.instance
        contra = administrador.first_name[0] + administrador.numero_documento_identificacion + administrador.last_name[
            0]
        administrador.set_password(contra)
        administrador.username = administrador.email
        administrador.is_active = True
        administrador.roles = "Administrador"

        self.object = form.save()

        grupo_administrador, grupo_administrador_creado = Group.objects.get_or_create(name='Administrador')
        grupo_administrador.user_set.add(self.object)
        messages.success(self.request, "Se ha registrado exitosamente el usuario")

        tenant_name = connection.tenant.schema_name
        veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
        
        # Send Email to create user
        send_mail('Bienvenido a Hopspet ' + veterinaria_tenat.nombre_veterinaria ,
        'Buen día \n \n'+
        'Sus datos para el inicio de sesión en la plataforma Hopspet: ' + '\n\n URL: http://' + tenant_name + '.hopspet.com \n' +
        'Usuario:' + administrador.email +'\nContraseña: '+ contra + '\n\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [administrador.email],
        fail_silently=True)

        mensaje = "Binvenido a Hops Pets, a continuacion los datos para ingresar al sistema: url: http://" + tenant_name + ".hopspet.com" + ", Usuario: " + administrador.username + ", Contraseña: " + contra
        #notificar_por_whatsapp(mensaje, administrador.celular)

        return super(CrearAdministrador, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
        return super(CrearAdministrador, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearAdministrador, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['administradores'] = True
        return context


class EditarAdministrador(SuccessMessageMixin, UpdateView):
    model = Administrador
    template_name = "crear-administrador.html"
    success_url = reverse_lazy('listar_administradores')
    form_class = ModificarAdministradorForm

    success_message = "El usuario fue modificado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarAdministrador, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarAdministrador, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['administradores'] = True
        return context


class VisualizarAdministrador(DetailView):
    model = Administrador
    template_name = "detalle-administrador.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VisualizarAdministrador, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VisualizarAdministrador, self).get_context_data(**kwargs)
        context['administradores'] = True
        return context


class ListarAdministradores(ListView):
    model = Administrador
    template_name = "listar-administradores.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarAdministradores, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarAdministradores, self).get_context_data(**kwargs)
        context['administradores'] = True
        return context


class CrearVeterinario(SuccessMessageMixin, CreateView):
    model = Veterinario
    form_class = CrearVeterinarioForm
    template_name = "crear-veterinario.html"
    success_url = reverse_lazy('listar_veterinarios')
    success_msg = "El usuario fue creado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearVeterinario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        veterinario = form.instance
        contra = veterinario.first_name[0] + veterinario.numero_documento_identificacion + veterinario.last_name[
            0]
        veterinario.set_password(contra)
        veterinario.username = veterinario.email
        veterinario.is_active = True
        veterinario.roles = "Veterinario"

        # Send Email to create user
        tenant_name = connection.tenant.schema_name
        veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
        
        send_mail('Bienvenido a Hopspet ' + veterinaria_tenat.nombre_veterinaria ,
        'Buen día \n \n'+
        'Sus datos para el inicio de sesión en la plataforma Hopspet: ' + '\n\n URL: http://' + tenant_name + '.hopspet.com \n' +
        'Usuario:' + veterinario.email +'\nContraseña: '+ contra + '\n\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [veterinario.email],
        fail_silently=True)

        mensaje = "Binvenido a Hops Pets, a continuacion los datos para ingresar al sistema: url: http://" + tenant_name + ".hopspet.com" + ", Usuario: " + veterinario.username + ", Contraseña: " + contra
        #notificar_por_whatsapp(mensaje, veterinario.celular)

        self.object = form.save()

        grupo_veterinario, grupo_veterinario_creado = Group.objects.get_or_create(name='Veterinario')
        grupo_veterinario.user_set.add(self.object)
        messages.success(self.request, "Se ha registrado exitosamente el usuario")

        return super(CrearVeterinario, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
        return super(CrearVeterinario, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearVeterinario, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['veterinarios'] = True
        return context


class EditarVeterinario(SuccessMessageMixin, UpdateView):
    model = Veterinario
    template_name = "crear-veterinario.html"
    success_url = reverse_lazy('listar_veterinarios')
    form_class = ModificarVeterinarioForm

    success_message = "El usuario fue modificado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVeterinario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        veterinario = form.instance
        veterinario.username = veterinario.email
        self.object = form.save()
        return super(EditarVeterinario, self).form_valid(form)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarVeterinario, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['veterinarios'] = True
        return context


class ListarVeterinarios(ListView):
    model = Veterinario
    template_name = "listar-veterinarios.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarVeterinarios, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(ListarVeterinarios, self).get_queryset()
        queryset = queryset.order_by('-is_active','first_name')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListarVeterinarios, self).get_context_data(**kwargs)
        context['veterinarios'] = True
        return context


class VisualizarVeterinario(DetailView):
    model = Veterinario
    template_name = "detalle-veterinario.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VisualizarVeterinario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VisualizarVeterinario, self).get_context_data(**kwargs)
        context['veterinarios'] = True
        return context


class CrearAsistente(SuccessMessageMixin, CreateView):
    model = Asistente
    form_class = CrearAsistenteForm
    template_name = "crear-asistente.html"
    success_url = reverse_lazy('listar_asistentes')
    success_msg = "El usuario fue creado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearAsistente, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        asistente = form.instance
        contra = asistente.first_name[0] + asistente.numero_documento_identificacion + asistente.last_name[
            0]
        asistente.set_password(contra)
        asistente.username = asistente.email
        asistente.roles = "Asistente"
        asistente.is_active = True

        tenant_name = connection.tenant.schema_name
        veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)

        # Send Email to create user
        send_mail('Bienvenido a Hopspet ' + veterinaria_tenat.nombre_veterinaria ,
        'Buen día \n \n'+
        'Sus datos para el inicio de sesión en la plataforma Hopspet: ' + '\n\n URL: http://' + tenant_name + '.hopspet.com \n' +
        'Usuario:' + asistente.email +'\nContraseña: '+ contra + '\n\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [asistente.email],
        fail_silently=True)

        mensaje = "Binvenido a Hops Pets, a continuacion los datos para ingresar al sistema: url: http://" + tenant_name + ".hopspet.com"+ ", Usuario: " + asistente.username + ", Contraseña: "+contra
        #notificar_por_whatsapp(mensaje, asistente.celular)

        self.object = form.save()

        grupo_asistente, grupo_asistente_creado = Group.objects.get_or_create(name='Asistente')
        grupo_asistente.user_set.add(self.object)
        messages.success(self.request, "Se ha registrado exitosamente el usuario")

        return super(CrearAsistente, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
        return super(CrearAsistente, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearAsistente, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['asistentes'] = True
        return context


class EditarAsistente(SuccessMessageMixin, UpdateView):
    model = Asistente
    template_name = "crear-asistente.html"
    success_url = reverse_lazy('listar_asistentes') 
    form_class = ModificarAsistenteForm

    success_message = "El usuario fue modificado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarAsistente, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarAsistente, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['asistentes'] = True
        return context


class ListarAsistentes(ListView):
    model = Asistente
    template_name = "listar-asistentes.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarAsistentes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarAsistentes, self).get_context_data(**kwargs)
        context['asistentes'] = True
        return context


class VisualizarAsistente(DetailView):
    model = Asistente
    template_name = "detalle-asistente.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VisualizarAsistente, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VisualizarAsistente, self).get_context_data(**kwargs)
        context['asistentes'] = True
        return context


class CambiarControsena():
    """Mauro Castillo
    this class manage all function of user recovery password
     """
    
    def change_password(request):
        if request.method == 'POST':
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, '¡Su contraseña a sido actualizado!')
                return redirect('cambiar_contrasena')
            else:
                messages.error(request, 'Por favor corrija el error a continuación.')
        else:
            form = PasswordChangeForm(request.user)
        return render(request, 'change-password.html', {
            'form': form
        })


def notification_email_citas():

    tenant_name = connection.tenant.schema_name
    veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
    tomorrow = datetime.now() + timedelta(days=1)
    tomorrow_string = tomorrow.date().strftime("%d %B, %Y")
    citas_despues = Cita.objects.filter(fecha__date=tomorrow.date())
    info_petshop_template = "\n Coordialmente \n {0}, Dirección:{1} \r Telefono: {2} \r Celular: {3} \r\r"
    info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre_veterinaria, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)

    #print("entra a la notificacion email",veterinsria_tenant)

    for cita in citas_despues:
        send_mail('Recordatorio Cita Clínica Veterinaria || ' + veterinaria_tenat.nombre_veterinaria,
        'Buen día \n \n'+
        '\n Estimada/o '+ cita.mascota.cliente.nombres +
        '.\r Te recordamos que tu mascota: ' + cita.mascota.nombre +' tiene una cita médica para el día: ' + tomorrow_string + info_petshop_template + '\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [cita.mascota.cliente.email],
        fail_silently=False)



def notification_email_birthday():
    tenant_name = connection.tenant.schema_name
    veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
    tomorrow = datetime.now() + timedelta(days=1)
    tomorrow_string = tomorrow.date().strftime("%d %B, %Y")
    birthdays = Mascota.objects.filter(fecha_nacimiento__month=tomorrow.date().month, fecha_nacimiento__day=tomorrow.date().day)
    for birthday in birthdays:
        send_mail('Feliz Cumpleaños '+ birthday.nombre,
        'Buen día \n'+
        '\nEstimada/o '+ birthday.cliente.nombres +
        ', queremos festejar contigo este grande día, deseándole a '+ birthday.nombre +' un feliz cumpleaños' +
        ',\r te desea,'+ veterinaria_tenat.nombre_veterinaria ,
        'soporte@hopspet.com',
        [birthday.cliente.email],
        fail_silently=False)


def notification_email_desparasitacion():
    tenant_name = connection.tenant.schema_name
    veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
    tomorrow = datetime.now() + timedelta(days=1)
    tomorrow_string = tomorrow.date().strftime("%d %B, %Y")
    desparasitaciones = Desparasitacion.objects.filter(fecha__date=tomorrow.date())

    info_petshop_template = "\n Coordialmente \n {0}, Dirección:{1} \r Telefono: {2} \r Celular: {3} \r\r"
    info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre_veterinaria, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)

    for desparasitacion in desparasitaciones:
        send_mail('Recordatorio Desparasitacion || ' + veterinaria_tenat.nombre_veterinaria ,
        'Buen día \n\n'+
        'Estimada/o '+ desparasitacion.mascota.cliente.nombres +
        '. \r Te recordamos que tu mascota " ' + desparasitacion.mascota.nombre +'" tiene una cita de desparacitación, para el día:' + tomorrow_string + info_petshop_template + '\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [desparasitacion.mascota.cliente.email],
        fail_silently=False)


def notification_email_estetica():
    tenant_name = connection.tenant.schema_name
    veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
    tomorrow = datetime.now() + timedelta(days=1)
    tomorrow_string = tomorrow.date().strftime("%d %B, %Y")
    esteticas = FacturaEstetica.objects.filter(fecha__date=tomorrow.date())

    info_petshop_template = "\n Coordialmente \n {0}, Dirección:{1} \r Telefono: {2} \r Celular: {3} \r\r"
    info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre_veterinaria, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)
    for estetica in esteticas:
        send_mail('Recordatorio Cita Estetica || ' + veterinaria_tenat.nombre_veterinaria ,
        'Buen día \n'+
        'Estimada/o: '+ estetica.mascota.cliente.nombres +'.\r'+
        '.\n Te recordamos que tu mascota ' + estetica.mascota.nombre +' tiene una cita de estetica, para el día:' + tomorrow_string + info_petshop_template + '\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [estetica.mascota.cliente.email],
        fail_silently=False)


def notification_email_vacunacion():
    tenant_name = connection.tenant.schema_name
    veterinaria_tenat = Tenant.objects.get(nombre_tenant = tenant_name)
    tomorrow = datetime.now() + timedelta(days=1)
    tomorrow_string = tomorrow.date().strftime("%d %B, %Y")
    vacunaciones = Vacunacion.objects.filter(fecha__date=tomorrow.date())

    info_petshop_template = "\n Coordialmente \n {0}, Dirección:{1} \r Telefono: {2} \r Celular: {3} \r\r"
    info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre_veterinaria, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)
    
    for vacunacion in vacunaciones:
        send_mail('Recordatorio Cita Vacunación || ' + veterinaria_tenat.nombre_veterinaria ,
        'Buen día \n \n'+
        'Estimada/o: '+ vacunacion.mascota.cliente.nombres +
        '.\r Te recordamos que tu mascota ' + vacunacion.mascota.nombre +' tiene una cita vacunación, para el día: ' + tomorrow_string + info_petshop_template + '\n Muchas gracias por tu atención,' ,
        'soporte@hopspet.com',
        [vacunacion.mascota.cliente.email],
        fail_silently=False)


def send_email_notification(request):

    #print("Hola desde funcion send_email_notification")

    post_mail = PostEMail.objects.all()
    

    if not post_mail:
    
        PostEMail.objects.create(date=datetime.today())
        # Mauro castillo Send of notification
        all_send_email()
        return

    else:
        #database_time_post_email = PostEMail.objects.last().date
        #next_date_send_notification = database_time_post_email + timedelta(days=1)
        #next_date_send_notification_test = database_time_post_email     
        #delta_time_test=today_date - database_time_post_email.replace(tzinfo=None)
        #delta_time = today_date - next_date_send_notification.replace(tzinfo=None)
        #delta_time_testminusone = today_date - next_date_send_notification_test.replace(tzinfo=None)
        motivos_cita = MotivoCita.objects.filter(nombre= "Consulta")
        if not motivos_cita:
            new_motivo_consulta = MotivoCita.objects.create(nombre="Consulta",descripcion= "",precio = 0)
            new_motivo_consulta.save()
        motivos_control = MotivoCita.objects.filter(nombre= "Control")
        if not motivos_control:
            new_motivo_control = MotivoCita.objects.create(nombre="Control",descripcion= "",precio = 0)
            new_motivo_control.save()
        inside_histories = HistoriaClinica.objects.filter(motivo_cita = None)
        if inside_histories:
            for histories in inside_histories:
                element = get_object_or_404 (MotivoCita, pk = 1)
                histories.motivo_cita = element
                histories.save()
        send_email_cita_medica(request)
        send_email_cita_vacunacion(request)
        send_email_cita_estetica(request)
        send_email_cita_desparasitacion(request)
        
        #if delta_time_test.days >= 0:
        #    PostEMail.objects.create(date=datetime.today())
        #    all_send_email()

def send_email_cita_medica(request):
    tomorrow_date = datetime.today() + timedelta(days=1)
    today_date = datetime.today()
    element = Cita.objects.filter(email_sent =False,fecha__year=today_date.year,fecha__month=today_date.month, fecha__date__gte= today_date.date(), fecha__date__lte= tomorrow_date.date())
    for cita in element:
        cita.email_sent = True
        cita.save()
        veterinaria_tenat = Veterinaria.objects.get(pk='1')
        info_petshop_template = "\n \n Coordialmente, \n \n {0} \r Dirección: {1} \r Telefono: {2} \r Celular: {3} \r\r"
        info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)
        cita_temp= cita.fecha - timedelta(hours = 5)

        mensaje = "Hola "+cita.mascota.cliente.nombres + ", te recordamos que tu mascota " +cita.mascota.nombre+" tiene cita medica programada para el dia "+str(cita_temp.strftime("%d/%m/%Y a las %H:%M"))

        if cita.mascota.cliente.notificaciones_wpp:
            notificar_por_whatsapp(cita.mascota.cliente.nombres,cita.mascota.nombre,'cita medica',str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")), cita.mascota.cliente.celular)
        else:
            send_mail('No Responder: Recordatorio Cita Médica || ' + veterinaria_tenat.nombre,
            'Estimada/o '+ cita.mascota.cliente.nombres +
            ',\n \n Te recordamos que tu mascota ' + cita.mascota.nombre +' tiene una cita médica para el día ' + str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")) + info_petshop_template
            + '\n\n **Mensaje generado automáticamente, por favor no responder. Para cualquier inquietud lo invitamos a que haga uso de los canales de comunicación de la veterinaria**' ,
            'soporte@hopspet.com',
            [cita.mascota.cliente.email],
            fail_silently=False)
        
def send_email_cita_vacunacion(request):
    tomorrow_date = datetime.today() + timedelta(days=1)
    today_date = datetime.today()
    element = Vacunacion.objects.filter(email_sent =False,fecha__year=today_date.year,fecha__month=today_date.month, fecha__date__gte= today_date.date(), fecha__date__lte= tomorrow_date.date())
    for vacunacion in element:
        vacunacion.email_sent = True
        vacunacion.save()
        veterinaria_tenat = Veterinaria.objects.get(pk='1')
        info_petshop_template = "\n \n Coordialmente, \n \n {0} \r Dirección: {1} \r Telefono: {2} \r Celular: {3} \r\r"
        info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)
        cita_temp= vacunacion.fecha - timedelta(hours = 5)

        mensaje = "Hola " + vacunacion.mascota.cliente.nombres + ", te recordamos que tu mascota " + vacunacion.mascota.nombre + " tiene cita para vacunacion programada para el dia " + str(
            cita_temp.strftime("%d/%m/%Y a las %H:%M"))

        if vacunacion.mascota.cliente.notificaciones_wpp:
            notificar_por_whatsapp(vacunacion.mascota.cliente.nombres,vacunacion.mascota.nombre,'cita para vacunacion',str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")), vacunacion.mascota.cliente.celular)
        else:
            send_mail('No Responder: Recordatorio Cita Vacunación || ' + veterinaria_tenat.nombre,
            'Estimada/o '+ vacunacion.mascota.cliente.nombres +
            ',\n \n Te recordamos que tu mascota ' + vacunacion.mascota.nombre +' tiene una cita de vacunación para el día ' + str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")) + info_petshop_template
            + '\n\n **Mensaje generado automáticamente, por favor no responder. Para cualquier inquietud lo invitamos a que haga uso de los canales de comunicación de la veterinaria**',
            'soporte@hopspet.com',
            [vacunacion.mascota.cliente.email],
            fail_silently=False)
        
def send_email_cita_estetica(request):
    tomorrow_date = datetime.today() + timedelta(days=1)
    today_date = datetime.today()
    element = FacturaEstetica.objects.filter(email_sent =False,fecha__year=today_date.year,fecha__month=today_date.month, fecha__date__gte= today_date.date(), fecha__date__lte= tomorrow_date.date())
    for estetica in element:
        estetica.email_sent = True
        estetica.save()
        veterinaria_tenat = Veterinaria.objects.get(pk='1')
        info_petshop_template = "\n \n Coordialmente, \n \n {0} \r Dirección: {1} \r Telefono: {2} \r Celular: {3} \r\r"
        info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)
        cita_temp= estetica.fecha - timedelta(hours = 5)

        mensaje = "Hola " + estetica.mascota.cliente.nombres + ", te recordamos que tu mascota " + estetica.mascota.nombre + " tiene cita para estetica programada para el dia " + str(
            cita_temp.strftime("%d/%m/%Y a las %H:%M"))

        if estetica.mascota.cliente.notificaciones_wpp:
            notificar_por_whatsapp(estetica.mascota.cliente.nombres,estetica.mascota.nombre,'cita para estetica',str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")), estetica.mascota.cliente.celular)
        else:
            send_mail('No Responder: Recordatorio Cita Estetica || ' + veterinaria_tenat.nombre ,
            'Estimada/o '+ estetica.mascota.cliente.nombres +',\n'+
            ' \n Te recordamos que tu mascota ' + estetica.mascota.nombre +' tiene una cita de estetica para el día ' + str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")) + info_petshop_template
            + '\n\n **Mensaje generado automáticamente, por favor no responder. Para cualquier inquietud lo invitamos a que haga uso de los canales de comunicación de la veterinaria**',
            'soporte@hopspet.com',
            [estetica.mascota.cliente.email],
            fail_silently=False)

def send_email_cita_desparasitacion(request):
    tomorrow_date = datetime.today() + timedelta(days=1)
    today_date = datetime.today()
    element = Desparasitacion.objects.filter(email_sent =False,fecha__year=today_date.year,fecha__month=today_date.month, fecha__date__gte= today_date.date(), fecha__date__lte= tomorrow_date.date())
    for desparasitacion in element:
        desparasitacion.email_sent = True
        desparasitacion.save()
        veterinaria_tenat = Veterinaria.objects.get(pk='1')
        info_petshop_template = "\n \n Coordialmente, \n \n {0} \r Dirección: {1} \r Telefono: {2} \r Celular: {3} \r\r"
        info_petshop_template = info_petshop_template.format(veterinaria_tenat.nombre, veterinaria_tenat.direccion, veterinaria_tenat.telefono, veterinaria_tenat.celular)
        cita_temp= desparasitacion.fecha - timedelta(hours = 5)

        mensaje = "Hola " + desparasitacion.mascota.cliente.nombres + ", te recordamos que tu mascota " + desparasitacion.mascota.nombre + " tiene cita para desparasitacion programada para el dia " + str(
            cita_temp.strftime("%d/%m/%Y a las %H:%M"))

        if desparasitacion.mascota.cliente.notificaciones_wpp:
            notificar_por_whatsapp(desparasitacion.mascota.cliente.nombres,desparasitacion.mascota.nombre,'cita para desparasitacion',str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")), desparasitacion.mascota.cliente.celular)
        else:
            send_mail('No Responder: Recordatorio Desparasitacion || ' + veterinaria_tenat.nombre ,
            'Estimada/o '+ desparasitacion.mascota.cliente.nombres +
            ', \n \n Te recordamos que tu mascota ' + desparasitacion.mascota.nombre +' tiene una cita de desparasitación para el día ' + str(cita_temp.strftime("%d/%m/%Y a las %I:%M%p")) + info_petshop_template
            + '\n\n **Mensaje generado automáticamente, por favor no responder. Para cualquier inquietud lo invitamos a que haga uso de los canales de comunicación de la veterinaria**',
            'soporte@hopspet.com',
            [desparasitacion.mascota.cliente.email],
            fail_silently=False)

def all_send_email():
    notification_email_citas()
    notification_email_estetica()
    notification_email_desparasitacion()
    notification_email_birthday()
    notification_email_vacunacion()


def fix_roles_user():
    # this function asigned the value to field usuario's roles
    veterinarios = Veterinario.objects.all()

    for venterinario in veterinarios:
        object_user = venterinario
        object_user.roles = "Veterinario"
        object_user.save()

    administradores = Administrador.objects.all()
    for administrador in administradores:
        object_user = administrador
        object_user.roles = "Administrador"
        object_user.save()

    asistentes = Asistente.objects.all()
    for asistente in asistentes:
        object_user = asistente
        object_user.roles = "Asistente"
        object_user.save()


def inactivar_usuario(request):
    id_usuario = request.GET.get('id', '')
    mi_usuario = get_object_or_404(Usuario, pk=id_usuario)
    if mi_usuario.is_active:
        mi_usuario.is_active = False
        messages.success(request, 'Usuario inactivado con exito!')
    else:
        mi_usuario.is_active = True
        messages.success(request, 'Usuario activado con exito!')
    mi_usuario.save()
    data = {
        'modificacion': True
    }
    return JsonResponse(data)

""" def terminosCondiciones(request,id_user):
    veterinaria = Veterinaria.objects.all()
    datos = Tenant.objects.filter(nombre_veterinaria=veterinaria[0].nombre)
    datos = datos[0]

    return render(request, 'terminos_condiciones.html', {
        'datos': datos,
        'id_user': id_user,
    })

def terminosCondicionesOtherUser(request):
    user = Administrador.objects.all()
    
    return render(request, 'terminos_condiciones_other_user.html', {
        'datos': user,
    }) """
""" 
def aceptarTermCond(request):
    id_user = request.GET.get('id_user', '')
    observaciones = request.GET.get('observaciones', '')

    veterinaria = Veterinaria.objects.all()
    datos = Tenant.objects.filter(nombre_veterinaria=veterinaria[0].nombre)
    fecha_actual = datetime.now()

    term_cond = TerminosCondiciones.objects.create(fecha_registro=fecha_actual,revisado=True,cliente_id=id_user,veterinaria=datos[0],observaciones=observaciones)
    term_cond.save()

    data = {
        'actualizado': True
    }
    return JsonResponse(data) """