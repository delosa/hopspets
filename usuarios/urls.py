from django.urls import path
from .views import *
from .forms import IniciarSesionTenantForm
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('iniciar_sesion/', auth_views.LoginView.as_view(template_name='./loginTenant.html', form_class=IniciarSesionTenantForm), name="iniciar_sesion"),
    path('salir/', auth_views.LogoutView.as_view(), {'next_page': 'iniciar_sesion'}, name="cerrar_sesion"),
    path('index/', Index.as_view(), name="index"),
    path('crear_administrador/', CrearAdministrador.as_view(), name="crear_administrador"),
    path('crear_veterinario/', CrearVeterinario.as_view(), name="crear_veterinario"),
    path('crear_asistente/', CrearAsistente.as_view(), name="crear_asistente"),
    path('editar_administrador/<int:pk>/', EditarAdministrador.as_view(), name="editar_administrador"),
    path('editar_veterinario/<int:pk>/', EditarVeterinario.as_view(), name="editar_veterinario"),
    path('editar_asistente/<int:pk>/', EditarAsistente.as_view(), name="editar_asistente"),
    path('listar_administradores/', ListarAdministradores.as_view(), name="listar_administradores"),
    path('listar_veterinarios/', ListarVeterinarios.as_view(), name="listar_veterinarios"),
    path('listar_asistentes/', ListarAsistentes.as_view(), name="listar_asistentes"),
    path('visualizar_administrador/<int:pk>/', VisualizarAdministrador.as_view(), name="visualizar_administrador"),
    path('visualizar_veterinario/<int:pk>/', VisualizarVeterinario.as_view(), name="visualizar_veterinario"),
    path('visualizar_asistente/<int:pk>/', VisualizarAsistente.as_view(), name="visualizar_asistente"),
    path('cambiar_contrasena/', CambiarControsena.change_password, name="cambiar_contrasena"),
    #path('terminos_condiciones/<int:id_user>/',terminosCondiciones, name="terminos_condiciones"),
    #path('terminos_condiciones_other_user/',terminosCondicionesOtherUser, name="terminos_condiciones_other_user"),
    #path('aceptar_terminos_condiciones/', aceptarTermCond, name="aceptar_terminos_condiciones"),
    
    # Password reset views
    path('reiniciar_contrasena/', auth_views.PasswordResetView.as_view(template_name='password_change_form.html',email_template_name='password_reset_html_email.html', subject_template_name = 'password_reset_subject.txt' ), name="reiniciar_contrasena"),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='password_change_form.html', email_template_name='password_reset_html_email.html' ), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='password_reset_done'),
    path('password_reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'), name='password_reset_confirm'),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
    path('ajax/notificaciones/', notificaciones, name="notificaciones"),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
    path('inactivar_usuario/', inactivar_usuario, name="inactivar_usuario"),
]
