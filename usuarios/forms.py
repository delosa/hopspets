# -*- coding: utf-8 -*-
from .models import *
from django import forms
from django.contrib.auth.forms import *


class IniciarSesionTenantForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(IniciarSesionTenantForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = 'Usuario'
        self.fields['password'].label = 'Contraseña'

    class Meta:
        model = Usuario

        username = forms.CharField(label="Usuario", max_length=100,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
        password = forms.CharField(label="Contraseña", max_length=100,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password'}))


class CrearAdministradorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearAdministradorForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['email'].label = 'Correo Electrónico'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['segundo_nombre'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['email'].required = True
        self.fields['foto'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Administrador
        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'email',
            'telefono',
            'celular',
            'sexo',
            'direccion',
            'foto'

        )

        widgets = {
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'email': forms.EmailInput(),
            'foto': forms.FileInput(),
            'direccion': forms.TextInput(attrs={'max_length': '100'})

        }

    def clean(self):

        cleaned_data = super(CrearAdministradorForm, self).clean()
        correo = cleaned_data.get("email")

        numero_documento = cleaned_data.get("numero_documento_identificacion")

        if User.objects.filter(username=correo):
            self._errors['email'] = [
                'Ya existe un usuario con este correo electrónico']
        elif numero_documento != "" and Administrador.objects.filter(numero_documento_identificacion=numero_documento):
            self._errors['numero_documento_identificacion'] = [
                'Ya existe otro usuario con este numero de documento']
        else:
            return cleaned_data


class CrearVeterinarioForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearVeterinarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['email'].label = 'Correo Electrónico'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['segundo_nombre'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['email'].required = True
        self.fields['foto'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Veterinario
        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'email',
            'telefono',
            'celular',
            'sexo',
            'direccion',
            'especializacion',
            'no_tarjeta',
            'foto'

        )

        widgets = {
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'especializacion': forms.TextInput(attrs={'max_length': '100'}),
            'no_tarjeta': forms.TextInput(attrs={'max_length': '100'}),
            'email': forms.EmailInput(),
            'foto': forms.FileInput(),
            'direccion': forms.TextInput(attrs={'max_length': '100'})

        }

    def clean(self):

        cleaned_data = super(CrearVeterinarioForm, self).clean()
        correo = cleaned_data.get("email")
        numero_documento = cleaned_data.get("numero_documento_identificacion")

        if User.objects.filter(username=correo):
            self._errors['email'] = [
                'Ya existe un usuario con este correo electrónico']
        elif numero_documento != "" and Veterinario.objects.filter(numero_documento_identificacion=numero_documento):
            self._errors['numero_documento_identificacion'] = [
                'Ya existe otro usuario con este número de documento']
        else:
            return cleaned_data


class CrearAsistenteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearAsistenteForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['email'].label = 'Correo Electrónico'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['segundo_nombre'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['email'].required = True
        self.fields['foto'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Asistente
        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'email',
            'telefono',
            'celular',
            'sexo',
            'direccion',
            'foto'

        )

        widgets = {
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'email': forms.EmailInput(),
            'foto': forms.FileInput(),

        }

    def clean(self):

        print('holaaa')

        cleaned_data = super(CrearAsistenteForm, self).clean()
        correo = cleaned_data.get("email")

        numero_documento = cleaned_data.get("numero_documento_identificacion")

        if User.objects.filter(username=correo):
            self._errors['email'] = [
                'Ya existe un usuario con este correo electrónico']
        elif numero_documento != "" and Asistente.objects.filter(numero_documento_identificacion=numero_documento):
            self._errors['numero_documento_identificacion'] = [
                'Ya existe otro usuario con este numero de documento']
        else:
            return cleaned_data


class ModificarAdministradorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarAdministradorForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['numero_documento_identificacion'].label = 'No. Documento'
        self.fields['segundo_apellido'].required = False
        self.fields['segundo_nombre'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Administrador

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'foto',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto': forms.FileInput(),
        }


class ModificarVeterinarioForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarVeterinarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['numero_documento_identificacion'].label = 'No. Documento'
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['segundo_nombre'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Veterinario

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'especializacion',
            'no_tarjeta',
            'foto',
            'email',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'no_tarjeta': forms.TextInput(attrs={'max_length': '100'}),
            'especializacion': forms.TextInput(attrs={'max_length': '100'}),
            'foto': forms.FileInput(),
        }


class ModificarAsistenteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarAsistenteForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['numero_documento_identificacion'].label = 'No. Documento'
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['segundo_nombre'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Asistente

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'foto',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto': forms.FileInput(),
        }