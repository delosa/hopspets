from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from citas.models import *


class CrearTratamiento(SuccessMessageMixin, CreateView):
    model = Tratamiento
    form_class = CrearTratamientoForm
    template_name = "registrar_tratamiento.html"
    success_url = reverse_lazy('listar_tratamientos')
    success_message = "¡El medicamento fue agregado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearTratamiento, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        tratamiento = form.instance
        self.object = form.save()
        return super(CrearTratamiento, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearTratamiento, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_tratamientos'] = True
        return context


class ListarTratamientos(ListView):
    model = Tratamiento
    template_name = "listar_tratamientos.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarTratamientos, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(ListarTratamientos, self).get_queryset()
        return queryset.order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(ListarTratamientos, self).get_context_data(**kwargs)
        context['menu_tratamientos'] = True
        return context

class ListarMotivosCitas(ListView):
    model = MotivoCita
    template_name = "listar_motivo_cita.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarMotivosCitas, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(ListarMotivosCitas, self).get_queryset()
        return queryset.order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(ListarMotivosCitas, self).get_context_data(**kwargs)
        context['menu_tipos_citas'] = True
        return context

class EditarTratamiento(SuccessMessageMixin, UpdateView):
    model = Tratamiento
    form_class = CrearTratamientoForm
    template_name = "registrar_tratamiento.html"
    success_url = reverse_lazy('listar_tratamientos')
    success_message = "¡El tratamiento se modificó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarTratamiento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarTratamiento, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_tratamientos'] = True
        return context

class EditarMotivosCitas(SuccessMessageMixin, UpdateView):
    model = MotivoCita
    form_class = ModificarMotivoForm
    template_name = "editar_motivo_cita.html"
    success_url = reverse_lazy('listar_motivos_cita')
    success_message = "¡El motivo se modificó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarMotivosCitas, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarMotivosCitas, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_tipos_citas'] = True
        return context
