from .models import *
from django import forms
from citas.models import *


class CrearTratamientoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearTratamientoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['precio'].required = False

    class Meta:
        model = Tratamiento
        fields = (
            'nombre',
            'precio',
            'descripcion'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '5'}),
            'precio': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '})
        }

class ModificarMotivoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarMotivoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['precio'].required = False
        self.fields['nombre'].widget.attrs['readonly'] = True

    class Meta:
        model = MotivoCita
        fields = (
            'nombre',
            'precio',
            'descripcion'
        )
        

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '5'}),
            'precio': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '0'}),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '})
        }

