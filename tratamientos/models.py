from django.db import models

class Tratamiento(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', unique=True, error_messages={'unique': "Ya existe un tratamiento con este nombre."})
    descripcion = models.CharField(max_length=2000, verbose_name='Descripción')
    precio = models.CharField(max_length=50, verbose_name='Precio', null=True, blank=True)

    def __str__(self):
        return self.nombre
