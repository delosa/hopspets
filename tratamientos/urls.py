from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_tratamiento/', CrearTratamiento.as_view(), name="registrar_tratamiento"),
    path('listar_tratamientos/', ListarTratamientos.as_view(), name="listar_tratamientos"),
    path('editar_tratamiento/<int:pk>/', EditarTratamiento.as_view(), name="editar_tratamiento"),
    path('listar_motivos_cita/', ListarMotivosCitas.as_view(), name="listar_motivos_cita"),
    path('editar_motivos_citas/<int:pk>/', EditarMotivosCitas.as_view(), name="editar_motivos_citas"),
]