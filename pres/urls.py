from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from .forms import *

urlpatterns = [
	path('listar_pres/<str:fecha_filtro>/', ListarPRES.as_view(), name="listar_pres"),
	path('listar_pres_mes/<str:mes>/<str:ano>/', ListarPRESMes.as_view(), name="listar_pres_mes"),
	path('agregar_pres/<int:id_user>/', AgregarPRES.as_view(), name="agregar_pres"),
	path('crear_solucion/<int:id_pres>/<int:id_user>/', CrearSolucion.as_view(), name="crear_solucion"),
	path('editar_solucion/<int:pk>/<int:id_pres>/', EditarSolucion.as_view(), name="editar_solucion"),
	path('detalle_pres_sol/<int:id_pres>/', DetallePresSol, name='detalle_pres_sol'),
	path('detalle_pres_sol_notificacion/<int:id_pres>/', DetallePresSolNotificacion, name='detalle_pres_sol_notificacion'),
]