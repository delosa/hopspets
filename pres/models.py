# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from usuariosPublicos.models import UsuarioPublico
from veterinarias.models import Tenant
from django.core.validators import RegexValidator
from sorl.thumbnail import ImageField, get_thumbnail
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re

def get_upload_to(instance, filename):
    return 'upload/%s/%s' % ('PRES', filename)

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1] 
    valid_extensions = ['.zip','.tar','.rar' ,'.pdf','.PDF','.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')

TIPOS_PRES = (('Pregunta','Pregunta'),('Reporte de Error','Reporte de Error'),('Sugerencia','Sugerencia'))
ESTADO = (('Pendiente','Pendiente'),('Gestionando','Gestionando'),('Resuelto','Resuelto'))

class PRES(models.Model):
    veterinaria = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    tipo_pres = models.CharField(max_length=50, verbose_name="Tipo de PRES",choices=TIPOS_PRES)
    fecha_registro = models.DateField(verbose_name="Fecha de Registro")
    creacion_tenants = models.ForeignKey(UsuarioPublico, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Creacion de Pres en Tenant")
    descripcion = models.CharField(max_length=2000, verbose_name='Descripcion', null=True, blank=True)
    nombre_cliente = models.CharField(max_length=100, verbose_name='Nombre Cliente', null=True, blank=True)
    cliente_id = models.IntegerField(default=0)
    correo_cliente = models.CharField(max_length=100, null=True, blank=True)
    adjunto_pres = models.FileField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)

    def archivo_pres(self):
        if len(str(self.adjunto_pres).split('/')) == 2:
            return str(self.adjunto_pres).split('/')[1]
        elif len(str(self.adjunto_pres).split('/')) == 1:
            return str(self.adjunto_pres).split('/')[0]
        else:
            return str(self.adjunto_pres)
   
class SolucionPRES(models.Model):
    pres = models.ForeignKey(PRES, on_delete=models.CASCADE)
    fecha_resolucion = models.DateField(verbose_name="Fecha de Resolucion")
    responsable_solucion = models.ForeignKey(UsuarioPublico, on_delete=models.CASCADE)
    estado = models.CharField(max_length=100, verbose_name="Estado del Tramite",choices=ESTADO, default='Pendiente')
    solucion = models.CharField(max_length=2000, verbose_name='Solucion', null=True, blank=True)
    adjunto_solucion = models.FileField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    revisado = models.BooleanField(default=False)

    def archivo_solucion(self):
        if len(str(self.adjunto_solucion).split('/')) == 2:
            return str(self.adjunto_solucion).split('/')[1]
        elif len(str(self.adjunto_solucion).split('/')) == 1:
            return str(self.adjunto_solucion).split('/')[0]
        else:
            return str(self.adjunto_solucion)
