from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404

class CrearPRESForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearPRESForm, self).__init__(*args, **kwargs)
        self.fields['fecha_registro'].initial = datetime.now()

    class Meta:
        model = PRES
        fields = (
            'veterinaria',
            'tipo_pres',
            'fecha_registro',
            'descripcion',
            'adjunto_pres',
        )

        widgets = {
            'descripcion': forms.Textarea(),
            'fecha_registro': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'adjunto_pres': forms.FileInput()
        }

class CrearSolucionPRESForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearSolucionPRESForm, self).__init__(*args, **kwargs)

    class Meta:
        model = SolucionPRES
        fields = (
            'estado',
            'solucion',
            'adjunto_solucion',
        )

        widgets = {
            'solucion': forms.Textarea(),
            'adjunto_solucion': forms.FileInput()
        }

class EditarSolucionPRESForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditarSolucionPRESForm, self).__init__(*args, **kwargs)

    class Meta:
        model = SolucionPRES
        fields = (
            'estado',
            'solucion',
            'adjunto_solucion',
        )

        widgets = {
            'solucion': forms.Textarea(),
            'adjunto_solucion': forms.FileInput()
        }