from django.shortcuts import render,get_object_or_404,redirect,render_to_response
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from django.core.mail import send_mail
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth.models import User
from usuariosPublicos.models import UsuarioPublico
from usuarios.models import *
from django.db import connection

class ListarPRES(ListView):
	model = PRES
	template_name = "listar_pres.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(ListarPRES,self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ListarPRES, self).get_context_data(**kwargs)
		context['pres'] = True
		pres = PRES.objects.all()
		soluciones = SolucionPRES.objects.all()

		hoy = datetime.now().date()

		if self.kwargs['fecha_filtro'] == 'hoy':
			pres = pres.filter(fecha_registro=hoy)
		elif self.kwargs['fecha_filtro'] == 'semana':
			esta_semana = hoy.isocalendar()[1]
			pres = pres.filter(fecha_registro__week=esta_semana, fecha_registro__year=hoy.year)
		elif self.kwargs['fecha_filtro'] == 'mes':
			pres = pres.filter(fecha_registro__month=hoy.month, fecha_registro__year=hoy.year)
		else:
			pres = PRES.objects.all()

		datatemplate = []
		for dato in pres:
			presCliente = {}
			presCliente['id'] = dato.id
			presCliente['fecha_registro'] = dato.fecha_registro
			presCliente['creacion_tenants'] = dato.creacion_tenants
			presCliente['nombre_cliente'] = dato.nombre_cliente
			presCliente['tipo_pres'] = dato.tipo_pres
			presCliente['veterinaria'] = dato.veterinaria
			for solucion in soluciones:
				if solucion.pres.id == dato.id:
					presCliente['id_sol'] = solucion.id
					presCliente['estado'] = solucion.estado
					presCliente['fecha_resolucion'] = solucion.fecha_resolucion
					presCliente['responsable_solucion'] = solucion.responsable_solucion
			datatemplate.append(presCliente)

		context['pres'] = datatemplate
		context['fecha_hoy'] = hoy
		context['fecha_filtro'] = self.kwargs['fecha_filtro']

		return context
		
class AgregarPRES(SuccessMessageMixin, CreateView):
	model = PRES 
	form_class = CrearPRESForm
	template_name = "agregar_pres.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(AgregarPRES, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return '/pres/listar_pres/semana'

	def form_valid(self, form):
		pres = form.instance
		pres.creacion_tenants = get_object_or_404(UsuarioPublico, id=self.kwargs['id_user'])
		self.object = form.save()
		messages.success(self.request, "Se ha guardado su PRES exitosamente")
		if form.is_valid():
			send_mail('Nuevo PRES en el Administrador de Tenant en HopsPets', 
					'Se registro un nuevo PRES con las siguientes caracteristicas: ' + 
					'\n \n Nro de Ticket: ' + str(pres.tipo_pres[0]) + '0000' + str(pres.id) + 
					'\n Fecha de Registro: ' + str(pres.fecha_registro) + 
					'\n Tipo de PRES: ' + str(pres.tipo_pres) +
					'\n Nombre de Veterinaria: ' + str(pres.veterinaria) + 
					'\n Creado Por: ' + str(pres.creacion_tenants) +
					'\n Descripcion del PRES: ' + str(pres.descripcion) + 
					'\n Estado: Pendiente' +
					'\n \n  URL: http://hopspet.com/pres/listar_pres/semana/ \n', 
					'soporte@hopspet.com',
					['cto@hops.com.co'],
					fail_silently=False)
		return super(AgregarPRES, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, "Error al registrar su pres, por favor revise los datos")
		return super(AgregarPRES, self).form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super(AgregarPRES, self).get_context_data(**kwargs)
		context['pres'] = True
		return context

class CrearSolucion(SuccessMessageMixin, CreateView):
	models = SolucionPRES
	form_class = CrearSolucionPRESForm
	template_name = "agregar_solucion.html"
	success_message = "¡Se actualizo la solucion con exito!"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(CrearSolucion, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return '/pres/listar_pres/semana/'

	def form_valid(self, form):
		sol_pres = form.instance
		sol_pres.pres = get_object_or_404(PRES, id=self.kwargs['id_pres'])
		sol_pres.fecha_resolucion = datetime.now()
		sol_pres.responsable_solucion = get_object_or_404(UsuarioPublico, id=self.kwargs['id_user'])
		self.object = form.save()

		if form.is_valid():
			if sol_pres.estado == 'Resuelto':
				if sol_pres.pres.creacion_tenants: 
					creador = sol_pres.pres.creacion_tenants
					if creador.roles == "Comerciales":
						send_mail('Su Ticket en Gestion ya fue solucionado!!',
								'El ticket ' + str(sol_pres.pres.tipo_pres[0]) + '000' + str(sol_pres.pres.id) +
								'\n \n Nombre de Veterinaria: ' + str(sol_pres.pres.veterinaria.nombre_veterinaria) +
								'\n Problema: ' + str(sol_pres.pres.descripcion) +
								'\n Solucion: ' + str(sol_pres.solucion) +
								'\n Responsable de la Solucion: ' + str(sol_pres.responsable_solucion) +
								'\n \n  URL: http://hopspet.com/pres/listar_pres/semana/ \n', 
								'soporte@hopspet.com',
								[creador.email],
								fail_silently=False)
				else:
					if sol_pres.pres.nombre_cliente:
						send_mail('Su Ticket ya fue solucionado!!',
								'El ticket que genero: ' + str(sol_pres.pres.tipo_pres[0]) + '000' + str(sol_pres.pres.id) +
								'\n \n Fecha de Registro: ' + str(sol_pres.pres.fecha_registro) +
								'\n Problema: ' + str(sol_pres.pres.descripcion) +
								'\n \n Fecha de Resolucion: ' + str(sol_pres.fecha_resolucion) +
								'\n Solucion: ' + str(sol_pres.solucion) +
								'\n Responsable de la Solucion: ' + str(sol_pres.responsable_solucion) + ' - Soporte' +
								'\n \n Ver Detalle de Solucion' +
								'\n URL: http://'+ str(sol_pres.pres.veterinaria) +'.hopspet.com/veterinaria/pres_solucion/semana/ \n', 
								'soporte@hopspet.com',
								[sol_pres.pres.correo_cliente],
								fail_silently=False)
		return super(CrearSolucion, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, "Error al registrar su pres, por favor revise los datos")
		return super(CrearSolucion, self).form_invalid(form)

	def get_context_data(self, **kwargs):
		id_pres = self.kwargs['id_pres']

		context = super(CrearSolucion, self).get_context_data(**kwargs)
		context['modificar'] = False
		context['pres'] = True
		context['pres_sol'] = get_object_or_404(PRES, id=id_pres)

		return context

class EditarSolucion(SuccessMessageMixin, UpdateView):
	model = SolucionPRES
	form_class = EditarSolucionPRESForm
	template_name = "agregar_solucion.html"
	success_message = "Se Edito su Solucion con exito!"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(EditarSolucion, self).dispatch(request, *args, **kwargs)
	
	def get_success_url(self):
		return '/pres/listar_pres/semana/'

	def form_valid(self, form):
		sol_pres = form.instance
		sol_pres.fecha_resolucion = datetime.now()
		self.object = form.save()

		if form.is_valid():
			if sol_pres.estado == 'Resuelto':
				if sol_pres.pres.creacion_tenants: 
					creador = sol_pres.pres.creacion_tenants
					if creador.roles == "Comerciales":
						send_mail('Su Ticket en Gestion ya fue solucionado!!',
								'El ticket ' + str(sol_pres.pres.tipo_pres[0]) + '000' + str(sol_pres.pres.id) +
								'\n \n Nombre de Veterinaria: ' + str(sol_pres.pres.veterinaria.nombre_veterinaria) +
								'\n Problema: ' + str(sol_pres.pres.descripcion) +
								'\n Solucion: ' + str(sol_pres.solucion) +
								'\n Responsable de la Solucion: ' + str(sol_pres.responsable_solucion) +
								'\n \n  URL: http://hopspet.com/pres/listar_pres/semana/ \n', 
								'soporte@hopspet.com',
								[creador.email],
								fail_silently=False)
				else:
					if sol_pres.pres.nombre_cliente:
						send_mail('Su Ticket ya fue solucionado!!',
								'El ticket que genero: ' + str(sol_pres.pres.tipo_pres[0]) + '000' + str(sol_pres.pres.id) +
								'\n \n Fecha de Registro: ' + str(sol_pres.pres.fecha_registro) +
								'\n Problema: ' + str(sol_pres.pres.descripcion) +
								'\n \n Fecha de Resolucion: ' + str(sol_pres.fecha_resolucion) +
								'\n Solucion: ' + str(sol_pres.solucion) +
								'\n Responsable de la Solucion: ' + str(sol_pres.responsable_solucion) + ' - Soporte' +
								'\n \n Ver Detalle de Solucion' +
								'\n URL: http://'+ str(sol_pres.pres.veterinaria) +'hopspet.com/veterinaria/pres_solucion/semana/ \n', 
								'soporte@hopspet.com',
								[sol_pres.pres.correo_cliente],
								fail_silently=False)
		return super(EditarSolucion, self).form_valid(form)

	def get_context_data(self, **kwargs):
		id_pres = self.kwargs['id_pres']
		context = super(EditarSolucion, self).get_context_data(**kwargs)
		context['modificar'] = True
		context['pres'] = True
		context['fecha'] = datetime.now()
		context['pres_sol'] = get_object_or_404(PRES, id=id_pres)

		return context

def DetallePresSol(request, id_pres):

	pres = PRES.objects.filter(id=id_pres)
	sol_pres = SolucionPRES.objects.filter(pres_id=id_pres)
	return render(request, 'detalle_pres_sol.html',{'pres': pres, 'sol_pres': sol_pres})

def DetallePresSolNotificacion(request, id_pres):

	pres = PRES.objects.filter(id=id_pres)
	sol_pres = SolucionPRES.objects.filter(pres_id=id_pres)
	return render(request, 'detalle_pres_sol_notificacion.html',{'pres': pres, 'sol_pres': sol_pres})

class ListarPRESMes(ListView):
	model = PRES
	template_name = "listar_pres.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(ListarPRESMes, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ListarPRESMes, self).get_context_data(**kwargs)
		context['pres'] = True
		pres = PRES.objects.all()
		soluciones = SolucionPRES.objects.all()

		mes = self.kwargs['mes']
		ano = self.kwargs['ano']

		pres = pres.filter(fecha_registro__month=mes, fecha_registro__year=ano)

		datatemplate = []
		for dato in pres:
			presCliente = {}
			presCliente['id'] = dato.id
			presCliente['fecha_registro'] = dato.fecha_registro
			presCliente['creacion_tenants'] = dato.creacion_tenants
			presCliente['nombre_cliente'] = dato.nombre_cliente
			presCliente['tipo_pres'] = dato.tipo_pres
			presCliente['veterinaria'] = dato.veterinaria
			for solucion in soluciones:
				if solucion.pres.id == dato.id:
					presCliente['estado'] = solucion.estado
					presCliente['fecha_resolucion'] = solucion.fecha_resolucion
					presCliente['responsable_solucion'] = solucion.responsable_solucion
			datatemplate.append(presCliente)

		context['pres'] = datatemplate
		context['fecha_filtro'] = 'mes'
		context['mes'] = mes
		context['ano'] = ano

		return context