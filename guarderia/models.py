from django.db import models
from mascotas.models import Mascota
from django.core.validators import RegexValidator
from django.contrib.auth.models import Group
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re
from usuarios.models import Usuario

ESTADOS_GUARDERIA = (('Activa','Activa'),('Pagada','Pagada'))
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores númericos')

def get_upload_to_ingreso(instance, filename):
    return 'upload/%s/%s' % (instance.numero_documento_identificacion_ingreso, filename)

def get_upload_to_retiro(instance, filename):
    return 'upload/%s/%s' % (instance.numero_documento_identificacion_retiro, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')


class Guarderia(models.Model):
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    observaciones = models.CharField(max_length=2000, verbose_name='Observaciones')
    estado = models.CharField(max_length=20, verbose_name="Estado", choices=ESTADOS_GUARDERIA, default='Activa')

    def __str__(self):
        return self.mascota.nombre + " - " + str(self.fecha_inicio.strftime('%b. %d, %Y, %H:%M %p'))

class DatosGuarderia(models.Model):
    guarderia = models.ForeignKey(Guarderia, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=2000, verbose_name='Descripción')
    representante_ingreso = models.CharField(max_length=100, verbose_name="Representante Ingreso")
    numero_documento_identificacion_ingreso = models.CharField(max_length=20,verbose_name="Número de Documento Ingreso",validators=[numeric])
    foto_ingreso = models.ImageField(upload_to=get_upload_to_ingreso, validators=[validate_file_extension], null=True, blank=True)
    representante_retiro = models.CharField(max_length=100, verbose_name="Representante Retiro")
    numero_documento_identificacion_retiro = models.CharField(max_length=20,verbose_name="Número de Documento Retiro",validators=[numeric])
    foto_retiro = models.ImageField(upload_to=get_upload_to_retiro, validators=[validate_file_extension], null=True, blank=True)
    descripcion_salida = models.CharField(max_length=2000, verbose_name='Descripción de Salida', null=True, blank=True)
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '%s (%s)' % (self.descripcion, self.guarderia)