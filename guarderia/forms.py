from .models import *
from django import forms
from datetime import datetime


class CrearGuarderiaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearGuarderiaForm, self).__init__(*args, **kwargs)
        self.fields['fecha_inicio'].initial = datetime.now()
        self.fields['observaciones'].required = False

    class Meta:
        model = Guarderia
        fields = (
            'observaciones',
            'fecha_inicio',
            'fecha_fin',
        )

        widgets = {
            'observaciones': forms.Textarea(attrs={'rows': '10', 'placeholder': ''}),
            'fecha_inicio': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'fecha_fin': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }

class CrearGuarderiaFormAgenda(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearGuarderiaFormAgenda, self).__init__(*args, **kwargs)
        #self.fields['mascota'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['observaciones'].required = False

    class Meta:
        model = Guarderia
        fields = (
            'mascota',
            'observaciones',
            'fecha_inicio',
            'fecha_fin',
        )

        widgets = {
            'observaciones': forms.Textarea(attrs={'rows': '10', 'placeholder': ''}),
            'fecha_inicio': forms.DateTimeInput(attrs={'class': 'datetime-input','id':'firstdate'}),
            'fecha_fin': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }

class CrearDatosGuarderia(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearDatosGuarderia, self).__init__(*args, **kwargs)
        self.fields['descripcion'].label = 'Descripción de la Mascota en la Guardería'
        self.fields['representante_ingreso'].label = 'Nombre del Representante de Ingreso'
        self.fields['numero_documento_identificacion_ingreso'].label = 'Número de Identificación R. I.'
        self.fields['foto_ingreso'].label = 'Foto de Ingreso'
        self.fields['descripcion'].required = True
        self.fields['representante_ingreso']. required = True
        self.fields['numero_documento_identificacion_ingreso'].required = True
        self.fields['foto_ingreso']. required = False
        self.fields['representante_retiro'].label = 'Nombre del Representante de Retiro'
        self.fields['numero_documento_identificacion_retiro'].label = 'Número de Identificación R. R.'
        self.fields['foto_retiro'].label = 'Foto de Retiro'
        self.fields['representante_retiro']. required = True
        self.fields['numero_documento_identificacion_retiro'].required = True
        self.fields['foto_retiro']. required = False

    class Meta:
        model = DatosGuarderia
        fields = (
            'descripcion',
            'representante_ingreso',
            'numero_documento_identificacion_ingreso',
            'foto_ingreso',
            'representante_retiro',
            'numero_documento_identificacion_retiro',
            'foto_retiro',
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '10', 'placeholder': ''}),
            'numero_documento_identificacion_ingreso': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto_ingreso': forms.FileInput(),
            'numero_documento_identificacion_retiro': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto_retiro': forms.FileInput(),
        }

class CrearIngresoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearIngresoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].label = 'Descripción de la Mascota al Ingresar en la Guardería'
        self.fields['representante_ingreso'].label = 'Nombre del Representante de Ingreso'
        self.fields['numero_documento_identificacion_ingreso'].label = 'Número de Identificación R. I.'
        self.fields['foto_ingreso'].label = 'Foto de Ingreso'
        self.fields['descripcion'].required = True
        self.fields['representante_ingreso']. required = True
        self.fields['numero_documento_identificacion_ingreso'].required = True
        self.fields['foto_ingreso']. required = False

    class Meta:
        model = DatosGuarderia
        fields = (
            'representante_ingreso',
            'numero_documento_identificacion_ingreso',
            'descripcion',
            'foto_ingreso',
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '10', 'placeholder': ''}),
            'numero_documento_identificacion_ingreso': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto_ingreso': forms.FileInput(),
        }

""" class CrearRetiroForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearRetiroForm, self).__init__(*args, **kwargs)
        self.fields['representante_retiro'].label = 'Nombre del Representante de Retiro'
        self.fields['numero_documento_identificacion_retiro'].label = 'Número de Identificación R. R.'
        self.fields['foto_retiro'].label = 'Foto de Retiro'
        self.fields['representante_retiro']. required = True
        self.fields['numero_documento_identificacion_retiro'].required = True
        self.fields['foto_retiro']. required = False

    class Meta:
        model = DatosGuarderia
        fields = (
            'representante_retiro',
            'numero_documento_identificacion_retiro',
            'foto_retiro',
        )

        widgets = {
            'numero_documento_identificacion_retiro': forms.NumberInput(attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto_retiro': forms.FileInput(),
        } """