from django.urls import path
from .views import *

urlpatterns = [
    path('programar_guarderia/<int:id_mascota>/', CrearGuarderia.as_view(), name="programar_guarderia"),
    path('editar_guarderia/<int:pk>/', EditarGuarderia.as_view(), name="editar_guarderia"),
    path('eliminar_guarderia/', eliminar_guarderia, name="eliminar_guarderia"),
    path('registrar_cita_guarderia/<str:fecha>/', registrar_cita_guarderia, name="registrar_cita_guarderia"),
    path('editar_guarderia_agenda/<int:pk>/', EditarGuarderiaAgenda.as_view(), name="editar_guarderia_agenda"),
    path('pdf_guarderia/<int:id_mascota>/<str:fecha_inicio>/<str:fecha_fin>/<int:id_guarderia>/', PdfGuarderia.as_view(), name="pdf_guarderia"),
    path('detalle_impresion_guarderia/<int:id_mascota>/<str:fecha_inicio>/<str:fecha_fin>/<int:id_guarderia>/', DetalleImpresionGuarderia.as_view(), name="detalle_impresion_guarderia"),
    path('datos_guarderia/<int:id_guarderia>/', DatoGuarderia.as_view(), name="datos_guarderia"),
    path('registrar_representante_ingreso/<int:id_guarderia>/', RegistrarRepresentanteIngreso.as_view(), name="registrar_representante_ingreso"),
    path('registrar_representante_egreso/<int:id_datosg>/', RegistrarRepresentanteEgreso, name="registrar_representante_egreso"),
    path('act_representante_retiro/', actRepresentanteRetiro, name="act_representante_retiro")
]