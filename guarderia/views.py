from django.views.generic import *
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404,render,redirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from mascotas.models import Mascota
from django.http import JsonResponse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.urls import reverse_lazy
from veteriariaTenant.models import Veterinaria
from usuarios.models import Usuario
from django.contrib.auth.models import User


class CrearGuarderia(SuccessMessageMixin, CreateView):
    model = Guarderia
    form_class = CrearGuarderiaForm
    template_name = "registrar_guarderia.html"
    success_message = "¡Guardería creada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearGuarderia, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_mascota =  get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return '/mascotas/detalle_mascota/' + str(mi_mascota.id)

    def form_valid(self,form):
        guarderia = form.instance
        guarderia.mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])

        self.object = form.save()
        return super(CrearGuarderia, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearGuarderia, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return context


class EditarGuarderia(SuccessMessageMixin, UpdateView):
    model = Guarderia
    form_class = CrearGuarderiaForm
    template_name = "registrar_guarderia.html"
    success_message = "¡Guardería editada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarGuarderia, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_guarderia = get_object_or_404(Guarderia, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(mi_guarderia.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarGuarderia, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_guarderia = get_object_or_404(Guarderia, pk=self.kwargs['pk'])
        context['mascota'] = mi_guarderia.mascota
        return context


class EditarGuarderiaAgenda(SuccessMessageMixin, UpdateView):
    model = Guarderia
    form_class = CrearGuarderiaForm
    template_name = "registrar_guarderia.html"
    success_url = reverse_lazy('listar_citas')
    success_message = "¡Guardería editada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarGuarderiaAgenda, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarGuarderiaAgenda, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_guarderia = get_object_or_404(Guarderia, pk=self.kwargs['pk'])
        context['mascota'] = mi_guarderia.mascota
        return context


def eliminar_guarderia(request):
    id_guarderia = request.GET.get('id', '')
    mi_guarderia = get_object_or_404(Guarderia, pk=id_guarderia)
    id_mascota = mi_guarderia.mascota.id
    if mi_guarderia.estado == 'Activa':
        mi_guarderia.delete()
    data = {
        'eliminacion': True,
        'id_mascota': id_mascota
    }
    return JsonResponse(data)

def registrar_cita_guarderia(request,fecha):
    if request.method == "GET":
        return render(request,'registrar_cita_guarderia_agenda.html',{'citas': True,'fecha':fecha,'form':CrearGuarderiaFormAgenda()})
    if request.method == "POST":
        print("inside POST")
        form_cita =CrearGuarderiaFormAgenda(request.POST)
        form_cita.save()

        return redirect('listar_citas')

class RegistrarRepresentanteIngreso(SuccessMessageMixin, CreateView):
    model = DatosGuarderia
    form_class = CrearIngresoForm
    template_name = 'registrar_representante_ingreso.html'
    success_message = "Ingreso de Guarderia creado con exito"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RegistrarRepresentanteIngreso, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/citas/listar_citas/'

    def form_valid(self, form):
        datos = form.instance
        datos.guarderia = get_object_or_404(Guarderia, pk=self.kwargs['id_guarderia'])
        datos.responsable = get_object_or_404(Usuario, id=self.request.user.id)
        self.object = form.save()
        return super(RegistrarRepresentanteIngreso, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(RegistrarRepresentanteIngreso, self).get_context_data(**kwargs)
        guarderia = get_object_or_404(Guarderia, pk=self.kwargs['id_guarderia'])

        context['modificar'] = False
        context['guarderia'] = guarderia

        return context

class PdfGuarderia(TemplateView):
    template_name = 'pdf_guarderia.html'

    def get_context_data(self, **kwargs):
        context = super(PdfGuarderia, self).get_context_data(**kwargs)
        mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        fecha_inicio = self.kwargs['fecha_inicio']
        fecha_fin = self.kwargs['fecha_fin']
        id_guarderia = self.kwargs['id_guarderia']

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        guarderias = Guarderia.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(fecha_inicio=fecha_inicio).filter(fecha_fin=fecha_fin)
        datos_guarderias = DatosGuarderia.objects.filter(guarderia_id=id_guarderia)

        for guarderia in guarderias:
            context['guarderia'] = guarderia

        for dato in datos_guarderias:
            context['dato'] = dato

        context['fecha_hoy'] = datetime.now().date()
        context['mi_mascota'] = mascota

        return context

class DetalleImpresionGuarderia(TemplateView):
    template_name = 'detalle_impresion_guarderia.html'

    def get_context_data(self, **kwargs):
        context = super(DetalleImpresionGuarderia, self).get_context_data(**kwargs)
        mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        fecha_inicio = self.kwargs['fecha_inicio']
        fecha_fin = self.kwargs['fecha_fin']
        id_guarderia = self.kwargs['id_guarderia']

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        guarderias = Guarderia.objects.filter(mascota_id=self.kwargs['id_mascota']).filter(fecha_inicio=fecha_inicio).filter(fecha_fin=fecha_fin)
        datos_guarderias = DatosGuarderia.objects.filter(guarderia_id=id_guarderia)

        for guarderia in guarderias:
            context['guarderia'] = guarderia

        for dato in datos_guarderias:
            context['dato'] = dato


        context['fecha_hoy'] = datetime.now().date()
        context['mi_mascota'] = mascota

        return context

class DatoGuarderia(SuccessMessageMixin, CreateView):
    model = DatosGuarderia
    form_class = CrearDatosGuarderia
    template_name = "datos_guarderia.html"
    success_message = "¡Datos Guarderia actualizada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DatoGuarderia, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/mascotas/listar_mascotas/'

    def form_valid(self,form):
        guarderia = form.instance
        guarderia.guarderia = get_object_or_404(Guarderia, pk=self.kwargs['id_guarderia'])
        guarderia.responsable = get_object_or_404(Usuario, id=self.request.user.id)
        self.object = form.save()
        return super(DatoGuarderia, self).form_valid(form)

    def get_context_data(self, **kwargs):
        model = DatosGuarderia
        context = super(DatoGuarderia, self).get_context_data(**kwargs)
        guarderia_val = DatosGuarderia.objects.filter(guarderia_id=self.kwargs['id_guarderia'])
        
        if guarderia_val:
            context['agregar'] = False
            context['datos'] = guarderia_val
            
        else:
            context['agregar'] = True
        
        context['guarderia'] = get_object_or_404(Guarderia, pk=self.kwargs['id_guarderia'])

        return context

def RegistrarRepresentanteEgreso(request, id_datosg):

    return render(request, 'registrar_representante_egreso.html', {
        'id_datosg': id_datosg,
    })

def actRepresentanteRetiro(request):
    id_datosg = request.GET.get('id_datosg', '')
    representante = request.GET.get('representante', '')
    identificacion = request.GET.get('identificacion', '')
    descripcion_salida = request.GET.get('descripcion_salida', '')
    foto = request.GET.get('foto', '')

    datos = DatosGuarderia.objects.filter(id=id_datosg)
    #datos = datos[0]
    for dato in datos:
        dato.representante_retiro = representante
        dato.numero_documento_identificacion_retiro = identificacion
        dato.descripcion_salida = descripcion_salida
        dato.foto_retiro = foto
        dato.save()

    #term_cond = TerminosCondiciones.objects.create(fecha_registro=fecha_actual,revisado=True,cliente_id=id_user,veterinaria=datos[0],observaciones=observaciones)
    #term_cond.save()

    data = {
        'actualizado': True
    }
    return JsonResponse(data)