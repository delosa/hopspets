from django.apps import AppConfig


class GuarderiaConfig(AppConfig):
    name = 'guarderia'
