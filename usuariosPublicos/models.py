# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.contrib.auth.models import Group
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re


alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Unicamente se aceptan caracteres alfanumericos.')
alpha = RegexValidator(r'^[a-zA-Z]*$', 'Unicamente se aceptan caracteres alfanumericos.')
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores númericos')


# Create your models here.


class UsuarioPublico(User):
    IDENTIFICACION_SEXO = (('MASCULINO', 'Masculino'), ('FEMENINO', 'Femenino'))
    IDENTIFICACION_CHOICES = (
        ('CC', 'Cédula de ciudadanía'), ('PAS', 'Pasaporte'))

    tipo_documento_identificacion = models.CharField(max_length=20, verbose_name="tipo de identificación",
                                                     choices=IDENTIFICACION_CHOICES)
    numero_documento_identificacion = models.CharField(max_length=20,
                                                       verbose_name="número de documento de identificación",
                                                       validators=[numeric])
    telefono = models.CharField(max_length=20, verbose_name="número telefónico", validators=[numeric])
    celular = models.CharField(max_length=20, verbose_name="número telefónico", validators=[numeric])
    segundo_nombre = models.CharField(max_length=100, verbose_name="segundo nombre", blank=True, validators=[alpha])
    segundo_apellido = models.CharField(max_length=100, verbose_name="primer apellido", validators=[alpha])
    sexo = models.CharField(max_length=20, verbose_name="Sexo", choices=IDENTIFICACION_SEXO, null=True, blank=True)
    direccion = models.CharField(max_length=100, verbose_name="Dirección")
    fecha_nacimiento = models.DateField()
    foto = models.ImageField(upload_to=None, null=True, blank=True)
    roles = models.CharField(max_length=100, verbose_name="roles", null=True, blank=True, default="Administrador")

    def save(self, *args, **kwargs):
        if self.foto:
            self.foto = self.compressImage(self.foto)
        super(UsuarioPublico, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        return '%s %s (%s)' % (self.first_name, self.last_name, self.roles)


class Administrador(UsuarioPublico):
    pass

class Comerciales(UsuarioPublico):
    pass
