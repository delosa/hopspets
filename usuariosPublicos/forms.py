# -*- coding: utf-8 -*-
from .models import *
from django import forms
from django.contrib.auth.forms import *


class IniciarSesionPublicForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(IniciarSesionPublicForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = 'Usuario'
        self.fields['password'].label = 'Contraseña'

    class Meta:
        model = UsuarioPublico


class CrearAdministradorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearAdministradorForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['email'].label = 'Correo Electronico'
        self.fields['celular'].label = 'Celular'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['segundo_nombre'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['email'].required = True

    class Meta:
        model = Administrador
        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'email',
            'fecha_nacimiento',
            'telefono',
            'celular',
            'sexo',
            'direccion',
            'foto'

        )

        widgets = {
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'telefono': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'celular': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'email': forms.EmailInput(),
            'foto': forms.FileInput(),
            'direccion': forms.TextInput(attrs={'max_length': '100'}),
            'fecha_nacimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'})

        }

    def clean(self):

        print('holaaa')

        cleaned_data = super(CrearAdministradorForm, self).clean()
        correo = cleaned_data.get("email")

        print(correo)

        try:
            if User.objects.filter(username=correo):
                self._errors['email'] = [
                    'Ya existe un usuario con este correo electrónico']
        except:
            pass


class ModificarAdministradorForm(forms.ModelForm):


    class Meta:
        model = Administrador

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'foto',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100', 'readonly': 'readonly'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100', 'readonly': 'readonly'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100', 'readonly': 'readonly'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100', 'readonly': 'readonly'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1', 'readonly': 'readonly'}),
            'celular': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'telefono': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'direccion': forms.TextInput(attrs={'max_length': '100'}),
            'foto': forms.FileInput(),
        }


class CrearComercialesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearComercialesForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['email'].label = 'Correo Electronico'
        self.fields['celular'].label = 'Celular'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['segundo_nombre'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['email'].required = True

    class Meta:
        model = Comerciales
        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'email',
            'fecha_nacimiento',
            'telefono',
            'celular',
            'sexo',
            'direccion',
            'foto'

        )

        widgets = {
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'telefono': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'celular': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'email': forms.EmailInput(),
            'foto': forms.FileInput(),
            'direccion': forms.TextInput(attrs={'max_length': '100'}),
            'fecha_nacimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'})

        }

    def clean(self):

        print('holaaa')

        cleaned_data = super(CrearComercialesForm, self).clean()
        correo = cleaned_data.get("email")

        print(correo)

        try:
            if User.objects.filter(username=correo):
                self._errors['email'] = [
                    'Ya existe un usuario con este correo electrónico']
        except:
            pass