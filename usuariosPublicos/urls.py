from django.urls import path
from .views import *
from django.contrib.auth import views as auth_views
from .forms import IniciarSesionPublicForm

urlpatterns = [
    path('iniciar_sesion/', auth_views.LoginView.as_view(template_name='loginPublico.html', form_class=IniciarSesionPublicForm), name="iniciar_sesion"),
    path('salir/', auth_views.LogoutView.as_view(), {'next_page': 'iniciar_sesion'}, name="cerrar_sesion"),
    path('index/', Index.as_view(), name="index"),
    path('crear_administrador/', CrearAdministrador.as_view(), name="crear_administrador"),
    path('editar_administrador/<int:pk>/', EditarAdministrador.as_view(), name="editar_administrador"),
    path('listar_administradores/', ListarAdministradores.as_view(), name="listar_administradores"),
    path('visualizar_administrador/<int:pk>/', VisualizarAdministrador.as_view(), name="visualizar_administrador"),
    path('crear_comercial/', CrearComerciales.as_view(), name="crear_comerciales"),
    path('listar_comerciales/', ListarComerciales.as_view(), name="listar_comerciales"),
    path('cambiar_contrasena/<int:id_user>/', CambiarContrasena, name="cambiar_contrasena"),
    path('cambiar_contra/', cambiarContra, name="cambiar_contra"),
    path('reiniciar_contrasena/', auth_views.PasswordResetView.as_view(template_name='password_change_form.html',email_template_name='password_reset_html_email.html', subject_template_name = 'password_reset_subject.txt' ), name="reiniciar_contrasena"),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='password_change_form.html', email_template_name='password_reset_html_email.html' ), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='password_reset_done'),
    path('password_reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'), name='password_reset_confirm'),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
]