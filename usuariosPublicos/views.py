from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from hopsPets.utilities import *
from django.http import JsonResponse
from django.db import connection
from veterinarias.models import Tenant
from django.shortcuts import get_object_or_404
from clientes.models import Cliente
from mascotas.models import Mascota
from django.contrib.auth.models import User
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
import json


class Index(TemplateView):
    template_name = "./indexPublico.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(Index, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect('/veterinarias/listar_veterinarias/')


class CrearAdministrador(SuccessMessageMixin, CreateView):
    model = Administrador
    form_class = CrearAdministradorForm
    template_name = "crear_administrador_publico.html"
    success_url = reverse_lazy('listar_administradores')
    success_msg = "¡El usuario fue creado exitosamente!"

    #@method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearAdministrador, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        administrador = form.instance
        contra = administrador.first_name[0] + administrador.numero_documento_identificacion + administrador.last_name[
            0]
        administrador.set_password(contra)
        administrador.username = administrador.email
        administrador.is_active = True
        administrador.roles = "Administrador"

        self.object = form.save()

        grupo_administrador, grupo_administrador_creado = Group.objects.get_or_create(name='Administrador')
        grupo_administrador.user_set.add(self.object)
        messages.success(self.request, "Se ha registrado exitosamente el usuario")

        return super(CrearAdministrador, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
        return super(CrearAdministrador, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearAdministrador, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['administradores'] = True
        return context


class EditarAdministrador(SuccessMessageMixin, UpdateView):
    model = Administrador
    template_name = "crear-administrador.html"
    success_url = reverse_lazy('listar_administradores')
    form_class = ModificarAdministradorForm

    success_message = "¡El usuario fue modificado exitosamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarAdministrador, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarAdministrador, self).get_context_data(**kwargs)
        context['modificar'] = True
        return context


class VisualizarAdministrador(DetailView):
    model = Administrador
    template_name = "detalle-administrador.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VisualizarAdministrador, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VisualizarAdministrador, self).get_context_data(**kwargs)
        return context


class ListarAdministradores(ListView):
    model = Administrador
    template_name = "listar_administradores_publico.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarAdministradores, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarAdministradores, self).get_context_data(**kwargs)
        context['administradores'] = True
        return context


def enviar_mensaje_masivo(request):
    tenant = get_object_or_404(Tenant, id=20)
    connection.set_tenant(tenant)
    for cliente in Cliente.objects.filter(notificaciones_wpp=True):
        mascotas_cliente = ''
        for mascota in Mascota.objects.filter(cliente=cliente):
            mascotas_cliente += " " + mascota.nombre

        if mascotas_cliente == '':
            mascotas_cliente += " tu mascota"
        notificar_por_whatsapp_dr_mascot(cliente.nombres,mascotas_cliente,cliente.celular)
    connection.set_schema_to_public()
    data = {
        'enviados': True,
    }
    return JsonResponse(data)


##*************************Comerciales**********************************************####

class CrearComerciales(SuccessMessageMixin, CreateView):
    model = Comerciales
    form_class = CrearComercialesForm
    template_name = "crear_comerciales_publico.html"
    success_url = reverse_lazy('listar_comerciales')
    success_msg = "¡El usuario fue creado exitosamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearComerciales, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        comerciales = form.instance
        contra = comerciales.first_name[0] + comerciales.numero_documento_identificacion + comerciales.last_name[
            0]
        comerciales.set_password(contra)
        comerciales.username = comerciales.email
        comerciales.is_active = True
        comerciales.roles = "Comerciales"

        self.object = form.save()

        grupo_comerciales, grupo_comerciales_creado = Group.objects.get_or_create(name='Comerciales')
        grupo_comerciales.user_set.add(self.object)
        messages.success(self.request, "Se ha registrado exitosamente el usuario")

        return super(CrearComerciales, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
        return super(CrearComerciales, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearComerciales, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['comerciales'] = True
        return context

class ListarComerciales(ListView):
    model = Comerciales
    template_name = "listar_comerciales_publico.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarComerciales, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarComerciales, self).get_context_data(**kwargs)
        context['comerciales'] = True
        return context

def CambiarContrasena(request, id_user):
    return render(request, 'cambiar_contrasena_publico.html', {'id_user': id_user})

def cambiarContra(request):
    id_user = request.GET.get('id_user', '')
    contra = request.GET.get('contra', '')

    user = User.objects.get(id=id_user)
    user.set_password(contra)
    user.save()

    data = {
        'actualizado': True
    }
    return JsonResponse(data)