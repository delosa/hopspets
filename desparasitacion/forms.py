from .models import *
from django import forms
from datetime import datetime
from productos.models import Producto, Medicamento
from vacunacion.models import Vacuna
from insumos.models import Insumo


class CrearDesparasitanteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearDesparasitanteForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['cantidad'].label = 'Unidades disponibles'
        self.fields['foto'].label = 'Imagen'
        self.fields['cantidad_por_unidad'].label = 'Especificar cantidad por unidad'
        self.fields['precio_venta'].initial = '0'
        self.fields['cantidad'].initial = '1'
        self.fields['cantidad_unidad'].initial = '100'

    class Meta:
        model = Desparasitante
        fields = (
            'nombre',
            'codigo',
            'codigo_inventario',
            'descripcion',
            'cantidad',
            'precio_venta',
            'unidad_medida',
            'foto',
            'cantidad_por_unidad',
            'cantidad_unidad',
            'precio_por_unidad',
            'fecha_vencimiento',
            'tipo_uso',
            'stock_minimo',
            'bodega',
            'precio_compra'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '10'}),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}),
            'codigo': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'codigo_inventario': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'fecha_vencimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'precio_compra': forms.NumberInput(attrs={'max_length': '11', 'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(CrearDesparasitanteForm, self).clean()
        codigo = cleaned_data.get("codigo_inventario")
        nombre = cleaned_data.get("nombre")
        fecha_vencimiento = cleaned_data.get("fecha_vencimiento")
        cantidad_por_unidad = cleaned_data.get("cantidad_por_unidad")
        cantidad_unidad = cleaned_data.get("cantidad_unidad")

        desparasitante_instance = self.instance

        if codigo != None:
            try:
                if Producto.objects.filter(codigo=codigo) or Vacuna.objects.filter(
                        codigo_inventario=codigo) or Medicamento.objects.filter(codigo_inventario=codigo) or Insumo.objects.filter(codigo_inventario=codigo):
                    self._errors['codigo_inventario'] = [
                        'Ya existe un elemento del inventario con este codigo']
                elif Desparasitante.objects.filter(codigo_inventario=codigo).exclude(id=desparasitante_instance.id):
                    for desparasitante in Desparasitante.objects.filter(codigo_inventario=codigo).exclude(id=desparasitante_instance.id):
                        if desparasitante.nombre != nombre:
                            self._errors['codigo_inventario'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                            self._errors['nombre'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                        elif desparasitante.fecha_vencimiento == fecha_vencimiento:
                            self._errors['fecha_vencimiento'] = [
                                'Este desparasitante ya se encuentra registrado en el inventario con la misma fecha de vencimiento']
            except:
                pass

        if cantidad_por_unidad == True and cantidad_unidad < 1:
            self._errors['cantidad_unidad'] = [
                'La cantidad no puede ser menor que uno']


class CrearDesparasitacionForm(forms.ModelForm):

    periocidad_field = forms.ChoiceField(label='Repetir desparasitacion')

    def __init__(self, *args, **kwargs):
        super(CrearDesparasitacionForm, self).__init__(*args, **kwargs)
        self.fields['observaciones'].required = False
        self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['veterinario'].empty_label = None
        self.fields['fecha'].initial = datetime.now()
        self.fields['fecha'].label = 'Fecha'
        self.fields['periocidad_field'].choices = [('No','No'),('Si', 'Si')]

    class Meta:
        model = Desparasitacion
        fields = (
            'fecha',
            'descripcion',
            'observaciones',
            'veterinario',
        )

        widgets = {
            'observaciones': forms.Textarea(attrs={'rows': '7'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }

    def clean_cantidad_field(self):
        cantidad = self.cleaned_data['cantidad_field']

        if cantidad <= 0 :
            raise forms.ValidationError(
                'El número de veces debe ser mayor que 0')

        return cantidad

class CrearDesparasitacionFormAgenda(forms.ModelForm):

    periocidad_field = forms.ChoiceField(label='Repetir desparasitacion')

    def __init__(self, *args, **kwargs):
        super(CrearDesparasitacionFormAgenda, self).__init__(*args, **kwargs)
        self.fields['observaciones'].required = False
        #self.fields['mascota'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        #self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['fecha'].initial = datetime.now()
        self.fields['fecha'].label = 'Fecha'
        self.fields['periocidad_field'].choices = [('No','No'),('Si', 'Si')]

    class Meta:
        model = Desparasitacion
        fields = (
            'mascota',
            'fecha',
            'descripcion',
            'observaciones',
            'veterinario',
        )

        widgets = {
            'observaciones': forms.Textarea(attrs={'rows': '7'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input','id':'firstdate'}),
        }

    def clean_cantidad_field(self):
        cantidad = self.cleaned_data['cantidad_field']

        if cantidad <= 0 :
            raise forms.ValidationError(
                'El número de veces debe ser mayor que 0')

        return cantidad

class EditarDesparasitacionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarDesparasitacionForm, self).__init__(*args, **kwargs)
        self.fields['observaciones'].required = False
        self.fields['veterinario'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['veterinario'].empty_label = None
        self.fields['fecha'].label = 'Fecha'

    class Meta:
        model = Desparasitacion
        fields = (
            'fecha',
            'descripcion',
            'observaciones',
            'veterinario',
            'estado',
        )

        widgets = {
            'observaciones': forms.Textarea(attrs={'rows': '7'}),
            'fecha': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
        }
