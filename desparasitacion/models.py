from django.db import models
from usuarios.models import Veterinario
from mascotas.models import Mascota
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from bodegas.models import Bodega


def get_upload_to(instance, filename):
    return 'upload/desparasitantes/%s/%s' % (instance.nombre, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extension del archivo no es valida.')


ESTADOS = (('Activo','Activo'),('Inactivo','Inactivo'))
UNIDADES_MEDIDA = (('ml','Mililitros (ml)'),('l','Litros (l)'),('g','Gramos (g)'),('mg','Miligramos (mg)'),('kg','Kilogramos (kg)'), ('tabs', 'Tabletas (tabs)'))

TIPOS_DE_USO = (('Interno', 'Interno'), ('Comercial', 'Comercial'),('Interno/Comercial', 'Interno/Comercial'))

class Desparasitante(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    codigo = models.CharField(max_length=50, verbose_name='Número de lote', blank=True, null=True)
    codigo_inventario = models.CharField(max_length=50, verbose_name='Código de barras', blank=True, null=True)
    descripcion = models.CharField(max_length=800, verbose_name='Descripción', null=True, blank=True)
    estado = models.CharField(max_length=20, verbose_name="Estado", choices=ESTADOS, default='Activo')
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    precio_venta = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    unidad_medida = models.CharField(max_length=100, verbose_name='Unidad de medida', choices=UNIDADES_MEDIDA, default='ml')
    cantidad_unidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad por unidad', default=0)
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    cantidad_por_unidad = models.BooleanField(default=False)
    precio_por_unidad = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    cantidad_total_unidades = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad total unidades', default=0)
    tipo_uso = models.CharField(max_length=50, verbose_name='Tipo de uso', choices=TIPOS_DE_USO, default='Interno/Comercial')
    fecha_vencimiento = models.DateField(blank=True, null=True)
    stock_minimo = models.PositiveIntegerField(verbose_name='Stock minimo', default=1)
    bodega = models.ForeignKey(Bodega, on_delete=models.CASCADE, default=1)
    precio_compra = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    def save(self, *args, **kwargs):
        if self.cantidad_total_unidades == 0:
            self.cantidad_total_unidades = float(self.cantidad) * float(self.cantidad_unidad)

        if self.foto:
            if self.foto.url.endswith('.png'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(Desparasitante, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        if self.codigo:
            return self.nombre + " - " + self.codigo
        else:
            return self.nombre


ESTADOS_DESPARASITACION = (('Activo','Activo'),('Pagada','Pagada'))
ESTADOS_APLICACION_DESPARASITACION = (('Pendiente','Pendiente'),('Aplicada','Aplicada'))


class Desparasitacion(models.Model):
    fecha = models.DateTimeField()
    desparasitante = models.ForeignKey(Desparasitante, on_delete=models.CASCADE, blank=True, null=True)
    observaciones = models.CharField(max_length=800, verbose_name='Observaciones')
    veterinario = models.ForeignKey(Veterinario, on_delete=models.CASCADE, blank=True, null=True, limit_choices_to={'is_active': True})
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    estado = models.CharField(max_length=20, verbose_name="Estado", choices=ESTADOS_DESPARASITACION, default='Activo')
    costo = models.DecimalField(max_digits=50, decimal_places=0, verbose_name='Costo', default=0)
    descripcion = models.CharField(max_length=500, verbose_name='Descripción', blank=True, null=True)
    estado_aplicacion = models.CharField(max_length=20, verbose_name="Estado aplicación",
                                         choices=ESTADOS_APLICACION_DESPARASITACION, default='Pendiente')
    email_sent = models.BooleanField(default = False)


class AplicacionDesparasitacion(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    desparasitante = models.ForeignKey(Desparasitante, on_delete=models.CASCADE)
    desparasitacion = models.ForeignKey(Desparasitacion, on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad aplicada', default=0)
    unidad = models.CharField(max_length=200, verbose_name='Unidad')