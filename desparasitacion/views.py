from django.shortcuts import render
from django.views.generic import *
from .forms import *
from .models import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from mascotas.models import Mascota
from django.shortcuts import get_object_or_404,redirect
import datetime
from dateutil.relativedelta import relativedelta
from productos.models import HistorialInventario
from django.http import JsonResponse
from django.core import serializers
import json
import math
from gastos.models import GastoDesparasitante
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from insumos.models import Insumo, InsumoDesparasitacion
from decimal import Decimal
from django.db.models import Q
from usuarios.models import Usuario
from itertools import chain
from django.db.models import Count
from django.core.serializers.json import DjangoJSONEncoder


class CrearDesparasitante(SuccessMessageMixin, CreateView):
    model = Desparasitante
    form_class = CrearDesparasitanteForm
    template_name = "registrar_desparasitante.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡Desparasitante creado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearDesparasitante, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        desparasitante = form.instance
        self.object = form.save()

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro',tipo_elemento='Desparasitante', id_elemento=desparasitante.id,responsable=responsable)
        historia_transaccion.save()

        return super(CrearDesparasitante, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearDesparasitante, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_inventario'] = True
        return context


class ListarDesparasitantes(ListView):
    model = Desparasitante
    template_name = "listar_desparasitantes.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarDesparasitantes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarDesparasitantes, self).get_context_data(**kwargs)
        context['menu_desparasitantes'] = True
        return context


class EditarDesparasitante(SuccessMessageMixin, UpdateView):
    model = Desparasitante
    form_class = CrearDesparasitanteForm
    template_name = "registrar_desparasitante.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡Desparasitante editado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarDesparasitante, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        desparasitante = form.instance

        mi_desparasitante = get_object_or_404(Desparasitante, id=desparasitante.id)

        if (mi_desparasitante.cantidad != desparasitante.cantidad) or (mi_desparasitante.cantidad_unidad != desparasitante.cantidad_unidad):
            cantidad_nueva = desparasitante.cantidad
            desparasitante.cantidad_total_unidades = cantidad_nueva * desparasitante.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',tipo_elemento='Desparasitante', id_elemento=desparasitante.id,responsable=responsable)
        historia_transaccion.save()

        self.object = form.save()
        return super(EditarDesparasitante, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarDesparasitante, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        return context


class DetalleDesparasitante(DetailView):
    model = Desparasitante
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleDesparasitante, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleDesparasitante, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'desparasitante'

        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Desparasitante',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        compras = GastoDesparasitante.objects.filter(desparasitante_id=self.kwargs['pk'])

        context['compras'] = compras

        mi_elemento = Desparasitante.objects.get(id=self.kwargs['pk'])

        if mi_elemento.cantidad_unidad != 0 and mi_elemento.cantidad_por_unidad:
            unidades_enteras = math.floor(float(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))
            cantidad_no_entera = round((float(mi_elemento.cantidad_total_unidades) % int(mi_elemento.cantidad_unidad)),1)
        else:
            unidades_enteras = mi_elemento.cantidad
            cantidad_no_entera = 0

        if cantidad_no_entera == 0:
            if mi_elemento.cantidad_por_unidad:
                context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                    mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida
            else:
                context['disponibilidad'] = str(unidades_enteras) + " unidades"
        else:
            context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida + " y 1 unidad de " + str(
                cantidad_no_entera) + " " + mi_elemento.unidad_medida

        return context


class EditarDesparasitacion(SuccessMessageMixin, UpdateView):
    model = Desparasitacion
    form_class = EditarDesparasitacionForm
    template_name = "editar_desparasitacion.html"
    success_message = "¡Desparasitación editada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarDesparasitacion, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(mi_desparasitacion.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarDesparasitacion, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=self.kwargs['pk'])
        context['mascota'] = mi_desparasitacion.mascota
        return context


class EditarDesparasitacionAgenda(SuccessMessageMixin, UpdateView):
    model = Desparasitacion
    form_class = EditarDesparasitacionForm
    template_name = "editar_desparasitacion.html"
    success_url = reverse_lazy('listar_citas')
    success_message = "¡Desparasitación editada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarDesparasitacionAgenda, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarDesparasitacionAgenda, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=self.kwargs['pk'])
        context['mascota'] = mi_desparasitacion.mascota
        return context


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


def aplicar_desparasitacion(request, id_desparasitacion):
    if request.method == 'POST':
        req = json.loads(request.body.decode('utf-8'))
        id_desparasitacion = str(req['id_desparasitacion'])

        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=id_desparasitacion)

        costo_desparasitacion = 0

        for desparasitante in req['desparasitantes_agregados']:
            mi_desparasitante = get_object_or_404(Desparasitante, pk=desparasitante['id_desparasitante'])

            cantidad = str(desparasitante['cantidad_requerida'])
            unidad = str(desparasitante['unidades'])

            aplicacion_desparasitacion = AplicacionDesparasitacion(desparasitante=mi_desparasitante, desparasitacion=mi_desparasitacion, cantidad=cantidad, unidad=unidad)
            aplicacion_desparasitacion.save()

            if unidad == 'Unidad(es)':

                costo_desparasitacion = costo_desparasitacion + (float(cantidad) * int(mi_desparasitante.precio_venta))

                mi_desparasitante.cantidad = float(mi_desparasitante.cantidad) - float(cantidad)

                mi_desparasitante.cantidad_total_unidades = float(mi_desparasitante.cantidad_total_unidades) - ( float(cantidad) * float(mi_desparasitante.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad) + " Unidad(es)."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion,
                                                           tipo_elemento='Desparasitante',
                                                           id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas')
                historia_transaccion.save()

            else:

                nuevo_valor = math.floor( float(cantidad) / float(mi_desparasitante.cantidad_unidad) )

                costo_desparasitacion = costo_desparasitacion + (float(cantidad) * int(mi_desparasitante.precio_por_unidad))

                mi_desparasitante.cantidad_total_unidades = float(mi_desparasitante.cantidad_total_unidades) - float(cantidad)

                mi_desparasitante.cantidad = math.floor( float(mi_desparasitante.cantidad_total_unidades) / float(mi_desparasitante.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad) + " " + mi_desparasitante.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                           id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas')
                historia_transaccion.save()

            mi_desparasitante.save()

        mi_desparasitacion.estado_aplicacion = 'Aplicada'
        mi_desparasitacion.costo = costo_desparasitacion
        mi_desparasitacion.save()

        for Item in req['insumos_seleccionados']:
            insumo_select = get_object_or_404(Insumo, pk=Item[0])
            if Item[10] == True:
                insumo_select.cantidad_total_unidades = float(
                    insumo_select.cantidad_total_unidades) - float(Item[11])
                insumo_select.cantidad = math.floor(
                    float(insumo_select.cantidad_total_unidades) / float(
                        insumo_select.cantidad_unidad))
                historiatratamiento = InsumoDesparasitacion(insumo=insumo_select, desparasitacion=mi_desparasitacion,
                                                     cantidad_requerida=Item[11], unidad_medida="fraccion")
                historiatratamiento.save()
                insumo_select.save()
            else:

                insumo_select.cantidad = int(insumo_select.cantidad) - int(Item[11])
                insumo_select.cantidad_total_unidades = float(
                    insumo_select.cantidad_total_unidades) - (float(Item[11]) * float(
                    insumo_select.cantidad_unidad))
                historiatratamiento = InsumoDesparasitacion(insumo=insumo_select, desparasitacion=mi_desparasitacion,
                                                     cantidad_requerida=Item[11], unidad_medida="unidad")
                historiatratamiento.save()
                insumo_select.save()

        data = {
            'id_mascota': mi_desparasitacion.mascota.id,
        }
        return JsonResponse(data)

    else:

        insumos = json.dumps(list(Insumo.objects.filter(estado='Activo',bodega=1).values_list('id', 'nombre', 'codigo', 'cantidad', 'unidad_medida','cantidad_unidad', 'cantidad_total_unidades','codigo_inventario', 'cantidad_por_unidad','fecha_vencimiento','fecha_vencimiento','estado')),cls=DjangoJSONEncoder)

        insumos_por_codigo = json.dumps(list(chain(
            Insumo.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
                codigo_inventario__isnull=True).values_list('nombre', 'codigo_inventario', 'id', 'estado'),
            Insumo.objects.filter(estado='Activo', codigo_inventario__isnull=True,bodega=1).values_list('nombre',
                                                                                               'codigo_inventario',
                                                                                               'id', 'estado'))),
            cls=DjangoJSONEncoder)

        insumos_repetidos = []
        for element in list(
                Insumo.objects.filter(bodega=1).values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
            if element['cantidad'] > 1:
                insumos_repetidos.append(element['codigo_inventario'])

        return render(request, 'aplicar_desparasitacion.html',{
            'mi_desparasitacion': get_object_or_404(Desparasitacion, pk=id_desparasitacion),
            'mascotas': True,
            'insumos': insumos,
            'insumos_por_codigo': insumos_por_codigo,
            'codigos_insumos': insumos_repetidos
        })


def obtener_desparasitantes(request):

    all_desparasitantes = Desparasitante.objects.filter(estado='Activo',bodega=1).exclude(tipo_uso='Comercial')

    elementos_por_codigo = chain(
        Desparasitante.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
            codigo_inventario__isnull=True).exclude(tipo_uso='Comercial'),
        Desparasitante.objects.filter(codigo_inventario__isnull=True, estado='Activo',bodega=1).exclude(tipo_uso='Comercial'))
    elementos_repetidos = []
    for element in list(
            Desparasitante.objects.filter(bodega=1).exclude(tipo_uso='Comercial').values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
        if element['cantidad'] > 1:
            elementos_repetidos.append(element['codigo_inventario'])

    data = {
        'all_desparasitantes': serializers.serialize('json', all_desparasitantes),
        'elementos_por_codigo': serializers.serialize('json', elementos_por_codigo),
        'codigos_elementos': elementos_repetidos
    }
    return JsonResponse(data)


def registrar_desparasitacion(request, id_mascota):
    if request.method == 'POST':
        print("Prueba POST registrar desparasitacion")
        form = CrearDesparasitacionForm()
        mi_mascota = get_object_or_404(Mascota, pk=id_mascota)

        req = json.loads(request.body.decode('utf-8'))
        fecha = datetime.datetime.strptime(str(req['fecha']), '%d/%m/%Y %H:%M')
        veterinario = str(req['veterinario'])
        descripcion = str(req['descripcion'])
        observaciones = str(req['observaciones'])

        mi_veterinario = get_object_or_404(Veterinario, pk=veterinario)

        nueva_desparasitacion = Desparasitacion(fecha=fecha, observaciones=observaciones, veterinario=mi_veterinario, mascota=mi_mascota, descripcion=descripcion)
        nueva_desparasitacion.save()

        if(req['repetir_desparasitacion']):
            for vacunacion in req['elementos_seleccionados']:

                mi_fecha = datetime.datetime.strptime(str(vacunacion['fecha']), '%d/%m/%Y %H:%M')
                mi_descripcion = str(vacunacion['descripcion'])

                nueva_desparasitacion = Desparasitacion(fecha=mi_fecha, veterinario=mi_veterinario, mascota=mi_mascota, descripcion=mi_descripcion)
                nueva_desparasitacion.save()

        if (mi_mascota.cliente.notificaciones_wpp) and (len(mi_mascota.cliente.celular) != 10 or not mi_mascota.cliente.celular.isdigit()):
            messages.error(request, 'Debe ingresar un numero de celular valido para que se le notifique al propietario de la mascota por whatsapp')

        return render(request, 'registrar_desparasitacion.html', {
            'form': form,
            'mascota': mi_mascota,
            'mascotas': True,
        })

    else:
        print("Prueba GET registrar desparasitacion")
        form = CrearDesparasitacionForm()
        mi_mascota = get_object_or_404(Mascota, pk=id_mascota)

        return render(request, 'registrar_desparasitacion.html', {
            'form': form,
            'mascota': mi_mascota,
            'mascotas': True,
        })


def eliminar_desparasitacion(request):
    id_desparasitacion = request.GET.get('id', '')
    mi_desparasitacion = get_object_or_404(Desparasitacion, pk=id_desparasitacion)
    id_mascota = mi_desparasitacion.mascota.id
    if mi_desparasitacion.estado_aplicacion == 'Pendiente' and mi_desparasitacion.estado == 'Activo':
        mi_desparasitacion.delete()
    data = {
        'eliminacion': True,
        'id_mascota': id_mascota
    }
    return JsonResponse(data)


class DetalleDesparasitacion(DetailView):
    model = Desparasitacion
    template_name = "detalle_desparasitacion.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleDesparasitacion, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleDesparasitacion, self).get_context_data(**kwargs)
        context['mascotas'] = True
        aplicaciones = AplicacionDesparasitacion.objects.filter(desparasitacion=self.kwargs['pk'])
        if aplicaciones:
            context['aplicaciones'] = aplicaciones

        insumos_utilizados = InsumoDesparasitacion.objects.filter(desparasitacion=self.kwargs['pk'])
        if insumos_utilizados:
            context['insumos_utilizados'] = insumos_utilizados

        return context

def eliminar_desparacitante(request):
    id_desparacitante = request.GET.get('id', '')
    desparasitante = get_object_or_404(Desparasitante, pk=id_desparacitante)
    desparasitante.delete()
    messages.success(request, '¡Desparasitante eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def inactivar_desparacitante(request):
    id_desparacitante = request.GET.get('id', '')
    desparasitante = get_object_or_404(Desparasitante, pk=id_desparacitante)
    desparasitante.estado = 'Inactivo'
    desparasitante.save()
    messages.success(request, '¡Desparasitante eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def registrar_cita_desparasitacion(request,fecha):
    if request.method == "GET":
        return render(request,'registrar_desparasitacion_agenda.html',{'citas':True,'fecha':fecha,'form':CrearDesparasitacionFormAgenda()})
    if request.method == "POST":
        print("IN POST")
        form_cita_desparasitacion = CrearDesparasitacionFormAgenda(request.POST)
        desparasitacion_in = form_cita_desparasitacion.instance
        #print(str(form_cita_desparasitacion.descripcion))
        req = json.loads(request.body.decode('utf-8'))
        print(str(req['repetir_desparasitacion']))
        if (req['repetir_desparasitacion']):
            mascota_new = get_object_or_404(Mascota, pk=req['mascota'])
            veterinario_new = get_object_or_404(Veterinario, pk= req['veterinario'])
            fecha = datetime.datetime.strptime(req['fecha'], '%d/%m/%Y %H:%M')
            new_desparasitacion = Desparasitacion(fecha = fecha,observaciones = req['observaciones'],veterinario=veterinario_new,mascota = mascota_new,descripcion= req['descripcion'])
            new_desparasitacion.save()
            print(str(req['elementos_seleccionados']))
            for desparasitacion_in in req['elementos_seleccionados']:
                print(desparasitacion_in)
                fecha_in = datetime.datetime.strptime(desparasitacion_in['fecha'], '%d/%m/%Y %H:%M')
                new_desparasitacion_in_array = Desparasitacion(fecha = fecha_in,veterinario=veterinario_new,mascota = mascota_new,descripcion= desparasitacion_in['descripcion'])
                new_desparasitacion_in_array.save()
        else:
            mascota_new = get_object_or_404(Mascota, pk=req['mascota'])
            veterinario_new = get_object_or_404(Veterinario, pk= req['veterinario'])
            fecha = datetime.datetime.strptime(req['fecha'], '%d/%m/%Y %H:%M')
            new_desparasitacion = Desparasitacion(fecha = fecha,observaciones = req['observaciones'],veterinario=veterinario_new,mascota = mascota_new,descripcion= req['descripcion'])
            new_desparasitacion.save()
        return redirect('/citas/listar_citas')

