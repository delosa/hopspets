from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_desparasitante/', CrearDesparasitante.as_view(), name="registrar_desparasitante"),
    path('listar_desparasitantes/', ListarDesparasitantes.as_view(), name="listar_desparasitantes"),
    path('editar_desparasitante/<int:pk>/', EditarDesparasitante.as_view(), name="editar_desparasitante"),
    #path('registrar_desparasitacion/<int:id_mascota>/', CrearDesparasitacion.as_view(), name="registrar_desparasitacion"),
    path('editar_desparasitacion/<int:pk>/', EditarDesparasitacion.as_view(), name="editar_desparasitacion"),
    path('detalle_desparasitante/<int:pk>/', DetalleDesparasitante.as_view(), name='detalle_desparasitante'),
    path('aplicar_desparasitacion/<int:id_desparasitacion>/', aplicar_desparasitacion, name="aplicar_desparasitacion"),
    path('ajax/obtener_desparasitantes/', obtener_desparasitantes, name="obtener_desparasitantes"),
    path('registrar_desparasitacion/<int:id_mascota>/', registrar_desparasitacion, name="registrar_desparasitacion"),
    path('eliminar_desparasitacion/', eliminar_desparasitacion, name="eliminar_desparasitacion"),
    path('detalle_desparasitacion/<int:pk>/', DetalleDesparasitacion.as_view(), name='detalle_desparasitacion'),
    path('eliminar_desparacitante/', eliminar_desparacitante, name="eliminar_desparacitante"),
    path('inactivar_desparacitante/', inactivar_desparacitante, name="inactivar_desparacitante"),
    path('registrar_cita_desparasitacion/<str:fecha>/', registrar_cita_desparasitacion, name="registrar_cita_desparasitacion"),
    path('editar_desparasitacion_agenda/<int:pk>/', EditarDesparasitacionAgenda.as_view(), name="editar_desparasitacion_agenda"),
     
]