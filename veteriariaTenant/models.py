from django.db import models
from django.core.validators import RegexValidator
from django.conf import settings
import re, os


re_alpha = re.compile('[^\W\d_]+$', re.UNICODE)
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores numericos')
alpha = RegexValidator(re_alpha, 'Solamente se reciben caracteres de A-Z')


def get_upload_to(instance, filename):
    return 'upload/%s/%s' % (instance.nombre, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'El logo de debe estar en formato "png", "jpg" o "jpeg".')


OPCIONES_REGIMEN = (('Común','Común'),('Simplificado','Simplificado'))


class Veterinaria(models.Model):
    nit = models.CharField(max_length=100, verbose_name="Nit veterinaria")
    nombre = models.CharField(max_length=100, verbose_name="Nombre veterinaria", validators=[alpha])
    ciudad = models.CharField(blank=True, null=True, max_length=100, verbose_name='Ciudad')
    direccion = models.CharField(blank=True, null=True, max_length=100, verbose_name='Direccion')
    telefono = models.CharField(blank=True, null=True, max_length=100, verbose_name='Telefono')
    celular = models.CharField(blank=True, null=True, max_length=100, verbose_name='Celular')
    fecha_fundacion = models.DateField(blank=True, null=True, verbose_name="Fecha de fundacion")
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    regimen = models.CharField(max_length=20, verbose_name="Régimen", choices=OPCIONES_REGIMEN, default='Simplificado')
    mensaje_notificacion_wpp = models.CharField(blank=True, null=True, max_length=500, verbose_name='Mensaje whatsapp')
    imprimir_logo = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre_veterinaria
