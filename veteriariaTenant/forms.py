from .models import *
from django import forms
from pres.models import *
from pagoAdmin.models import PagoAdmin
from datetime import datetime
from usuarios.models import *


class CrearVeterinariaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearVeterinariaForm, self).__init__(*args, **kwargs)
        self.fields['foto'].label = 'Logo'
        self.fields['foto'].required = False
        self.fields['telefono'].required = False
        self.fields['celular'].required = False

    class Meta:
        model = Veterinaria
        fields = (
            'nit',
            'nombre',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'fecha_fundacion',
            'foto',
        )

        widgets = {
            'nombre': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'ciudad': forms.TextInput(attrs={'required': 'false', 'max_length': '100'}),
            'direccion': forms.TextInput(attrs={'required': 'false', 'max_length': '100'}),
            'foto': forms.FileInput(),
            'fecha_fundacion': forms.DateInput(attrs={'class': 'form-control datetimepicker'})

        }

    def clean(self):

        cleaned_data = super(CrearVeterinariaForm, self).clean()
        nit = cleaned_data.get("nit")


        try:
            if Veterinaria.objects.filter(nit=nit):
                self._errors['nit'] = [
                    'Ya existe una Veterinaria con este número de NIT']
            if Veterinaria.objects.count() > 0:
                self._errors['nombre'] = [
                    'Ya existe una Veterinaria creada']
        except:
            pass


class ModificarVeterinariaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarVeterinariaForm, self).__init__(*args, **kwargs)
        self.fields['foto'].label = 'Logo'
        self.fields['foto'].required = False
        self.fields['telefono'].required = False
        self.fields['celular'].required = False
        self.fields['imprimir_logo'].label = 'Imprimir logo en facturacion POS'

    class Meta:
        model = Veterinaria
        fields = (
            'nit',
            'nombre',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'regimen',
            'foto',
            'mensaje_notificacion_wpp',
            'imprimir_logo'
        )

        widgets = {
            'nombre': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'ciudad': forms.TextInput(attrs={'required': 'false', 'max_length': '100'}),
            'direccion': forms.TextInput(attrs={'required': 'false', 'max_length': '100'}),
            'foto': forms.FileInput(),
            'mensaje_notificacion_wpp': forms.Textarea(attrs={'rows': '7'})
        }

class CrearPRESClienteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearPRESClienteForm, self).__init__(*args, **kwargs)
        self.fields['fecha_registro'].initial = datetime.now()

    class Meta:
        model = PRES
        fields = (
            'tipo_pres',
            'fecha_registro',
            'descripcion',
            'adjunto_pres',
        )

        widgets = {
            'descripcion': forms.Textarea(),
            'fecha_registro': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'adjunto_pres': forms.FileInput()
        }

class CrearPagoAdminClienteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrearPagoAdminClienteForm, self).__init__(*args, **kwargs)
        self.fields['fecha_pago'].initial = datetime.now()
        self.fields['adjunto'].label = "Adjunto Comprobante"

    class Meta:
        model = PagoAdmin
        fields = (
            'fecha_pago',
            'medio_pago',
            'valor_pagado',
            'adjunto',
        )

        widgets = {
            'fecha_pago': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'adjunto': forms.FileInput()
        }

class ModificarAdministradorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarAdministradorForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['numero_documento_identificacion'].label = 'No. Documento'
        self.fields['segundo_apellido'].required = False
        self.fields['segundo_nombre'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Administrador

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'foto',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto': forms.FileInput(),
        }


class ModificarVeterinarioForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarVeterinarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['numero_documento_identificacion'].label = 'No. Documento'
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['segundo_nombre'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Veterinario

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'especializacion',
            'no_tarjeta',
            'foto',
            'email',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'no_tarjeta': forms.TextInput(attrs={'max_length': '100'}),
            'especializacion': forms.TextInput(attrs={'max_length': '100'}),
            'foto': forms.FileInput(),
        }


class ModificarAsistenteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarAsistenteForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Primer nombre'
        self.fields['last_name'].label = 'Primer apellido'
        self.fields['segundo_apellido'].label = 'Segundo apellido'
        self.fields['numero_documento_identificacion'].label = 'No. Documento'
        self.fields['celular'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False
        self.fields['segundo_apellido'].required = False
        self.fields['segundo_nombre'].required = False
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Asistente

        fields = (
            'first_name',
            'segundo_nombre',
            'last_name',
            'segundo_apellido',
            'numero_documento_identificacion',
            'celular',
            'telefono',
            'direccion',
            'foto',
        )


        widgets = {
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'segundo_apellido': forms.TextInput(attrs={'max_length': '100'}),
            'segundo_nombre': forms.TextInput(attrs={'max_length': '100'}),
            'numero_documento_identificacion': forms.NumberInput(
                attrs={'required': 'true', 'max_length': '11', 'min': '1'}),
            'foto': forms.FileInput(),
        }