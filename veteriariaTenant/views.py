from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from imagenesActualizacion.models import ImagenActualizacion
from videosTutoriales.models import VideosTutoriales
from veterinarias.models import Tenant
from promocionEvento.models import *
from pres.models import *
from usuarios.models import *
from veterinarias.models import Tenant
from pagoAdmin.models import PagoAdmin
from usuariosPublicos.models import *
from django.core.mail import send_mail

# Create your views here.

class CrearVeterinaria(CreateView):
    model = Veterinaria
    form_class = CrearVeterinariaForm
    template_name = "crear_veterinaria.html"
    success_url = reverse_lazy('listar_veterinarias')
    success_message = "¡La veterinaria fue creada exitosamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearVeterinaria, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        veterinaria = form.instance
        self.object = form.save()
        return super(CrearVeterinaria, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearVeterinaria, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['veterinaria'] = True
        return context


class EditarVeterinaria(SuccessMessageMixin, UpdateView):
    model = Veterinaria
    template_name = "crear_veterinaria.html"
    success_url = reverse_lazy('listar_veterinarias')
    form_class = ModificarVeterinariaForm

    success_message = "¡La veterinaria fue modificada exitosamente!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVeterinaria, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarVeterinaria, self).get_context_data(**kwargs)
        context['modificar'] = True
        return context


class ListarVeterinaria(ListView):
    model = Veterinaria
    template_name = "listar-veterinaria.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarVeterinaria, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarVeterinaria, self).get_context_data(**kwargs)
        context['veterinaria'] = True
        return context

class PanelUsuario(ListView):
    model = Veterinaria
    template_name = "panel_usuario.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PanelUsuario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PanelUsuario, self).get_context_data(**kwargs)
        usuario = Usuario.objects.all()
        veterinario = Veterinario.objects.all()
        context['panel'] = True
        context['usuario'] = usuario
        context['veterinario'] = veterinario
        return context

class VideosTutoriales(ListView):
    model = VideosTutoriales
    template_name = "videos-tutoriales.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VideosTutoriales, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VideosTutoriales, self).get_context_data(**kwargs)
        context['videosTutoriales'] = True
        return context

class ImagenActualizacion(ListView):
    model = ImagenActualizacion
    template_name = "imagenes-actualizacion.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ImagenActualizacion, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ImagenActualizacion, self).get_context_data(**kwargs)
        context['imagenesActualizacion'] = True
        return context

class DetallePlan(ListView):
    model = Veterinaria
    template_name = "detalle-plan.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetallePlan, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetallePlan, self).get_context_data(**kwargs)
        context['datos'] = True
        veterinarias = Veterinaria.objects.all()

        for veterinaria in veterinarias:
            datos = Tenant.objects.filter(nombre_veterinaria=veterinaria.nombre)
            context['datos'] = datos

        return context

class PromocionesEventos(ListView):
    model = PromocionEvento
    template_name = "promociones-eventos.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PromocionesEventos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PromocionesEventos, self).get_context_data(**kwargs)
        context['promociones'] = True
        return context

class PRESSolucion(ListView):
    model = Veterinaria
    template_name = "pres_soluciones.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PRESSolucion, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PRESSolucion, self).get_context_data(**kwargs)
        context['pres'] = True
        veterinarias = Veterinaria.objects.all()

        for veterinaria in veterinarias:
            pres = PRES.objects.filter(veterinaria__nombre_veterinaria=veterinaria.nombre)
            soluciones = SolucionPRES.objects.filter(pres__veterinaria__nombre_veterinaria=veterinaria.nombre)

            hoy = datetime.now().date()

            if self.kwargs['fecha_filtro'] == 'hoy':
                pres = pres.filter(fecha_registro=hoy)
            elif self.kwargs['fecha_filtro'] == 'semana':
                esta_semana = hoy.isocalendar()[1]
                pres = pres.filter(fecha_registro__week=esta_semana, fecha_registro__year=hoy.year)
            elif self.kwargs['fecha_filtro'] == 'mes':
                pres = pres.filter(fecha_registro__month=hoy.month, fecha_registro__year=hoy.year)
            else:
                pres = PRES.objects.all()

            dataTemplate = []
            for dato in pres:
                presCliente = {}
                presCliente['id'] = dato.id
                presCliente['fecha_registro'] = dato.fecha_registro
                presCliente['creacion_tenants'] = dato.creacion_tenants
                presCliente['nombre_cliente'] = dato.nombre_cliente
                presCliente['tipo_pres'] = dato.tipo_pres
                for solucion in soluciones:
                    if solucion.pres.id == dato.id:
                        presCliente['estado'] = solucion.estado
                        presCliente['fecha_resolucion'] = solucion.fecha_resolucion
                        presCliente['responsable_solucion'] = solucion.responsable_solucion
                dataTemplate.append(presCliente)

            context['pres'] = dataTemplate
            context['fecha_filtro'] = self.kwargs['fecha_filtro']
        return context

class PRESSolucionMes(ListView):
    model = Veterinaria
    template_name = "pres_soluciones.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PRESSolucionMes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PRESSolucionMes, self).get_context_data(**kwargs)
        context['pres'] = True
        veterinarias = Veterinaria.objects.all()

        for veterinaria in veterinarias:
            pres = PRES.objects.filter(veterinaria__nombre_veterinaria=veterinaria.nombre)
            soluciones = SolucionPRES.objects.filter(pres__veterinaria__nombre_veterinaria=veterinaria.nombre)

            mes = self.kwargs['mes']
            ano = self.kwargs['ano']

            pres = pres.filter(fecha_registro__month=mes, fecha_registro__year=ano)

            dataTemplate = []
            for dato in pres:
                presCliente = {}
                presCliente['id'] = dato.id
                presCliente['fecha_registro'] = dato.fecha_registro
                #presCliente['creacion_tenants'] = UsuarioPublico.objects.get(id=dato.creacion_tenants.id)
                presCliente['nombre_cliente'] = dato.nombre_cliente
                presCliente['tipo_pres'] = dato.tipo_pres
                for solucion in soluciones:
                    if solucion.pres.id == dato.id:
                        presCliente['estado'] = solucion.estado
                        presCliente['fecha_resolucion'] = solucion.fecha_resolucion
                        presCliente['responsable_solucion'] = solucion.responsable_solucion
                dataTemplate.append(presCliente)

            context['pres'] = dataTemplate
            context['fecha_filtro'] = 'mes'
            context['mes'] = mes
            context['ano'] = ano

            return context

class AgregarPRESCliente(SuccessMessageMixin, CreateView):
    model = PRES
    form_class = CrearPRESClienteForm
    template_name = "agregar_pres_cliente.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AgregarPRESCliente, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/veterinaria/pres_solucion/hoy/'

    def form_valid(self, form):
        veterinaria = Veterinaria.objects.all()
        usuario = Usuario.objects.filter(id=self.kwargs['id_user'])
        nombre = usuario[0].first_name + " " + usuario[0].last_name + " " + "(" + usuario[0].roles + ")"
        vete = veterinaria[0].nit
        pres = form.instance
        pres.veterinaria = get_object_or_404(Tenant, codigo_tenant=vete)
        pres.nombre_cliente = nombre
        pres.cliente_id = self.kwargs['id_user']
        pres.correo_cliente = usuario[0].email
        self.object = form.save()
        messages.success(self.request, "Se ha guardado su ticket Exitosamente")
        if form.is_valid():
            send_mail('Nuevo PRES en el Panel de Usuario en ' + str(pres.veterinaria) +' en HopsPets',
                    'Se registro un nuevo PRES con las siguientes caracteristicas: ' + 
                    '\n \n Nro de Ticket: ' + str(pres.tipo_pres[0]) + '0000' + str(pres.id) + 
                    '\n Fecha de Registro: ' + str(pres.fecha_registro) + 
                    '\n Tipo de PRES: ' + str(pres.tipo_pres) +
                    '\n Nombre de Veterinaria: ' + str(pres.veterinaria) + 
                    '\n Creado Por: ' + str(pres.nombre_cliente) +
                    '\n Descripcion del PRES: ' + str(pres.descripcion) + 
                    '\n Estado: Pendiente' +
                    '\n \n  URL: http://hopspet.com/pres/listar_pres/semana/ \n', 
                    'soporte@hopspet.com', 
                    ['cto@hops.com.co'],
                    fail_silently=False)
        return super(AgregarPRESCliente, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar su ticket, por favor revise los datos")
        return super(AgregarPRESCliente, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(AgregarPRESCliente, self).get_context_data(**kwargs)
        context['pres'] = True
        return context

class VerPagoAdmin(ListView):
    model = Veterinaria
    template_name = "pago_admin.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VerPagoAdmin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VerPagoAdmin, self).get_context_data(**kwargs)
        context['pago'] = True
        veterinarias = Veterinaria.objects.all()

        for veterinaria in veterinarias:
            pago = PagoAdmin.objects.filter(veterinaria__nombre_veterinaria=veterinaria.nombre)
            hoy = datetime.now().date()

            if self.kwargs['fecha_filtro'] == 'hoy':
                pago = pago.filter(fecha_pago=hoy)
            elif self.kwargs['fecha_filtro'] == 'semana':
                esta_semana = hoy.isocalendar()[1]
                pago = pago.filter(fecha_pago__week=esta_semana, fecha_pago__year=hoy.year)
            elif self.kwargs['fecha_filtro'] == 'mes':
                pago = pago.filter(fecha_pago__month=hoy.month, fecha_pago__year=hoy.year)
            else:
                pago = PagoAdmin.objects.filter(veterinaria__nombre_veterinaria=veterinaria.nombre)

            context['pagos'] = pago
            context['fecha_filtro'] = self.kwargs['fecha_filtro']

        return context

class VerPagoAdminMes(ListView):
    model = PagoAdmin
    template_name = "pago_admin.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(VerPagoAdminMes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VerPagoAdminMes,self).get_context_data(**kwargs)
        context['pago'] = True
        mes = self.kwargs['mes']
        ano = self.kwargs['ano']
        veterinarias = Veterinaria.objects.all()
        for veterinaria in veterinarias:
            pago = PagoAdmin.objects.filter(veterinaria__nombre_veterinaria=veterinaria.nombre)
            pagos_admin = pago.filter(fecha_pago__month=mes, fecha_pago__year=ano)

            context['pagos'] = pagos_admin
            context['mes'] = mes
            context['ano'] = ano
            context['fecha_filtro'] = 'mes'

        return context

class AgregarPagoAdminCliente(SuccessMessageMixin, CreateView):
    model = PagoAdmin
    form_class = CrearPagoAdminClienteForm
    template_name = "agregar_pago_admin_cliente.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AgregarPagoAdminCliente, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/veterinaria/pago_admin/semana'

    def form_valid(self, form):
        veterinaria = Veterinaria.objects.all()
        usuario = Usuario.objects.filter(id=self.kwargs['id_user'])
        nombre = usuario[0].first_name + " " + usuario[0].last_name + " (" + usuario[0].roles + ")"
        vete = veterinaria[0].nit
        pago = form.instance
        pago.veterinaria = get_object_or_404(Tenant, codigo_tenant=vete)
        pago.responsable_cliente = nombre
        self.object = form.save()
        messages.success(self.request, "Se ha guardado su registro de pago exitosamente")
        return super(AgregarPagoAdminCliente, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar su pago, revise los datos por favor")
        return super(AgregarPagoAdminCliente, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(AgregarPagoAdminCliente, self).get_context_data(**kwargs)
        context['pagos'] = True

        return context

def DetallePresSol(request, id_pres):
	pres = PRES.objects.filter(id=id_pres)
	sol_pres = SolucionPRES.objects.filter(pres_id=id_pres)
	return render(request, 'detalle_pres_sol.html',{'pres': pres, 'sol_pres': sol_pres})

def DetallePresSolNotificacion(request, id_pres):
    pres = PRES.objects.filter(id=id_pres)
    sol_pres = SolucionPRES.objects.filter(pres_id=id_pres)
    for sol in sol_pres:
        sol.revisado = True
        sol.save()
    return render(request, 'detalle_pres_sol_notificacion.html', {'pres': pres, 'sol_pres': sol_pres})

def index(request):
    return redirect('/usuario/index/')

class EditarAdministrador(SuccessMessageMixin, UpdateView):
    model = Administrador
    template_name = "crear-administrador_user.html"
    success_url = reverse_lazy('panel_usuario')
    form_class = ModificarAdministradorForm

    success_message = "El usuario fue modificado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarAdministrador, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarAdministrador, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['panel'] = True
        return context

class EditarVeterinario(SuccessMessageMixin, UpdateView):
    model = Veterinario
    template_name = "crear-veterinario_user.html"
    success_url = reverse_lazy('panel_usuario')
    form_class = ModificarVeterinarioForm

    success_message = "El usuario fue modificado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarVeterinario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        veterinario = form.instance
        veterinario.username = veterinario.email
        self.object = form.save()
        return super(EditarVeterinario, self).form_valid(form)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarVeterinario, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['panel'] = True
        return context

class EditarAsistente(SuccessMessageMixin, UpdateView):
    model = Asistente
    template_name = "crear-asistente_user.html"
    success_url = reverse_lazy('panel_usuario') 
    form_class = ModificarAsistenteForm

    success_message = "El usuario fue modificado exitosamente"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarAsistente, self).dispatch(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
        )

    def get_context_data(self, **kwargs):
        context = super(EditarAsistente, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['panel'] = True
        return context

def DetallePromo(request, id_promo):
    promo = PromocionEvento.objects.get(id=id_promo)

    return render(request, 'detalle-promocion-eventos.html', {
        'promo': promo,
    })