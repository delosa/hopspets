from django.apps import AppConfig


class VeteriariatenantConfig(AppConfig):
    name = 'veteriariaTenant'
