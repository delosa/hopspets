from django.urls import path
from .views import *

urlpatterns = [
    path('crear-veterinaria/', CrearVeterinaria.as_view(), name="crear_veterinaria"),
    path('listar_veterinaria/', ListarVeterinaria.as_view(), name="listar_veterinarias"),
    path('panel_usuario/', PanelUsuario.as_view(), name="panel_usuario"),
    path('editar_veterinaria/<int:pk>/', EditarVeterinaria.as_view(), name="editar_veterinaria"),
    path('videos_tutoriales/', VideosTutoriales.as_view(), name="videos_tutoriales"),
    path('imagenes_actualizacion/', ImagenActualizacion.as_view(), name="imagenes_actualizacion"),
    path('detalle_plan/', DetallePlan.as_view(), name="detalle_plan"),
    path('promociones_eventos/', PromocionesEventos.as_view(), name="promociones_eventos"),
    path('pres_solucion/<str:fecha_filtro>/', PRESSolucion.as_view(), name="pres_solucion"),
    path('pres_solucion_mes/<str:mes>/<str:ano>/', PRESSolucionMes.as_view(), name="pres_solucion_mes"),
    path('detalle_pres_sol/<int:id_pres>/', DetallePresSol, name='detalle_pres_sol'),
    path('detalle_pres_sol_notificacion/<int:id_pres>/', DetallePresSolNotificacion, name='detalle_pres_sol_notificacion'),
    path('agregar_pres_cliente/<int:id_user>/', AgregarPRESCliente.as_view(), name="agregar_pres"),
    path('pago_admin/<str:fecha_filtro>/', VerPagoAdmin.as_view(), name="pago_admin"),
    path('pago_admin_mes/<str:mes>/<str:ano>/', VerPagoAdminMes.as_view(), name="pago_admin_mes"),
    path('agregar_pago_admin/<int:id_user>/', AgregarPagoAdminCliente.as_view(), name="agregar_pago_admin"),
    path('editar_administrador_p/<int:pk>/', EditarAdministrador.as_view(), name="editar_administrador_p"),
    path('editar_veterinario_p/<int:pk>/', EditarVeterinario.as_view(), name="editar_veterinario_p"),
    path('editar_asistente_p/<int:pk>/', EditarAsistente.as_view(), name="editar_asistente_p"),
    path('detalle_promo_event/<int:id_promo>/', DetallePromo, name="detalle_promo_event"),
]