"""hopsPets URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from veteriariaTenant.views import index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('usuario/', include('usuarios.urls')),
    path('mascotas/', include('mascotas.urls')),
    path('clientes/', include('clientes.urls')),
    path('productos/', include('productos.urls')),
    path('gastos/', include('gastos.urls')),
    path('impuestos/', include('impuestos.urls')),
    path('estetica/', include('estetica.urls')),
    path('citas/', include('citas.urls')),
    path('veterinaria/', include('veteriariaTenant.urls')),
    path('', index ,name="index"),
    path('caja/', include('caja.urls')),
    path('pagos/', include('pagos.urls')),
    path('formulas/', include('formulas.urls')),
    path('tratamientos/', include('tratamientos.urls')),
    path('vacunacion/', include('vacunacion.urls')),
    path('desparasitacion/', include('desparasitacion.urls')),
    path('guarderia/', include('guarderia.urls')),
    path('informes/', include('informes.urls')),
    path('consentimiento/', include('consentimiento.urls')),
    path('insumos/', include('insumos.urls')),
    path('categorias/', include('categoriasInventario.urls')),
    path('bodegas/', include('bodegas.urls')),
    path('plan_cuentas/', include('planCuentas.urls')),
    path('puntoDeVenta/', include('puntoDeVenta.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
