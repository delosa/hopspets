import requests
from django.contrib import messages
from veteriariaTenant.models import Veterinaria
from datetime import datetime, timedelta
from twilio.rest import Client

account_sid = 'AC0cd710806f5c207461a0c498e0a7f101'
auth_token = 'eef97590852736895d7fcd61c2ea3756'

def notificar_por_whatsapp(cliente,mascota,tipo_cita,fecha,numero_destino):

    hoy_mas_treinta = datetime.today() + timedelta(minutes=30)

    if len(numero_destino) == 10 and numero_destino.isdigit() and datetime.strptime(fecha, '%d/%m/%Y a las %I:%M%p') > hoy_mas_treinta:

        mensaje_completo = ''

        #####################Mensaje recordatorio######################

        mensaje_general = "Estimada/o *"+cliente+"* \n\n"
        mensaje_general += "Te recordamos que tu mascota *"+mascota+"* tiene una "+tipo_cita+" el dia "+ fecha

        mensaje_completo += mensaje_general

        #####################Datos de veterinaria######################

        mi_veterinaria = Veterinaria.objects.first()

        datos_veterinaria = "\n\nCordialmente, \n\n*"+ str(mi_veterinaria.nombre) + "* \n" + str(mi_veterinaria.direccion)

        if mi_veterinaria.telefono:
            datos_veterinaria += "\n" + str(mi_veterinaria.telefono)

        if mi_veterinaria.celular:
            datos_veterinaria += "\n" + str(mi_veterinaria.celular)

        #if mi_veterinaria.mensaje_notificacion_wpp:
        #    datos_veterinaria += "\n" + str(mi_veterinaria.mensaje_notificacion_wpp)

        datos_veterinaria += "\n\n" + "Mensaje generado automáticamente, por favor no responder. Para cualquier inquietud lo invitamos a que haga uso de los canales de comunicación de la veterinaria"

        mensaje_completo += datos_veterinaria

        ###############################################################

        client = Client(account_sid, auth_token)
        message = client.messages.create(from_='whatsapp:+15732451067',
                                         body=mensaje_completo,
                                         to='whatsapp:+57' + numero_destino)
        print(message.sid)


def notificar_por_whatsapp_dr_mascot(cliente,mascotas,telefono_cliente):
    mensaje = "Estimada/o " + cliente + ", *DR. MASCOT* Veterinaria, *QUIERE INFORMARTE*, para ti que eres cliente "
    mensaje += "preferencial en esta época de cuarentena estamos prestando el servicio de cita médi"
    mensaje += "ca prioritaria *PROGRAMADA*, urgencias vitales y venta de medicamentos y alimentos "
    mensaje += "para"+mascotas+", comunícate con nosotros a los siguientes números telefónicos o vía What"
    mensaje += "sApp para agendarte o confirmar tu pedido. *3013936481 – 0323780735.*\n\n"
    mensaje += "Te recordamos que es vital que la mascota este acompañada solo por 1 persona con su"
    mensaje += "s elementos de protección, guantes y tapabocas. La compra de insumos se atenderá ún"
    mensaje += "icamente en el establecimiento.\n\n"
    mensaje += "Los servicios de peluquería y domicilio estarán suspendidos hasta nueva orden.\n\n"
    mensaje += "Agradecemos tu comprensión.\n\n"
    mensaje += "ESTAMOS CONTIGO Y TU MASCOTA DURANTE COVID-19\n\n"
    mensaje += "Por tu salud y la nuestra quédate en casa.\n\n"
    mensaje += "_Mensaje generado automáticamente, por favor no responder. Para cualquier inquietud lo invitamos a que haga uso de los canales de comunicación de la veterinaria._"

    client = Client(account_sid, auth_token)
    message = client.messages.create(from_='whatsapp:+15732451067',
                                     body=mensaje,
                                     to='whatsapp:+57' + telefono_cliente)
    print(message.sid)