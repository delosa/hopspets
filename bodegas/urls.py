from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_bodega/', CrearBodega.as_view(), name="registrar_bodega"),
    path('listar_bodegas/', ListarBodegas.as_view(), name="listar_bodegas"),
    path('editar_bodega/<int:pk>/', EditarBodega.as_view(), name="editar_bodega"),
    path('eliminar_bodega/', eliminar_bodega, name="eliminar_bodega"),
]