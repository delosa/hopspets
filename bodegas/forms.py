from .models import *
from django import forms


class CrearBodegaForm(forms.ModelForm):

    class Meta:
        model = Bodega
        fields = (
            'nombre',
            'descripcion',
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '5'})
        }

