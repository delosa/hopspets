from django.shortcuts import get_object_or_404
from django.views.generic import *
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.contrib import messages
from .forms import *
from .models import *


class CrearBodega(SuccessMessageMixin, CreateView):
    model = Bodega
    form_class = CrearBodegaForm
    template_name = "registrar_bodega.html"
    success_url = reverse_lazy('listar_bodegas')
    success_message = "¡La bodega se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearBodega, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        bodega = form.instance
        self.object = form.save()
        return super(CrearBodega, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearBodega, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_bodegas'] = True
        return context


class ListarBodegas(ListView):
    model = Bodega
    template_name = "listar_bodegas.html"

    def get_queryset(self):
        queryset = super(ListarBodegas, self).get_queryset()
        return queryset.order_by('-pk')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarBodegas, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarBodegas, self).get_context_data(**kwargs)
        context['menu_bodegas'] = True
        return context


class EditarBodega(SuccessMessageMixin, UpdateView):
    model = Bodega
    form_class = CrearBodegaForm
    template_name = "registrar_bodega.html"
    success_url = reverse_lazy('listar_bodegas')
    success_message = "¡La bodega fue modificada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarBodega, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarBodega, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_bodegas'] = True
        return context


def eliminar_bodega(request):
    id_bodega = request.GET.get('id', '')
    mi_bodega = get_object_or_404(Bodega, pk=id_bodega)
    mi_bodega.delete()
    messages.success(request, 'Bodega eliminada con exito!')
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)
