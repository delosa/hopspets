from django.db import models


class Bodega(models.Model):
    nombre = models.CharField(max_length=200, verbose_name='Nombre', unique=True, error_messages={'unique': "Ya existe una bodega con este nombre."})
    descripcion = models.CharField(max_length=500, verbose_name='Descripción', null=True, blank=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre
