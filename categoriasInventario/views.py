from django.shortcuts import get_object_or_404
from .models import CategoriaInventario, ElementoCategoriaInventario
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import *
from django.http import JsonResponse
from django.contrib import messages
from .forms import *
from productos.models import HistorialInventario
import math
from usuarios.models import Usuario


class CrearCategoriaInventario(SuccessMessageMixin, CreateView):
    model = CategoriaInventario
    form_class = CrearCategoriaForm
    template_name = "registrar_categoria_inventario.html"
    success_url = reverse_lazy('listar_categorias_inventario')
    success_message = "¡La categoria se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearCategoriaInventario, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        categoria_inventario = form.instance
        self.object = form.save()
        return super(CrearCategoriaInventario, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearCategoriaInventario, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['categorias_inventario'] = True
        return context


class ListarCategoriaInventario(ListView):
    model = CategoriaInventario
    template_name = "listar_categorias_inventario.html"

    def get_queryset(self):
        queryset = super(ListarCategoriaInventario, self).get_queryset()
        return queryset.order_by('-pk')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarCategoriaInventario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarCategoriaInventario, self).get_context_data(**kwargs)
        context['categorias_inventario'] = True
        return context


class EditarCategoriaInventario(SuccessMessageMixin, UpdateView):
    model = CategoriaInventario
    fields = ['nombre']
    template_name = "registrar_categoria_inventario.html"
    success_url = reverse_lazy('listar_categorias_inventario')
    success_message = "¡La categoria fue modificada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarCategoriaInventario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarCategoriaInventario, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['categorias_inventario'] = True
        return context


def eliminar_categoria_inventario(request):
    id_categoria_inventario = request.GET.get('id', '')
    mi_categoria_inventario = get_object_or_404(CategoriaInventario, pk=id_categoria_inventario)
    mi_categoria_inventario.delete()
    messages.success(request, 'Categoria eliminada con exito!')
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)


class CrearElementoCategoria(SuccessMessageMixin, CreateView):
    model = ElementoCategoriaInventario
    form_class = CrearElementoCategoriaForm
    template_name = "registrar_elemento_categoria.html"
    success_message = "¡El elemento se agregó con exito!"
    success_url = reverse_lazy('listar_productos')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearElementoCategoria, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        elemento = form.instance
        elemento.categoria = get_object_or_404(CategoriaInventario, pk=self.kwargs['id_categoria'])
        self.object = form.save()

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro', tipo_elemento='Categoria',
                                                   id_elemento=elemento.id, responsable=responsable)
        historia_transaccion.save()

        return super(CrearElementoCategoria, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearElementoCategoria, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_inventario'] = True
        context['categoria'] = get_object_or_404(CategoriaInventario, pk=self.kwargs['id_categoria'])
        return context


class DetalleElementoCategoria(DetailView):
    model = ElementoCategoriaInventario
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleElementoCategoria, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleElementoCategoria, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'categoria'

        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Categoria',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        #compras = GastoDesparasitante.objects.filter(desparasitante_id=self.kwargs['pk'])

        #context['compras'] = compras

        mi_elemento = ElementoCategoriaInventario.objects.get(id=self.kwargs['pk'])

        if mi_elemento.cantidad_unidad != 0 and mi_elemento.cantidad_por_unidad:
            unidades_enteras = math.floor(float(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))
            cantidad_no_entera = round((float(mi_elemento.cantidad_total_unidades) % int(mi_elemento.cantidad_unidad)),1)
        else:
            unidades_enteras = mi_elemento.cantidad
            cantidad_no_entera = 0

        if cantidad_no_entera == 0:
            if mi_elemento.cantidad_por_unidad:
                context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                    mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida
            else:
                context['disponibilidad'] = str(unidades_enteras) + " unidades"
        else:
            context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida + " y 1 unidad de " + str(
                cantidad_no_entera) + " " + mi_elemento.unidad_medida

        return context


class EditarElementoCategoria(SuccessMessageMixin, UpdateView):
    model = ElementoCategoriaInventario
    form_class = CrearElementoCategoriaForm
    template_name = "registrar_elemento_categoria.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡El elemento fue modificado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarElementoCategoria, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        elemento = form.instance

        mi_elemento = get_object_or_404(ElementoCategoriaInventario, id=elemento.id)

        if (mi_elemento.cantidad != elemento.cantidad) or (mi_elemento.cantidad_unidad != elemento.cantidad_unidad):
            cantidad_nueva = elemento.cantidad
            elemento.cantidad_total_unidades = cantidad_nueva * elemento.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',tipo_elemento='Categoria', id_elemento=elemento.id,responsable=responsable)
        historia_transaccion.save()

        self.object = form.save()
        return super(EditarElementoCategoria, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarElementoCategoria, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        context['categoria'] = get_object_or_404(ElementoCategoriaInventario, pk=self.kwargs['pk']).categoria
        return context


def eliminar_elemento_categoria(request):
    id_elemento = request.GET.get('id', '')
    elemento = get_object_or_404(ElementoCategoriaInventario, pk=id_elemento)
    elemento.delete()
    messages.success(request, '¡Elemento eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def inactivar_elemento_categoria(request):
    id_elemento = request.GET.get('id', '')
    elemento = get_object_or_404(ElementoCategoriaInventario, pk=id_elemento)
    elemento.estado = 'Inactivo'
    elemento.save()
    messages.success(request, '¡Elemento eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)