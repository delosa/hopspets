from django.apps import AppConfig


class CategoriasinventarioConfig(AppConfig):
    name = 'categoriasInventario'
