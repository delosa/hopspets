from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_categoria_inventario/', CrearCategoriaInventario.as_view(), name="registrar_categoria_inventario"),
    path('listar_categorias_inventario/', ListarCategoriaInventario.as_view(), name="listar_categorias_inventario"),
    path('editar_categoria_inventario/<int:pk>/', EditarCategoriaInventario.as_view(), name="editar_categoria_inventario"),
    path('eliminar_categoria_inventario/', eliminar_categoria_inventario, name="eliminar_categoria_inventario"),
    path('registrar_elemento_categoria/<int:id_categoria>', CrearElementoCategoria.as_view(), name="registrar_elemento_categoria"),
    path('detalle_elemento_categoria/<int:pk>/', DetalleElementoCategoria.as_view(), name='detalle_elemento_categoria'),
    path('editar_elemento_categoria/<int:pk>/', EditarElementoCategoria.as_view(), name="editar_elemento_categoria"),
    path('eliminar_elemento_categoria/', eliminar_elemento_categoria, name="eliminar_elemento_categoria"),
    path('inactivar_elemento_categoria/', inactivar_elemento_categoria, name="inactivar_elemento_categoria"),
]