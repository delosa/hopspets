from .models import *
from django import forms
from vacunacion.models import Vacuna
from desparasitacion.models import Desparasitante
from insumos.models import Insumo
from productos.models import Producto, Medicamento


class CrearElementoCategoriaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearElementoCategoriaForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['cantidad'].label = 'Unidades disponibles'
        self.fields['foto'].label = 'Imagen'
        self.fields['cantidad_por_unidad'].label = 'Especificar cantidad por unidad'
        self.fields['precio_venta'].initial = '0'
        self.fields['cantidad'].initial = '1'
        self.fields['cantidad_unidad'].initial = '100'
        self.fields['codigo_inventario'].label = 'Código'

    class Meta:
        model = ElementoCategoriaInventario
        fields = (
            'nombre',
            'codigo',
            'codigo_inventario',
            'descripcion',
            'cantidad',
            'precio_venta',
            'foto',
            'cantidad_por_unidad',
            'unidad_medida',
            'cantidad_unidad',
            'precio_por_unidad',
            'fecha_vencimiento',
            'tipo_uso',
            'stock_minimo',
            'bodega',
            'precio_compra'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '10'}),
            'nombre': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'codigo': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'codigo_inventario': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'fecha_vencimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'precio_compra': forms.NumberInput(attrs={'max_length': '11', 'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(CrearElementoCategoriaForm, self).clean()
        codigo = cleaned_data.get("codigo_inventario")
        nombre = cleaned_data.get("nombre")
        fecha_vencimiento = cleaned_data.get("fecha_vencimiento")
        cantidad_por_unidad = cleaned_data.get("cantidad_por_unidad")
        cantidad_unidad = cleaned_data.get("cantidad_unidad")

        elemento_instance = self.instance

        if codigo != None:
            try:
                if Producto.objects.filter(codigo=codigo) or Vacuna.objects.filter(
                        codigo_inventario=codigo) or Desparasitante.objects.filter(
                    codigo_inventario=codigo) or Insumo.objects.filter(codigo_inventario=codigo) or Medicamento.objects.filter(codigo_inventario=codigo):
                    self._errors['codigo_inventario'] = [
                        'Ya existe un elemento del inventario con este codigo']
                elif ElementoCategoriaInventario.objects.filter(codigo_inventario=codigo).exclude(id=elemento_instance.id):
                    for elemento in ElementoCategoriaInventario.objects.filter(codigo_inventario=codigo).exclude(
                            id=elemento_instance.id):
                        if elemento.nombre != nombre:
                            self._errors['codigo_inventario'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                            self._errors['nombre'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                        elif elemento.fecha_vencimiento == fecha_vencimiento:
                            self._errors['fecha_vencimiento'] = [
                                'Este medicamento ya se encuentra registrado en el inventario con la misma fecha de vencimiento']
            except:
                pass

        if cantidad_por_unidad == True and cantidad_unidad < 1:
            self._errors['cantidad_unidad'] = [
                'La cantidad no puede ser menor que uno']


class CrearCategoriaForm(forms.ModelForm):

    class Meta:
        model = CategoriaInventario
        fields = (
            'nombre',
        )

    def clean(self):

        cleaned_data = super(CrearCategoriaForm, self).clean()
        nombre = cleaned_data.get("nombre").upper()

        if nombre in ('PRODUCTO', 'PRODUCTOS', 'VACUNA', 'VACUNAS', 'DESPARASITANTE', 'DESPARASITANTES', 'INSUMO', 'INSUMOS', 'MEDICAMENTO', 'MEDICAMENTOS'):
            self._errors['nombre'] = [
                'Ya existe una categoria con este nombre']
