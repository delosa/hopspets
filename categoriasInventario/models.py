from django.db import models
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from bodegas.models import Bodega


class CategoriaInventario(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre de categoria', unique=True, error_messages={'unique': "Ya existe una categoria con este nombre."})
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


def get_upload_to(instance, filename):
    return 'upload/categorias_inventario/%s/%s' % (instance.nombre, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')

ESTADOS = (('Activo','Activo'),('Inactivo','Inactivo'))

UNIDADES_MEDIDA = (('mililitros', 'Mililitros (ml)'), ('litros', 'Litros (l)'), ('gramos', 'Gramos (g)'),
                   ('miligramos', 'Miligramos (mg)'), ('kilogramos', 'Kilogramos (kg)'), ('libras', 'Libras (lb)'))

TIPOS_DE_USO = (('Interno', 'Interno'), ('Comercial', 'Comercial'),('Interno/Comercial', 'Interno/Comercial'))

class ElementoCategoriaInventario(models.Model):
    categoria = models.ForeignKey(CategoriaInventario, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    codigo = models.CharField(max_length=50, verbose_name='Número de lote', blank=True, null=True)
    codigo_inventario = models.CharField(max_length=50, verbose_name='Código', blank=True, null=True)
    descripcion = models.CharField(max_length=800, verbose_name='Descripción', null=True, blank=True)
    estado = models.CharField(max_length=20, verbose_name="Estado", choices=ESTADOS, default='Activo')
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    precio_venta = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    unidad_medida = models.CharField(max_length=100, verbose_name='Unidad de medida', choices=UNIDADES_MEDIDA, default='ml')
    cantidad_unidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad por unidad', default=0)
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    cantidad_por_unidad = models.BooleanField(default=False)
    precio_por_unidad = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    cantidad_total_unidades = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad total unidades', default=0)
    tipo_uso = models.CharField(max_length=50, verbose_name='Tipo de uso', choices=TIPOS_DE_USO, default='Interno/Comercial')
    fecha_vencimiento = models.DateField(blank=True, null=True)
    stock_minimo = models.PositiveIntegerField(verbose_name='Stock minimo', default=1)
    bodega = models.ForeignKey(Bodega, on_delete=models.CASCADE, default=1)
    precio_compra = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    def save(self, *args, **kwargs):
        if self.cantidad_total_unidades == 0:
            self.cantidad_total_unidades = float(self.cantidad) * float(self.cantidad_unidad)
        if self.foto:
            if self.foto.url.endswith('.png'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(ElementoCategoriaInventario, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        if self.codigo:
            return self.nombre + " - " + self.codigo
        else:
            return self.nombre