from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

# Create your views here.


"""class RegistrarPago(CreateView):
    model = Pago
    template_name = 'pago.html'

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        item_form = todo_itemSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  item_form=item_form))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        item_form = todo_itemSet(self.request.POST)
        if form.is_valid() and item_form.is_valid():
            self.object = form.save()
            item_form.instance = self.object
            item_form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
            self.get_context_data(form=form,
                                  item_form=item_form))


class ListarPagos(ListView):
    model = Pago
    template_name = "listar_pagos.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagos, self).get_context_data(**kwargs)
        context['menu_pagos'] = True
        return context"""
