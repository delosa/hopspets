from gastos.models import Gasto
from productos.models import Pago
from django.db.models import Sum

def utilidades_general(request, date_now):

    print(">>> date_now ", date_now)

    totales_pagos = Pago.objects.filter(fecha__year=date_now).exclude(estado_pago='Anulada').aggregate(Sum('total_factura'))['total_factura__sum']
    totales_gasto = Gasto.objects.filter(fecha__year=date_now).exclude(estado_pago='Anulada').aggregate(Sum('valor_total'))['valor_total__sum']
    utilidadesGenerales = 0

    if totales_pagos is not None and totales_gasto is not None :
      utilidadesGenerales =  totales_pagos - totales_gasto

    utilidades_pagos = Pago.objects.exclude(estado_pago='Anulada').values('fecha__month','estado_pago','total_factura','saldo').annotate(Sum('total_factura')).filter(fecha__year=date_now).order_by('fecha__month')
    utilidades_gastos = Gasto.objects.exclude(estado_pago='Anulada').values('fecha__month','estado_pago','valor_total','saldo').annotate(Sum('valor_total')).filter(fecha__year=date_now).order_by('fecha__month')
    pagos_gastos_mensaje(utilidades_gastos, utilidades_pagos)
    
    contexto =  {'informe': True,
        'title': "Utilidades del año",
        'object_list': {},
        'year':date_now,
        'todos': True,
        'total_ventas': utilidadesGenerales,
        'array_gasto': utilidades_pagos,
        'array_pagos': utilidades_gastos,
        'object_list': pagos_gastos_mensaje(utilidades_gastos, utilidades_pagos),
        'menu_reportes_utilidades': True,
      }
    return contexto


def pagos_gastos_mensaje(arregleGasto, arreglePagos):

    arreglo_mensuale =[
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0},
      {'value': 0,'mes':'---','gasto':0,'venta':0}
      ]
    array_mes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']

    for x in arregleGasto:
      print("x: ")
      print(x)
      index = x['fecha__month'] - 1
      if x['estado_pago'] == 'Pendiente':
          arreglo_mensuale[index]['value'] -= (x['valor_total'] - x['saldo'])
          arreglo_mensuale[index]['gasto'] += (x['valor_total'] - x['saldo'])
      else:
          arreglo_mensuale[index]['value'] -= x['valor_total__sum']
          arreglo_mensuale[index]['gasto'] += x['valor_total__sum']
 
    for x in arreglePagos:

      index = x['fecha__month'] - 1
      if x['estado_pago'] == 'Pendiente':
          arreglo_mensuale[index]['value'] += (x['total_factura'] - x['saldo'])
          arreglo_mensuale[index]['venta'] += (x['total_factura'] - x['saldo'])
      else:
          arreglo_mensuale[index]['value'] += x['total_factura__sum']
          arreglo_mensuale[index]['venta'] += x['total_factura__sum']

    for i in range(0,12):
      arreglo_mensuale[i]['mes'] = array_mes[i]

    #print("arreglo_mensuale:")
    #print(arreglo_mensuale)

    #print("arreglePagos:")
    #print(arreglePagos)
    
    return arreglo_mensuale