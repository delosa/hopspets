from django.urls import path
from . import views

urlpatterns = [
    path('ventas_totales/', views.total_ventas, name="ventas_totales"),
    path('gastos_totales/', views.total_gasto, name="gastos_totales"),
    path('informe_utilidades/', views.informe_utilidades, name="informe_utilidades"),
    path('ingresos_diarios/<str:fecha>/', views.ReporteIngresosDiarios.as_view(), name='ingresos_diarios'),
    path('ingresos_rango_fecha/<str:fecha_inicio>/<str:fecha_fin>/', views.ReporteIngresosRangoFecha.as_view(), name='ingresos_rango_fecha'),
    path('egresos_rango_fecha/<str:fecha_inicio>/<str:fecha_fin>/', views.ReporteEgresosRangoFecha.as_view(), name='egreso_rango_fecha'),
    path('ingresos_mensuales/<str:año>/<str:tipo>/', views.ReporteIngresosMensuales.as_view(), name='ingresos_mensuales'),
    path('egresos_mensuales/<str:año>/<str:tipo>/', views.ReporteEgresosMensuales.as_view(), name='egresos_mensuales'),
    path('actividad_usuario/<str:fecha_inicio>/<str:fecha_fin>/<str:tipo_cita>/<int:id_responsable>/<int:id_area_consulta>/<int:id_propietario>/<int:id_mascota>/', views.ReporteActividadUsuario.as_view(), name='actividad_usuario'),
    path('historial_inventario/<str:fecha_inicio>/<str:fecha_fin>/<str:categoria>/<str:motivo>/', views.ReporteHistorialInventario.as_view(), name='historial_inventario'),
]