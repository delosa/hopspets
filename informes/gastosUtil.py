from gastos.models import Gasto, GastoProducto, GastoMedicamento, GastoVacuna, GastoDesparasitante, GastoServicio, GastoOtro
from django.db.models import Sum


def total_gasto_todos(request, date_now):
    """
        Mauro Castillo gasto
    """
    ingresos_totales_gasto = Gasto.objects.values('fecha__month').annotate(Sum('valor_total')).filter(fecha__year=date_now).order_by('fecha__month')
    totales_gasto= Gasto.objects.filter(fecha__year=date_now).aggregate(Sum('valor_total'))['valor_total__sum']
    
    formato_numero_to_fecha(ingresos_totales_gasto)
    contexto =  {'informe': True,
        'title': "Egresos del año",
        'object_list': formato_numero_to_fecha(ingresos_totales_gasto),
        'year':date_now,
        'todos': True,
        'total_ventas': totales_gasto
      }

    return contexto


def total_gasto_producto(request, date_now):
    """
        Mauro Castillo producto
    """

    ingresos_totales_estetica = GastoProducto.objects.values('gasto__fecha__month').annotate(Sum('gasto__valor_total')).filter(gasto__fecha__year=date_now).order_by('gasto__fecha__month')
    print(">>> ingresos_totales_estetic: ", ingresos_totales_estetica)
    totales_gastos =  GastoProducto.objects.filter(gasto__fecha__year=date_now).aggregate(Sum('gasto__valor_total'))['gasto__valor_total__sum']

    contexto =  {'informe': True,
        'title': "Egresos por productos del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_estetica),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_gastos
      }

    return contexto


def total_gasto_medicamento(request, date_now):
    """
        Mauro Castillo Medicamentos
    """

    ingresos_totales_medicamento= GastoMedicamento.objects.values('gasto__fecha__month').annotate(Sum('gasto__valor_total')).filter(gasto__fecha__year=date_now).order_by('gasto__fecha__month')
    totales_gastos =  GastoMedicamento.objects.filter(gasto__fecha__year=date_now).aggregate(Sum('gasto__valor_total'))['gasto__valor_total__sum']
    contexto =  {'informe': True,
        'title': "Egresos por medicamentos del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_medicamento),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_gastos
      }

    return contexto


def total_gasto_vacuna(request, date_now):
    """
        Mauro Castillo Vacuna
    """

    ingresos_totales_vacuna= GastoVacuna.objects.values('gasto__fecha__month').annotate(Sum('gasto__valor_total')).filter(gasto__fecha__year=date_now).order_by('gasto__fecha__month')
    totales_gastos =  GastoVacuna.objects.filter(gasto__fecha__year=date_now).aggregate(Sum('gasto__valor_total'))['gasto__valor_total__sum']
    contexto =  {'informe': True,
        'title': "Egresos por vacunas del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_vacuna),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_gastos
      }

    return contexto


def total_gasto_desparasitante(request, date_now):
    """
        Mauro Castillo Desparasitante
    """

    ingresos_totales_desparacitantes = GastoDesparasitante.objects.values('gasto__fecha__month').annotate(Sum('gasto__valor_total')).filter(gasto__fecha__year=date_now).order_by('gasto__fecha__month')
    totales_gastos =  GastoDesparasitante.objects.filter(gasto__fecha__year=date_now).aggregate(Sum('gasto__valor_total'))['gasto__valor_total__sum']
    contexto =  {'informe': True,
        'title': "Egresos por desparasitantes del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_desparacitantes),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_gastos
      }

    return contexto


def total_gasto_servicios(request, date_now):
    """
        Mauro Castillo Desparasitante
    """

    ingresos_totales_servicios = GastoServicio.objects.values('gasto__fecha__month').annotate(Sum('gasto__valor_total')).filter(gasto__fecha__year=date_now).order_by('gasto__fecha__month')
    totales_gastos =  GastoServicio.objects.filter(gasto__fecha__year=date_now).aggregate(Sum('gasto__valor_total'))['gasto__valor_total__sum']
    contexto =  {'informe': True,
        'title': "Egresos por servicios del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_servicios),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_gastos
      }

    return contexto


def total_gasto_otros(request, date_now):
    """
        Mauro Castillo Desparasitante
    """

    ingresos_totales_servicios = GastoOtro.objects.values('gasto__fecha__month').annotate(Sum('gasto__valor_total')).filter(gasto__fecha__year=date_now).order_by('gasto__fecha__month')
    totales_gastos = GastoOtro.objects.filter(gasto__fecha__year=date_now).aggregate(Sum('gasto__valor_total'))['gasto__valor_total__sum']
    contexto =  {'informe': True,
        'title': "Egresos por otros del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_servicios),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_gastos
      }

    return contexto
# ------------------------------------- Utilidades -----------------------------------------------------
# Mauro Castillo
# Utilis 
def formato_numero_to_fecha(gastos_list):

    meses_latino = {0:'no definido', 1:'Enero', 2:'Febrero',3:'Marzo', 4:'Abril', 5:'Mayo', 6:'Junio', 7:'Julio', 8:'Agosto', 9:'Septiembre', 10:'Octubre', 11:'Noviembre', 12:'Diciembre'}
    salida = []
    for x in gastos_list:
        x['mes_latino'] = meses_latino[x['fecha__month']]
        x['index'] = x['fecha__month']
        salida.append(x)

    return salida

def formato_numero_to_fecha_tercero(gastos_list):

    meses_latino = {0:'no definido', 1:'Enero', 2:'Febrero',3:'Marzo', 4:'Abril', 5:'Mayo', 6:'Junio', 7:'Julio', 8:'Agosto', 9:'Septiembre', 10:'Octubre', 11:'Noviembre', 12:'Diciembre'}
    salida = []
    print(" >>> Gasto list <<< ", gastos_list)

    for x in gastos_list:
        x['mes_latino'] = meses_latino[x['gasto__fecha__month']]
        x['index'] = x['gasto__fecha__month']
        x['valor_total__sum'] = x['gasto__valor_total__sum']
        salida.append(x)

    return salida