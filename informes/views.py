from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
import datetime
from . import pagosUtil
from . import gastosUtil
from . import utilidades
from django.views.generic import TemplateView
from productos.models import *
from django.db.models import Sum
from gastos.models import *
from desparasitacion.models import *
from insumos.models import *
from vacunacion.models import *
from citas.models import *
from tratamientos.models import *
from categoriasInventario.models import *
import random
import json
from django.core.serializers.json import DjangoJSONEncoder
from itertools import chain
from django.core import serializers


#@method_decorator(login_required)
def total_ventas(request):

    """
        Mauro Castillo totales
    """
    date_now = datetime.datetime.now().year
    datetime.datetime.now()
    type_informe = '1'
    contexto = {}

    if request.method == 'GET':
        date_now = request.GET.get('fecha_consulta')
        type_informe = request.GET.get('tipo_informe')

        if type_informe is None or type_informe == '':
            type_informe = '1'
        
        if date_now is None or date_now == '':
            date_now= datetime.datetime.now().year


    if type_informe == '1':
        contexto = pagosUtil.total_ventas_todos(request, date_now)

    if type_informe == '2':
        contexto = pagosUtil.total_ventas_estetica(request, date_now)

    if type_informe == '3':
        contexto = pagosUtil.total_ventas_producto(request, date_now)

    if type_informe == '4':
        contexto = pagosUtil.total_ventas_vacunacion(request, date_now)

    if type_informe == '5':
        contexto = pagosUtil.total_ventas_tratamientos(request, date_now)
    
    if type_informe == '6':
        contexto = pagosUtil.total_ventas_desparitacion(request, date_now)

    if type_informe == '7':
        contexto = pagosUtil.total_ventas_guarderia(request, date_now)
        
    #print(">>> contexto: ", contexto)
    return render(request, 'informe_tabla.html', contexto)

#@method_decorator(login_required)
def total_gasto(request):
    
    """
        Mauro Castillo gastos
    """
    date_now = datetime.datetime.now().year
    datetime.datetime.now()
    type_informe = '1'
    contexto = {}

    if request.method == 'GET':
        date_now = request.GET.get('fecha_consulta')
        type_informe = request.GET.get('tipo_informe')

        if type_informe is None or type_informe == '':
            type_informe = '1'
        
        if date_now is None or date_now == '':
            date_now= datetime.datetime.now().year


    if type_informe == '1':
        contexto = gastosUtil.total_gasto_todos(request, date_now)

    if type_informe == '2':
        contexto = gastosUtil.total_gasto_producto(request, date_now)

    if type_informe == '3':
        contexto = gastosUtil.total_gasto_medicamento(request, date_now)

    if type_informe == '4':
        contexto = gastosUtil.total_gasto_vacuna(request, date_now)

    if type_informe == '5':
        contexto = gastosUtil.total_gasto_servicios(request, date_now)
    
    if type_informe == '6':
        contexto = gastosUtil.total_gasto_desparasitante(request, date_now)

    if type_informe == '7':
        contexto = gastosUtil.total_gasto_servicios(request, date_now)

    if type_informe == '8':
        contexto = gastosUtil.total_gasto_otros(request, date_now)

    return render(request, 'informe_gasto.html', contexto)


def informe_utilidades(request):
    
    """
        Mauro Castillo utilidades
    """
    date_now = datetime.datetime.now().year
    
    if request.method == 'GET':
        date_now = request.GET.get('fecha_consulta')
        
    
    if date_now is None or date_now == '':
            date_now= datetime.datetime.now().year
    
    contexto = utilidades.utilidades_general(request, date_now)
    return render(request, 'informe_utilidades.html', contexto)


class ReporteIngresosDiarios(TemplateView):
    template_name = 'ingresos_diarios.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteIngresosDiarios, self).get_context_data(**kwargs)
        
        fecha = self.kwargs['fecha']
        
        ingresos_hoy_productos = PagoProducto.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_medicamentos = PagoMedicamento.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_vacunas = PagoVacuna.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_desparasitantes = PagoDesparasitante.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_atencion_cita = PagoTratamiento.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_atencion_estetica = PagoEstetica.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_atencion_vacunacion = PagoVacunacion.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_atencion_desparasitacion = PagoDesparasitacion.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_atencion_guarderia = PagoGuarderia.objects.filter(pago__fecha=datetime.datetime.strptime(fecha,'%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_areas_consulta = PagoAreaConsulta.objects.filter(pago__fecha=datetime.datetime.strptime(fecha, '%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        ingresos_hoy_otros = PagoOtro.objects.filter(pago__fecha=datetime.datetime.strptime(fecha, '%Y-%m-%d').date()).exclude(pago__estado_pago='Anulada')
        pagos_hoy = Pago.objects.filter(fecha=datetime.datetime.strptime(fecha, '%Y-%m-%d').date()).exclude(estado_pago='Anulada')

        total_ingresos_hoy_productos = 0
        total_ingresos_hoy_medicamentos = 0
        total_ingresos_hoy_vacunas = 0
        total_ingresos_hoy_desparasitantes = 0
        total_ingresos_hoy_atencion_cita = 0
        total_ingresos_hoy_atencion_estetica = 0
        total_ingresos_hoy_atencion_vacunacion = 0
        total_ingresos_hoy_atencion_desparasitacion = 0
        total_ingresos_hoy_atencion_guarderia = 0
        total_ingresos_hoy_areas_consulta = 0
        total_ingresos_hoy_otros = 0
        total_descuentos_hoy = 0

        suma_total_ingresos_hoy = 0

        array_ingresos = []
        
        for ingreso in ingresos_hoy_productos:
            total_ingresos_hoy_productos += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_medicamentos:
            total_ingresos_hoy_medicamentos += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_vacunas:
            total_ingresos_hoy_vacunas += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_desparasitantes:
            total_ingresos_hoy_desparasitantes += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_atencion_cita:
            total_ingresos_hoy_atencion_cita += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_atencion_estetica:
            total_ingresos_hoy_atencion_estetica += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_atencion_vacunacion:
            total_ingresos_hoy_atencion_vacunacion += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_atencion_desparasitacion:
            total_ingresos_hoy_atencion_desparasitacion += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_atencion_guarderia:
            total_ingresos_hoy_atencion_guarderia += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_areas_consulta:
            total_ingresos_hoy_areas_consulta += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in ingresos_hoy_otros:
            total_ingresos_hoy_otros += ingreso.total
            suma_total_ingresos_hoy += ingreso.total

        for ingreso in pagos_hoy:
            total_descuentos_hoy -= ingreso.descuento
            suma_total_ingresos_hoy -= ingreso.descuento

        context['menu_reporte_ingresos_diarios'] = True
        context['total_ingresos_hoy_productos'] = total_ingresos_hoy_productos
        context['total_ingresos_hoy_medicamentos'] = total_ingresos_hoy_medicamentos
        context['total_ingresos_hoy_vacunas'] = total_ingresos_hoy_vacunas
        context['total_ingresos_hoy_desparasitantes'] = total_ingresos_hoy_desparasitantes
        context['total_ingresos_hoy_atencion_cita'] = total_ingresos_hoy_atencion_cita
        context['total_ingresos_hoy_atencion_estetica'] = total_ingresos_hoy_atencion_estetica
        context['total_ingresos_hoy_atencion_vacunacion'] = total_ingresos_hoy_atencion_vacunacion
        context['total_ingresos_hoy_atencion_desparasitacion'] = total_ingresos_hoy_atencion_desparasitacion
        context['total_ingresos_hoy_atencion_guarderia'] = total_ingresos_hoy_atencion_guarderia
        context['total_ingresos_hoy_areas_consulta'] = total_ingresos_hoy_areas_consulta
        context['total_ingresos_hoy_otros'] = total_ingresos_hoy_otros
        context['total_descuentos_hoy'] = total_descuentos_hoy
        context['suma_total_ingresos_hoy'] = suma_total_ingresos_hoy

        array_ingresos.append(int(total_ingresos_hoy_productos))
        array_ingresos.append(int(total_ingresos_hoy_medicamentos))
        array_ingresos.append(int(total_ingresos_hoy_vacunas))
        array_ingresos.append(int(total_ingresos_hoy_desparasitantes))
        array_ingresos.append(int(total_ingresos_hoy_atencion_cita))
        array_ingresos.append(int(total_ingresos_hoy_atencion_vacunacion))
        array_ingresos.append(int(total_ingresos_hoy_atencion_desparasitacion))
        array_ingresos.append(int(total_ingresos_hoy_atencion_estetica))
        array_ingresos.append(int(total_ingresos_hoy_atencion_guarderia))
        array_ingresos.append(int(total_ingresos_hoy_areas_consulta))
        array_ingresos.append(int(total_ingresos_hoy_otros))
        array_ingresos.append(int(total_descuentos_hoy))

        array_labels = []

        array_labels.append('Venta de productos')
        array_labels.append('Venta de medicamentos')
        array_labels.append('Venta de vacunas')
        array_labels.append('Venta de desparasitantes')
        array_labels.append('Atencion de citas medicas')
        array_labels.append('Aplicacion de vacunaciones')
        array_labels.append('Aplicacion de desparasitantes')
        array_labels.append('Estaticas')
        array_labels.append('Guarderias')
        array_labels.append('Procedimientos (Areas de consulta)')
        array_labels.append('Otros')
        array_labels.append('Descuentos')

        context['fecha'] = fecha
        context['array_ingresos'] = array_ingresos
        context['array_labels'] = array_labels
        
        return context


class ReporteIngresosMensuales(TemplateView):
    template_name = 'ingresos_mensuales.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteIngresosMensuales, self).get_context_data(**kwargs)

        año = self.kwargs['año']
        tipo_venta = self.kwargs['tipo']
        meses = [['Enero'],['Febrero'],['Marzo'],['Abril'],['Mayo'],['Junio'],['Julio'],['Agosto'],['Septiembre'],['Octubre'],['Noviembre'],['Diciembre']]
        ventas_por_cada_mes = []

        total_ventas_año = 0

        if tipo_venta == 'generales':
            for i in range(12):
                total_ventas_mes = 0

                ventas_del_mes = Pago.objects.filter(fecha__year=año, fecha__month=i + 1).exclude(estado_pago='Anulada')
                abonos_del_mes = AbonoPago.objects.filter(fecha__year=año, fecha__month=i + 1)

                for venta in ventas_del_mes:
                    if PagoProducto.objects.filter(pago__id=venta.id).exists() == False:
                        if PagoMedicamento.objects.filter(pago__id=venta.id).exists() == False:
                            if PagoVacuna.objects.filter(pago__id=venta.id).exists() == False:
                                if PagoDesparasitante.objects.filter(pago__id=venta.id).exists() == False:
                                    if PagoTratamiento.objects.filter(pago__id=venta.id).exists() == False:
                                        if PagoEstetica.objects.filter(pago__id=venta.id).exists() == False:
                                            if PagoVacunacion.objects.filter(pago__id=venta.id).exists() == False:
                                                if PagoDesparasitacion.objects.filter(pago__id=venta.id).exists() == False:
                                                    if PagoGuarderia.objects.filter(pago__id=venta.id).exists() == False:
                                                        if PagoAreaConsulta.objects.filter(pago__id=venta.id).exists() == False:
                                                            if PagoOtro.objects.filter(pago__id=venta.id).exists() == False:
                                                                if PagoElementoInventario.objects.filter(pago__id=venta.id).exists() == False:
                                                                    ventas_del_mes = ventas_del_mes.exclude(id=venta.id)
                for venta in ventas_del_mes:
                    if venta.estado_pago == 'Pagado':
                        if venta.forma_pago == 'Debito':
                            total_ventas_mes += venta.total_factura
                        else:
                            suma_abonos = 0
                            for abono in AbonoPago.objects.filter(pago=venta, fecha__year=año, fecha__month= i+1):
                                suma_abonos += abono.valor_abonado
                            if suma_abonos == venta.total_factura:
                                total_ventas_mes += venta.total_factura
                            else:
                                total_ventas_mes += suma_abonos
                    else:
                        suma_abonos = 0
                        for abono in AbonoPago.objects.filter(pago=venta, fecha__year=año, fecha__month= i+1):
                            suma_abonos += abono.valor_abonado
                        total_ventas_mes += suma_abonos

                    if abonos_del_mes.filter(pago=venta).exists():
                        abonos_del_mes = abonos_del_mes.exclude(pago=venta)

                meses[i].append(total_ventas_mes)
                ventas_por_cada_mes.append(float(total_ventas_mes))

                total_ventas_año += total_ventas_mes

            context['titulo'] = 'Ingresos generales'
            context['tipo_filtro'] = tipo_venta
        else:

            for i in range(12):
                total_ventas_mes = 0
                abonos_del_mes = AbonoPago.objects.filter(fecha__year=año, fecha__month=i + 1)
                ventas_del_mes = []

                if tipo_venta == 'esteticas':
                    ventas_del_mes = PagoEstetica.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por esteticas'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'productos':
                    ventas_del_mes = PagoProducto.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por productos'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'medicamentos':
                    ventas_del_mes = PagoMedicamento.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por medicamentos'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'vacunas':
                    ventas_del_mes = PagoVacuna.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por vacunas'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'desparasitantes':
                    ventas_del_mes = PagoDesparasitante.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por desparasitantes'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'citas':
                    ventas_del_mes = PagoTratamiento.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por citas medicas'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'vacunaciones':
                    ventas_del_mes = PagoVacunacion.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por vacunaciones'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'desparasitaciones':
                    ventas_del_mes = PagoDesparasitacion.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por desparasitaciones'
                    context['tipo_filtro'] = tipo_venta

                elif tipo_venta == 'guarderias':
                    ventas_del_mes = PagoGuarderia.objects.filter(pago__fecha__year=año, pago__fecha__month=i + 1)
                    context['titulo'] = 'Ingresos por guarderias'
                    context['tipo_filtro'] = tipo_venta

                else:
                    ventas_del_mes = PagoElementoInventario.objects.filter(elemento__categoria__id=tipo_venta,pago__fecha__year=año, pago__fecha__month=i + 1)
                    categoria = CategoriaInventario.objects.get(id=tipo_venta)
                    context['titulo'] = 'Ingresos por '+ str(categoria.nombre)
                    context['tipo_filtro'] = categoria.nombre

                for venta in ventas_del_mes:
                    if venta.pago.estado_pago == 'Pagado':
                        if venta.pago.forma_pago == 'Debito':
                            total_ventas_mes += venta.total
                        else:
                            suma_abonos = 0
                            for abono in AbonoPago.objects.filter(pago=venta.id, fecha__year=año, fecha__month= i+1):
                                suma_abonos += abono.valor_abonado
                            if suma_abonos == venta.total:
                                total_ventas_mes += venta.total
                            else:
                                total_ventas_mes += suma_abonos

                meses[i].append(total_ventas_mes)
                ventas_por_cada_mes.append(float(total_ventas_mes))

                total_ventas_año += total_ventas_mes

        context['total_ventas_año'] = total_ventas_año
        context['menu_reportes_ingresos_mensuales'] = True
        context['año'] = año
        context['tipo'] = tipo_venta
        context['meses'] = meses
        context['ventas_por_cada_mes'] = ventas_por_cada_mes
        context['categorias'] = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))

        return context


class ReporteEgresosMensuales(TemplateView):
    template_name = 'egresos_mensuales.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteEgresosMensuales, self).get_context_data(**kwargs)

        año = self.kwargs['año']
        tipo_egreso = self.kwargs['tipo']
        meses = [['Enero'],['Febrero'],['Marzo'],['Abril'],['Mayo'],['Junio'],['Julio'],['Agosto'],['Septiembre'],['Octubre'],['Noviembre'],['Diciembre']]
        egresos_por_cada_mes = []

        total_egresos_año = 0

        if tipo_egreso == 'generales':
            for i in range(12):
                total_egresos_mes = 0

                egresos_del_mes = Gasto.objects.filter(fecha__year=año, fecha__month=i + 1)

                for egreso in egresos_del_mes:
                    if egreso.estado_pago == 'Pagado':
                        total_egresos_mes += egreso.valor_total
                    if egreso.estado_pago == 'Pendiente':
                        total_egresos_mes += egreso.valor_total - egreso.saldo
                meses[i].append(total_egresos_mes)
                egresos_por_cada_mes.append(int(total_egresos_mes))

                total_egresos_año += total_egresos_mes

            context['titulo'] = 'Egresos generales'
            context['tipo_filtro'] = tipo_egreso
        else:

            for i in range(12):
                total_egresos_mes = 0

                egresos_del_mes = []

                if tipo_egreso == 'productos':
                    egresos_del_mes = GastoProducto.objects.filter(gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    context['titulo'] = 'Egresos por productos'
                    context['tipo_filtro'] = tipo_egreso

                elif tipo_egreso == 'medicamentos':
                    egresos_del_mes = GastoMedicamento.objects.filter(gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    context['titulo'] = 'Egresos por medicamentos'
                    context['tipo_filtro'] = tipo_egreso

                elif tipo_egreso == 'vacunas':
                    egresos_del_mes = GastoVacuna.objects.filter(gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    context['titulo'] = 'Egresos por vacunas'
                    context['tipo_filtro'] = tipo_egreso

                elif tipo_egreso == 'desparasitantes':
                    egresos_del_mes = GastoDesparasitante.objects.filter(gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    context['titulo'] = 'Egresos por desparasitantes'
                    context['tipo_filtro'] = tipo_egreso

                elif tipo_egreso == 'servicios':
                    egresos_del_mes = GastoServicio.objects.filter(gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    context['titulo'] = 'Egresos por servicios'
                    context['tipo_filtro'] = tipo_egreso

                elif tipo_egreso == 'otros':
                    egresos_del_mes = GastoOtro.objects.filter(gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    context['titulo'] = 'Egresos por otros'
                    context['tipo_filtro'] = tipo_egreso

                else:
                    egresos_del_mes = GastoElementoInventario.objects.filter(elemento__categoria__id=tipo_egreso,gasto__fecha__year=año, gasto__fecha__month=i + 1)
                    categoria = CategoriaInventario.objects.get(id=tipo_egreso)
                    context['titulo'] = 'Egresos por '+ str(categoria.nombre)
                    context['tipo_filtro'] = categoria.nombre

                for egreso in egresos_del_mes:
                    if egreso.gasto.estado_pago == 'Pagado':
                        total_egresos_mes += egreso.precio_total
                meses[i].append(total_egresos_mes)
                egresos_por_cada_mes.append(int(total_egresos_mes))

                total_egresos_año += total_egresos_mes

        context['total_egresos_año'] = total_egresos_año
        context['menu_reportes_egresos_mensuales'] = True
        context['año'] = año
        context['tipo'] = tipo_egreso
        context['meses'] = meses
        context['egresos_por_cada_mes'] = egresos_por_cada_mes
        context['categorias'] = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))

        return context

class ReporteIngresosRangoFecha(TemplateView):
    template_name = 'ingresos_rango_fecha.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteIngresosRangoFecha, self).get_context_data(**kwargs)
        
        fecha_inicio = datetime.datetime.strptime(self.kwargs['fecha_inicio'],'%Y-%m-%d').date()
        fecha_fin = datetime.datetime.strptime(self.kwargs['fecha_fin'],'%Y-%m-%d').date()
        
        ingresos_productos = PagoProducto.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_medicamentos = PagoMedicamento.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_vacunas = PagoVacuna.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_desparasitantes = PagoDesparasitante.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_categorias = PagoElementoInventario.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_atencion_cita = PagoTratamiento.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_atencion_estetica = PagoEstetica.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_atencion_vacunacion = PagoVacunacion.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_atencion_desparasitacion = PagoDesparasitacion.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_atencion_guarderia = PagoGuarderia.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_areas_consulta = PagoAreaConsulta.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_otros = PagoOtro.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente')
        ingresos_abonos = AbonoPago.objects.filter(fecha__range=[fecha_inicio, fecha_fin]).exclude(pago__estado_pago='Anulada')
        pagos = Pago.objects.exclude(estado_pago='Anulada')

        categorias = CategoriaInventario.objects.filter(estado=True)

        suma_total_ingresos = 0

        total_ingresos_productos = 0
        total_ingresos_medicamentos = 0
        total_ingresos_vacunas = 0
        total_ingresos_desparasitantes = 0

        total_ingresos_categorias = []
        for categoria in categorias:
            total_ingresos = 0
            for ingreso in PagoElementoInventario.objects.exclude(pago__estado_pago='Anulada').exclude(pago__estado_pago='Pendiente').filter(elemento__categoria=categoria).filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
                total_ingresos += ingreso.total
            total_ingresos_categorias.append(total_ingresos)
            suma_total_ingresos += total_ingresos

        total_ingresos_atencion_cita = 0
        total_ingresos_atencion_estetica = 0
        total_ingresos_atencion_vacunacion = 0
        total_ingresos_atencion_desparasitacion = 0
        total_ingresos_atencion_guarderia = 0
        total_ingresos_areas_consulta = 0
        total_ingresos_otros = 0
        total_ingresos_abonos = 0
        total_descuentos = 0

        array_ingresos = []
        
        for ingreso in ingresos_productos.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_productos += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_medicamentos.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_medicamentos += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_vacunas.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_vacunas += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_desparasitantes.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_desparasitantes += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_atencion_cita.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_atencion_cita += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_atencion_estetica.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_atencion_estetica += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_atencion_vacunacion.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_atencion_vacunacion += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_atencion_desparasitacion.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_atencion_desparasitacion += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_atencion_guarderia.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_atencion_guarderia += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_areas_consulta.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_areas_consulta += ingreso.total
            suma_total_ingresos += ingreso.total

        for ingreso in ingresos_otros.filter(pago__forma_pago='Debito').filter(pago__fecha__range=[fecha_inicio, fecha_fin]):
            total_ingresos_otros += ingreso.total
            suma_total_ingresos += ingreso.total

        for pago in pagos.filter(forma_pago='Debito').filter(fecha__range=[fecha_inicio, fecha_fin]):
            total_descuentos -= pago.descuento
            suma_total_ingresos -= pago.descuento

        for pago_pendiente in pagos.filter(estado_pago='Pendiente'):
            for abono in AbonoPago.objects.filter(pago=pago_pendiente):
                if fecha_inicio <= abono.fecha.date() and abono.fecha.date() <= fecha_fin:
                    total_ingresos_abonos += abono.valor_abonado
                    suma_total_ingresos += abono.valor_abonado


        for pago_completo in pagos.filter(estado_pago='Pagado',forma_pago='Credito'):

            suma_abonos = 0
            for abono in AbonoPago.objects.filter(pago=pago_completo):
                if fecha_inicio <= abono.fecha.date() and abono.fecha.date() <= fecha_fin:
                    suma_abonos += abono.valor_abonado

            if suma_abonos == pago_completo.total_factura:

                for ingreso in ingresos_productos.filter(pago=pago_completo):
                    total_ingresos_productos += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_medicamentos.filter(pago=pago_completo):
                    total_ingresos_medicamentos += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_vacunas.filter(pago=pago_completo):
                    total_ingresos_vacunas += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_desparasitantes.filter(pago=pago_completo):
                    total_ingresos_desparasitantes += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_categorias.filter(pago=pago_completo):
                    for index,categoria in enumerate(categorias):
                        if ingreso.elemento.categoria == categoria:
                            total_ingresos_categorias[index] += ingreso.total
                            suma_total_ingresos += ingreso.total

                for ingreso in ingresos_atencion_cita.filter(pago=pago_completo):
                    total_ingresos_atencion_cita += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_atencion_estetica.filter(pago=pago_completo):
                    total_ingresos_atencion_estetica += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_atencion_vacunacion.filter(pago=pago_completo):
                    total_ingresos_atencion_vacunacion += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_atencion_desparasitacion.filter(pago=pago_completo):
                    total_ingresos_atencion_desparasitacion += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_atencion_guarderia.filter(pago=pago_completo):
                    total_ingresos_atencion_guarderia += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_areas_consulta.filter(pago=pago_completo):
                    total_ingresos_areas_consulta += ingreso.total
                    suma_total_ingresos += ingreso.total

                for ingreso in ingresos_otros.filter(pago=pago_completo):
                    total_ingresos_otros += ingreso.total
                    suma_total_ingresos += ingreso.total

                total_descuentos -= pago_completo.descuento
                suma_total_ingresos -= pago_completo.descuento

            else:
                total_ingresos_abonos += suma_abonos
                suma_total_ingresos += suma_abonos


        context['menu_reportes_ingresos_rango_fecha'] = True
        context['total_ingresos_productos'] = total_ingresos_productos
        context['total_ingresos_medicamentos'] = total_ingresos_medicamentos
        context['total_ingresos_vacunas'] = total_ingresos_vacunas
        context['total_ingresos_desparasitantes'] = total_ingresos_desparasitantes
        context['total_ingresos_categorias'] = total_ingresos_categorias
        context['total_ingresos_atencion_cita'] = total_ingresos_atencion_cita
        context['total_ingresos_atencion_estetica'] = total_ingresos_atencion_estetica
        context['total_ingresos_atencion_vacunacion'] = total_ingresos_atencion_vacunacion
        context['total_ingresos_atencion_desparasitacion'] = total_ingresos_atencion_desparasitacion
        context['total_ingresos_atencion_guarderia'] = total_ingresos_atencion_guarderia
        context['total_ingresos_areas_consulta'] = total_ingresos_areas_consulta
        context['total_ingresos_otros'] = total_ingresos_otros
        context['total_ingresos_abonos'] = total_ingresos_abonos
        context['total_descuentos'] = total_descuentos
        context['suma_total_ingresos'] = suma_total_ingresos

        array_ingresos.append(int(total_ingresos_productos))
        array_ingresos.append(int(total_ingresos_medicamentos))
        array_ingresos.append(int(total_ingresos_vacunas))
        array_ingresos.append(int(total_ingresos_desparasitantes))
        for total_ingreso_categoria in total_ingresos_categorias:
            array_ingresos.append(int(total_ingreso_categoria))
        array_ingresos.append(int(total_ingresos_atencion_cita))
        array_ingresos.append(int(total_ingresos_atencion_vacunacion))
        array_ingresos.append(int(total_ingresos_atencion_desparasitacion))
        array_ingresos.append(int(total_ingresos_atencion_estetica))
        array_ingresos.append(int(total_ingresos_atencion_guarderia))
        array_ingresos.append(int(total_ingresos_areas_consulta))
        array_ingresos.append(int(total_ingresos_otros))
        array_ingresos.append(int(total_ingresos_abonos))
        array_ingresos.append(int(total_descuentos))

        context['fecha_inicio'] = self.kwargs['fecha_inicio']
        context['fecha_fin'] = self.kwargs['fecha_fin']
        context['array_ingresos'] = array_ingresos



        context['categorias'] = categorias

        labels_grafico = []

        labels_grafico.append('Venta de productos')
        labels_grafico.append('Venta de medicamentos')
        labels_grafico.append('Venta de vacunas')
        labels_grafico.append('Venta de desparasitantes')

        for categoria in categorias:
            labels_grafico.append('Venta de '+categoria.nombre)

        labels_grafico.append('Atencion de citas medicas')
        labels_grafico.append('Aplicacion de vacunaciones')
        labels_grafico.append('Aplicacion de desparasitantes')
        labels_grafico.append('Estética')
        labels_grafico.append('Guardería')
        labels_grafico.append('Procedimiento (Area de consulta)')
        labels_grafico.append('Otros')
        labels_grafico.append('Abonos')
        labels_grafico.append('Descuentos')

        context['labels_grafico'] = labels_grafico

        'rgba(255, 99, 132, 0.2)'

        backgroundColors = []

        for label in labels_grafico:
            backgroundColors.append('rgba('+str(random.randint(1,255))+', '+str(random.randint(1,255))+', '+str(random.randint(1,255))+', 0.2)')

        context['backgroundColors'] = backgroundColors
        
        return context

class ReporteEgresosRangoFecha(TemplateView):
    template_name = 'egresos_rango_fecha.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteEgresosRangoFecha, self).get_context_data(**kwargs)
        
        fecha_inicio = datetime.datetime.strptime(self.kwargs['fecha_inicio'],'%Y-%m-%d').date()
        fecha_fin = datetime.datetime.strptime(self.kwargs['fecha_fin'],'%Y-%m-%d').date()
        
        egresos_productos = GastoProducto.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_medicamentos = GastoMedicamento.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_vacunas = GastoVacuna.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_desparasitantes = GastoDesparasitante.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_categorias = GastoElementoInventario.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_insumos = GastoInsumo.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_servicios = GastoServicio.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_otros = GastoOtro.objects.exclude(gasto__estado_pago='Anulada').exclude(gasto__estado_pago='Pendiente')
        egresos_abonos = AbonoGasto.objects.filter(fecha__range=[fecha_inicio, fecha_fin]).exclude(gasto__estado_pago='Anulada')
        gastos = Gasto.objects.exclude(estado_pago='Anulada')

        categorias = CategoriaInventario.objects.filter(estado=True)

        suma_total_egresos = 0

        total_egresos_productos = 0
        total_egresos_medicamentos = 0
        total_egresos_vacunas = 0
        total_egresos_desparasitantes = 0
        total_egresos_insumos = 0

        total_egresos_categorias = []
        for categoria in categorias:
            total_egresos = 0
            for egreso in GastoElementoInventario.objects.exclude(gasto__estado_pago='Anulada').exclude(
                    gasto__estado_pago='Pendiente').filter(elemento__categoria=categoria).filter(
                    gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
                total_egresos += egreso.precio_total
            total_egresos_categorias.append(total_egresos)
            suma_total_egresos += total_egresos

        total_egresos_servicios = 0
        total_egresos_otros = 0
        total_egresos_abonos = 0
        total_descuentos = 0


        array_egresos = []
        
        for egreso in egresos_productos.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_productos += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in egresos_medicamentos.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_medicamentos += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in egresos_vacunas.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_vacunas += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in egresos_desparasitantes.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_desparasitantes += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in egresos_insumos.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_insumos += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in egresos_servicios.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_servicios += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in egresos_otros.filter(gasto__forma_pago='Debito').filter(gasto__fecha__range=[fecha_inicio, fecha_fin]):
            total_egresos_otros += egreso.precio_total
            suma_total_egresos += egreso.precio_total

        for egreso in gastos.filter(forma_pago='Debito').filter(fecha__range=[fecha_inicio, fecha_fin]):
            total_descuentos -= egreso.descuento
            suma_total_egresos -= egreso.descuento

        for gasto_pendiente in gastos.filter(estado_pago='Pendiente'):
            for abono in AbonoGasto.objects.filter(gasto=gasto_pendiente):
                if fecha_inicio <= abono.fecha.date() and abono.fecha.date() <= fecha_fin:
                    total_egresos_abonos += abono.valor_abonado
                    suma_total_egresos += abono.valor_abonado

        for gasto_completo in gastos.filter(estado_pago='Pagado',forma_pago='Credito'):
            suma_abonos_egresos = 0
            for abono in AbonoGasto.objects.filter(gasto=gasto_completo):
                if fecha_inicio <= abono.fecha.date() and abono.fecha.date() <= fecha_fin:
                    suma_abonos_egresos += abono.valor_abonado

            if suma_abonos_egresos == gasto_completo.valor_total:

                for egreso in egresos_productos.filter(gasto=gasto_completo):
                    total_egresos_productos += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                for egreso in egresos_medicamentos.filter(gasto=gasto_completo):
                    total_egresos_medicamentos += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                for egreso in egresos_vacunas.filter(gasto=gasto_completo):
                    total_egresos_vacunas += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                for egreso in egresos_desparasitantes.filter(gasto=gasto_completo):
                    total_egresos_desparasitantes += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                for egreso in egresos_insumos.filter(gasto=gasto_completo):
                    total_egresos_insumos += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                for egreso in egresos_categorias.filter(gasto=gasto_completo):
                    for index,categoria in enumerate(categorias):
                        if egreso.elemento.categoria == categoria:
                            total_egresos_categorias[index] += egreso.precio_total
                            suma_total_egresos += egreso.precio_total

                for egreso in egresos_servicios.filter(gasto=gasto_completo):
                    total_egresos_servicios += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                for egreso in egresos_otros.filter(gasto=gasto_completo):
                    total_egresos_otros += egreso.precio_total
                    suma_total_egresos += egreso.precio_total

                total_descuentos -= gasto_completo.descuento
                suma_total_egresos -= gasto_completo.descuento

            else:
                total_egresos_abonos += suma_abonos_egresos
                suma_total_egresos += suma_abonos_egresos

        context['menu_reportes_egresos_rango_fecha'] = True
        context['total_egresos_productos'] = total_egresos_productos
        context['total_egresos_medicamentos'] = total_egresos_medicamentos
        context['total_egresos_vacunas'] = total_egresos_vacunas
        context['total_egresos_desparasitantes'] = total_egresos_desparasitantes
        context['total_egresos_insumos'] = total_egresos_insumos
        context['total_egresos_categorias'] = total_egresos_categorias
        context['total_egresos_servicios'] = total_egresos_servicios
        context['total_egresos_otros'] = total_egresos_otros
        context['total_egresos_abonos'] = total_egresos_abonos
        context['total_descuentos'] = total_descuentos
        context['suma_total_egresos'] = suma_total_egresos

        array_egresos.append(int(total_egresos_productos))
        array_egresos.append(int(total_egresos_medicamentos))
        array_egresos.append(int(total_egresos_vacunas))
        array_egresos.append(int(total_egresos_desparasitantes))
        array_egresos.append(int(total_egresos_insumos))
        for total_egreso_categoria in total_egresos_categorias:
            array_egresos.append(int(total_egreso_categoria))
        array_egresos.append(int(total_egresos_servicios))
        array_egresos.append(int(total_egresos_otros))
        array_egresos.append(int(total_egresos_abonos))
        array_egresos.append(int(total_descuentos))



        context['fecha_inicio'] = self.kwargs['fecha_inicio']
        context['fecha_fin'] = self.kwargs['fecha_fin']
        context['array_egresos'] = array_egresos

        context['categorias'] = categorias

        labels_grafico = []

        labels_grafico.append('Compras de Productos')
        labels_grafico.append('Compras de Medicamentos')
        labels_grafico.append('Compras de Vacunas')
        labels_grafico.append('Compras de Desparasitantes')
        labels_grafico.append('Compras de Insumo')

        for categoria in categorias:
            labels_grafico.append('Compras de ' + categoria.nombre)

        labels_grafico.append('Servicios')
        labels_grafico.append('Otros')
        labels_grafico.append('Abonos')
        labels_grafico.append('Descuentos')

        context['labels_grafico'] = labels_grafico

        'rgba(255, 99, 132, 0.2)'

        backgroundColors = []

        for label in labels_grafico:
            backgroundColors.append(
                'rgba(' + str(random.randint(1, 255)) + ', ' + str(random.randint(1, 255)) + ', ' + str(
                    random.randint(1, 255)) + ', 0.2)')

        context['backgroundColors'] = backgroundColors
        
        return context

class ReporteActividadUsuario(TemplateView):
    template_name = 'actividad_usuario.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteActividadUsuario, self).get_context_data(**kwargs)

        context['menu_reportes_actividades_usuarios'] = True

        fecha_inicio = datetime.datetime.strptime(self.kwargs['fecha_inicio'],'%Y-%m-%d').date()
        fecha_fin = datetime.datetime.strptime(self.kwargs['fecha_fin'],'%Y-%m-%d').date()
        tipo_cita = self.kwargs['tipo_cita']
        id_responsable = self.kwargs['id_responsable']
        id_area_consulta = self.kwargs['id_area_consulta']
        id_propietario = self.kwargs['id_propietario']
        id_mascota = self.kwargs['id_mascota']

        context['responsables'] = Usuario.objects.all()
        context['areas_consulta'] = Tratamiento.objects.all()
        context['all_propietarios'] = Cliente.objects.all()
        if id_propietario != 0:
            context['all_mascotas'] = Mascota.objects.all().filter(cliente=id_propietario)
        else:
            context['all_mascotas'] = Mascota.objects.all()

        if id_mascota != 0:
            mi_mascota = Mascota.objects.get(id=id_mascota)
            context['all_mascotas'] = Mascota.objects.all().filter(cliente=mi_mascota.cliente.id)
            id_propietario = mi_mascota.cliente.id

        context['tipo_cita'] = tipo_cita
        context['id_responsable'] = id_responsable
        context['id_area_consulta'] = id_area_consulta
        context['id_propietario'] = id_propietario
        context['id_mascota'] = id_mascota

        citas_esteticas = []
        citas_desparasitacion = []
        aplicacion_desp = []
        insumos_desp = []
        citas_guarderias = []
        pago_guarderia = []
        citas_vacunacion = []
        aplicacion_vac = []
        insumos_vac = []
        citas_citasMedicas = []
        procedimiento_citaMedica = []
        insumos_citasMedicas = []
        inyectologias_citasMedicas = []

        if (tipo_cita == 'generales' or tipo_cita == 'undefined'):
            citas_esteticas = FacturaEstetica.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
            citas_desparasitacion = Desparasitacion.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
            aplicacion_desp = AplicacionDesparasitacion.objects.all()
            insumos_desp = InsumoDesparasitacion.objects.all()
            citas_guarderias = Guarderia.objects.filter(fecha_inicio__date__range=[fecha_inicio, fecha_fin])
            pago_guarderia = PagoGuarderia.objects.all()
            citas_vacunacion = Vacunacion.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
            aplicacion_vac = AplicacionVacunacion.objects.all()
            insumos_vac = InsumoVacunacion.objects.all()
            citas_citasMedicas = HistoriaClinica.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
            procedimiento_citaMedica = MotivoArea.objects.all()
            insumos_citasMedicas = InsumoHistoria.objects.all()
            inyectologias_citasMedicas = HistoriaMedicamento.objects.all()
            pagos_citasMedicas = PagoTratamiento.objects.filter(tratamiento__fecha__date__range=[fecha_inicio, fecha_fin])
            context['pagos_citasMedicas'] = pagos_citasMedicas

            costos_atenciones = []

            for historia in citas_citasMedicas:
                costo_atencion = historia.costo
                for procedimiento in procedimiento_citaMedica:
                    if historia.id == procedimiento.historia_clinica.id and procedimiento.estado == 'Pagada':
                        pago_procedimiento = PagoAreaConsulta.objects.exclude(pago__estado_pago='Anulada').distinct('area_consulta').get(area_consulta=procedimiento)
                        costo_atencion += pago_procedimiento.total
                costos_atenciones.append(costo_atencion)

            context['costos_atenciones'] = costos_atenciones

            if id_responsable != 0:
                citas_esteticas = citas_esteticas.filter(veterinario=id_responsable)
                citas_desparasitacion = citas_desparasitacion.filter(veterinario=id_responsable)
                citas_guarderias = []
                citas_vacunacion = citas_vacunacion.filter(veterinario=id_responsable)
                citas_citasMedicas = citas_citasMedicas.filter(creada_por=id_responsable)

            if id_propietario != 0:
                citas_esteticas = citas_esteticas.filter(mascota__cliente=id_propietario)
                citas_desparasitacion = citas_desparasitacion.filter(mascota__cliente=id_propietario)
                citas_guarderias = Guarderia.objects.filter(mascota__cliente=id_propietario)
                citas_vacunacion = citas_vacunacion.filter(mascota__cliente=id_propietario)
                citas_citasMedicas = citas_citasMedicas.filter(mascota__cliente=id_propietario)

            if id_mascota != 0:
                citas_esteticas = citas_esteticas.filter(mascota=id_mascota)
                citas_desparasitacion = citas_desparasitacion.filter(mascota=id_mascota)
                citas_guarderias = Guarderia.objects.filter(mascota=id_mascota)
                citas_vacunacion = citas_vacunacion.filter(mascota=id_mascota)
                citas_citasMedicas = citas_citasMedicas.filter(mascota=id_mascota)

            if id_area_consulta != 0:
                for cita in citas_citasMedicas:
                    if not cita.id in list(procedimiento_citaMedica.filter(area_consulta=id_area_consulta).values_list('historia_clinica__id', flat=True)):
                        citas_citasMedicas = citas_citasMedicas.exclude(id=cita.id)

        else:
            if tipo_cita == 'esteticas':
                citas_esteticas = FacturaEstetica.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
                if id_responsable != 0:
                    citas_esteticas = citas_esteticas.filter(veterinario=id_responsable)
                if id_propietario != 0:
                    citas_esteticas = citas_esteticas.filter(mascota__cliente=id_propietario)
                if id_mascota != 0:
                    citas_esteticas = citas_esteticas.filter(mascota=id_mascota)

            if tipo_cita == 'desparasitaciones':
                citas_desparasitacion = Desparasitacion.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
                aplicacion_desp = AplicacionDesparasitacion.objects.all()
                insumos_desp = InsumoDesparasitacion.objects.all()
                if id_responsable != 0:
                    citas_desparasitacion = citas_desparasitacion.filter(veterinario=id_responsable)
                if id_propietario != 0:
                    citas_desparasitacion = citas_desparasitacion.filter(mascota__cliente=id_propietario)
                if id_mascota != 0:
                    citas_desparasitacion = citas_desparasitacion.filter(mascota=id_mascota)

            if tipo_cita == 'guarderias':
                citas_guarderias = Guarderia.objects.filter(fecha_inicio__date__range=[fecha_inicio, fecha_fin])
                pago_guarderia = PagoGuarderia.objects.all()
                if id_responsable != 0:
                    citas_guarderias = citas_guarderias.filter(veterinario=id_responsable)
                if id_propietario != 0:
                    citas_guarderias = citas_guarderias.filter(mascota__cliente=id_propietario)
                if id_mascota != 0:
                    citas_guarderias = citas_guarderias.filter(mascota=id_mascota)

            if tipo_cita == 'vacunaciones':
                citas_vacunacion = Vacunacion.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
                aplicacion_vac = AplicacionVacunacion.objects.all()
                insumos_vac = InsumoVacunacion.objects.all()
                if id_responsable != 0:
                    citas_vacunacion = citas_vacunacion.filter(veterinario=id_responsable)
                if id_propietario != 0:
                    citas_vacunacion = citas_vacunacion.filter(mascota__cliente=id_propietario)
                if id_mascota != 0:
                    citas_vacunacion = citas_vacunacion.filter(mascota=id_mascota)

            if tipo_cita == 'citas':
                citas_citasMedicas = HistoriaClinica.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin])
                procedimiento_citaMedica = MotivoArea.objects.all()
                insumos_citasMedicas = InsumoHistoria.objects.all()
                inyectologias_citasMedicas = HistoriaMedicamento.objects.all()
                pagos_citasMedicas = PagoTratamiento.objects.filter(tratamiento__fecha__date__range=[fecha_inicio, fecha_fin])
                context['pagos_citasMedicas'] = pagos_citasMedicas

                costos_atenciones = []

                for historia in citas_citasMedicas:
                    costo_atencion = historia.costo
                    for procedimiento in procedimiento_citaMedica:
                        if historia.id == procedimiento.historia_clinica.id and procedimiento.estado == 'Pagada':
                            pago_procedimiento = PagoAreaConsulta.objects.exclude(pago__estado_pago='Anulada').distinct('area_consulta').get(area_consulta=procedimiento)
                            costo_atencion += pago_procedimiento.total
                    costos_atenciones.append(costo_atencion)

                context['costos_atenciones'] = costos_atenciones


                if id_responsable != 0:
                    citas_citasMedicas = citas_citasMedicas.filter(creada_por=id_responsable)
                if id_propietario != 0:
                    citas_citasMedicas = citas_citasMedicas.filter(mascota__cliente=id_propietario)
                if id_mascota != 0:
                    citas_citasMedicas = citas_citasMedicas.filter(mascota=id_mascota)
                if id_area_consulta != 0:
                    for cita in citas_citasMedicas:
                        if not cita.id in list(procedimiento_citaMedica.filter(area_consulta=id_area_consulta).values_list('historia_clinica__id',flat=True)):
                            citas_citasMedicas = citas_citasMedicas.exclude(id=cita.id)



        context['citas_esteticas'] = citas_esteticas
        context['citas_desparasitacion'] = citas_desparasitacion
        context['aplicacion_desp'] = aplicacion_desp
        context['insumos_desp'] = insumos_desp
        context['citas_guarderias'] = citas_guarderias
        context['pago_guarderia'] = pago_guarderia
        context['citas_vacunacion'] = citas_vacunacion
        context['aplicacion_vac'] = aplicacion_vac
        context['insumos_vac'] = insumos_vac
        context['citas_citasMedicas'] = citas_citasMedicas
        context['procedimiento_citaMedica'] = procedimiento_citaMedica
        context['insumos_citasMedicas'] = insumos_citasMedicas
        context['inyectologias_citasMedicas'] = inyectologias_citasMedicas

        return context

class ReporteHistorialInventario(TemplateView):
    template_name = 'historial_inventario.html'

    def get_context_data(self, **kwargs):
        context = super(ReporteHistorialInventario, self).get_context_data(**kwargs)

        context['menu_reportes_historial_inventario'] = True

        fecha_inicio = datetime.datetime.strptime(self.kwargs['fecha_inicio'],'%Y-%m-%d').date()
        fecha_fin = datetime.datetime.strptime(self.kwargs['fecha_fin'],'%Y-%m-%d').date()
        categoria = self.kwargs['categoria']
        motivo = self.kwargs['motivo']

        context['categoria'] = categoria
        context['motivo'] = motivo

        context['categorias'] = CategoriaInventario.objects.all()

        if categoria == 'todos':
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin]).exclude(id_transaccion=0)
        elif categoria == 'medicamentos':
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin], tipo_elemento='Medicamento')
        elif categoria == 'productos':
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin], tipo_elemento='Producto')
        elif categoria == 'vacunas':
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin], tipo_elemento='Vacuna').exclude(id_transaccion=0)
        elif categoria == 'desparasitantes':
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin], tipo_elemento='Desparasitante').exclude(id_transaccion=0)
        elif categoria == 'insumos':
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin], tipo_elemento='Insumo')
        else:
            historial = HistorialInventario.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin], tipo_elemento='Categoria')
            context['categoria'] = int(categoria)

        if motivo == 'inventario':
            historial = historial.filter(tipo_transaccion='Inventario')
            context['historial'] = historial
        elif motivo == 'compra':
            historial = historial.filter(tipo_transaccion='Compras')
            context['historial'] = historial
        elif motivo == 'venta':
            historial = historial.filter(tipo_transaccion='Ventas')
            context['historial'] = historial
        elif motivo == 'todos':
            context['historial'] = historial

        if motivo == 'aplicacion' or motivo == 'todos':

            if categoria == 'vacunas' or categoria == 'todos':
                aplicacion_vacunaciones = AplicacionVacunacion.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin]).order_by('fecha')
                pago_vacunaciones = PagoVacunacion.objects.all()

                context['aplicacion_vacunaciones'] = aplicacion_vacunaciones
                context['pago_vacunaciones'] = pago_vacunaciones

            if categoria == 'desparasitantes' or categoria == 'todos':
                aplicacion_desparasitaciones = AplicacionDesparasitacion.objects.filter(fecha__date__range=[fecha_inicio, fecha_fin]).order_by('fecha')
                pago_desparasitaciones = PagoDesparasitacion.objects.all()

                context['aplicacion_desparasitaciones'] = aplicacion_desparasitaciones
                context['pago_desparasitaciones'] = pago_desparasitaciones


        context['medicamentos'] = Medicamento.objects.filter(estado='Activo')
        context['productos'] = Producto.objects.filter(estado=True)
        context['vacunas'] = Vacuna.objects.filter(estado='Activa')
        context['desparasitantes'] = Desparasitante.objects.filter(estado='Activo')
        context['insumos'] = Insumo.objects.filter(estado='Activo')
        context['elementos_categorias'] = ElementoCategoriaInventario.objects.filter(estado='Activo')

        context['ventas_productos'] = PagoProducto.objects.filter(pago__fecha__range=[fecha_inicio, fecha_fin])
        context['ventas_medicamentos'] = PagoMedicamento.objects.filter(pago__fecha__range=[fecha_inicio, fecha_fin])
        context['ventas_vacunas'] = PagoVacuna.objects.filter(pago__fecha__range=[fecha_inicio, fecha_fin])
        context['ventas_desparasitantes'] = PagoDesparasitante.objects.filter(pago__fecha__range=[fecha_inicio, fecha_fin])
        context['ventas_elementos_categorias'] = PagoElementoInventario.objects.filter(pago__fecha__range=[fecha_inicio, fecha_fin])

        context['compras_productos'] = GastoProducto.objects.filter(gasto__fecha__range=[fecha_inicio, fecha_fin])
        context['compras_medicamentos'] = GastoMedicamento.objects.filter(gasto__fecha__range=[fecha_inicio, fecha_fin])
        context['compras_vacunas'] = GastoVacuna.objects.filter(gasto__fecha__range=[fecha_inicio, fecha_fin])
        context['compras_desparasitantes'] = GastoDesparasitante.objects.filter(gasto__fecha__range=[fecha_inicio, fecha_fin])
        context['compras_elementos_categorias'] = GastoElementoInventario.objects.filter(gasto__fecha__range=[fecha_inicio, fecha_fin])
        context['compras_insumos'] = GastoInsumo.objects.filter(gasto__fecha__range=[fecha_inicio, fecha_fin])


        return context