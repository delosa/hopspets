from productos.models import Pago, PagoProducto, PagoEstetica, PagoVacunacion, PagoTratamiento, PagoDesparasitacion, PagoGuarderia
from django.db.models import Sum


def total_ventas_todos(request, date_now):
    """
        Mauro Castillo producto
    """
    ingresos_totales_pagos = Pago.objects.values('fecha__month').annotate(Sum('total_factura')).filter(fecha__year=date_now).order_by('fecha__month')
    totales_pagos = Pago.objects.filter(fecha__year=date_now).aggregate(Sum('total_factura'))['total_factura__sum']
    
    formato_numero_to_fecha(ingresos_totales_pagos)
    contexto =  {'informe': True,
        'title': "Ingresos del año",
        'object_list': formato_numero_to_fecha(ingresos_totales_pagos),
        'year':date_now,
        'todos': True,
        'total_ventas': totales_pagos
      }

    return contexto


def total_ventas_producto(request, date_now):
    """
        Mauro Castillo producto
    """

    ingresos_totales_estetica = PagoProducto.objects.values('pago__fecha__month').annotate(Sum('pago__total_factura')).filter(pago__fecha__year=date_now).order_by('pago__fecha__month')
    totales_pagos = PagoProducto.objects.filter(pago__fecha__year=date_now).aggregate(Sum('pago__total_factura'))['pago__total_factura__sum']
    contexto =  {'informe': True,
        'title': "Ingresos por productos del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_estetica),
        'year':date_now,
        'producto': True,
        'total_ventas': totales_pagos
      }

    return contexto



def total_ventas_estetica(request, date_now):
    """
        Mauro Castillo estetica
    """

    ingresos_totales_estetica = PagoEstetica.objects.values('pago__fecha__month').annotate(Sum('pago__total_factura')).filter(pago__fecha__year=date_now).order_by('pago__fecha__month')
    totales_pagos = PagoEstetica.objects.filter(pago__fecha__year=date_now).aggregate(Sum('pago__total_factura'))['pago__total_factura__sum']
    contexto =  {'informe': True,
        'title': "Ingresos por esteticas del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_estetica),
        'year':date_now,
        'estetica': True,
        'total_ventas': totales_pagos
      }

    return contexto


def total_ventas_vacunacion(request, date_now):
    """
        Mauro Castillo vacunacion
    """

    ingresos_totales_vacunacion = PagoVacunacion.objects.values('pago__fecha__month').annotate(Sum('pago__total_factura')).filter(pago__fecha__year=date_now).order_by('pago__fecha__month')
    totales_pagos = PagoVacunacion.objects.filter(pago__fecha__year=date_now).aggregate(Sum('pago__total_factura'))['pago__total_factura__sum']
    contexto =  {'informe': True,
        'title': "Ingresos por vacunación del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_vacunacion),
        'year':date_now,
        'vacunacion': True,
        'total_ventas': totales_pagos
      }

    return contexto


def total_ventas_tratamientos(request, date_now):
    """
        Mauro Castillo tratamientos
    """

    ingresos_totales_tratamiento = PagoTratamiento.objects.values('pago__fecha__month').annotate(Sum('pago__total_factura')).filter(pago__fecha__year=date_now).order_by('pago__fecha__month')
    totales_pagos = PagoTratamiento.objects.filter(pago__fecha__year=date_now).aggregate(Sum('pago__total_factura'))['pago__total_factura__sum']
    contexto =  {'informe': True,
        'title': "Ingresos tratamientos del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_tratamiento),
        'year':date_now,
        'tratamiento': True,
        'total_ventas': totales_pagos
      }

    return contexto

def total_ventas_desparitacion(request, date_now):
    """
        Mauro Castillo desparicitacion
    """

    ingresos_totales_desparasitacion = PagoDesparasitacion.objects.values('pago__fecha__month').annotate(Sum('pago__total_factura')).filter(pago__fecha__year=date_now).order_by('pago__fecha__month')
    totales_pagos = PagoDesparasitacion.objects.filter(pago__fecha__year=date_now).aggregate(Sum('pago__total_factura'))['pago__total_factura__sum']
    contexto =  {'informe': True,
        'title': "Ingresos por desparacitación del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_desparasitacion),
        'year':date_now,
        'desparisitacion': True,
        'total_ventas': totales_pagos
      }

    return contexto

def total_ventas_guarderia(request, date_now):
    """
        Mauro Castillo guarderia
    """

    ingresos_totales_guarderia = PagoGuarderia.objects.values('pago__fecha__month').annotate(Sum('pago__total_factura')).filter(pago__fecha__year=date_now).order_by('pago__fecha__month')
    totales_pagos = PagoGuarderia.objects.filter(pago__fecha__year=date_now).aggregate(Sum('pago__total_factura'))['pago__total_factura__sum']
    contexto =  {'informe': True,
        'title': "Ingresos por guarderia del año ",
        'object_list': formato_numero_to_fecha_tercero(ingresos_totales_guarderia),
        'year':date_now,
        'guarderia': True,
        'total_ventas': totales_pagos
      }

    return contexto
# ------------------------------------- Utilidades -----------------------------------------------------
# Mauro Castillo
# Utilis 
def formato_numero_to_fecha(pagos_list):

    meses_latino = {0:'no definido', 1:'Enero', 2:'Febrero',3:'Marzo', 4:'Abril', 5:'Mayo', 6:'Junio', 7:'Julio', 8:'Agosto', 9:'Septiembre', 10:'Octubre', 11:'Noviembre', 12:'Diciembre'}
    salida = []
    for x in pagos_list:
        x['mes_latino'] = meses_latino[x['fecha__month']]
        x['index'] = x['fecha__month']
        salida.append(x)

    return salida

def formato_numero_to_fecha_tercero(pagos_list):

    meses_latino = {0:'no definido', 1:'Enero', 2:'Febrero',3:'Marzo', 4:'Abril', 5:'Mayo', 6:'Junio', 7:'Julio', 8:'Agosto', 9:'Septiembre', 10:'Octubre', 11:'Noviembre', 12:'Diciembre'}
    salida = []

    for x in pagos_list:
        x['mes_latino'] = meses_latino[x['pago__fecha__month']]
        x['index'] = x['pago__fecha__month']
        x['total_factura__sum'] = x['pago__total_factura__sum']
        salida.append(x)

    return salida