from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from clientes.models import Cliente
from citas.models import Cita, HistoriaClinica, Documento, Hospitalizacion, MotivoArea, Cirugia, SeguimientoHistoria
from estetica.models import FacturaEstetica
from formulas.models import Formula
from veteriariaTenant.models import Veterinaria
from datetime import datetime
from vacunacion.models import Vacunacion, AplicacionVacunacion
from desparasitacion.models import Desparasitacion, AplicacionDesparasitacion
from consentimiento.models import Consentimiento, ConsentimientoMascota
from guarderia.models import Guarderia, DatosGuarderia
from itertools import chain
from django.http import JsonResponse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
import json
from productos.models import MedicamentoHospitalizacion, MedicamentoCirugia
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import get_template
from insumos.models import InsumoHospitalizacion, InsumoDesparasitacion, InsumoVacunacion
from itertools import chain
from django.db.models import Q
from usuarios.models import Usuario
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView
from openpyxl import Workbook


############################ TIPOS DE MASCOTA ###########################


class CrearTipoMascota(SuccessMessageMixin, CreateView):
    model = TipoMascota
    fields = ['nombre']
    template_name = "registrar_tipo_mascota.html"
    success_url = reverse_lazy('listar_tipo_mascota')
    success_message = "¡El tipo de mascota se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearTipoMascota, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        tipo_mascota = form.instance
        self.object = form.save()
        return super(CrearTipoMascota, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearTipoMascota, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['tipos_mascota'] = True
        return context


class ListarTipoMascota (ListView):
    model = TipoMascota
    template_name = "listar_tipo_mascota.html"

    def get_queryset(self):
        queryset = super(ListarTipoMascota, self).get_queryset()
        return queryset.order_by('-pk')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarTipoMascota, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarTipoMascota, self).get_context_data(**kwargs)
        context['tipos_mascota'] = True
        return context


class EditarTipoMascota(SuccessMessageMixin, UpdateView):
    model = TipoMascota
    fields = ['nombre']
    template_name = "registrar_tipo_mascota.html"
    success_url = reverse_lazy('listar_tipo_mascota')
    success_message = "¡El tipo de mascota fue modificada con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarTipoMascota, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarTipoMascota, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['tipos_mascota'] = True
        return context


class EliminarTipoMascota(SuccessMessageMixin, DeleteView):
    model = TipoMascota
    template_name = "tipomascota_confirm_delete.html"
    success_url = reverse_lazy('listar_tipo_mascota')
    success_message = "¡La mascota se eliminó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EliminarTipoMascota, self).dispatch(request, *args, **kwargs)


############################ RAZAS ###########################


class CrearRaza(CreateView):
    model = Raza
    fields = ['nombre', 'tipo_mascota']
    template_name = "registrar_raza.html"
    success_url = reverse_lazy('listar_razas')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearRaza, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        raza = form.instance
        self.object = form.save()
        return super(CrearRaza, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearRaza, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['razas'] = True
        return context


class ListarRaza (ListView):
    model = Raza
    template_name = "listar_raza.html"

    def get_queryset(self):
        queryset = super(ListarRaza, self).get_queryset()
        return queryset.order_by('-pk')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarRaza, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarRaza, self).get_context_data(**kwargs)
        context['razas'] = True
        return context


class EditarRaza(SuccessMessageMixin, UpdateView):
    model = Raza
    fields = ['nombre', 'tipo_mascota']
    template_name = "registrar_raza.html"
    success_url = reverse_lazy('listar_razas')
    success_message = "¡La raza se modificó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarRaza, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarRaza, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['razas'] = True
        return context


"""class EliminarRaza(DeleteView):
    model = Raza
    template_name = "raza_confirm_delete.html"
    success_url = reverse_lazy('listar_razas')"""


@login_required
def EliminarRaza(request,pk):
    raza = get_object_or_404(Raza, pk=pk)
    raza.delete()
    return redirect('listar_razas')

############################ MASCOTAS ###########################


class CrearMascota(SuccessMessageMixin, CreateView):
    model = Mascota
    form_class = CrearMascotaForm
    template_name = "registrar_mascota.html"
    success_url = reverse_lazy('listar_mascotas')
    success_message = "¡La Mascota se agregó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearMascota, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        mascota = form.instance
        self.object = form.save()
        return super(CrearMascota, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearMascota, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        return context


class CrearMascotaCliente(SuccessMessageMixin, CreateView):
    model = Mascota
    form_class = CrearMascotaClienteForm
    template_name = "registrar_mascota_cliente.html"
    success_message = "¡La Mascota se agregó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearMascotaCliente, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/clientes/detalle_cliente/' + str(self.kwargs['id_cliente']) + '/#lista_mascotas'

    def form_valid(self,form):
        mascota = form.instance
        mascota.cliente = get_object_or_404(Cliente, pk=self.kwargs['id_cliente'])
        self.object = form.save()
        return super(CrearMascotaCliente, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearMascotaCliente, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['cliente'] = get_object_or_404(Cliente, pk=self.kwargs['id_cliente'])
        return context


class ListarMascotas(ListView):
    model = Mascota
    template_name = "listar_mascotas.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarMascotas, self).get_queryset()
        return queryset.order_by('nombre')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarMascotas, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarMascotas, self).get_context_data(**kwargs)
        
        context['mascotas'] = True
        context['motivos_fallecimiento'] = MotivoFallecimiento.objects.all()
        
        return context


class EditarMascota(SuccessMessageMixin, UpdateView):
    model = Mascota
    form_class = EditarMascotaForm
    template_name = "registrar_mascota.html"
    success_url = reverse_lazy('listar_mascotas')
    success_message = "¡Se editó la mascota con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarMascota, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_mascota =  get_object_or_404(Mascota, pk=self.kwargs['pk'])
        return '/clientes/detalle_cliente/' + str(mi_mascota.cliente.id)

    def get_context_data(self, **kwargs):
        context = super(EditarMascota, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        return context


class EliminarMascota(SuccessMessageMixin, DeleteView):
    model = Mascota
    template_name = "mascota_confirm_delete.html"
    success_url = reverse_lazy('listar_mascotas')
    success_message = "¡La Mascota se eliminó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EliminarMascota, self).dispatch(request, *args, **kwargs)

class DetalleMascota(DetailView):
    model = Mascota
    template_name = "detalle_mascota.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleMascota, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleMascota, self).get_context_data(**kwargs)
        context['mascotas'] = True

        if self.request.user.usuario.roles == 'Veterinario':
            citas = Cita.objects.filter(mascota_id=self.kwargs['pk'], estado='Activa', veterinario__id=self.request.user.usuario.id).order_by('fecha')
            vacunaciones = Vacunacion.objects.filter(mascota_id=self.kwargs['pk'], estado_aplicacion='Pendiente', veterinario__id=self.request.user.usuario.id).order_by('fecha')
            desparasitaciones = Desparasitacion.objects.filter(mascota_id=self.kwargs['pk'], estado_aplicacion='Pendiente', veterinario__id=self.request.user.usuario.id).order_by('fecha')
        else:
            citas = Cita.objects.filter(mascota_id=self.kwargs['pk']).order_by('fecha')
            vacunaciones = Vacunacion.objects.filter(mascota_id=self.kwargs['pk']).order_by('fecha')
            desparasitaciones = Desparasitacion.objects.filter(mascota_id=self.kwargs['pk']).order_by('fecha')

        context['citas_mascota'] = citas
        context['esteticas_mascota'] = FacturaEstetica.objects.filter(mascota_id=self.kwargs['pk']).order_by('estado','fecha')
        context['formulas_mascota'] = Formula.objects.filter(mascota_id=self.kwargs['pk']).order_by('-fecha')
        context['historia_mascota'] = HistoriaClinica.objects.filter(mascota_id=self.kwargs['pk']).order_by('fecha')
        context['documentos_historia_mascota'] = Documento.objects.filter(historia__mascota__id=self.kwargs['pk'])
        context['vacunacion_mascota'] = vacunaciones
        context['desparasitacion_mascota'] = desparasitaciones
        context['guarderia_mascota'] = Guarderia.objects.filter(mascota_id=self.kwargs['pk']).order_by('estado','fecha_inicio')
        context['vacunaciones_aplicadas'] = AplicacionVacunacion.objects.filter(vacunacion__mascota__id=self.kwargs['pk'])
        context['desparasitaciones_aplicadas'] = AplicacionDesparasitacion.objects.filter(desparasitacion__mascota__id=self.kwargs['pk'])
        context['consentimiento_mascota'] = ConsentimientoMascota.objects.filter(mascota_id=self.kwargs['pk']).order_by('firmado','fecha')

        hay_veterinaria = Veterinaria.objects.filter(pk='1')

        if hay_veterinaria:
            context['mi_veterinaria'] = Veterinaria.objects.get(pk='1')

        historias = HistoriaClinica.objects.all().values('cita')
        citas = Cita.objects.filter(mascota_id=self.kwargs['pk'], fecha__date__lte=datetime.now().date()).exclude(
            id__in=historias)

        if citas:
            context['agregar_historia'] = True
        else:
            context['agregar_historia'] = False

        qs1 = Cita.objects.filter(mascota_id=self.kwargs['pk']).filter(estado='Atendida')
        qs2 = FacturaEstetica.objects.filter(mascota_id=self.kwargs['pk']).filter(estado_de_atencion='Atendida')
        qs3 = Vacunacion.objects.filter(mascota_id=self.kwargs['pk']).filter(estado_aplicacion='Aplicada')
        qs4 = Desparasitacion.objects.filter(mascota_id=self.kwargs['pk']).filter(estado_aplicacion='Aplicada')
        qs5 = Guarderia.objects.filter(mascota_id=self.kwargs['pk'])
        qs6 = Formula.objects.filter(mascota_id=self.kwargs['pk'])
        qs7 = HistoriaClinica.objects.filter(mascota_id=self.kwargs['pk'])
        qs8 = Hospitalizacion.objects.filter(historia_clinica__mascota_id=self.kwargs['pk']).order_by('fecha_hospitalizacion')
        qs9 = Cirugia.objects.filter(historia_clinica__mascota_id=self.kwargs['pk'])
        qs10 = SeguimientoHistoria.objects.filter(mascota_id=self.kwargs['pk'])

        for q in qs3:
            q.tipo = 'Vacunacion'

        for q in qs4:
            q.tipo = 'Desparasitacion'

        for q in qs5:
            q.fecha = q.fecha_inicio
            q.guarderia = q

        for q in qs6:
            q.formula = 'Formula'

        for q in qs7:
            q.historia = 'Historia'

        for q in qs8:
            q.hospitalizacion = 'Hospitalizacion'
            q.fecha = q.fecha.replace(year=q.fecha_hospitalizacion.year, month=q.fecha_hospitalizacion.month,day=q.fecha_hospitalizacion.day,hour=23,minute=59,second=30)
            #print(q.fecha)

        for q in qs9:
            q.cirugia = 'Cirugia'

        for q in qs10:
            q.seguimiento = 'Seguimiento'

        historial_mascota = sorted(chain(qs1, qs2, qs3, qs4, qs5, qs6, qs7,qs8,qs9,qs10), key=lambda instance: instance.fecha, reverse=True)
        #historial_mascota = chain(historial_mascota,qs8)

        procedimientos_mascota = MotivoArea.objects.filter(historia_clinica__mascota_id=self.kwargs['pk']) 

        context['historial_mascota'] = historial_mascota
        context['procedimientos_mascota'] = procedimientos_mascota

        medicamentos_aplicados_hospitalizaciones = MedicamentoHospitalizacion.objects.filter(hospitalizacion__historia_clinica__mascota_id=self.kwargs['pk'])

        print("medicamentos_aplicados_hospitalizaciones:")
        print(medicamentos_aplicados_hospitalizaciones)

        context['medicamentos_aplicados_hospitalizaciones'] = medicamentos_aplicados_hospitalizaciones

        medicamentos_aplicados_hospitalizaciones_id_historias = []

        for medicamento_aplicado in medicamentos_aplicados_hospitalizaciones:
            medicamentos_aplicados_hospitalizaciones_id_historias.append(medicamento_aplicado.hospitalizacion.id)

        context['medicamentos_aplicados_hospitalizaciones_id_historias'] = medicamentos_aplicados_hospitalizaciones_id_historias


        ########################################### MEDICAMENTOS APLICADOS CIRUGIA #################################

        medicamentos_aplicados_cirugias = MedicamentoCirugia.objects.filter(cirugia__historia_clinica__mascota_id=self.kwargs['pk'])

        context['medicamentos_aplicados_cirugias'] = medicamentos_aplicados_cirugias

        medicamentos_aplicados_cirugias_id_historias = []

        for medicamento_aplicado in medicamentos_aplicados_cirugias:
            medicamentos_aplicados_cirugias_id_historias.append(medicamento_aplicado.cirugia.id)

        context[
            'medicamentos_aplicados_cirugias_id_historias'] = medicamentos_aplicados_cirugias_id_historias

        ############################################################################################################

        ########################################### INSUMOS UTILIZADOS #################################

        insumos_vacunaciones = InsumoVacunacion.objects.filter(vacunacion__mascota_id=self.kwargs['pk'])

        context['insumos_vacunaciones'] = insumos_vacunaciones

        insumos_vacunaciones_ids = []

        for insumo_vacunacion in insumos_vacunaciones:
            insumos_vacunaciones_ids.append(insumo_vacunacion.vacunacion.id)

        context['insumos_vacunaciones_ids'] = insumos_vacunaciones_ids

        ############################################################################################################

        insumos_desparasitaciones = InsumoDesparasitacion.objects.filter(desparasitacion__mascota_id=self.kwargs['pk'])

        context['insumos_desparasitaciones'] = insumos_desparasitaciones

        insumos_desparasitaciones_ids = []

        for insumo_desparasitacion in insumos_desparasitaciones:
            insumos_desparasitaciones_ids.append(insumo_desparasitacion.desparasitacion.id)

        context['insumos_desparasitaciones_ids'] = insumos_desparasitaciones_ids

        ############################################################################################################

        insumos_hospitalizaciones = InsumoHospitalizacion.objects.filter(hospitalizacion__historia_clinica__mascota_id=self.kwargs['pk'])

        context['insumos_hospitalizaciones'] = insumos_hospitalizaciones

        insumos_hospitalizaciones_ids = []

        for insumo_hospitalizacion in insumos_hospitalizaciones:
            insumos_hospitalizaciones_ids.append(insumo_hospitalizacion.hospitalizacion.id)

        context['insumos_hospitalizaciones_ids'] = insumos_hospitalizaciones_ids

        context['motivos_fallecimiento'] = MotivoFallecimiento.objects.all()

        representantesGuarderias = []
        guarderias = Guarderia.objects.filter(mascota_id=self.kwargs['pk']).order_by('estado','fecha_inicio')
        for guarderia in guarderias:
            datos = DatosGuarderia.objects.filter(guarderia=guarderia)
            representantes = {}
            for dato in datos:
                representantes['id'] = dato.id
                representantes['guarderia'] = dato.guarderia
                representantes['descripcion'] = dato.descripcion
                representantes['representante_ingreso'] = dato.representante_ingreso
                representantes['numero_documento_identificacion_ingreso'] = dato.numero_documento_identificacion_ingreso
                representantes['foto_ingreso'] = dato.foto_ingreso
                representantes['representante_retiro'] = dato.representante_retiro
                representantes['numero_documento_identificacion_retiro'] = dato.numero_documento_identificacion_retiro
                representantes['descripcion_salida'] = dato.descripcion_salida
                representantes['foto_retiro'] = dato.foto_retiro
                representantes['responsable'] = dato.responsable
                representantesGuarderias.append(representantes)
        
        context['representantesGuarderias'] = representantesGuarderias

        return context


def inactivar_mascota(request):
    id_mascota = request.GET.get('id', '')
    id_motivo_fallecimiento = request.GET.get('id_motivo_fallecimiento', '')
    comentarios_fallecimiento = request.GET.get('comentarios_fallecimiento', '')
    fecha_fallecimiento = datetime.strptime(str(request.GET.get('fecha_fallecimiento', '')), "%d/%m/%Y %H:%M")
    motivo_fallecimiento = MotivoFallecimiento.objects.get(id=id_motivo_fallecimiento)
    mi_mascota = get_object_or_404(Mascota, pk=id_mascota)
    mi_mascota.fecha_fallecimiento = fecha_fallecimiento
    mi_mascota.usuario_registro_fallecimiento = get_object_or_404(Usuario, id=request.user.id)
    mi_mascota.motivo_fallecimiento = motivo_fallecimiento
    mi_mascota.comentarios_fallecimiento = comentarios_fallecimiento
    mi_mascota.estado = False
    mi_mascota.estado_hospitalizacion = False
    mi_mascota.save()
    messages.success(request, 'El estado de la mascota fue modificado con exito')
    data = {
        'modificacion': True
    }
    return JsonResponse(data)


def activar_mascota(request):
    id_mascota = request.GET.get('id', '')
    mi_mascota = get_object_or_404(Mascota, pk=id_mascota)
    mi_mascota.estado = True
    mi_mascota.save()
    success_message = "!Se editó la mascota con exito!"
    data = {
        'modificacion': True
    }
    return JsonResponse(data)


def eliminar_mascota(request):
    id_mascota = request.GET.get('id', '')
    mi_mascota = get_object_or_404(Mascota, pk=id_mascota)
    id_cliente = mi_mascota.cliente.id
    mi_mascota.delete()
    messages.success(request, '!Mascota eliminada con exito!')
    data = {
        'eliminacion': True,
        'id_cliente': id_cliente
    }
    return JsonResponse(data)

def enviar_historia(request):
    id_mascota = request.GET.get('id', '')
    mi_mascota = get_object_or_404(Mascota, pk=id_mascota)
    
    subject = "Historia Clínica de"+ mi_mascota.nombre
    from_email = "soporte.hopspets@hops.com.co"
    to_email = [mi_mascota.cliente.email]
    
    message = EmailMultiAlternatives(subject=subject, body="Prueba", from_email= from_email, to=to_email)
    html_template = get_template("mail_templates/historia_mail.html").render()
    message.attach_alternative(html_template, "text/html")
    message.send()
    data = {
        'emailCliente': to_email
    }
    return JsonResponse(data)



def eliminar_tipo_mascota(request):
    id_tipo_mascota = request.GET.get('id', '')
    mi_tipo_mascota = get_object_or_404(TipoMascota, pk=id_tipo_mascota)
    if not Raza.objects.filter(tipo_mascota=id_tipo_mascota):
        mi_tipo_mascota.delete()
        messages.success(request, 'Tipo de mascota eliminado con exito!')
        data = {
            'eliminacion': True,
        }
    else:
        messages.error(request, 'No se puede eliminar porque existen razas asociadas a este tipo de mascota')
        data = {
            'eliminacion': False,
        }
    return JsonResponse(data)


def eliminar_raza(request):
    id_raza = request.GET.get('id', '')
    mi_raza = get_object_or_404(Raza, pk=id_raza)
    if not Mascota.objects.filter(raza=id_raza):
        mi_raza.delete()
        messages.success(request, 'Raza eliminada con exito!')
        data = {
            'eliminacion': True,
        }
    else:
        messages.error(request, 'No se puede eliminar porque existen mascotas asociadas a esta raza')
        data = {
            'eliminacion': False,
        }
    return JsonResponse(data)


class BuscarMascota(ListView):
    model = Mascota
    template_name = "buscar_mascota.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(BuscarMascota, self).get_queryset()
        criterio_busqueda = self.kwargs['criterio_busqueda'].split()
        for palabra in criterio_busqueda:
            qs1 = Mascota.objects.filter(
                Q(nombre__icontains=palabra) |
                Q(numero_carnet__icontains=palabra) |
                Q(numero_micro_chip__icontains=palabra) |
                Q(raza__nombre__icontains=palabra) |
                Q(cliente__nombres__icontains=palabra) |
                Q(cliente__apellidos__icontains=palabra)
            ).order_by('nombre')
            queryset = queryset & qs1
        return queryset

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BuscarMascota, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BuscarMascota, self).get_context_data(**kwargs)
        context['mascotas'] = True
        context['criterio_busqueda'] = self.kwargs['criterio_busqueda']
        context['motivos_fallecimiento'] = MotivoFallecimiento.objects.all()
        return context


class CrearMotivoFallecimiento(SuccessMessageMixin, CreateView):
    model = MotivoFallecimiento
    fields = ['nombre_motivo']
    template_name = "registrar_motivo_fallecimiento.html"
    success_url = reverse_lazy('listar_motivo_fallecimiento')
    success_message = "¡El motivo de fallecimiento se creó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearMotivoFallecimiento, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        motivo_fallecimiento = form.instance
        self.object = form.save()
        return super(CrearMotivoFallecimiento, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearMotivoFallecimiento, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_motivos_fallecimiento'] = True
        return context


class ListarMotivoFallecimiento (ListView):
    model = MotivoFallecimiento
    template_name = "listar_motivo_fallecimiento.html"

    def get_queryset(self):
        queryset = super(ListarMotivoFallecimiento, self).get_queryset()
        return queryset.order_by('-pk')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarMotivoFallecimiento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarMotivoFallecimiento, self).get_context_data(**kwargs)
        context['menu_motivos_fallecimiento'] = True
        return context


class EditarMotivoFallecimiento(SuccessMessageMixin, UpdateView):
    model = MotivoFallecimiento
    fields = ['nombre_motivo']
    template_name = "registrar_motivo_fallecimiento.html"
    success_url = reverse_lazy('listar_motivo_fallecimiento')
    success_message = "¡El motivo de fallecimiento fue modificado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarMotivoFallecimiento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditarMotivoFallecimiento, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_motivos_fallecimiento'] = True
        return context


def eliminar_motivo_fallecimiento(request):
    id_motivo_fallecimiento = request.GET.get('id', '')
    mi_motivo_fallecimiento = get_object_or_404(MotivoFallecimiento, pk=id_motivo_fallecimiento)
    mi_motivo_fallecimiento.delete()
    messages.success(request, 'Motivo de fallecimiento eliminado con exito!')
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)


def completar_fallecimiento_mascota(request):
    id_mascota = request.GET.get('id', '')
    id_motivo_fallecimiento = request.GET.get('id_motivo_fallecimiento', '')
    comentarios_fallecimiento = request.GET.get('comentarios_fallecimiento', '')
    fecha_fallecimiento = datetime.strptime(str(request.GET.get('fecha_fallecimiento', '')), "%d/%m/%Y %H:%M")
    #print("fecha_fallecimiento")
    #print(fecha_fallecimiento)
    motivo_fallecimiento = MotivoFallecimiento.objects.get(id=id_motivo_fallecimiento)
    mi_mascota = get_object_or_404(Mascota, pk=id_mascota)
    mi_mascota.fecha_fallecimiento = fecha_fallecimiento
    mi_mascota.usuario_registro_fallecimiento = get_object_or_404(Usuario, id=request.user.id)
    mi_mascota.motivo_fallecimiento = motivo_fallecimiento
    mi_mascota.comentarios_fallecimiento = comentarios_fallecimiento
    mi_mascota.estado = False
    mi_mascota.save()
    messages.success(request, 'Información de fallecimiento completada con exito')
    data = {
        'modificacion': True
    }
    return JsonResponse(data)

class ReporteMascotas(TemplateView):
    template_name = "listar_mascotas.html"
    def get(self, request, *args, **kwargs):
        mascotas = Mascota.objects.all()
        motivos_fa = MotivoFallecimiento.objects.all()
        wb = Workbook()
        ws = wb.active 

        ws['A1'] = 'Mascotas Registradas'

        ws['A3'] = 'Nombre'
        ws['B3'] = 'Raza'
        ws['C3'] = 'Peso'
        ws['D3'] = 'Fecha de Nacimiento'
        ws['E3'] = 'Sexo'
        ws['F3'] = 'Temperamento'
        ws['G3'] = 'Número de Carnet'
        ws['H3'] = 'Estado Reproductivo'
        ws['I3'] = 'Número de Partos'
        ws['J3'] = 'Color'
        ws['K3'] = 'Fecha de Fallecimiento'
        ws['L3'] = 'Motivo de Fallecimiento'
        ws['M3'] = 'Comentarios del Fallecimiento'
        ws['N3'] = 'Propietario'

        num_row = 5

        for mascota in mascotas:
            ws.cell(row = num_row, column = 1).value = mascota.nombre
            ws.cell(row = num_row, column = 2).value = mascota.raza.nombre
            ws.cell(row = num_row, column = 3).value = mascota.peso
            ws.cell(row = num_row, column = 4).value = mascota.fecha_nacimiento
            ws.cell(row = num_row, column = 5).value = mascota.sexo
            ws.cell(row = num_row, column = 6).value = mascota.temperamento
            ws.cell(row = num_row, column = 7).value = mascota.numero_carnet
            ws.cell(row = num_row, column = 8).value = mascota.estado_reproductivo
            ws.cell(row = num_row, column = 9).value = mascota.numero_partos
            ws.cell(row = num_row, column = 10).value = mascota.color
            ws.cell(row = num_row, column = 11).value = mascota.fecha_fallecimiento
            for motivo in motivos_fa:
                if motivo.id == mascota.motivo_fallecimiento_id:
                    ws.cell(row = num_row, column = 12).value = motivo.nombre_motivo
            ws.cell(row = num_row, column = 13).value = mascota.comentarios_fallecimiento
            ws.cell(row = num_row, column = 14).value = mascota.cliente.nombres + " " + mascota.cliente.apellidos


            num_row += 1

        filename = "Mascotas.xlsx"
        response = HttpResponse(content_type = "application/ms-excel")
        contenido = "attachment; filename = {0}".format(filename)
        response["Content-Disposition"] = contenido
        wb.save(response)

        return response
