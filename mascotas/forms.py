from .models import *
from django import forms


class CrearMascotaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearMascotaForm, self).__init__(*args, **kwargs)
        self.fields['raza'].empty_label = None
        self.fields['raza'].widget.attrs = {'class': 'form-control'}
        self.fields['temperamento'].widget.attrs = {'class': 'form-control'}
        self.fields['cliente'].widget.attrs = {'class': 'form-control'}
        self.fields['cliente'].label = 'Propietario'
        self.fields['sexo'].empty_label = None
        self.fields['peso'].required = False
        self.fields['numero_carnet'].required = False
        self.fields['fecha_nacimiento'].required = False
        self.fields['alergias'].required = False
        if Mascota.objects.last():
            id_historia = Mascota.objects.last().id + 1
            if Mascota.objects.filter(id=id_historia) or Mascota.objects.filter(numero_carnet=id_historia):
                id_historia = int(Mascota.objects.last().numero_carnet) + 1
        else:
            id_historia = 1
        self.fields['numero_carnet'].initial = id_historia

    class Meta:
        model = Mascota
        fields = (
            'numero_carnet',
            'numero_micro_chip',
            'nombre',
            'temperamento',
            'raza',
            'cliente',
            'peso',
            'fecha_nacimiento',
            'sexo',
            'color',
            'alergias',
            'estado_reproductivo',
            'foto',
        )

        widgets = {
            'numero_carnet': forms.NumberInput(),
            'fecha_nacimiento': forms.DateInput(attrs={'class': 'form-control fecha-nacimiento'}),
            'foto': forms.FileInput()

        }

    def clean(self):

        cleaned_data = super(CrearMascotaForm, self).clean()
        numero_carnet = cleaned_data.get("numero_carnet")

        try:
            if numero_carnet == None:
                pass
            elif Mascota.objects.filter(numero_carnet=numero_carnet):
                self._errors['numero_carnet'] = [
                    'Ya existe una mascota con este número de carnet']
        except:
            pass


class EditarMascotaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarMascotaForm, self).__init__(*args, **kwargs)
        self.fields['raza'].empty_label = None
        self.fields['cliente'].empty_label = None
        self.fields['sexo'].empty_label = None
        self.fields['peso'].required = False
        self.fields['alergias'].required = False
        self.fields['estado_reproductivo'].required = False

    class Meta:
        model = Mascota
        fields = (
            'numero_carnet',
            'numero_micro_chip',
            'nombre',
            'raza',
            'temperamento',
            'cliente',
            'peso',
            'fecha_nacimiento',
            'sexo',
            'color',
            'alergias',
            'estado_reproductivo',
            'foto'
        )

        widgets = {
            'numero_carnet': forms.NumberInput(),
            'fecha_nacimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'foto': forms.FileInput()

        }

    def clean(self):

        cleaned_data = super(EditarMascotaForm, self).clean()
        numero_carnet = cleaned_data.get("numero_carnet")

        try:
            if numero_carnet == None:
                pass
            elif Mascota.objects.filter(numero_carnet=numero_carnet).l:
                self._errors['numero_carnet'] = [
                    'Ya existe una mascota con este número de carnet']
        except:
            pass


class CrearMascotaClienteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearMascotaClienteForm, self).__init__(*args, **kwargs)
        self.fields['raza'].empty_label = None
        self.fields['raza'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['temperamento'].widget.attrs = {'class': 'selectpicker', 'data-live-search': 'true'}
        self.fields['sexo'].empty_label = None
        self.fields['peso'].required = False
        self.fields['numero_carnet'].required = False
        self.fields['fecha_nacimiento'].required = False
        self.fields['alergias'].required = False
        self.fields['estado_reproductivo'].required = False
        if Mascota.objects.last():
            id_historia = Mascota.objects.last().id + 1
            if Mascota.objects.filter(id=id_historia) or Mascota.objects.filter(numero_carnet=id_historia):
                id_historia = int(Mascota.objects.last().numero_carnet) + 1
        else:
            id_historia = 1
        self.fields['numero_carnet'].initial = id_historia

    class Meta:
        model = Mascota
        fields = (
            'numero_carnet',
            'numero_micro_chip',
            'nombre',
            'raza',
            'temperamento',
            'peso',
            'fecha_nacimiento',
            'sexo',
            'color',
            'alergias',
            'estado_reproductivo',
            'foto',
        )

        widgets = {
            'numero_carnet': forms.NumberInput(),
            'fecha_nacimiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
            'foto': forms.FileInput()

        }

    def clean(self):

        cleaned_data = super(CrearMascotaClienteForm, self).clean()
        numero_carnet = cleaned_data.get("numero_carnet")


        try:
            if numero_carnet == None:
                pass
            elif Mascota.objects.filter(numero_carnet=numero_carnet):
                self._errors['numero_carnet'] = [
                    'Ya existe una mascota con este número de carnet']
        except:
            pass
