from django.db import models
from clientes.models import Cliente
from datetime import date
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import dateutil
from usuarios.models import Usuario


class TipoMascota(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Tipo de mascota', unique=True, error_messages={'unique': "Ya existe un tipo de mascota con este nombre."})
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class Raza(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre de la raza', unique=False, error_messages={'unique': "Ya existe una raza con este nombre."})
    tipo_mascota = models.ForeignKey(TipoMascota, on_delete=models.CASCADE)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre + " (" + self.tipo_mascota.nombre + ")"


TIPOS_DE_SEXO = (('MA','Macho'),('HE','Hembra'),('IN','Indefinido'))
TEMPERAMENTO = (('agresivo','Agresivo'),('timido','Tímido'),('pasivo-agresivo','Pasivo-agresivo'),('social', 'Social'),('independiente', 'Independiente'))


class MotivoFallecimiento(models.Model):
    nombre_motivo = models.CharField(max_length=300, verbose_name='Motivo', unique=True, error_messages={'unique': "Ya existe un motivo de fallecimiento con este nombre."})
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre_motivo


def get_upload_to(instance, filename):
    return 'upload/%s/%s/%s' % (instance.cliente.numero_documento, instance.nombre, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extension del archivo no es valida.')


class Mascota(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    raza = models.ForeignKey(Raza, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    peso = models.CharField(max_length=50, verbose_name='Peso', null=True, blank=True)
    rh = models.CharField(max_length=50, verbose_name='Grupo sanguineo')
    fecha_nacimiento = models.DateField(verbose_name="Nacimiento",null=True, blank=True)
    sexo = models.CharField(max_length=2, verbose_name="Sexo", choices=TIPOS_DE_SEXO, default='MA')
    temperamento = models.CharField(max_length=20, verbose_name="Temperamento", choices=TEMPERAMENTO, default='social')
    numero_carnet = models.CharField(max_length=20, verbose_name='No. historia',null=True, blank=True)
    estado = models.BooleanField(default=True)
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    senales = models.CharField(max_length=2000, verbose_name='Señales', null=True, blank=True)
    estado_reproductivo = models.CharField(max_length=2000, verbose_name='Estado reproductivo', null=True, blank=True)
    numero_partos = models.CharField(max_length=2000, verbose_name='Número de partos', null=True, blank=True)
    color = models.CharField(max_length=2000, verbose_name='Color', null=True, blank=True)
    estado_hospitalizacion = models.BooleanField(default=False)
    alergias = models.CharField(max_length=500, verbose_name='Alergias',null=True, blank=True)
    numero_micro_chip = models.CharField(max_length=200, verbose_name='No. microchip', null=True, blank=True)
    motivo_fallecimiento = models.ForeignKey(MotivoFallecimiento, on_delete=models.CASCADE, blank=True, null=True)
    comentarios_fallecimiento = models.CharField(max_length=3000, verbose_name='Comentarios fallecimiento', null=True, blank=True)
    fecha_fallecimiento = models.DateTimeField(blank=True, null=True)
    usuario_registro_fallecimiento = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Responsable")

    def save(self, *args, **kwargs):
        if self.foto:
            if self.foto.url.endswith('.png') or self.foto.url.endswith('.PNG'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(Mascota, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def edad(self):
        today = date.today()
        fecha_nacimiento = self.fecha_nacimiento

        mi_edad = dateutil.relativedelta.relativedelta(today,fecha_nacimiento)

        label_años = ''
        label_meses = ''
        label_dias = ''

        if mi_edad.years > 1:
            label_años = 'años'
        else:
            label_años = 'año'

        if mi_edad.months > 1:
            label_meses = 'meses'
        else:
            label_meses = 'mes'

        if mi_edad.days > 1:
            label_dias = 'dias'
        else:
            label_dias = 'dia'

        if mi_edad.years > 0:
            if mi_edad.months > 0:
                return str(mi_edad.years) + " " + label_años + " y " + str(mi_edad.months) + " " + label_meses
            else:
                return str(mi_edad.years) + " " + label_años
        else:
            if mi_edad.months > 0:
                return str(mi_edad.months) + " " + label_meses
            else:
                return str(mi_edad.days) + " " + label_dias

    def __str__(self):
        return (self.nombre + " - " + self.cliente.nombres + " - " + self.cliente.numero_documento)
