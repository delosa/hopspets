from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_insumo/', CrearInsumo.as_view(), name="registrar_insumo"),
    path('editar_insumo/<int:pk>/', EditarInsumo.as_view(), name="editar_insumo"),
    path('detalle_insumo/<int:pk>/', DetalleInsumo.as_view(), name='detalle_insumo'),
    path('eliminar_insumo/', eliminar_insumo, name="eliminar_insumo"),
    path('inactivar_insumo/', inactivar_insumo, name="inactivar_insumo"),
    path('ajax/obtener_insumos/', obtener_insumos, name="obtener_insumos"),
]