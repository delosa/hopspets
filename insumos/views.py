from django.shortcuts import render
from django.views.generic import *
from .forms import *
from .models import *
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from productos.models import HistorialInventario
import math
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.contrib import messages
from gastos.models import GastoInsumo
from usuarios.models import Usuario
from django.db.models import Q
from itertools import chain
from django.db.models import Count
from django.core import serializers


class CrearInsumo(SuccessMessageMixin, CreateView):
    model = Insumo
    form_class = CrearInsumoForm
    template_name = "registrar_insumo.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "Insumo agregado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearInsumo, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        insumo = form.instance

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Registro',tipo_elemento='Insumo',responsable=responsable)
        historia_transaccion.save()

        insumo.save()
        historia_transaccion.id_elemento = insumo.id
        historia_transaccion.save()

        return super(CrearInsumo, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearInsumo, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_inventario'] = True
        return context


class EditarInsumo(SuccessMessageMixin, UpdateView):
    model = Insumo
    form_class = CrearInsumoForm
    template_name = "registrar_insumo.html"
    success_url = reverse_lazy('listar_productos')
    success_message = "¡El insumo fue modificado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarInsumo, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        insumo = form.instance

        mi_insumo = get_object_or_404(Insumo, id=insumo.id)

        if (mi_insumo.cantidad != insumo.cantidad) or (mi_insumo.cantidad_unidad != insumo.cantidad_unidad):
            cantidad_nueva = insumo.cantidad
            insumo.cantidad_total_unidades = cantidad_nueva * insumo.cantidad_unidad

        responsable = get_object_or_404(Usuario, id=self.request.user.id)

        historia_transaccion = HistorialInventario(nombre_transaccion='Modificación de información',tipo_elemento='Insumo', id_elemento=insumo.id,responsable=responsable)
        historia_transaccion.save()

        self.object = form.save()
        return super(EditarInsumo, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarInsumo, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_inventario'] = True
        return context


class DetalleInsumo(DetailView):
    model = Insumo
    template_name = "detalle_elemento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleInsumo, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleInsumo, self).get_context_data(**kwargs)
        context['menu_inventario'] = True
        context['tipo_elemento'] = 'insumo'

        historial_inventario = HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                                  id_elemento=self.kwargs['pk'],
                                                                  tipo_transaccion='Inventario')
        context['historial_inventario'] = historial_inventario

        historial_ventas = HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                              id_elemento=self.kwargs['pk'],
                                                              tipo_transaccion='Ventas')
        context['historial_ventas'] = historial_ventas

        historial_compras = HistorialInventario.objects.filter(tipo_elemento='Insumo',
                                                               id_elemento=self.kwargs['pk'],
                                                               tipo_transaccion='Compras')
        context['historial_compras'] = historial_compras

        compras = GastoInsumo.objects.filter(insumo_id=self.kwargs['pk'])

        context['compras'] = compras

        mi_elemento = Insumo.objects.get(id=self.kwargs['pk'])

        if mi_elemento.cantidad_unidad != 0 and mi_elemento.cantidad_por_unidad:
            unidades_enteras = math.floor(float(mi_elemento.cantidad_total_unidades) / int(mi_elemento.cantidad_unidad))
            cantidad_no_entera = round((float(mi_elemento.cantidad_total_unidades) % int(mi_elemento.cantidad_unidad)),1)
        else:
            unidades_enteras = mi_elemento.cantidad
            cantidad_no_entera = 0

        if cantidad_no_entera == 0:
            if mi_elemento.cantidad_por_unidad:
                context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                    mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida
            else:
                context['disponibilidad'] = str(unidades_enteras) + " unidades"
        else:
            context['disponibilidad'] = str(unidades_enteras) + " unidades de " + str(
                mi_elemento.cantidad_unidad) + " " + mi_elemento.unidad_medida + " y 1 unidad de " + str(
                cantidad_no_entera) + " " + mi_elemento.unidad_medida

        return context


def eliminar_insumo(request):
    id_insumo = request.GET.get('id', '')
    insumo = get_object_or_404(Insumo, pk=id_insumo)
    insumo.delete()
    messages.success(request, '¡Insumo eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def inactivar_insumo(request):
    id_insumo = request.GET.get('id', '')
    insumo = get_object_or_404(Insumo, pk=id_insumo)
    insumo.estado = 'Inactivo'
    insumo.save()
    messages.success(request, '¡Insumo eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


def obtener_insumos(request):

    all_insumos = Insumo.objects.filter(estado='Activo',bodega=1).exclude(Q(cantidad=0) & Q(cantidad_por_unidad=False)).exclude(Q(cantidad_total_unidades=0) & Q(cantidad_por_unidad=True))

    elementos_por_codigo = chain(
        Insumo.objects.distinct('codigo_inventario').filter(estado='Activo',bodega=1).exclude(
            codigo_inventario__isnull=True),
        Insumo.objects.filter(codigo_inventario__isnull=True, estado='Activo',bodega=1))
    elementos_repetidos = []
    for element in list(
            Insumo.objects.filter(bodega=1).values('codigo_inventario').annotate(cantidad=Count('codigo_inventario'))):
        if element['cantidad'] > 1:
            elementos_repetidos.append(element['codigo_inventario'])
    data = {
        'all_insumos': serializers.serialize('json', all_insumos),
        'elementos_por_codigo': serializers.serialize('json', elementos_por_codigo),
        'codigos_elementos': elementos_repetidos
    }
    return JsonResponse(data)