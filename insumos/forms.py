from .models import *
from django import forms
from datetime import datetime
from productos.models import Producto, Medicamento
from desparasitacion.models import Desparasitante
from vacunacion.models import Vacuna


class CrearInsumoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearInsumoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].required = False
        self.fields['cantidad'].label = 'Unidades disponibles'
        self.fields['foto'].label = 'Imagen'
        self.fields['cantidad_por_unidad'].label = 'Especificar cantidad por unidad'
        self.fields['cantidad'].initial = '1'
        self.fields['cantidad_unidad'].initial = '100'

    class Meta:
        model = Insumo
        fields = (
            'nombre',
            'codigo',
            'codigo_inventario',
            'descripcion',
            'cantidad',
            'fecha_vencimiento',
            'foto',
            'cantidad_por_unidad',
            'unidad_medida',
            'cantidad_unidad',
            'stock_minimo',
            'bodega',
            'precio_compra'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '10'}),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}),
            'fecha_vencimiento': forms.DateInput(attrs={'class': 'form-control fecha-vencimiento'}),
            'codigo': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'codigo_inventario': forms.TextInput(
                attrs={'class': 'form-control', 'autocomplete': 'off', 'pattern': '[A-Za-z+0-9()-.#_Ññáéíóú/ ]+',
                       'title': 'Enter Characters Only '}),
            'precio_compra': forms.NumberInput(attrs={'max_length': '11', 'min': '0'}),
        }

    def clean(self):

        cleaned_data = super(CrearInsumoForm, self).clean()
        codigo = cleaned_data.get("codigo_inventario")
        nombre = cleaned_data.get("nombre")
        fecha_vencimiento = cleaned_data.get("fecha_vencimiento")
        cantidad_por_unidad = cleaned_data.get("cantidad_por_unidad")
        cantidad_unidad = cleaned_data.get("cantidad_unidad")

        insumo_instance = self.instance

        if codigo != None:
            try:
                if Producto.objects.filter(codigo=codigo) or Desparasitante.objects.filter(
                        codigo_inventario=codigo) or Medicamento.objects.filter(codigo_inventario=codigo) or Vacuna.objects.filter(codigo_inventario=codigo):
                    self._errors['codigo_inventario'] = [
                        'Ya existe un elemento del inventario con este codigo']
                elif Insumo.objects.filter(codigo_inventario=codigo).exclude(id=insumo_instance.id):
                    for insumo in Insumo.objects.filter(codigo_inventario=codigo).exclude(id=insumo_instance.id):
                        if insumo.nombre != nombre:
                            self._errors['codigo_inventario'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                            self._errors['nombre'] = [
                                'Ya existe un elemento del inventario con el mismo codigo y diferente nombre']
                        elif insumo.fecha_vencimiento == fecha_vencimiento:
                            self._errors['fecha_vencimiento'] = [
                                'Este insumo ya se encuentra registrado en el inventario con la misma fecha de vencimiento']
            except:
                pass

        if cantidad_por_unidad == True and cantidad_unidad < 1:
            self._errors['cantidad_unidad'] = [
                'La cantidad no puede ser menor que uno']