from django.db import models
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from citas.models import HistoriaClinica, Hospitalizacion
from desparasitacion.models import Desparasitacion
from vacunacion.models import Vacunacion
from bodegas.models import Bodega


ESTADOS = (('Activo','Activo'),('Inactivo','Inactivo'))
UNIDADES_MEDIDA = (('ml','Mililitros (ml)'),('l','Litros (l)'),('g','Gramos (g)'),('mg','Miligramos (mg)'),('kg','Kilogramos (kg)'), ('tabs', 'Tabletas (tabs)'))


def get_upload_to(instance, filename):
    return 'upload/insumos/%s/%s' % (instance.nombre, filename)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')


class Insumo(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    codigo = models.CharField(max_length=50, verbose_name='Número de lote', blank=True, null=True)
    codigo_inventario = models.CharField(max_length=50, verbose_name='Código de barras', blank=True, null=True)
    descripcion = models.CharField(max_length=800, verbose_name='Descripción', null=True, blank=True)
    estado = models.CharField(max_length=20, verbose_name="Estado", choices=ESTADOS, default='Activo')
    cantidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    fecha_vencimiento = models.DateField(null=True, blank=True)
    unidad_medida = models.CharField(max_length=100, verbose_name='Unidad de medida', choices=UNIDADES_MEDIDA, default='ml')
    cantidad_unidad = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad por unidad', default=0)
    foto = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True)
    cantidad_por_unidad = models.BooleanField(default=False)
    cantidad_total_unidades = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad total unidades', default=0)
    stock_minimo = models.PositiveIntegerField(verbose_name='Stock minimo', default=1)
    bodega = models.ForeignKey(Bodega, on_delete=models.CASCADE, default=1)
    precio_compra = models.DecimalField(max_digits=50, decimal_places=0, default=0)

    def save(self, *args, **kwargs):
        if self.cantidad_total_unidades == 0:
            self.cantidad_total_unidades = float(self.cantidad) * float(self.cantidad_unidad)
        if self.foto:
            if self.foto.url.endswith('.png'):
                print("Es un png")
                self.foto = self.foto
            else:
                self.foto = self.compressImage(self.foto)
        super(Insumo, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()
            imageTemproaryResized = imageTemproary.resize((1020, 573))
            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        if self.codigo:
            return self.nombre + " - " + self.codigo
        else:
            return self.nombre


class InsumoHistoria(models.Model):
    historia = models.ForeignKey(HistoriaClinica, on_delete=models.CASCADE)
    insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
    cantidad_requerida = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=200, verbose_name='Unidad de medida')
    tipo_unidad_medida = models.CharField(max_length=200, verbose_name='Tipo de unidad')


class InsumoDesparasitacion(models.Model):
    desparasitacion = models.ForeignKey(Desparasitacion, on_delete=models.CASCADE)
    insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
    cantidad_requerida = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=200, verbose_name='Unidad de medida')
    tipo_unidad_medida = models.CharField(max_length=200, verbose_name='Tipo de unidad')


class InsumoVacunacion(models.Model):
    vacunacion = models.ForeignKey(Vacunacion, on_delete=models.CASCADE)
    insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
    cantidad_requerida = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=200, verbose_name='Unidad de medida')
    tipo_unidad_medida = models.CharField(max_length=200, verbose_name='Tipo de unidad')


class InsumoHospitalizacion(models.Model):
    hospitalizacion = models.ForeignKey(Hospitalizacion, on_delete=models.CASCADE)
    insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
    cantidad_requerida = models.DecimalField(max_digits=9, decimal_places=1, verbose_name='Cantidad', default=0)
    unidad_medida = models.CharField(max_length=200, verbose_name='Unidad de medida')
    tipo_unidad_medida = models.CharField(max_length=200, verbose_name='Tipo de unidad')