from django.shortcuts import render,get_object_or_404,redirect,render_to_response
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

class SubirImagenActualizacion(SuccessMessageMixin, CreateView):
	model = ImagenActualizacion 
	form_class = CrearImagenActualizacionForm
	template_name = "subir_imagen_actualizacion.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(SubirImagenActualizacion, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return '/imagenesActualizacion/listar_imagenes_actualizaciones/'

	def form_valid(self, form):
		imagen = form.instance
		self.object = form.save()
		messages.success(self.request, "Se ha guardado su imagen exitosamente")
		return super(SubirImagenActualizacion, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, "Error al registrar su imagen, por favor revise los datos")
		return super(SubirImagenActualizacion, self).form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super(SubirImagenActualizacion, self).get_context_data(**kwargs)
		context['imagen'] = True
		return context


class ListarImagenesActualizaciones(ListView):
    model = ImagenActualizacion
    template_name = "listar_imagenes_actualizaciones.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarImagenesActualizaciones, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarImagenesActualizaciones, self).get_context_data(**kwargs)
        context['imagenesActualizaciones'] = True

        imagenesActualizaciones = ImagenActualizacion.objects.all()

        context['imagenesActualizaciones'] = imagenesActualizaciones
        
        return context

def DetalleImagen(request, id_img):
	img = ImagenActualizacion.objects.get(id=id_img)

	return render(request, 'detalle_imagenes_actualizacion.html', {
		'img': img,
	})