from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from .forms import *

urlpatterns = [
	path('subida_imagen_actualizacion/', SubirImagenActualizacion.as_view(), name="subida_imagen_actualizacion"),
	path('listar_imagenes_actualizaciones/', ListarImagenesActualizaciones.as_view(), name="listar_imagenes_actualizaciones"),
	path('detalle_image_actualizacion/<int:id_img>/', DetalleImagen, name="detalle_image_actualizacion"),
]