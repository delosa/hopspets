from django.apps import AppConfig


class ImagenesActualizacionConfig(AppConfig):
    name = 'imagenesActualizacion'
