from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404

class CrearImagenActualizacionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearImagenActualizacionForm, self).__init__(*args, **kwargs)


        self.fields['fecha_subida'].initial = datetime.now()
        self.fields['fecha_subida'].label = "Fecha de Subida"
        self.fields['titulo_imagen'].required = True
        self.fields['titulo_imagen'].label = "Título de la Imagen"
        self.fields['descripcion_imagen'].widget.attrs = {'rows': '5', 'placeholder':'Describa de que se trata la imagen'}
        self.fields['descripcion_imagen'].label = "Descripción de la Imagen"
        self.fields['categoria_imagen'].required = True
        self.fields['categoria_imagen'].label = "Categoría de la Imagen"
        self.fields['imagen_actualizacion'].required = True
        self.fields['imagen_actualizacion'].label = 'Imagen de Actualizacion'
        self.fields['imagen_actualizacion'].widget.attrs = {'multiple': 'true'}

    class Meta:
        model = ImagenActualizacion
        fields = (
            'fecha_subida',
            'titulo_imagen',
            'descripcion_imagen',
            'categoria_imagen',
            'imagen_actualizacion',
        )

        widgets = {
            'descripcion_imagen': forms.Textarea(),
            'fecha_subida': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'imagen_actualizacion': forms.FileInput()
        }