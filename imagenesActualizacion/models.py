# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from sorl.thumbnail import ImageField, get_thumbnail
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import re

def get_upload_to(instance, filename):
    return 'upload/%s/%s' % ('ImagenActualizacion', filename)

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'La extensión del archivo no es valida.')

class ImagenActualizacion(models.Model):
    fecha_subida = models.DateTimeField()
    titulo_imagen = models.CharField(max_length=30, verbose_name="titulo_imagen")
    descripcion_imagen = models.CharField(max_length=200, verbose_name="descripcion_imagen")
    categoria_imagen = models.CharField(max_length=30, verbose_name="categoria_imagen")
    imagen_actualizacion = models.ImageField(upload_to=get_upload_to, validators=[validate_file_extension], null=True, blank=True, verbose_name="imagen_actualizacion")

    """ def nombre_archivo(self):
        if len(str(self.imagen_actualizacion).split('/')) == 2:
            return str(self.imagen_actualizacion).split('/')[1]
        elif len(str(self.imagen_actualizacion).split('/')) == 1:
            return str(self.imagen_actualizacion).split('/')[0]
        else:
            return str(self.imagen_actualizacion) """

    def save(self, *args, **kwargs):
        if self.imagen_actualizacion:
            if self.imagen_actualizacion.url.endswith('.png') or self.imagen_actualizacion.url.endswith('.PNG'):
                print("Es un png")
                self.imagen_actualizacion = self.imagen_actualizacion
            else:
                self.imagen_actualizacion = self.compressImage(self.imagen_actualizacion)
        super(ImagenActualizacion, self).save(*args, **kwargs)

    def compressImage(self, uploadedImage):
        try:
            imageTemproary = Image.open(uploadedImage)
            outputIoStream = BytesIO()

            if imageTemproary.width > 1028 or imageTemproary.height > 1028:
                maxsize = (1028, 1028)
                imageTemproary.thumbnail(maxsize, Image.ANTIALIAS)

            imageTemproary.save(outputIoStream, format='JPEG', quality=60)
            outputIoStream.seek(0)
            uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                                 'image/jpeg', sys.getsizeof(outputIoStream), None)
        except:
            print("imagen no encontrada")
        return uploadedImage

    def __str__(self):
        return self.titulo_imagen