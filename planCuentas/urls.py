from django.urls import path
from .views import *

urlpatterns = [
    path('listar_subcuentas/', ListarSubcuenta.as_view(), name="listar_subcuentas"),
    path('registrar_subcuenta/', registrar_subcuenta, name="registrar_subcuenta"),
    path('ajax/obtener_cuentas/', obtener_cuentas, name="obtener_cuentas"),
    path('eliminar_subcuenta/', eliminar_subcuenta, name="eliminar_subcuenta"),
    path('editar_subcuenta/<int:pk>/', editar_subcuenta, name="editar_subcuenta"),
]