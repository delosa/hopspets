from .models import *
from django import forms


class CrearSubCuentaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearSubCuentaForm, self).__init__(*args, **kwargs)
        self.fields['codigo'].label = 'Código'
        self.fields['tipo_subcuenta'].label = 'Tipo'

    class Meta:
        model = SubCuenta
        fields = (
            'codigo',
            'nombre',
            'tipo_subcuenta',
        )