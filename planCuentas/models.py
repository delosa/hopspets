from django.db import models
from django.core.validators import RegexValidator

numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores numericos')

class Clase(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="Nombre")
    codigo = models.DecimalField(max_digits=1, decimal_places=0, verbose_name="Clase")

    def __str__(self):
        return '%s - %s' % (self.codigo, self.nombre)

class Grupo(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="Nombre")
    codigo = models.DecimalField(max_digits=1, decimal_places=0, verbose_name="Grupo")
    clase = models.ForeignKey(Clase, on_delete=models.CASCADE)

    def __str__(self):
        return '%s - %s' % (self.codigo, self.nombre)

class Cuenta(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="Nombre")
    codigo = models.DecimalField(max_digits=2, decimal_places=0, verbose_name="Código Cuenta", validators=[numeric])
    editar = models.BooleanField()
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)

    def __str__(self):
        return '%s - %s' % (self.codigo, self.nombre)

TIPOS_DE_SUBCUENTA = (('CP', 'Cuentas por pagar'), ('ND', 'Nota débito'), ('G', 'Gastos'), ('C', 'Compras'), ('CC', 'Cuentas por cobrar'), ('NC', 'Nota Crédito'), ('CI', 'Comprobante de ingresos'), ('PV', 'Punto de venta'))

class SubCuenta(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="Nombre")
    codigo = models.DecimalField(max_digits=2, decimal_places=0, verbose_name="Código Sub-Cuenta", validators=[numeric])
    cuenta = models.ForeignKey(Cuenta, on_delete=models.CASCADE)
    tipo_subcuenta = models.CharField(max_length=20, verbose_name="Tipo de subcuenta", choices=TIPOS_DE_SUBCUENTA)

    def codigoCompleto(self):
        codigo_cuenta = str(self.cuenta.codigo)
        if len(codigo_cuenta) == 1:
            codigo_cuenta = '0'+codigo_cuenta

        codigo_subcuenta = str(self.codigo)
        if len(codigo_subcuenta) == 1:
            codigo_subcuenta = '0' + codigo_subcuenta

        return str(self.cuenta.grupo.clase.codigo) + str(self.cuenta.grupo.codigo) + codigo_cuenta + codigo_subcuenta

    def __str__(self):
        return '%s - %s' % (self.codigo, self.nombre)