from django.views.generic import *
from .models import *
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
import json
from django.shortcuts import get_object_or_404
from django.contrib import messages


def registrar_subcuenta(request):
    if request.method == 'POST':

        req = json.loads(request.body.decode('utf-8'))
        codigo = str(req['codigo'])
        nombre = str(req['nombre'])
        tipo_subcuenta = str(req['tipo_subcuenta'])
        cuenta = str(req['cuenta'])

        mi_cuenta = get_object_or_404(Cuenta, pk=cuenta)

        nueva_subcuenta = SubCuenta(codigo=codigo,nombre=nombre,tipo_subcuenta=tipo_subcuenta,cuenta=mi_cuenta)
        nueva_subcuenta.save()

        form = CrearSubCuentaForm()
        return render(request, 'registrar_subcuenta.html', {
            'form': form,
            'menu_plan_cuentas': True,
        })

    else:
        form = CrearSubCuentaForm()
        return render(request, 'registrar_subcuenta.html', {
            'form': form,
            'menu_plan_cuentas': True,
        })


class ListarSubcuenta (ListView):
    model = SubCuenta
    template_name = "listar_subcuentas.html"

    def get_queryset(self):
        queryset = super(ListarSubcuenta, self).get_queryset()
        return queryset.order_by('-pk')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarSubcuenta, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarSubcuenta, self).get_context_data(**kwargs)
        context['menu_plan_cuentas'] = True
        return context


def obtener_cuentas(request):

    all_cuentas = Cuenta.objects.all()
    all_grupos = Grupo.objects.all()
    all_clases = Clase.objects.all()
    all_subcuentas = SubCuenta.objects.all()

    data = {
        'all_cuentas': serializers.serialize('json', all_cuentas),
        'all_grupos': serializers.serialize('json', all_grupos),
        'all_clases': serializers.serialize('json', all_clases),
        'all_subcuentas': serializers.serialize('json', all_subcuentas),
    }
    return JsonResponse(data)


def eliminar_subcuenta(request):
    id_subcuenta = request.GET.get('id', '')
    mi_subcuenta = get_object_or_404(SubCuenta, pk=id_subcuenta)
    mi_subcuenta.delete()
    messages.success(request, 'Subcuenta eliminada con exito!')
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)


def editar_subcuenta(request,pk):
    if request.method == 'POST':

        req = json.loads(request.body.decode('utf-8'))
        codigo = str(req['codigo'])
        nombre = str(req['nombre'])
        tipo_subcuenta = str(req['tipo_subcuenta'])
        cuenta = str(req['cuenta'])

        mi_cuenta = get_object_or_404(Cuenta, pk=cuenta)

        mi_subcuenta = get_object_or_404(SubCuenta, pk=pk)
        mi_subcuenta.codigo = codigo
        mi_subcuenta.nombre = nombre
        mi_subcuenta.tipo_subcuenta = tipo_subcuenta
        mi_subcuenta.cuenta = mi_cuenta
        mi_subcuenta.save()

        form = CrearSubCuentaForm()
        return render(request, 'editar_subcuenta.html', {
            'form': form,
            'menu_plan_cuentas': True,
        })

    else:
        subcuenta = get_object_or_404(SubCuenta, pk=pk)
        form = CrearSubCuentaForm(instance=subcuenta)
        return render(request, 'editar_subcuenta.html', {
            'form': form,
            'id_clase': subcuenta.cuenta.grupo.clase.id,
            'id_grupo': subcuenta.cuenta.grupo.id,
            'id_cuenta': subcuenta.cuenta.id,
            'id_subcuenta': subcuenta.id,
            'menu_plan_cuentas': True,
        })