from .models import *
from django import forms
from datetime import datetime
from consentimiento.models import Consentimiento, ConsentimientoMascota

class CrearConsentimientoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearConsentimientoForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].label = 'Nombre'
        self.fields['descripcion'].label = 'Descripción'
        self.fields['tipo_consentimiento'].label = 'Tipo de formato'

    class Meta:
        model = Consentimiento
        fields = (
            'nombre',
            'descripcion',
            'tipo_consentimiento'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': '2'}),
            'nombre': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '}),
            'tipo_consentimiento': forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z+0-9()-.#_Ññáéíóú/ ]+', 'title':'Enter Characters Only '})
        }

class CrearConsentimientoMascotaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearConsentimientoMascotaForm, self).__init__(*args, **kwargs)
        self.fields['nombre_consentimiento'].label = 'Nombre'

    class Meta:
        model = ConsentimientoMascota
        fields = (
            'nombre_consentimiento',
            'archivo',
        )