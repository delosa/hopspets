from django.db import models
from mascotas.models import Mascota
from django.db import connection


def get_upload_to_documento(instance, filename):
    tenant_name = connection.tenant.schema_name
    return 'upload/%s/consentimientos/%s' % (tenant_name, filename)

class Consentimiento(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    descripcion = models.CharField(max_length=2000, verbose_name='Descripción')
    tipo_consentimiento = models.CharField(max_length=100, verbose_name='Tipo de consentimiento')

class ConsentimientoMascota(models.Model):
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    nombre_consentimiento = models.CharField(max_length=500, verbose_name='Nombre')
    fecha = models.DateTimeField(auto_now_add=True)
    firmado = models.BooleanField(default = False)
    archivo = models.FileField(upload_to=get_upload_to_documento, verbose_name='Archivo')