from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_consentimiento/', CrearConsentimiento.as_view(), name="registrar_consentimiento"),
    path('editar_consentimiento/<int:pk>/', EditarConsentimiento.as_view(), name="editar_consentimiento"),
    path('listar_consentimiento/', ListarConsentimiento.as_view(), name="listar_consentimiento"),
    path('detalle_consentimiento/<int:pk>/', DetalleConsentimiento.as_view(), name='detalle_consentimiento'),
    path('eliminar_consentimiento/', eliminar_consentimiento, name='eliminar_consentimiento'),
    path('registrar_consentimiento_mascota/<int:id_mascota>/', CrearConsentimientoMascota.as_view(), name="registrar_consentimiento_mascota"),
    path('eliminar_consentimiento_mascota/', eliminar_consentimiento_mascota, name="eliminar_consentimiento_mascota"),
    path('editar_consentimiento_mascota/<int:pk>/', EditarConsentimientoMascota.as_view(), name="editar_consentimiento_mascota"),
]