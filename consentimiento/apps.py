from django.apps import AppConfig


class ConsentimientoConfig(AppConfig):
    name = 'consentimiento'
