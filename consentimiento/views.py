from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect,render
from .forms import *
from django.core import serializers
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from consentimiento.models import Consentimiento, ConsentimientoMascota
from decimal import Decimal
from django.http import JsonResponse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from mascotas.models import Mascota

class CrearConsentimiento(SuccessMessageMixin, CreateView):
    model = Consentimiento
    form_class = CrearConsentimientoForm
    template_name = "registrar_consentimiento.html"
    success_url = reverse_lazy('listar_consentimiento')
    success_message = "¡Consentimiento creado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearConsentimiento, self).dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        consentimiento = form.instance
        self.object = form.save()
        return super(CrearConsentimiento, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearConsentimiento, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['menu_consentimiento'] = True
        return context

class ListarConsentimiento(ListView):
    model = Consentimiento
    template_name = "listar_consentimiento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarConsentimiento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarConsentimiento, self).get_context_data(**kwargs)
        context['menu_consentimiento'] = True
        return context

class EditarConsentimiento(SuccessMessageMixin, UpdateView):
    model = Consentimiento
    form_class = CrearConsentimientoForm
    template_name = "registrar_consentimiento.html"
    success_url = reverse_lazy('listar_consentimiento')
    success_message = "¡Consentimiento editado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarConsentimiento, self).dispatch(request, *args, **kwargs)
        
    def get_context_data(self, **kwargs):
        context = super(EditarConsentimiento, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['menu_consentimiento'] = True
        return context  
    
class DetalleConsentimiento(DetailView):
    model = Consentimiento
    template_name = "detalle_consentimiento.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleConsentimiento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleConsentimiento, self).get_context_data(**kwargs)
        context['menu_consentimiento'] = True
        return context

def eliminar_consentimiento(request):
    id_consentimiento = request.GET.get('id', '')
    consentimiento = get_object_or_404(Consentimiento, pk=id_consentimiento)
    consentimiento.delete()
    messages.success(request, '¡Consentimiento eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)

class CrearConsentimientoMascota(SuccessMessageMixin, CreateView):
    model = ConsentimientoMascota
    form_class = CrearConsentimientoMascotaForm
    template_name = "registrar_consentimiento_mascota.html"
    success_message = "¡Consentimiento creado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearConsentimientoMascota, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_mascota =  get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return '/mascotas/detalle_mascota/' + str(mi_mascota.id)

    def form_valid(self,form):
        consentimientoMascota = form.instance
        consentimientoMascota.mascota = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        self.object = form.save()
        return super(CrearConsentimientoMascota, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearConsentimientoMascota, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['mascotas'] = True
        context['mascota'] = get_object_or_404(Mascota, pk=self.kwargs['id_mascota'])
        return context


class EditarConsentimientoMascota(SuccessMessageMixin, UpdateView):
    model = ConsentimientoMascota
    form_class = CrearConsentimientoMascotaForm
    template_name = "registrar_consentimiento_mascota.html"
    success_message = "¡Consentimiento editado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarConsentimientoMascota, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        mi_consentimiento =  get_object_or_404(ConsentimientoMascota, pk=self.kwargs['pk'])
        return '/mascotas/detalle_mascota/' + str(mi_consentimiento.mascota.id)

    def get_context_data(self, **kwargs):
        context = super(EditarConsentimientoMascota, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['mascotas'] = True
        mi_consentimiento = get_object_or_404(ConsentimientoMascota, pk=self.kwargs['pk'])
        context['mascota'] = mi_consentimiento.mascota
        return context


def eliminar_consentimiento_mascota(request):
    id_consentimiento_mascota = request.GET.get('id', '')
    consentimiento_mascota = get_object_or_404(ConsentimientoMascota, pk=id_consentimiento_mascota)
    consentimiento_mascota.delete()
    messages.success(request, '¡Consentimiento de la mascota eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)