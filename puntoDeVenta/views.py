from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.http import JsonResponse
from django.http import HttpResponse
from datetime import datetime , timedelta
import decimal
from veteriariaTenant.models import Veterinaria
from clientes.models import Cliente
from tratamientos.models import Tratamiento
from estetica.models import FacturaEstetica, TipoEstetica
from mascotas.models import Mascota
from citas.models import HistoriaClinica,MotivoCita,MotivoArea
from productos.models import *
from gastos.models import *
from gastos.forms import *
from productos.forms import *
from desparasitacion.models import Desparasitacion, Desparasitante, AplicacionDesparasitacion
from vacunacion.models import Vacunacion, Vacuna, AplicacionVacunacion
from guarderia.models import Guarderia
from itertools import chain
from gastos.models import GastoProducto, GastoMedicamento
from decimal import Decimal
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
import math
from datetime import datetime
from categoriasInventario.models import *
from bodegas.models import Bodega

##**********************Punto de Venta***********************************##
def abrir_punto_de_venta(request):
    if request.method == "GET":
        now = datetime.now()
        cerrar_punto_emergencia()
        x = datetime(now.year, now.month, now.day)
        y = x + timedelta(days=1)

        punto_activo = False
        puntoactivo = False

        if Punto.objects.filter(estado=True):
            punto_activo = Punto.objects.filter(estado=True)
            for puntos in punto_activo:
                if request.user.id == puntos.responsable.id:
                    return redirect('/puntoDeVenta/listar_puntos_hoy/')
                else:
                    puntoactivo = False

        puntos = Punto.objects.all().order_by('-fecha_inicio')

        return render(request,'abrir_punto_de_venta.html',{'form':AbrirPuntoForm(),
        'menu_punto' : True, 'puntos': puntos, 'punto_activo': puntoactivo, 'puntos_generales': punto_activo})

def especificacion_punto(request):
    if request.method == "GET":
        now = datetime.now()
        cerrar_punto_emergencia()
        x = datetime(now.year, now.month, now.day)
        y = x + timedelta(days=1)

        punto_activo = False
        puntoactivo = False

        if Punto.objects.filter(estado=True):
            punto_activo = Punto.objects.filter(estado=True)
            for puntos in punto_activo:
                if request.user.id == puntos.responsable.id:

                    return redirect('/puntoDeVenta/listar_puntos_hoy/')
                else:
                    puntoactivo = False

        puntos = Punto.objects.all().order_by('-fecha_inicio')

        return render(request,'especificacion_punto.html',{'form':AbrirPuntoForm(),
        'menu_punto' : True, 'puntos': puntos, 'punto_activo': puntoactivo})

def agregar_punto(request):
    responsable = get_object_or_404(Usuario, id=request.user.id)
    dinero = request.GET.get('dinero', '')
    nombre = request.GET.get('nombre_punto', '')
    now = datetime.now()
    cerrar_punto_emergencia()
    add_punto = Punto(fecha_inicio = now,estado = True,dinero_inicial=dinero, responsable=responsable, nombre_punto=nombre)
    add_punto.save()
    data = {
        'eliminacion': True,
    }
    return JsonResponse(data)

def cerrar_punto(request,id_punto):
    punto = get_object_or_404(Punto,pk=id_punto)
    now = datetime.now()
    punto.estado = False
    punto.fecha_final = now
    punto.save()
    data = {
        'cerrar_punto': True
    }
    return JsonResponse(data)

def cerrar_punto_emergencia():
    puntos = Punto.objects.filter(estado=True)
    for punto in puntos:
        now = datetime.now()
        x = datetime(now.year, now.month, now.day)
        z = datetime(punto.fecha_inicio.year, punto.fecha_inicio.month, punto.fecha_inicio.day)
        y = x + timedelta(days=1)
        if z < x and not punto.fecha_final:
            punto_pago_selected = PuntoPago.objects.filter(punto = punto).exclude(pago__estado_pago='Anulada')
            punto_pago_selected2 = PuntoCompra.objects.filter(punto = punto).exclude(compra__estado_pago='Anulada')
            punto_abono_pago = PuntoAbonoPago.objects.filter(punto=punto).exclude(abono_pago__pago__estado_pago='Anulada')
            punto_abono_compra = PuntoAbonoCompra.objects.filter(punto = punto).exclude(abono_compra__gasto__estado_pago='Anulada')
            pagos = []
            total_venta = 0
            total_compra = 0
            for punto_p in punto_pago_selected:
                pago_temp = get_object_or_404(Pago,pk=punto_p.pago_id)
                if pago_temp.forma_pago == 'Debito':
                    total_venta += pago_temp.total_factura

            for abono_punto in punto_abono_pago:
                if abono_punto.valor_abonado == 0:
                    total_venta += abono_punto.abono_pago.valor_cuota
                else:
                    total_venta += abono_punto.valor_abonado
            for punto_co in punto_pago_selected2:
                compra_temp = get_object_or_404(Gasto,pk=punto_co.compra_id)
                if compra_temp.forma_pago == 'Debito':
                    total_compra += compra_temp.valor_total

            for abono_compra_punto in punto_abono_compra:
                if abono_compra_punto.valor_abonado == 0:
                    total_compra += abono_compra_punto.abono_compra.valor_cuota
                else:
                    total_compra += abono_compra_punto.valor_abonado

            punto.dinero_final = punto.dinero_inicial + total_venta - total_compra
            punto.fecha_final = now
            punto.estado = False
            punto.save()

class ListarPuntosHoy(ListView):
    model = Pago
    template_name = "listar_puntos_hoy.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPuntosHoy, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPuntosHoy, self).get_context_data(**kwargs)
        context['menu_punto'] = True
        cerrar_punto_emergencia()
        now = datetime.now()
        x = datetime(now.year, now.month, now.day)
        y = x + timedelta(days=1)
        punto = Punto.objects.filter(estado=True,responsable__id=self.request.user.id)
        punto_pago_selected = PuntoPago.objects.filter(punto = punto[0]).exclude(pago__estado_pago='Anulada')
        punto_pago_selected2 = PuntoCompra.objects.filter(punto = punto[0]).exclude(compra__estado_pago='Anulada')
        punto_abono_compra = PuntoAbonoCompra.objects.filter(punto = punto[0]).exclude(abono_compra__gasto__estado_pago='Anulada')
        pagos = []
        compras = []
        total_venta = 0
        total_compra = 0
        pago_credito_values = []

        for punto_p in punto_pago_selected:
            pago_temp = get_object_or_404(Pago,pk=punto_p.pago_id)
            if pago_temp.forma_pago == 'Debito':
                pago_credito_values.append("-")
                total_venta += pago_temp.total_factura
                pagos.append(pago_temp)

        for punto_co in punto_pago_selected2:
            compra_temp = get_object_or_404(Gasto,pk=punto_co.compra_id)
            if compra_temp.forma_pago == 'Debito':
                total_compra += compra_temp.valor_total
                compras.append(compra_temp)

        for abono_compra_punto in punto_abono_compra:
            if abono_compra_punto.valor_abonado == 0:
                total_compra += abono_compra_punto.abono_compra.valor_cuota
                compras.append(abono_compra_punto.abono_compra.gasto)
            else:
                total_compra += abono_compra_punto.valor_abonado
                compras.append(abono_compra_punto.abono_compra.gasto)

        motivos_pagos = []
        motivos_gastos = []

        for pago in pagos:

            motivos = []
            if PagoGuarderia.objects.filter(pago__id=pago.id):
                motivos.append("Guardería")

            if PagoProducto.objects.filter(pago__id=pago.id):
                motivos.append("Producto")

            if PagoMedicamento.objects.filter(pago__id=pago.id):
                motivos.append("Medicamento")

            if PagoVacuna.objects.filter(pago__id=pago.id):
                motivos.append("Vacuna")

            if PagoDesparasitante.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitante")

            if PagoTratamiento.objects.filter(pago__id=pago.id):
                motivos.append("Cita médica")

            if PagoEstetica.objects.filter(pago__id=pago.id):
                motivos.append("Estética")

            if PagoVacunacion.objects.filter(pago__id=pago.id):
                motivos.append("Vacunación")

            if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitación")

            if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                motivos.append("Procedimiento")

            if PagoOtro.objects.filter(pago__id=pago.id):
                motivos.append("Otro")

            motivos_pagos.append(motivos)

        for gasto in compras:

            motivos = []
            if GastoProducto.objects.filter(gasto__id=gasto.id):
                motivos.append("Producto")

            if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                motivos.append("Medicamento")

            if GastoVacuna.objects.filter(gasto__id=gasto.id):
                motivos.append("Vacuna")

            if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                motivos.append("Desparasitante")

            if GastoServicio.objects.filter(gasto__id=gasto.id):
                motivos.append("Servicio")

            if GastoOtro.objects.filter(gasto__id=gasto.id):
                motivos.append("Otro")

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos
        context['motivos_pagos'] = motivos_pagos
        context['pagos_hoy'] = pagos
        context['compras_hoy'] = compras
        context['total_venta'] = total_venta
        context['total_compra'] = total_compra
        context['abonos_pago'] = pago_credito_values
        context['dinero_inicial'] = punto[0].dinero_inicial
        context['total'] = punto[0].dinero_inicial + total_venta - total_compra
        context['abonos_compras'] = PuntoAbonoCompra.objects.filter(punto=punto[0]).exclude(gasto__estado_pago='Anulada')
        for puntos in punto:
            puntos.dinero_final = punto[0].dinero_inicial + total_venta - total_compra
            puntos.save()
        context['punto'] = punto[0]

        total_efectivo = punto[0].dinero_inicial
        total_ventas_efectivo = 0
        total_tarjeta_debito = 0
        total_consignacion = 0
        total_tarjeta_credito = 0
        total_otros = 0
        metodos_todos = MedioPago.objects.all()


        for pago in pagos:
            metodos = MedioPago.objects.filter(pago__id=pago.id)
            for metodo in metodos:
                if pago.forma_pago == 'Debito' and metodo.medio_pago == 'Efectivo':
                    total_ventas_efectivo += metodo.valor_pagado
                    total_efectivo += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Tarjeta debito':
                    total_tarjeta_debito += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Consignacion':
                    total_consignacion += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Tarjeta credito':
                    total_tarjeta_credito += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Otro':
                    total_otros += metodo.valor_pagado

        total_compras_efectivo = 0
        total_compras_tarjeta_debito = 0
        total_compras_consignacion = 0
        total_compras_tarjeta_credito = 0
        total_compras_otros = 0

        for compra in compras:
            if compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Efectivo':
                total_compras_efectivo += compra.valor_total
                total_efectivo -= compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta debito':
                total_compras_tarjeta_debito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Consignacion':
                total_compras_consignacion += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta credito':
                total_compras_tarjeta_credito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Otro':
                total_compras_otros += compra.valor_total

        for abono in PuntoAbonoCompra.objects.filter(punto=punto[0]).exclude(gasto__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_compras_efectivo += abono.valor_abonado
                total_efectivo -= abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_compras_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_compras_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_compras_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_compras_otros += abono.valor_abonado

        context['total_efectivo'] = total_efectivo
        context['total_ventas_efectivo'] = total_ventas_efectivo
        context['total_tarjeta_debito'] = total_tarjeta_debito
        context['total_consignacion'] = total_consignacion
        context['total_tarjeta_credito'] = total_tarjeta_credito
        context['total_otros'] = total_otros
        context['total_compras_efectivo'] = total_compras_efectivo
        context['total_compras_tarjeta_debito'] = total_compras_tarjeta_debito
        context['total_compras_consignacion'] = total_compras_consignacion
        context['total_compras_tarjeta_credito'] = total_compras_tarjeta_credito
        context['total_compras_otros'] = total_compras_otros
        context['metodos'] = metodos_todos

        return context

@login_required
def crearPago_punto(request):
    responsable = get_object_or_404(Usuario, id=request.user.id)
    if request.method == 'POST':
        form_pago = RegistrarPagoForm(request.POST)
        return redirect('/puntoDeVenta/listar_puntos_hoy/')
    else:
        form_pago = RegistrarPagoForm()
        all_productos = Producto.objects.filter(estado=True)
        all_desparasitante = Desparasitante.objects.filter(estado='Activo')
        all_medicamento = Medicamento.objects.filter(estado='Activo')
        all_vacuna = Vacuna.objects.filter(estado='Activa')
        array_productos = json.dumps(list(Producto.objects.filter(estado=True).values_list('id','nombre','codigo','codigo','tipo_producto','cantidad','precio_venta','cantidad_por_unidad','precio_por_unidad','cantidad_total_unidades','unidad_medida','cantidad_unidad')), cls=DecimalEncoder)
        array_tratamientos = json.dumps(list(Tratamiento.objects.all().values_list('id', 'nombre', 'nombre', 'precio')))
        array_motivo_cita = json.dumps(list(MotivoCita.objects.all().values_list('id', 'nombre', 'descripcion', 'precio')))
        array_area_consulta = json.dumps(list(MotivoArea.objects.all().values_list('area_consulta', 'historia_clinica','estado')),cls=DecimalEncoder)

        array_medicamentos = json.dumps(list(Medicamento.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad','precio_venta','cantidad_por_unidad','precio_por_unidad','unidad_medida','cantidad_unidad','cantidad_total_unidades', 'codigo_inventario')), cls=DecimalEncoder)
        array_vacunas = json.dumps(list(Vacuna.objects.filter(estado='Activa').values_list('id', 'nombre', 'codigo', 'cantidad','precio_venta','cantidad_por_unidad','precio_por_unidad','cantidad_total_unidades','unidad_medida','cantidad_unidad', 'codigo_inventario')), cls=DecimalEncoder)
        array_desparasitante = json.dumps(list(Desparasitante.objects.filter(estado='Activo').values_list('id', 'nombre', 'codigo', 'cantidad','precio_venta','cantidad_por_unidad','precio_por_unidad','cantidad_total_unidades','unidad_medida','cantidad_unidad', 'codigo_inventario')), cls=DecimalEncoder)

        all_puntos = Punto.objects.filter(estado=True,responsable=responsable)
        categorias_nuevas = CategoriaInventario.objects.all()
        elementos_categorias_nuevas = ElementoCategoriaInventario.objects.all()
        clientes = Cliente.objects.all()

        return render(request, 'registrar_punto_de_venta.html', {
            'form_pago': form_pago,
            'all_productos': all_productos,
            'all_medicamento': all_medicamento,
            'all_vacuna': all_vacuna,
            'all_desparasitante': all_desparasitante,
            'array': array_productos,
            'array_tratamientos': array_tratamientos,
            'menu_punto': True,
            'punto': all_puntos[0],
            'array_medicamentos': array_medicamentos,
            'array_vacunas': array_vacunas,
            'array_desparasitante': array_desparasitante,
            'array_motivo_cita': array_motivo_cita,
            'array_area_consulta':array_area_consulta,
            'categorias_nuevas': categorias_nuevas,
            'elementos_categorias_nuevas': elementos_categorias_nuevas,
            'clientes': clientes
        })

def filtrar_tipo_producto(request):
    tipo_producto = request.GET.get('tipo', None)

    if tipo_producto == 'productos':
        qs1 = Producto.objects.filter(estado=True).order_by('nombre')
        qs2 = PaqueteProducto.objects.all().order_by('nombre')

        lista_elementos = sorted(chain(qs1, qs2), key=lambda instance: instance.nombre.upper())

    if tipo_producto == 'vacunas':
        lista_elementos = Vacuna.objects.filter(estado='Activa').order_by('nombre')

    if tipo_producto == 'desparasitantes':
        lista_elementos = Desparasitante.objects.filter(estado='Activo').order_by('nombre')

    if tipo_producto == 'medicamentos':
        lista_elementos = Medicamento.objects.filter(estado='Activo').order_by('nombre')

    if tipo_producto == 'insumos':
        lista_elementos = Insumo.objects.filter(estado='Activo').order_by('nombre')

    if tipo_producto == 'todos':
        qs1 = Producto.objects.filter(estado=True)
        qs2 = PaqueteProducto.objects.all()
        qs3 = Vacuna.objects.filter(estado='Activa')
        qs4 = Desparasitante.objects.filter(estado='Activo')
        qs5 = Medicamento.objects.filter(estado='Activo')
        qs6 = Insumo.objects.filter(estado='Activo')

        for q in qs1:
            q.tipo_elemento = 'Producto'

        for q in qs2:
            q.tipo_elemento = 'Paquete'

        for q in qs3:
            q.tipo_elemento = 'Vacuna'

        for q in qs4:
            q.tipo_elemento = 'Desparasitante'

        for q in qs5:
            q.tipo_elemento = 'Medicamento'

        for q in qs6:
            q.tipo_elemento = 'Insumo'

        lista_elementos = sorted(chain(qs1, qs2, qs3, qs4, qs5, qs6), key=lambda instance: instance.nombre.upper())

    data = {
        'lista_elementos': serializers.serialize('json', lista_elementos)
    }
    return JsonResponse(data)

def registrarPago_punto(request):
    req = json.loads( request.body.decode('utf-8') )
    responsable = get_object_or_404(Usuario, id=request.user.id)

    if req['cliente'] == 0:
        date_now = datetime.now()
        x = datetime(date_now.year, date_now.month, date_now.day)
        fecha = x.strftime("%Y-%m-%d")
        nuevo_pago = Pago(fecha=fecha,comentarios=req['comentarios'], total_factura=req['total'],total_impuesto=req['impuesto'],sub_total=req['subtotal'],descuento=req['descuento'],responsable=responsable)
        nuevo_pago.save()
    else:
        date_now = datetime.now()
        x = datetime(date_now.year, date_now.month, date_now.day)
        fecha = x.strftime("%Y-%m-%d")
        cliente = get_object_or_404(Cliente, pk=req['cliente'])
        nuevo_pago = Pago(cliente=cliente, fecha=fecha,
                          comentarios=req['comentarios'], total_factura=req['total'], total_impuesto=req['impuesto'],
                          sub_total=req['subtotal'], descuento=req['descuento'], responsable=responsable)
        nuevo_pago.save()


    nuevo_pago.forma_pago = req['forma_pago']

    if req['forma_pago'] == 'Debito':
        nuevo_pago.tipo_pago_debito = req['medio_pago_debito']
        punto_on = Punto.objects.filter(estado= True,responsable=responsable)
        if punto_on:
            now = datetime.now()
            x = datetime(now.year, now.month, now.day)
            z = datetime.strptime(nuevo_pago.fecha, "%Y-%m-%d")
            y = x + timedelta(days=1)
            if z >= x and z < y:
                punto_pago = PuntoPago(punto = punto_on[0], pago=nuevo_pago)
                punto_pago.save()

    if req['forma_pago'] == 'Credito':
        nuevo_pago.numero_cuotas_credito = req['numero_cuotas']
        nuevo_pago.saldo = req['total']
        nuevo_pago.estado_pago = 'Pendiente'

        for cuota in req['cuotas']:
            mi_cuota = CuotaPago(pago=nuevo_pago, fecha_limite=cuota['fecha'], valor_cuota=cuota['abono'], saldo_cuota=cuota['abono'])
            if mi_cuota.fecha_limite == nuevo_pago.fecha:
                abono_pago = AbonoPago(pago=nuevo_pago, valor_abonado=mi_cuota.saldo_cuota, medio_pago=req['medio_pago_credito'])
                abono_pago.save()
                nuevo_pago.saldo = nuevo_pago.saldo - int(mi_cuota.saldo_cuota)
                nuevo_pago.save()
                mi_cuota.valor_pagado = int(mi_cuota.saldo_cuota)
                mi_cuota.saldo_cuota = 0
                mi_cuota.estado_pago = 'Pagada'
                mi_cuota.fecha_pago = mi_cuota.fecha_limite
                mi_cuota.save()
                punto_on = Punto.objects.filter(estado= True,responsable=responsable)
                if punto_on:
                    abono_punto_pago = PuntoAbonoPago(punto = punto_on[0], abono_pago=mi_cuota, medio_pago=req['medio_pago_credito'],pago=nuevo_pago,valor_abonado=mi_cuota.valor_pagado)
                    abono_punto_pago.save()

            else:
                mi_cuota.save()

    messages.success(request, 'Venta Registrada con Èxito!')

    nuevo_pago.save()

    for producto in req['productos']:

        if producto['tipo'] == 'Producto':
            mi_producto = get_object_or_404(Producto, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_producto = PagoProducto(producto=mi_producto,pago=nuevo_pago,cantidad=cantidad, total=int(producto['costo_total']))

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_producto.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_producto.cantidad = int(mi_producto.cantidad) - cantidad

                mi_producto.cantidad_total_unidades = int(mi_producto.cantidad_total_unidades) - ( cantidad * int(mi_producto.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                       id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_producto.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_producto.cantidad_total_unidades = int(mi_producto.cantidad_total_unidades) - cantidad

                mi_producto.cantidad = math.floor( int(mi_producto.cantidad_total_unidades) / int(mi_producto.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_producto.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                       id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
            pago_producto.save()
            mi_producto.save()

        if producto['tipo'] == 'Medicamento':
            mi_medicamento = get_object_or_404(Medicamento, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_medicamento = PagoMedicamento(medicamento=mi_medicamento, pago=nuevo_pago, cantidad=cantidad,
                                         total=int(producto['costo_total']))
            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_medicamento.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_medicamento.cantidad = int(mi_medicamento.cantidad) - cantidad

                mi_medicamento.cantidad_total_unidades = int(mi_medicamento.cantidad_total_unidades) - ( cantidad * int(mi_medicamento.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_medicamento.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_medicamento.cantidad_total_unidades = int(mi_medicamento.cantidad_total_unidades) - cantidad

                mi_medicamento.cantidad = math.floor( int(mi_medicamento.cantidad_total_unidades) / int(mi_medicamento.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_medicamento.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            pago_medicamento.save()
            mi_medicamento.save()



        if producto['tipo'] == 'Vacuna':
            mi_vacuna = get_object_or_404(Vacuna, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_vacuna = PagoVacuna(vacuna=mi_vacuna, pago=nuevo_pago, cantidad=cantidad,
                                         total=int(producto['costo_total']))



            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_vacuna.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_vacuna.cantidad = int(mi_vacuna.cantidad) - cantidad

                mi_vacuna.cantidad_total_unidades = int(mi_vacuna.cantidad_total_unidades) - ( cantidad * int(mi_vacuna.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_vacuna.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_vacuna.cantidad_total_unidades = int(mi_vacuna.cantidad_total_unidades) - cantidad

                mi_vacuna.cantidad = math.floor( int(mi_vacuna.cantidad_total_unidades) / int(mi_vacuna.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_vacuna.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            pago_vacuna.save()
            mi_vacuna.save()

        if producto['tipo'] == 'Desparasitante':
            mi_desparasitante = get_object_or_404(Desparasitante, pk=producto['id'])
            cantidad = float(producto['cantidad'])
            pago_desparasitante = PagoDesparasitante(desparasitante=mi_desparasitante, pago=nuevo_pago, cantidad=cantidad,
                                         total=int(producto['costo_total']))

            costo_producto = 0

            if str(producto['tipo_de_venta']) == 'unidad':
                pago_desparasitante.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_desparasitante.cantidad = int(mi_desparasitante.cantidad) - cantidad

                mi_desparasitante.cantidad_total_unidades = int(mi_desparasitante.cantidad_total_unidades) - ( cantidad * int(mi_desparasitante.cantidad_unidad) )

                descripcion = "Venta de " + producto['cantidad'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            if str(producto['tipo_de_venta']) == 'a granel':
                pago_desparasitante.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * int(producto['costo_unitario'])

                mi_desparasitante.cantidad_total_unidades = int(mi_desparasitante.cantidad_total_unidades) - cantidad

                mi_desparasitante.cantidad = math.floor( int(mi_desparasitante.cantidad_total_unidades) / int(mi_desparasitante.cantidad_unidad) )

                descripcion = "Venta de " + str(cantidad)+" "+mi_desparasitante.unidad_medida +"."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                       id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                       id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            mi_desparasitante.save()
            pago_desparasitante.save()

    for vacunacion in req['vacunaciones']:
        mi_vacunacion = get_object_or_404(Vacunacion, pk=vacunacion['id'])
        pago_vacunacion = PagoVacunacion(vacunacion=mi_vacunacion, pago=nuevo_pago, total=int(vacunacion['costo']))
        pago_vacunacion.save()
        mi_vacunacion.costo = vacunacion['costo']
        mi_vacunacion.estado = 'Pagada'
        mi_vacunacion.save()

    for desparasitacion in req['desparasitaciones']:
        mi_desparasitacion = get_object_or_404(Desparasitacion, pk=desparasitacion['id'])
        pago_desparasitacion = PagoDesparasitacion(desparasitacion=mi_desparasitacion, pago=nuevo_pago, total=int(desparasitacion['costo']))
        pago_desparasitacion.save()
        mi_desparasitacion.costo = desparasitacion['costo']
        mi_desparasitacion.estado = 'Pagada'
        mi_desparasitacion.save()

    for estetica in req['esteticas']:
        mi_estetica = get_object_or_404(FacturaEstetica, pk=estetica['id'])
        pago_estetica = PagoEstetica(estetica=mi_estetica, pago=nuevo_pago, total=int(estetica['valor_total']))
        pago_estetica.save()
        mi_estetica.saldo = 0
        mi_estetica.estado = 'Pagada'
        mi_estetica.save()

    for tratamiento in req['tratamientos']:
        if tratamiento['nombre_tratamiento'] == "Motivo Cita":
            mi_tratamiento = get_object_or_404(HistoriaClinica, pk=tratamiento['id'])
            pago_tratamiento = PagoTratamiento(tratamiento=mi_tratamiento, pago=nuevo_pago,
                                               total=int(tratamiento['costo']))
            pago_tratamiento.save()
            mi_tratamiento.costo = tratamiento['costo']
            mi_tratamiento.estado = 'Pagada'
            mi_tratamiento.save()
        if tratamiento['nombre_tratamiento'] == "Area Consulta":
            mi_tratamiento = get_object_or_404(HistoriaClinica, pk=tratamiento['id'])
            mi_area = get_object_or_404(Tratamiento, pk=tratamiento['motivo_cita'])
            motivo_cita = get_object_or_404(MotivoArea, historia_clinica=mi_tratamiento, area_consulta=mi_area)
            pago_area = PagoAreaConsulta(area_consulta=motivo_cita, pago=nuevo_pago, total=int(tratamiento['costo']))
            pago_area.save()
            motivo_cita.estado = 'Pagada'
            motivo_cita.save()

    for guarderia in req['guarderias']:
        mi_guarderia = get_object_or_404(Guarderia, pk=guarderia['id'])
        pago_guarderia = PagoGuarderia(guarderia=mi_guarderia, pago=nuevo_pago, total=int(guarderia['costo']))
        pago_guarderia.save()
        mi_guarderia.estado = 'Pagada'
        mi_guarderia.save()

    for otro in req['otros']:
        pago_otro = PagoOtro(descripcion=otro['nombre'], pago=nuevo_pago, total=int(otro['costo_total']))
        pago_otro.save()

    data = {
        'registrado': True,
        'id_pago': nuevo_pago.id
    }

    return JsonResponse(data)

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

def DetalleClientePunto(request):
    if request.method == 'POST':
        return redirect('/puntoDeVenta/registrar_pago_pdv/')
    else:
        form_pago = RegistrarPagoForm()

    return render(request, 'cliente_punto.html', {
            'form_pago': form_pago,
            'menu_punto': True,
        })

def GuardarPago(request):
    productos = request.GET.getlist('productos[]')
    total_pagar = request.GET.get('precio_total', None)
    cliente = request.GET.get('cliente', None)
    responsable = get_object_or_404(Usuario, id=request.user.id)
    valor = request.method

    punto_abierto = Punto.objects.filter(estado=True, responsable=responsable)
    prox_pago_punto = PuntoPago.objects.all().last()

    productosespera = PuntoEspera.objects.all()

    for prod in productosespera:
        productosespera.delete()

    datos_list = json.loads(productos[0])

    for product in datos_list:

        precio = float(product['precio_total'])
        if product['id_motivo'] != 0:
            guarda = PuntoEspera(id_producto=product['id'], tipo=product['tipo'], precio=precio, unidad=product['unidad'], granel=product['granel'], punto_abierto=punto_abierto[0].id, categoria=product['categoria'], id_motivo_area=product['id_motivo'])
        else:
            guarda = PuntoEspera(id_producto=product['id'], tipo=product['tipo'], precio=precio, unidad=product['unidad'], granel=product['granel'], punto_abierto=punto_abierto[0].id, categoria=product['categoria'])
        guarda.save()

    data = {
        'total_pagar': total_pagar,
        'cliente': cliente
    }
    return JsonResponse(data)

def registro_cliente(request):
    cliente = request.GET.get('cliente', None)
    responsable = get_object_or_404(Usuario, id=request.user.id)
    punto_abierto = Punto.objects.filter(estado=True, responsable=responsable)

    guarda = PuntoEsperaCliente(id_cliente=cliente,punto_abierto=punto_abierto[0].id)
    guarda.save()

    cliente_registrado = Cliente.objects.filter(pk=cliente)
    data = {
        'cliente_id': cliente,
        'nombres': str(cliente_registrado[0].nombres),
        'apellidos': str(cliente_registrado[0].apellidos),
        'tipo_documento': str(cliente_registrado[0].tipo_documento),
        'numero_documento': str(cliente_registrado[0].numero_documento),
        'ciudad': str(cliente_registrado[0].ciudad)
    }
    return JsonResponse(data)

def GenerarPago(request,pago,cliente):
    responsable = get_object_or_404(Usuario, id=request.user.id)
    all_puntos = Punto.objects.filter(estado=True,responsable=responsable)

    if cliente != 0:
        cliente_nombres = Cliente.objects.filter(pk=cliente)
        return render(request,'generar_pago_punto.html',{'menu_punto' : True, 'pago': pago, 'punto': all_puntos[0], 'cliente': cliente_nombres[0]})
    else:
        return render(request,'generar_pago_punto.html',{'menu_punto' : True, 'pago': pago, 'punto': all_puntos[0], 'cliente': 0})

def ValidarPago(request,pago,total):
    areas_consulta_pago = PagoAreaConsulta.objects.filter(pago=pago)
    productos_pago = PagoProducto.objects.filter(pago=pago)
    medicamentos_pago = PagoMedicamento.objects.filter(pago=pago)
    vacunas_pago = PagoVacuna.objects.filter(pago=pago)
    desparasitantes_pago = PagoDesparasitante.objects.filter(pago=pago)
    elementos_inventario_pago = PagoElementoInventario.objects.filter(pago=pago)
    tratamientos_pago = PagoTratamiento.objects.filter(pago=pago)
    esteticas_pago = PagoEstetica.objects.filter(pago=pago)
    vacunaciones_pago = PagoVacunacion.objects.filter(pago=pago)
    desparasitaciones_pago = PagoDesparasitacion.objects.filter(pago=pago)
    guarderias_pago = PagoGuarderia.objects.filter(pago=pago)
    otros_pago = PagoOtro.objects.filter(pago=pago)
    vacunaciones_aplicadas = AplicacionVacunacion.objects.all()
    desparasitaciones_aplicadas = AplicacionDesparasitacion.objects.all()
    abonos = AbonoPago.objects.filter(pago=pago)

    hay_veterinaria = Veterinaria.objects.filter(pk='1')

    productosEspera = PuntoEspera.objects.all()

    for productos in productosEspera:
        productos.delete()

    if hay_veterinaria:
        mi_veterinaria = Veterinaria.objects.get(pk='1')

    factura = TotalFacturaPunto.objects.filter(pk=total)
    medios_pago = MedioPago.objects.filter(pago=pago)

    return render(request, 'validar_pago.html', {
        'menu_punto' : True,
        'areas_consulta_pago': areas_consulta_pago,
        'productos_pago': productos_pago,
        'medicamentos_pago': medicamentos_pago,
        'vacunas_pago': vacunas_pago,
        'desparasitantes_pago': desparasitantes_pago,
        'elementos_inventario_pago': elementos_inventario_pago,
        'tratamientos_pago': tratamientos_pago,
        'esteticas_pago': esteticas_pago,
        'vacunaciones_pago': vacunaciones_pago,
        'desparasitaciones_pago': desparasitaciones_pago,
        'guarderias_pago': guarderias_pago,
        'otros_pago': otros_pago,
        'vacunaciones_aplicadas': vacunaciones_aplicadas,
        'desparasitaciones_aplicadas': desparasitaciones_aplicadas,
        'abonos': abonos,
        'mi_veterinaria': mi_veterinaria,
        'factura': factura[0],
        'medios_pago': medios_pago,
        'object': Pago.objects.get(id=pago)
        })

def HistorialPuntoDeVenta(request):
    puntos = Punto.objects.all().order_by('-fecha_inicio')

    puntos_generales = Punto.objects.filter(estado=True)

    puntosAbiertos = []

    for puntos_gen in puntos_generales:
        puntosA = {}
        total_efectivo = puntos_gen.dinero_inicial
        punto_pago_selected = PuntoPago.objects.filter(punto=puntos_gen).exclude(pago__estado_pago='Anulada')
        punto_pago_selected2 = PuntoCompra.objects.filter(punto=puntos_gen).exclude(compra__estado_pago='Anulada')
        punto_abono_compra = PuntoAbonoCompra.objects.filter(punto=puntos_gen).exclude(abono_compra__gasto__estado_pago='Anulada')
        pagos = []
        compras = []
        total_venta = 0
        total_compra = 0
        pago_credito_values = []
        compra_credito_values = []
        for punto_p in punto_pago_selected:
            pago_temp = get_object_or_404(Pago,pk=punto_p.pago_id)
            if pago_temp.forma_pago == 'Debito':
                pago_credito_values.append("-")
                total_venta += pago_temp.total_factura
                pagos.append(pago_temp)

        for punto_co in punto_pago_selected2:
            compra_temp = get_object_or_404(Gasto,pk=punto_co.compra_id)
            if compra_temp.forma_pago == 'Debito':
                compra_credito_values.append("-")
                total_compra += compra_temp.valor_total
                compras.append(compra_temp)

        for abono_compra_punto in punto_abono_compra:
            if abono_compra_punto.valor_abonado == 0:
                compra_credito_values.append(abono_compra_punto.abono_compra.valor_cuota)
                total_compra += abono_compra_punto.abono_compra.valor_cuota
                compras.append(abono_compra_punto.abono_compra.gasto)
            else:
                compra_credito_values.append(abono_compra_punto.valor_abonado)
                total_compra += abono_compra_punto.valor_abonado
                compras.append(abono_compra_punto.abono_compra.gasto)

        motivos_pagos = []
        motivos_gastos = []

        for pago in pagos:

            motivos = []
            if PagoGuarderia.objects.filter(pago__id=pago.id):
                motivos.append("Guardería")

            if PagoProducto.objects.filter(pago__id=pago.id):
                motivos.append("Producto")

            if PagoMedicamento.objects.filter(pago__id=pago.id):
                motivos.append("Medicamento")

            if PagoVacuna.objects.filter(pago__id=pago.id):
                motivos.append("Vacuna")

            if PagoDesparasitante.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitante")

            if PagoTratamiento.objects.filter(pago__id=pago.id):
                motivos.append("Cita médica")

            if PagoEstetica.objects.filter(pago__id=pago.id):
                motivos.append("Estética")

            if PagoVacunacion.objects.filter(pago__id=pago.id):
                motivos.append("Vacunación")

            if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitación")

            if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                motivos.append("Procedimiento")

            if PagoOtro.objects.filter(pago__id=pago.id):
                motivos.append("Otro")

            motivos_pagos.append(motivos)

        for gasto in compras:

            motivos = []
            if GastoProducto.objects.filter(gasto__id=gasto.id):
                motivos.append("Producto")

            if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                motivos.append("Medicamento")

            if GastoVacuna.objects.filter(gasto__id=gasto.id):
                motivos.append("Vacuna")

            if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                motivos.append("Desparasitante")

            if GastoServicio.objects.filter(gasto__id=gasto.id):
                motivos.append("Servicio")

            if GastoOtro.objects.filter(gasto__id=gasto.id):
                motivos.append("Otro")

            motivos_gastos.append(motivos)


        total_ventas_efectivo = 0
        total_tarjeta_debito = 0
        total_consignacion = 0
        total_tarjeta_credito = 0
        total_otros = 0


        for pago in pagos:
            metodos = MedioPago.objects.filter(pago__id=pago.id)
            for metodo in metodos:
                if pago.forma_pago == 'Debito' and metodo.medio_pago == 'Efectivo':
                    total_ventas_efectivo += metodo.valor_pagado
                    total_efectivo += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Tarjeta debito':
                    total_tarjeta_debito += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Consignacion':
                    total_consignacion += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Tarjeta credito':
                    total_tarjeta_credito += metodo.valor_pagado
                elif pago.forma_pago == 'Debito' and metodo.medio_pago == 'Otro':
                    total_otros += metodo.valor_pagado

        total_compras_efectivo = 0
        total_compras_tarjeta_debito = 0
        total_compras_consignacion = 0
        total_compras_tarjeta_credito = 0
        total_compras_otros = 0

        for compra in compras:
            if compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Efectivo':
                total_compras_efectivo += compra.valor_total
                total_efectivo -= compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta debito':
                total_compras_tarjeta_debito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Consignacion':
                total_compras_consignacion += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta credito':
                total_compras_tarjeta_credito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Otro':
                total_compras_otros += compra.valor_total

        for abono in PuntoAbonoCompra.objects.filter(punto=puntos_gen).exclude(gasto__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_compras_efectivo += abono.valor_abonado
                total_efectivo -= abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_compras_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_compras_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_compras_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_compras_otros += abono.valor_abonado

        puntosA['fecha_inicio'] = puntos_gen.fecha_inicio
        puntosA['responsable'] = puntos_gen.responsable
        puntosA['dinero_inicial'] = puntos_gen.dinero_inicial
        puntosA['total_venta'] = total_venta
        puntosA['total_compra'] = total_compra
        puntosA['total'] = puntos_gen.dinero_inicial + total_venta - total_compra

        puntosA['total_efectivo'] = total_efectivo
        puntosA['total_ventas_efectivo'] = total_ventas_efectivo
        puntosA['total_tarjeta_debito'] = total_tarjeta_debito
        puntosA['total_consignacion'] = total_consignacion
        puntosA['total_tarjeta_credito'] = total_tarjeta_credito
        puntosA['total_otros'] = total_otros

        puntosA['total_compras_efectivo'] = total_compras_efectivo
        puntosA['total_compras_tarjeta_debito'] = total_compras_tarjeta_debito
        puntosA['total_compras_consignacion'] = total_compras_consignacion
        puntosA['total_compras_tarjeta_credito'] = total_compras_tarjeta_credito
        puntosA['total_compras_otros'] = total_compras_otros

        puntosAbiertos.append(puntosA)


    return render(request, 'historial_punto_de_venta.html', {
            'menu_punto': True,
            'puntos': puntos,
            'puntos_generales': puntosAbiertos,
             })


def registrar_compra_punto(request):
    responsable = get_object_or_404(Usuario, id=request.user.id)
    if request.method == 'POST':
        form_registrar_compra = Formulario_registrar_compra()
        req = json.loads( request.body.decode('utf-8') )
        date_now = datetime.now()
        x = datetime(date_now.year, date_now.month, date_now.day)
        fecha = x.strftime("%Y-%m-%d")
        comentarios = str(req['comentarios'])
        subtotal = str(req['subtotal'])
        descuento = str(req['descuento'])
        total_impuesto = str(req['total_impuesto'])
        total = str(req['total'])

        responsable = get_object_or_404(Usuario, id=request.user.id)

        if req['proveedor']:
            id_proveedor = str(req['proveedor'])
            proveedor = get_object_or_404(Proveedor, pk=id_proveedor)
            nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                sub_total=subtotal, descuento=descuento, proveedor=proveedor, responsable=responsable, tipo_comprobante='Punto de Venta')
            nuevo_gasto.save()
        else:
            nuevo_gasto = Gasto(valor_total=total, fecha=fecha, comentarios=comentarios, total_impuesto=total_impuesto,
                                sub_total=subtotal, descuento=descuento, responsable=responsable, tipo_comprobante='Punto de Venta')
            nuevo_gasto.save()

        nuevo_gasto.forma_pago = req['forma_pago']

        if req['forma_pago'] == 'Debito':
            nuevo_gasto.tipo_pago_debito = req['medio_pago_debito']
            punto_on = Punto.objects.filter(estado=True, responsable=responsable)
            if punto_on[0]:
                now = datetime.now()
                x = datetime(now.year, now.month, now.day)
                z = datetime(x.year,x.month,x.day)
                y = x + timedelta(days=1)
                if z >= x and z < y:
                    punto_compra = PuntoCompra(punto = punto_on[0], compra=nuevo_gasto)
                    punto_compra.save()

        if req['forma_pago'] == 'Credito':
            nuevo_gasto.numero_cuotas_credito = req['numero_cuotas']
            nuevo_gasto.saldo = int(req['total'])
            nuevo_gasto.estado_pago = 'Pendiente'

            for cuota in req['cuotas']:
                mi_cuota = CuotaGasto(gasto=nuevo_gasto, fecha_limite=cuota['fecha'], valor_cuota=cuota['abono'],
                                     saldo_cuota=cuota['abono'])
                fecha_comparar_init = datetime.strptime(mi_cuota.fecha_limite, "%Y-%m-%d")
                fecha_comparar = datetime(x.year,x.month,x.day)
                fecha_limite_comparar = datetime(fecha_comparar_init.year,fecha_comparar_init.month,fecha_comparar_init.day)

                if fecha_limite_comparar == fecha_comparar:
                    abono_gasto = AbonoGasto(gasto=nuevo_gasto, valor_abonado=mi_cuota.saldo_cuota,
                                           medio_pago=req['medio_pago_credito'])
                    abono_gasto.save()
                    nuevo_gasto.saldo = nuevo_gasto.saldo - int(mi_cuota.saldo_cuota)
                    nuevo_gasto.save()
                    mi_cuota.valor_pagado = int(mi_cuota.saldo_cuota)
                    mi_cuota.saldo_cuota = 0
                    mi_cuota.estado_pago = 'Pagada'
                    mi_cuota.fecha_pago = mi_cuota.fecha_limite
                    mi_cuota.save()
                    punto_on = Punto.objects.filter(estado= True, responsable=responsable)
                    if punto_on:
                        punto_compra = PuntoAbonoCompra(punto = punto_on[0], abono_compra=mi_cuota, medio_pago=req['medio_pago_credito'],gasto=nuevo_gasto,valor_abonado=mi_cuota.valor_pagado)
                        punto_compra.save()

                else:
                    mi_cuota.save()

        messages.success(request, 'Compra registrada con exito!')

        nuevo_gasto.save()


        for elemento in req['elementos_agregados']:

            if(elemento['tipo'] == 'Servicio'):
                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_servicio = GastoServicio(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                                   precio_compra_unidad=elemento['costo_unitario'],
                                                   unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                                   impuesto=mi_impuesto, valor_total_impuesto=valor_total_impuesto)
                    gasto_servicio.save()
                else:
                    gasto_servicio = GastoServicio(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                                   precio_compra_unidad=elemento['costo_unitario'],
                                                   unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_servicio.save()

            elif (elemento['tipo'] == 'Otro'):
                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_otro = GastoOtro(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                                   precio_compra_unidad=elemento['costo_unitario'],
                                                   unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                                   impuesto=mi_impuesto,
                                                   valor_total_impuesto=valor_total_impuesto)
                    gasto_otro.save()
                else:
                    gasto_otro = GastoOtro(gasto=nuevo_gasto, nombre=elemento['nombre'],
                                                   precio_compra_unidad=elemento['costo_unitario'],
                                                   unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_otro.save()

            elif (elemento['tipo'] == 'Producto'):
                mi_elemento = get_object_or_404(Producto, pk=elemento['id_elemento'])
                mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                mi_elemento.save()

                descripcion = "Compra de " + elemento['unidades'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Compras', id_transaccion=nuevo_gasto.id)
                historia_transaccion.save()

                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_producto = GastoProducto(gasto=nuevo_gasto, producto=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                               impuesto=mi_impuesto,
                                               valor_total_impuesto=valor_total_impuesto)
                    gasto_producto.save()
                else:
                    gasto_producto = GastoProducto(gasto=nuevo_gasto, producto=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_producto.save()

            elif (elemento['tipo'] == 'Medicamento'):
                mi_elemento = get_object_or_404(Medicamento, pk=elemento['id_elemento'])
                mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                mi_elemento.save()

                descripcion = "Compra de " + elemento['unidades'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                           id_transaccion=nuevo_gasto.id)
                historia_transaccion.save()

                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_medicamento = GastoMedicamento(gasto=nuevo_gasto, medicamento=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                               impuesto=mi_impuesto,
                                               valor_total_impuesto=valor_total_impuesto)
                    gasto_medicamento.save()
                else:
                    gasto_medicamento = GastoMedicamento(gasto=nuevo_gasto, medicamento=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_medicamento.save()

            elif (elemento['tipo'] == 'Vacuna'):
                mi_elemento = get_object_or_404(Vacuna, pk=elemento['id_elemento'])
                mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                mi_elemento.save()

                descripcion = "Compra de " + elemento['unidades'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Vacuna',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                           id_transaccion=nuevo_gasto.id)
                historia_transaccion.save()

                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_vacuna = GastoVacuna(gasto=nuevo_gasto, vacuna=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                               impuesto=mi_impuesto,
                                               valor_total_impuesto=valor_total_impuesto)
                    gasto_vacuna.save()
                else:
                    gasto_vacuna = GastoVacuna(gasto=nuevo_gasto, vacuna=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_vacuna.save()

            elif (elemento['tipo'] == 'Desparasitante'):
                mi_elemento = get_object_or_404(Desparasitante, pk=elemento['id_elemento'])
                mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                mi_elemento.save()

                descripcion = "Compra de " + elemento['unidades'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Desparasitante',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                           id_transaccion=nuevo_gasto.id)
                historia_transaccion.save()

                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_desparasitante = GastoDesparasitante(gasto=nuevo_gasto, desparasitante=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                               impuesto=mi_impuesto,
                                               valor_total_impuesto=valor_total_impuesto)
                    gasto_desparasitante.save()
                else:
                    gasto_desparasitante = GastoDesparasitante(gasto=nuevo_gasto, desparasitante=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_desparasitante.save()

            elif (elemento['tipo'] == 'Insumo'):
                mi_elemento = get_object_or_404(Insumo, pk=elemento['id_elemento'])
                mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                mi_elemento.save()

                descripcion = "Compra de " + elemento['unidades'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Insumo',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                           id_transaccion=nuevo_gasto.id)
                historia_transaccion.save()

                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_insumo = GastoInsumo(gasto=nuevo_gasto, insumo=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                               impuesto=mi_impuesto,
                                               valor_total_impuesto=valor_total_impuesto)
                    gasto_insumo.save()
                else:
                    gasto_insumo = GastoInsumo(gasto=nuevo_gasto, insumo=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_insumo.save()

            else:
                mi_elemento = get_object_or_404(ElementoCategoriaInventario, pk=elemento['id_elemento'])
                mi_elemento.cantidad = mi_elemento.cantidad + int(elemento['unidades'])
                mi_elemento.cantidad_total_unidades = mi_elemento.cantidad_total_unidades + (int(elemento['unidades']) * mi_elemento.cantidad_unidad)
                mi_elemento.save()

                descripcion = "Compra de " + elemento['unidades'] + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Compras',
                                                           id_transaccion=nuevo_gasto.id)
                historia_transaccion.save()

                if elemento['id_impuesto'] != '0':
                    mi_impuesto = get_object_or_404(Impuesto, pk=elemento['id_impuesto'])
                    valor_total_impuesto = (float(mi_impuesto.porcentaje) * float(elemento['costo_total'])) / 100
                    gasto_elemento_categoria = GastoElementoInventario(gasto=nuevo_gasto, elemento=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'],
                                               impuesto=mi_impuesto,
                                               valor_total_impuesto=valor_total_impuesto)
                    gasto_elemento_categoria.save()
                else:
                    gasto_elemento_categoria = GastoElementoInventario(gasto=nuevo_gasto, elemento=mi_elemento,
                                               precio_compra_unidad=elemento['costo_unitario'],
                                               unidades=elemento['unidades'], precio_total=elemento['costo_total'])
                    gasto_elemento_categoria.save()

        data = {
            'registrado_exitoso': True,
            'id_compra': nuevo_gasto.id
        }
        return JsonResponse(data)

    else:
        form_registrar_compra = Formulario_registrar_compra()
        lista_categorias = json.dumps(list(CategoriaInventario.objects.filter(estado=True).values_list('id', 'nombre')))

        lista_bodegas = json.dumps(list(Bodega.objects.filter(estado=True).values_list('id', 'nombre')))

        return render(request, 'registrar_gasto_punto.html',{
            'form_registrar_compra': form_registrar_compra,
            'menu_punto': True,
            'lista_categorias': lista_categorias,
            'lista_bodegas': lista_bodegas
        })


def GuardarMetodo(request):
    metodo = request.GET.getlist('metodo[]')
    total_pagar = request.GET.get('precio_total', None)
    cliente = request.GET.get('cliente', None)
    responsable = get_object_or_404(Usuario, id=request.user.id)
    valor = request.method

    punto_abierto = Punto.objects.filter(estado=True, responsable=responsable)
    productosEspera = PuntoEspera.objects.filter(punto_abierto=punto_abierto[0].id)

    metodos = json.loads(metodo[0])

    total = TotalFacturaPunto(total_factura=total_pagar, punto_abierto=punto_abierto[0].id)
    total.save()

    date_now = datetime.now()
    x = datetime(date_now.year, date_now.month, date_now.day)
    fecha = x.strftime("%Y-%m-%d")

    responsable = get_object_or_404(Usuario, id=request.user.id)
    factura = TotalFacturaPunto.objects.all().last()

    if cliente != 0:
        cliente_nombres = Cliente.objects.filter(pk=cliente)

    nuevo_pago = Pago(fecha=fecha, total_factura=total_pagar, total_impuesto=0, sub_total=total_pagar, descuento=0,
                        forma_pago='Debito', saldo=0, estado_pago='Pagado',responsable=responsable, total_factura_punto=factura,tipo_comprobante='Punto de Venta')

    if cliente_nombres:
        nuevo_pago.cliente = cliente_nombres[0]

    nuevo_pago.save()

    punto_abierto = Punto.objects.filter(estado=True, responsable=responsable)
    if punto_abierto:
        now = datetime.now()
        x = datetime(now.year, now.month, now.day)
        z = datetime.strptime(nuevo_pago.fecha, "%Y-%m-%d")
        y = x + timedelta(days=1)
        if z >= x and z < y:
            punto_pago = PuntoPago(punto = punto_abierto[0], pago=nuevo_pago)
            punto_pago.save()

    cambio_total = 0

    for metodo in metodos:
        cambio_total += float(metodo['cambio'])
        valor_pagado = float(metodo['pago']) - float(metodo['cambio'])
        metodo_pago = MedioPago(pago=nuevo_pago,valor_pagado=valor_pagado,dinero_recibido=metodo['pago'],cambio=metodo['cambio'],medio_pago=metodo['tipo'])
        metodo_pago.save()

    nuevo_pago.cambio = cambio_total
    nuevo_pago.save()

    for productos in productosEspera:
        if productos.categoria == 'Existente':
            if productos.tipo == 'Producto':
                mi_producto = get_object_or_404(Producto, pk=productos.id_producto)
                cantidad = float(productos.unidad)
                pago_producto = PagoProducto(producto=mi_producto,pago=nuevo_pago,cantidad=cantidad, total=int(productos.precio))

                costo_producto = 0

                if productos.granel == False:
                    pago_producto.tipo_de_venta = 'unidad'
                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_producto.cantidad = float(mi_producto.cantidad) - cantidad

                    mi_producto.cantidad_total_unidades = float(mi_producto.cantidad_total_unidades) - ( cantidad * float(mi_producto.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad) + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                        id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()
                else:
                    pago_producto.tipo_de_venta = 'a granel'

                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_producto.cantidad_total_unidades = float(mi_producto.cantidad_total_unidades) - cantidad

                    mi_producto.cantidad = math.floor( float(mi_producto.cantidad_total_unidades) / float(mi_producto.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad)+" "+mi_producto.unidad_medida +"."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Producto',
                                                        id_elemento=mi_producto.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()

                pago_producto.save()
                mi_producto.save()

            elif productos.tipo == 'Medicamentos':
                mi_medicamento = get_object_or_404(Medicamento, pk=productos.id_producto)
                cantidad = float(productos.unidad)
                pago_medicamento = PagoMedicamento(medicamento=mi_medicamento, pago=nuevo_pago, cantidad=cantidad,
                                            total=int(productos.precio))
                costo_producto = 0

                if productos.granel == False:
                    pago_medicamento.tipo_de_venta = 'unidad'
                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_medicamento.cantidad = float(mi_medicamento.cantidad) - cantidad

                    mi_medicamento.cantidad_total_unidades = float(mi_medicamento.cantidad_total_unidades) - ( cantidad * float(mi_medicamento.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad) + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                        id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()
                else:
                    pago_medicamento.tipo_de_venta = 'a granel'

                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_medicamento.cantidad_total_unidades = float(mi_medicamento.cantidad_total_unidades) - cantidad

                    mi_medicamento.cantidad = math.floor( float(mi_medicamento.cantidad_total_unidades) / float(mi_medicamento.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad)+" "+mi_medicamento.unidad_medida +"."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                        id_elemento=mi_medicamento.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()


                pago_medicamento.save()
                mi_medicamento.save()

            elif productos.tipo == 'Vacunas':
                mi_vacuna = get_object_or_404(Vacuna, pk=productos.id_producto)
                cantidad = float(productos.unidad)
                pago_vacuna = PagoVacuna(vacuna=mi_vacuna, pago=nuevo_pago, cantidad=cantidad,
                                            total=int(productos.precio))

                costo_producto = 0

                if productos.granel == False:
                    pago_vacuna.tipo_de_venta = 'unidad'
                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_vacuna.cantidad = float(mi_vacuna.cantidad) - cantidad

                    mi_vacuna.cantidad_total_unidades = float(mi_vacuna.cantidad_total_unidades) - ( cantidad * float(mi_vacuna.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad) + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                        id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()
                else:
                    pago_vacuna.tipo_de_venta = 'a granel'

                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_vacuna.cantidad_total_unidades = float(mi_vacuna.cantidad_total_unidades) - cantidad

                    mi_vacuna.cantidad = math.floor( float(mi_vacuna.cantidad_total_unidades) / float(mi_vacuna.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad)+" "+mi_vacuna.unidad_medida +"."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                        id_elemento=mi_vacuna.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()

                pago_vacuna.save()
                mi_vacuna.save()

            elif productos.tipo == 'Desparasitantes':
                mi_desparasitante = get_object_or_404(Desparasitante, pk=productos.id_producto)
                cantidad = float(productos.unidad)
                pago_desparasitante = PagoDesparasitante(desparasitante=mi_desparasitante, pago=nuevo_pago, cantidad=cantidad,
                                            total=int(productos.precio))

                costo_producto = 0

                if productos.granel == False:
                    pago_desparasitante.tipo_de_venta = 'unidad'
                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_desparasitante.cantidad = float(mi_desparasitante.cantidad) - cantidad

                    mi_desparasitante.cantidad_total_unidades = float(mi_desparasitante.cantidad_total_unidades) - ( cantidad * float(mi_desparasitante.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad) + " unidades."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                        id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()
                else:
                    pago_desparasitante.tipo_de_venta = 'a granel'

                    costo_producto = costo_producto + (cantidad) * float(productos.precio)

                    mi_desparasitante.cantidad_total_unidades = float(mi_desparasitante.cantidad_total_unidades) - cantidad

                    mi_desparasitante.cantidad = math.floor( float(mi_desparasitante.cantidad_total_unidades) / float(mi_desparasitante.cantidad_unidad) )

                    descripcion = "Venta de " + str(cantidad)+" "+mi_desparasitante.unidad_medida +"."

                    historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Medicamento',
                                                        id_elemento=mi_desparasitante.id, tipo_transaccion='Ventas',
                                                        id_transaccion=nuevo_pago.id)
                    historia_transaccion.save()


                mi_desparasitante.save()
                pago_desparasitante.save()

            else:
                pago_otro = PagoOtro(descripcion=productos.tipo, pago=nuevo_pago, total=int(productos.precio))
                pago_otro.save()

        elif productos.categoria == 'Servicios':
            if productos.tipo == 'vacunacion':
                mi_vacunacion = get_object_or_404(Vacunacion, pk=productos.id_producto)
                pago_vacunacion = PagoVacunacion(vacunacion=mi_vacunacion, pago=nuevo_pago, total=int(productos.precio))
                pago_vacunacion.save()
                mi_vacunacion.costo = productos.precio
                mi_vacunacion.estado = 'Pagada'
                mi_vacunacion.save()

            if productos.tipo == 'desparasitacion':
                mi_desparasitacion = get_object_or_404(Desparasitacion, pk=productos.id_producto)
                pago_desparasitacion = PagoDesparasitacion(desparasitacion=mi_desparasitacion, pago=nuevo_pago, total=int(productos.precio))
                pago_desparasitacion.save()
                mi_desparasitacion.costo = productos.precio
                mi_desparasitacion.estado = 'Pagada'
                mi_desparasitacion.save()

            if productos.tipo == 'estetica':
                mi_estetica = get_object_or_404(FacturaEstetica, pk=productos.id_producto)
                pago_estetica = PagoEstetica(estetica=mi_estetica, pago=nuevo_pago, total=int(productos.precio))
                pago_estetica.save()
                mi_estetica.saldo = 0
                mi_estetica.estado = 'Pagada'
                mi_estetica.save()

            if productos.tipo == 'guarderia':
                mi_guarderia = get_object_or_404(Guarderia, pk=productos.id_producto)
                pago_guarderia = PagoGuarderia(guarderia=mi_guarderia, pago=nuevo_pago, total=int(productos.precio))
                pago_guarderia.save()
                mi_guarderia.estado = 'Pagada'
                mi_guarderia.save()

            if productos.tipo == "Motivo":
                mi_tratamiento = get_object_or_404(HistoriaClinica, pk=productos.id_producto)
                pago_tratamiento = PagoTratamiento(tratamiento=mi_tratamiento, pago=nuevo_pago,
                                                total=int(productos.precio))
                pago_tratamiento.save()
                mi_tratamiento.costo = int(productos.precio)
                mi_tratamiento.estado = 'Pagada'
                mi_tratamiento.save()

            if productos.tipo == "Area":
                mi_tratamiento = get_object_or_404(HistoriaClinica, pk=productos.id_producto)
                tratamiento_nombre = get_object_or_404(Tratamiento, pk=productos.id_motivo_area)
                mi_area = get_object_or_404(Tratamiento, pk=tratamiento_nombre.id)
                motivo_cita = get_object_or_404(MotivoArea, historia_clinica=mi_tratamiento, area_consulta=mi_area)
                pago_area = PagoAreaConsulta(area_consulta=motivo_cita, pago=nuevo_pago, total=int(productos.precio))
                pago_area.save()
                motivo_cita.estado = 'Pagada'
                motivo_cita.save()

        else:
            mi_elemento = get_object_or_404(ElementoCategoriaInventario, pk=productos.id_producto)
            cantidad = float(productos.unidad)
            pago_elemento = PagoElementoInventario(elemento=mi_elemento, pago=nuevo_pago,
                                                     cantidad=cantidad,
                                                     total=float(productos.precio))

            costo_producto = 0

            if productos.granel == False:
                pago_elemento.tipo_de_venta = 'unidad'
                costo_producto = costo_producto + (cantidad) * float(productos.precio)

                mi_elemento.cantidad = float(mi_elemento.cantidad) - cantidad

                mi_elemento.cantidad_total_unidades = float(mi_elemento.cantidad_total_unidades) - (
                            cantidad * float(mi_elemento.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " unidades."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id)
                historia_transaccion.save()
            else:
                pago_elemento.tipo_de_venta = 'a granel'

                costo_producto = costo_producto + (cantidad) * float(productos.precio)

                mi_elemento.cantidad_total_unidades = float(mi_elemento.cantidad_total_unidades) - cantidad

                mi_elemento.cantidad = math.floor(
                    int(mi_elemento.cantidad_total_unidades) / float(mi_elemento.cantidad_unidad))

                descripcion = "Venta de " + str(cantidad) + " " + mi_elemento.unidad_medida + "."

                historia_transaccion = HistorialInventario(nombre_transaccion=descripcion, tipo_elemento='Categoria',
                                                           id_elemento=mi_elemento.id, tipo_transaccion='Ventas',
                                                           id_transaccion=nuevo_pago.id)
                historia_transaccion.save()

            mi_elemento.save()
            pago_elemento.save()

    data = {
        'prueba': True,
        'pago': nuevo_pago.id,
        'factura': total.id
    }

    return JsonResponse(data)

class ListarPagosSelected(ListView):
    model = Pago
    template_name = "detalle_punto.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarPagosSelected, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarPagosSelected, self).get_context_data(**kwargs)
        context['menu_punto'] = True
        punto_selected = get_object_or_404(Punto, pk=self.kwargs['id_punto'])
        punto_pago_selected = PuntoPago.objects.filter(punto = punto_selected).exclude(pago__estado_pago='Anulada')
        punto_pago_selected2 = PuntoCompra.objects.filter(punto = punto_selected).exclude(compra__estado_pago='Anulada')
        punto_abono_compra = PuntoAbonoCompra.objects.filter(punto = punto_selected).exclude(abono_compra__gasto__estado_pago='Anulada')
        pagos = []
        compras = []
        inicio = datetime(punto_selected.fecha_inicio.year, punto_selected.fecha_inicio.month, punto_selected.fecha_inicio.day,punto_selected.fecha_inicio.hour,punto_selected.fecha_inicio.minute)
        inicio_modified = inicio - timedelta(hours= 5)
        final = datetime(punto_selected.fecha_final.year, punto_selected.fecha_final.month, punto_selected.fecha_final.day,punto_selected.fecha_final.hour,punto_selected.fecha_final.minute)
        final_modified = final - timedelta(hours= 5)
        total_venta = 0
        total_compra = 0
        pago_credito_values = []
        compra_credito_values = []
        for punto_p in punto_pago_selected:
            pago_temp = get_object_or_404(Pago,pk=punto_p.pago_id)
            if pago_temp.forma_pago == 'Debito':
                pago_credito_values.append("-")
                total_venta += pago_temp.total_factura
                pagos.append(pago_temp)

        for punto_co in punto_pago_selected2:
            compra_temp = get_object_or_404(Gasto,pk=punto_co.compra_id)
            if compra_temp.forma_pago == 'Debito':
                compra_credito_values.append("-")
                total_compra += compra_temp.valor_total
                compras.append(compra_temp)

        for abono_compra_punto in punto_abono_compra:
            if abono_compra_punto.valor_abonado == 0:
                compra_credito_values.append(abono_compra_punto.abono_compra.valor_cuota)
                total_compra += abono_compra_punto.abono_compra.valor_cuota
                compras.append(abono_compra_punto.abono_compra.gasto)
            else:
                compra_credito_values.append(abono_compra_punto.valor_abonado)
                total_compra += abono_compra_punto.valor_abonado
                compras.append(abono_compra_punto.abono_compra.gasto)

        motivos_pagos = []
        motivos_gastos = []



        for pago in pagos:

            motivos = []
            if PagoGuarderia.objects.filter(pago__id=pago.id):
                motivos.append("Guardería")

            if PagoProducto.objects.filter(pago__id=pago.id):
                motivos.append("Producto")

            if PagoMedicamento.objects.filter(pago__id=pago.id):
                motivos.append("Medicamento")

            if PagoVacuna.objects.filter(pago__id=pago.id):
                motivos.append("Vacuna")

            if PagoDesparasitante.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitante")

            if PagoTratamiento.objects.filter(pago__id=pago.id):
                motivos.append("Cita médica")

            if PagoEstetica.objects.filter(pago__id=pago.id):
                motivos.append("Estética")

            if PagoVacunacion.objects.filter(pago__id=pago.id):
                motivos.append("Vacunación")

            if PagoDesparasitacion.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitación")

            if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                motivos.append("Procedimiento")

            if PagoOtro.objects.filter(pago__id=pago.id):
                motivos.append("Otro")

            if PagoElementoInventario.objects.filter(pago__id=pago.id):
                for pago in PagoElementoInventario.objects.filter(pago__id=pago.id):
                    if not pago.elemento.categoria.nombre in motivos:
                        motivos.append(pago.elemento.categoria.nombre)

            motivos_pagos.append(motivos)

        for gasto in compras:

            motivos = []
            if GastoProducto.objects.filter(gasto__id=gasto.id):
                motivos.append("Producto")

            if GastoMedicamento.objects.filter(gasto__id=gasto.id):
                motivos.append("Medicamento")

            if GastoVacuna.objects.filter(gasto__id=gasto.id):
                motivos.append("Vacuna")

            if GastoDesparasitante.objects.filter(gasto__id=gasto.id):
                motivos.append("Desparasitante")

            if GastoServicio.objects.filter(gasto__id=gasto.id):
                motivos.append("Servicio")

            if GastoOtro.objects.filter(gasto__id=gasto.id):
                motivos.append("Otro")

            if GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                for gasto in GastoElementoInventario.objects.filter(gasto__id=gasto.id):
                    if not gasto.elemento.categoria.nombre in motivos:
                        motivos.append(gasto.elemento.categoria.nombre)

            motivos_gastos.append(motivos)

        context['motivos_gastos'] = motivos_gastos
        context['motivos_pagos'] = motivos_pagos
        context['pagos_hoy'] = pagos
        context['compras_hoy'] = compras
        context['total_venta'] = total_venta
        context['total_compra'] = total_compra
        context['dinero_inicial'] = punto_selected.dinero_inicial
        context['total'] = punto_selected.dinero_inicial + total_venta - total_compra
        context['punto'] = punto_selected
        month = ['nothing','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
        context['inicio_d'] = inicio_modified
        context['month'] = month[inicio_modified.month]
        context['final'] = final_modified
        context['abonos_pago'] = pago_credito_values
        context['abonos_compra'] = compra_credito_values
        context['abonos_compras'] = PuntoAbonoCompra.objects.filter(punto=punto_selected).exclude(gasto__estado_pago='Anulada')

        total_efectivo = 0
        total_ventas_efectivo = 0
        total_tarjeta_debito = 0
        total_consignacion = 0
        total_tarjeta_credito = 0
        total_otros = 0

        for pago in pagos:
            metodos = MedioPago.objects.filter(pago__id=pago.id)
            if pago.forma_pago == 'Debito' and metodos[0].medio_pago == 'Efectivo':
                total_ventas_efectivo += pago.total_factura
                total_efectivo += pago.total_factura
            elif pago.forma_pago == 'Debito' and metodos[0].medio_pago == 'Tarjeta debito':
                total_tarjeta_debito += pago.total_factura
            elif pago.forma_pago == 'Debito' and metodos[0].medio_pago == 'Consignacion':
                total_consignacion += pago.total_factura
            elif pago.forma_pago == 'Debito' and metodos[0].medio_pago == 'Tarjeta credito':
                total_tarjeta_credito += pago.total_factura
            elif pago.forma_pago == 'Debito' and metodos[0].medio_pago == 'Otro':
                total_otros += pago.total_factura

        total_compras_efectivo = 0
        total_compras_tarjeta_debito = 0
        total_compras_consignacion = 0
        total_compras_tarjeta_credito = 0
        total_compras_otros = 0

        for compra in compras:
            if compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Efectivo':
                total_compras_efectivo += compra.valor_total
                total_efectivo -= compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta debito':
                total_compras_tarjeta_debito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Consignacion':
                total_compras_consignacion += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Tarjeta credito':
                total_compras_tarjeta_credito += compra.valor_total
            elif compra.forma_pago == 'Debito' and compra.tipo_pago_debito == 'Otro':
                total_compras_otros += compra.valor_total

        for abono in PuntoAbonoCompra.objects.filter(punto=punto_selected).exclude(gasto__estado_pago='Anulada'):
            if abono.medio_pago == 'Efectivo':
                total_compras_efectivo += abono.valor_abonado
                total_efectivo -= abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta debito':
                total_compras_tarjeta_debito += abono.valor_abonado
            elif abono.medio_pago == 'Consignacion':
                total_compras_consignacion += abono.valor_abonado
            elif abono.medio_pago == 'Tarjeta credito':
                total_compras_tarjeta_credito += abono.valor_abonado
            elif abono.medio_pago == 'Otro':
                total_compras_otros += abono.valor_abonado

        context['total_efectivo'] = total_efectivo
        context['total_ventas_efectivo'] = total_ventas_efectivo
        context['total_tarjeta_debito'] = total_tarjeta_debito
        context['total_consignacion'] = total_consignacion
        context['total_tarjeta_credito'] = total_tarjeta_credito
        context['total_otros'] = total_otros

        context['total_compras_efectivo'] = total_compras_efectivo
        context['total_compras_tarjeta_debito'] = total_compras_tarjeta_debito
        context['total_compras_consignacion'] = total_compras_consignacion
        context['total_compras_tarjeta_credito'] = total_compras_tarjeta_credito
        context['total_compras_otros'] = total_compras_otros
        context['metodos'] = MedioPago.objects.all()

        return context

def buscar_producto(request):
    codigo_barras = request.GET.get('codigo_barras', None)
    productos=Producto.objects.filter(estado=True,codigo=codigo_barras)
    medicamento=Medicamento.objects.filter(estado='Activo',codigo_inventario=codigo_barras)
    desparasitantes = Desparasitante.objects.filter(estado='Activa',codigo_inventario=codigo_barras)
    vacunas=Vacuna.objects.filter(estado='Activo',codigo_inventario=codigo_barras).exclude(tipo_uso='Comercial')
    nuevas_categorias = ElementoCategoriaInventario.objects.filter(estado='Activo',codigo_inventario=codigo_barras).exclude(tipo_uso='Comercial')

    data = {
        'productos': serializers.serialize('json', productos),
        'medicamentos': serializers.serialize('json', medicamento),
        'desparasitantes': serializers.serialize('json', desparasitantes),
        'vacunas': serializers.serialize('json', vacunas),
        'nuevas_categorias': serializers.serialize('json', nuevas_categorias)
    }

    return JsonResponse(data)
