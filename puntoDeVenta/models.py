from django.db import models
from django.db import connection
from productos.models  import *
from gastos.models import *
from usuarios.models import Usuario

class Punto(models.Model):
    fecha_inicio = models.DateTimeField(auto_now_add=True)
    fecha_final = models.DateTimeField(null = True)
    estado = models.BooleanField(default = False)
    dinero_inicial = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    dinero_final = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    nombre_punto = models.CharField(max_length=100, verbose_name="Nombre del Punto")
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)

class PuntoPago(models.Model):
    punto = models.ForeignKey(Punto, on_delete=models.CASCADE)
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)

class PuntoCompra(models.Model):
    punto = models.ForeignKey(Punto, on_delete=models.CASCADE)
    compra = models.ForeignKey(Gasto, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)

class PuntoAbonoPago(models.Model):
    punto = models.ForeignKey(Punto, on_delete=models.CASCADE)
    abono_pago = models.ForeignKey(CuotaPago, on_delete=models.CASCADE, blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    valor_abonado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE, blank=True, null=True)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')

class PuntoAbonoCompra(models.Model):
    punto = models.ForeignKey(Punto, on_delete=models.CASCADE)
    abono_compra = models.ForeignKey(CuotaGasto, on_delete=models.CASCADE, blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    valor_abonado = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    gasto = models.ForeignKey(Gasto, on_delete=models.CASCADE, blank=True, null=True)
    medio_pago = models.CharField(max_length=200, verbose_name='Medio de pago')

class PuntoEsperaCliente(models.Model):
    id_cliente = models.IntegerField(default=0)
    punto_abierto = models.IntegerField(default=0)

class PuntoEspera(models.Model):
    id_producto = models.IntegerField(default=0)
    tipo = models.CharField(max_length=100, verbose_name="Tipo de Producto")
    precio = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    unidad = models.DecimalField(max_digits=9, decimal_places=1)
    granel =  models.BooleanField(default=False)
    precio_por_unidad = models.DecimalField(max_digits=50, decimal_places=0, default=0)
    punto_abierto = models.IntegerField(default=0)
    prox_pago_punto = models.IntegerField(default=0)
    categoria = models.CharField(max_length=50, verbose_name='Tipo de Categoría')
    cliente = models.ForeignKey(PuntoEsperaCliente, on_delete=models.CASCADE, blank=True, null=True)
    nombre = models.CharField(max_length=100, verbose_name="Nombre de Motivo", blank=True, null=True)
    id_motivo_area = models.IntegerField(default=0, blank=True, null=True)
