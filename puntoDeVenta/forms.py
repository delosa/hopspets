from .models import *
from django import forms


class AbrirPuntoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AbrirPuntoForm, self).__init__(*args, **kwargs)
        self.fields['nombre_punto'].required = True
        self.fields['nombre_punto'].label = 'Nombre *'
        self.fields['dinero_inicial'].required = False
        self.fields['dinero_inicial'].label = 'Especificar cantidad de dinero inicial'

    class Meta:
        model = Punto
        fields = (
            'nombre_punto',
            'dinero_inicial',
        )

        widgets = {
            'dinero_inicial': forms.NumberInput(attrs={'required': 'true', 'min': '0','id':'dinero_inicial_id'})
        }
