from django.urls import path
from .views import *

urlpatterns = [
    path('abrir_punto_de_venta/', abrir_punto_de_venta, name="abrir_punto_de_venta"),
    path('especificacion_punto/', especificacion_punto, name="especificacion_punto"),
    path('listar_puntos_hoy/', ListarPuntosHoy.as_view(), name="listar_puntos_hoy"),
    path('registrar_pago_pdv/', crearPago_punto, name="registrar_pago_pdv"),
    path('agregar_punto/', agregar_punto, name="agregar_punto"),
    path('cerrar_punto/<int:id_punto>', cerrar_punto, name="cerrar_punto"),
    path('cliente_punto/', DetalleClientePunto, name="cliente_punto"),
    path('guardar_pago/', GuardarPago, name="guardar_pago"),
    path('generar_pago/<str:pago>/<int:cliente>/', GenerarPago, name="generar_pago"),
    path('validar_pago/<int:pago>/<int:total>/', ValidarPago, name="validar_pago"),
    path('historial_punto_de_venta/', HistorialPuntoDeVenta, name="historial_punto_de_venta"),
    path('registrar_compra_punto_de_venta/', registrar_compra_punto, name="registrar_compra_punto_de_venta"),
    path('registro_cliente/', registro_cliente, name="registro_cliente"),
    path('guardar_metodo/', GuardarMetodo, name="guardar_metodo"),
    path('detalle_punto/<int:id_punto>/', ListarPagosSelected.as_view(), name="detalle_punto"),
    path('buscar_producto/', buscar_producto, name="buscar_producto"),
]
