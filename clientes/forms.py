from .models import *
from django import forms


class CrearClienteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearClienteForm, self).__init__(*args, **kwargs)
        self.fields['apellidos'].required = False
        self.fields['ciudad'].required = False
        self.fields['direccion'].required = False
        self.fields['email'].required = False
        self.fields['telefono'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].label = 'Teléfono'
        self.fields['numero_documento'].required = False
        self.fields['profesion'].required = False
        self.fields['notificaciones_wpp'].label = 'El propietario acepta recibir recordatorios de citas por whatsapp?'
        self.fields['celular'].label = 'Whatsapp'

    class Meta:
        model = Cliente
        fields = (
            'nombres',
            'apellidos',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'email',
            'tipo_documento',
            'numero_documento',
            'profesion',
            'notificaciones_wpp'
        )

        widgets = {
            'email': forms.EmailInput()
        }

    def clean(self):

        cleaned_data = super(CrearClienteForm, self).clean()
        numero_documento = cleaned_data.get("numero_documento")
        celular = cleaned_data.get("celular")
        notificaciones_wpp = cleaned_data.get("notificaciones_wpp")

        try:
            if numero_documento != "" and Cliente.objects.filter(numero_documento=numero_documento):
                self._errors['numero_documento'] = [
                    'Ya existe otro propietario con este número de documento']

            if notificaciones_wpp and  (len(celular) != 10 or not celular.isdigit()):
                self._errors['celular'] = [
                    'Debe ingresar un numero de celular valido para que se le notifique al propietario por whatsapp']
        except:
            pass



class EditarClienteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditarClienteForm, self).__init__(*args, **kwargs)
        self.fields['apellidos'].required = False
        self.fields['ciudad'].required = False
        self.fields['direccion'].required = False
        self.fields['email'].required = False
        self.fields['telefono'].required = False
        self.fields['celular'].required = False
        self.fields['telefono'].label = 'Teléfono'
        self.fields['profesion'].required = False
        self.fields['notificaciones_wpp'].label = 'El propietario acepta recibir recordatorios de citas por whatsapp?'
        self.fields['celular'].label = 'Whatsapp'

    class Meta:
        model = Cliente
        fields = (
            'nombres',
            'apellidos',
            'ciudad',
            'direccion',
            'telefono',
            'celular',
            'email',
            'tipo_documento',
            'numero_documento',
            'profesion',
            'notificaciones_wpp'
        )

        widgets = {
            'email': forms.EmailInput()
        }

    def clean(self):

        cleaned_data = super(EditarClienteForm, self).clean()
        numero_documento = cleaned_data.get("numero_documento")
        celular = cleaned_data.get("celular")
        notificaciones_wpp = cleaned_data.get("notificaciones_wpp")

        try:
            if numero_documento != "" and len(Cliente.objects.filter(numero_documento=numero_documento)) > 1:
                print(len(Cliente.objects.filter(numero_documento=numero_documento)))
                self._errors['numero_documento'] = [
                    'Ya existe otro propietario con este número de documento']

            if notificaciones_wpp and  (len(celular) != 10 or not celular.isdigit()):
                self._errors['celular'] = [
                    'Debe ingresar un numero de celular valido para que se le notifique al propietario por whatsapp']
        except:
            pass