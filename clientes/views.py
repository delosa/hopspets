from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from .forms import *
from mascotas.models import Mascota
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from citas.models import Cita
from productos.models import *
from estetica.models import FacturaEstetica
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from datetime import datetime
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from itertools import chain
from django.db.models import Q
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView
from openpyxl import Workbook


class CrearCliente(SuccessMessageMixin, CreateView):
    model = Cliente
    form_class = CrearClienteForm
    template_name = "registrar_cliente.html"
    success_url = reverse_lazy('listar_clientes')
    success_message = "¡Propietario registrado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearCliente, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/clientes/detalle_cliente/' + str(self.object.id)

    def form_valid(self,form):
        cliente = form.instance
        self.object = form.save()
        return super(CrearCliente, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearCliente, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['clientes'] = True
        return context

class CrearClientePunto(SuccessMessageMixin, CreateView):
    model = Cliente
    form_class = CrearClienteForm
    template_name = "registrar_cliente_punto.html"
    success_url = reverse_lazy('registrar_pago_pdv')
    success_message = "¡Cliente Registrado con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CrearClientePunto, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/puntoDeVenta/registrar_pago_pdv/'

    def form_valid(self,form):
        cliente = form.instance
        self.object = form.save()
        return super(CrearClientePunto, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearClientePunto, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['clientes'] = True
        return context


class ListarClientes(ListView):
    model = Cliente
    template_name = "listar_clientes.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarClientes, self).get_queryset()

        return queryset.order_by('nombres')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarClientes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarClientes, self).get_context_data(**kwargs)

        context['clientes'] = True
        clientes = Cliente.objects.all().order_by('nombres')

        estados_financieros = []

        paginator = Paginator(clientes, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            clientes = paginator.page(page)
        except PageNotAnInteger:
            clientes = paginator.page(1)
        except EmptyPage:
            clientes = paginator.page(paginator.num_pages)
        for cliente in clientes:
            estado_financiero = 'Al día'
            cuotas_pendientes = CuotaPago.objects.filter(pago__cliente=cliente, estado_pago='Pendiente')

            if cuotas_pendientes:
                estado_financiero = 'En deuda'

            for cuota in cuotas_pendientes:
                if cuota.fecha_limite <= datetime.now().date():
                    estado_financiero = 'Moroso'

            estados_financieros.append(estado_financiero)

        context['propietarios'] = clientes
        context['estados_financieros'] = estados_financieros
        combined_list = zip(clientes, estados_financieros)
        context['combined_list'] = combined_list
        
        return context


class EditarCliente(SuccessMessageMixin, UpdateView):
    model = Cliente
    form_class = EditarClienteForm
    template_name = "registrar_cliente.html"
    success_url = reverse_lazy('listar_clientes')
    success_message = "¡Cliente se editó con exito!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditarCliente, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return '/clientes/listar_clientes'

    def get_context_data(self, **kwargs):
        context = super(EditarCliente, self).get_context_data(**kwargs)
        context['modificar'] = True
        context['clientes'] = True
        return context


class DetalleCliente(DetailView):
    model = Cliente
    template_name = "detalle_cliente.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DetalleCliente, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetalleCliente, self).get_context_data(**kwargs)
        context['clientes'] = True
        context['mascotas_cliente'] = Mascota.objects.filter(cliente=self.kwargs['pk']).order_by('-pk')
        pagos_cliente = Pago.objects.filter(cliente=self.kwargs['pk']).order_by('-fecha','-id')
        context['pagos_cliente'] = pagos_cliente
        motivos_pagos = []

        for pago in pagos_cliente:
            motivos = []
            if PagoGuarderia.objects.filter(pago__id = pago.id):
                motivos.append("Guardería")

            if PagoProducto.objects.filter(pago__id = pago.id):
                motivos.append("Producto")

            if PagoMedicamento.objects.filter(pago__id=pago.id):
                motivos.append("Medicamento")

            if PagoVacuna.objects.filter(pago__id=pago.id):
                motivos.append("Vacuna")

            if PagoDesparasitante.objects.filter(pago__id=pago.id):
                motivos.append("Desparasitante")

            if PagoTratamiento.objects.filter(pago__id = pago.id):
                motivos.append("Cita médica")

            if PagoEstetica.objects.filter(pago__id = pago.id):
                motivos.append("Estética")

            if PagoVacunacion.objects.filter(pago__id = pago.id):
                motivos.append("Vacunación")

            if PagoDesparasitacion.objects.filter(pago__id = pago.id):
                motivos.append("Desparasitación")

            if PagoAreaConsulta.objects.filter(pago__id=pago.id):
                motivos.append("Procedimiento")

            if PagoElementoInventario.objects.filter(pago__id=pago.id):
                for pago in PagoElementoInventario.objects.filter(pago__id=pago.id):
                    if not pago.elemento.categoria.nombre in motivos:
                        motivos.append(pago.elemento.categoria.nombre)

            motivos_pagos.append(motivos)

        context['motivos_pagos'] = motivos_pagos

        estado_financiero = 'Al día'
        cuotas_pendientes = CuotaPago.objects.filter(pago__cliente=self.kwargs['pk'],estado_pago='Pendiente')

        if cuotas_pendientes:
            estado_financiero = 'En deuda'

        for cuota in cuotas_pendientes:
            if cuota.fecha_limite <= datetime.now().date():
                estado_financiero = 'Moroso'

        context['estado_financiero'] = estado_financiero

        return context


def eliminar_cliente(request):
    id_cliente = request.GET.get('id', '')
    mi_cliente = get_object_or_404(Cliente, pk=id_cliente)
    mi_cliente.delete()
    messages.success(request, '¡Propietario eliminado con exito!')
    data = {
        'eliminacion': True
    }
    return JsonResponse(data)


class BuscarCliente(ListView):
    model = Cliente
    template_name = "buscar_cliente.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(BuscarCliente, self).get_queryset()
        criterio_busqueda = self.kwargs['criterio_busqueda'].split()
        for palabra in criterio_busqueda:
            qs1 = Cliente.objects.filter(
                Q(nombres__icontains=palabra) |
                Q(apellidos__icontains=palabra) |
                Q(numero_documento__icontains=palabra) |
                Q(email__icontains=palabra) |
                Q(telefono__icontains=palabra) |
                Q(celular__icontains=palabra)
            ).order_by('nombres')
            queryset = queryset & qs1
        return queryset

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BuscarCliente, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BuscarCliente, self).get_context_data(**kwargs)

        clientes = Cliente.objects.all()
        criterio_busqueda = self.kwargs['criterio_busqueda'].split()
        for palabra in criterio_busqueda:
            qs1 = Cliente.objects.filter(
                Q(nombres__icontains=palabra) |
                Q(apellidos__icontains=palabra) |
                Q(numero_documento__icontains=palabra) |
                Q(email__icontains=palabra) |
                Q(telefono__icontains=palabra) |
                Q(celular__icontains=palabra)
            ).order_by('nombres')
            clientes = clientes & qs1

        estados_financieros = []

        paginator = Paginator(clientes, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            clientes = paginator.page(page)
        except PageNotAnInteger:
            clientes = paginator.page(1)
        except EmptyPage:
            clientes = paginator.page(paginator.num_pages)
        for cliente in clientes:
            estado_financiero = 'Al día'
            cuotas_pendientes = CuotaPago.objects.filter(pago__cliente=cliente, estado_pago='Pendiente')

            if cuotas_pendientes:
                estado_financiero = 'En deuda'

            for cuota in cuotas_pendientes:
                if cuota.fecha_limite <= datetime.now().date():
                    estado_financiero = 'Moroso'

            estados_financieros.append(estado_financiero)

        context['propietarios'] = clientes
        context['estados_financieros'] = estados_financieros
        combined_list = zip(clientes, estados_financieros)
        context['combined_list'] = combined_list

        context['clientes'] = True
        context['criterio_busqueda'] = self.kwargs['criterio_busqueda']
        return context


class ListarClientesDeudores(ListView):
    model = Cliente
    template_name = "listar_clientes.html"
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ListarClientesDeudores, self).get_queryset()
        queryset = CuotaPago.objects.filter(estado_pago='Pendiente').order_by(
            'pago__cliente__nombres').distinct('pago__cliente__id', 'pago__cliente__nombres')
        return queryset

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListarClientesDeudores, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarClientesDeudores, self).get_context_data(**kwargs)

        context['clientes'] = True
        clientes_deudores = CuotaPago.objects.filter(estado_pago='Pendiente').order_by('pago__cliente__nombres').distinct('pago__cliente__id','pago__cliente__nombres').values('pago','pago__cliente', 'pago__cliente__nombres','pago__cliente__apellidos','pago__cliente__tipo_documento','pago__cliente__numero_documento','pago__cliente__direccion','pago__cliente__telefono','pago__cliente__celular','pago__cliente__notificaciones_wpp')
        context['clientes_deudores'] = clientes_deudores

        estados_financieros_deudores = []

        paginator = Paginator(clientes_deudores, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            clientes_deudores = paginator.page(page)
        except PageNotAnInteger:
            clientes_deudores = paginator.page(1)
        except EmptyPage:
            clientes_deudores = paginator.page(paginator.num_pages)

        for cliente in clientes_deudores:
            estado_financiero = 'Al día'
            cuotas_pendientes = CuotaPago.objects.filter(pago__cliente=cliente['pago__cliente'], estado_pago='Pendiente')

            if cuotas_pendientes:
                estado_financiero = 'En deuda'

            for cuota in cuotas_pendientes:
                if cuota.fecha_limite <= datetime.now().date():
                    estado_financiero = 'Moroso'

            estados_financieros_deudores.append(estado_financiero)

        context['estados_financieros_deudores'] = estados_financieros_deudores
        combined_list = zip(clientes_deudores, estados_financieros_deudores)
        context['combined_list_deudores'] = combined_list

        return context

class ReportePersonalizadoExcel(TemplateView):
    template_name = "listar_clientes.html"
    def get(self, request, *args, **kwargs):
        clientes = Cliente.objects.all()
        wb = Workbook()
        ws = wb.active

        ws['A1'] = 'Propietarios Registrados'

        ws['A3'] = 'Nombres'
        ws['B3'] = 'Apellidos'
        ws['C3'] = 'Ciudad'
        ws['D3'] = 'Dirección'
        ws['E3'] = 'Télefono'
        ws['F3'] = 'Whatsapp'
        ws['G3'] = 'Email'
        ws['H3'] = 'Tipo de Documento'
        ws['I3'] = 'Número de Documento'
        ws['J3'] = 'Profesión'
        ws['K3'] = 'Estado'
        ws['L3'] = 'Notificaciones Whatsapp'

        num_row = 5

        for cliente in clientes:
            ws.cell(row = num_row, column = 1).value = cliente.nombres
            ws.cell(row = num_row, column = 2).value = cliente.apellidos
            ws.cell(row = num_row, column = 3).value = cliente.ciudad
            ws.cell(row = num_row, column = 4).value = cliente.direccion
            ws.cell(row = num_row, column = 5).value = cliente.telefono
            ws.cell(row = num_row, column = 6).value = cliente.celular
            ws.cell(row = num_row, column = 7).value = cliente.email
            ws.cell(row = num_row, column = 8).value = cliente.tipo_documento
            ws.cell(row = num_row, column = 9).value = cliente.numero_documento
            ws.cell(row = num_row, column = 10).value = cliente.profesion
            if cliente.estado == 1:
                ws.cell(row = num_row, column = 11).value = 'Activo'
            else:
                ws.cell(row = num_row, column = 11).value = 'Inactivo'
            if cliente.notificaciones_wpp == 1:
                ws.cell(row = num_row, column = 12).value = 'Activo'
            else:
                ws.cell(row = num_row, column = 12).value = 'Inactivo'

            num_row += 1


        nombre_archivo = "Propietarios.xlsx"
        response = HttpResponse(content_type = "application/ms-excel")
        contenido = "attachment; filename = {0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)

        return response