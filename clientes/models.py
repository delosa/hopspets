from django.db import models

TIPOS_DE_DOCUMENTO = (('CC', 'Cédula de Ciudadania'), ('CE', 'Cédula Extranjera'), ('NIT', 'NIT'), ('RUT', 'RUT'), ('CURP', 'CURP'))


class Cliente(models.Model):
    nombres = models.CharField(max_length=300, verbose_name='Nombres')
    apellidos = models.CharField(max_length=300, verbose_name='Apellidos')
    ciudad = models.CharField(max_length=200, verbose_name='Ciudad')
    direccion = models.CharField(max_length=200, verbose_name='Dirección')
    telefono = models.CharField(max_length=200, verbose_name='Teléfono')
    celular = models.CharField(max_length=200, verbose_name='Celular')
    email = models.CharField(max_length=200, verbose_name='Email')
    tipo_documento = models.CharField(max_length=20, verbose_name="Tipo de documento", choices=TIPOS_DE_DOCUMENTO, default='CC')
    numero_documento = models.CharField(max_length=20, verbose_name='Número de documento',blank=True)
    profesion = models.CharField(max_length=300, verbose_name='Profesion', null=True, blank=True)
    estado = models.BooleanField(default=True)
    notificaciones_wpp = models.BooleanField(default=False)

    def nombreCompleto(self):
        return self.nombres + " " + self.apellidos

    def __str__(self):
        return self.nombres + " " + self.apellidos + " - " + self.tipo_documento + " " + self.numero_documento
