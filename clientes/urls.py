from django.urls import path
from .views import *

urlpatterns = [
    path('registrar_cliente/', CrearCliente.as_view(), name="registrar_cliente"),
    path('registrar_cliente_punto/', CrearClientePunto.as_view(), name="registrar_cliente_punto"),
    path('listar_clientes/', ListarClientes.as_view(), name="listar_clientes"),
    path('editar_cliente/<int:pk>/', EditarCliente.as_view(), name="editar_cliente"),
    path('detalle_cliente/<int:pk>/', DetalleCliente.as_view(), name="detalle_cliente"),
    path('eliminar_cliente/', eliminar_cliente, name="eliminar_cliente"),
    path('buscar_cliente/<str:criterio_busqueda>/', BuscarCliente.as_view(), name="buscar_cliente"),
    path('listar_clientes_deudores/', ListarClientesDeudores.as_view(), name="listar_clientes_deudores"),
    path('reporte/', ReportePersonalizadoExcel.as_view(), name = 'reporte'),
]