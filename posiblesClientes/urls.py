from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from .forms import *

urlpatterns = [
	path('listar_posibles_clientes/<str:fecha_filtro>/', ListarPosiblesClientes.as_view(), name="listar_posibles_clientes"),
	path('listar_posibles_clientes_mes/<str:mes>/<str:ano>/', ListarPosiblesClientesMes.as_view(), name="listar_posibles_clientes_mes"),
	path('agregar_posible_cliente/<int:id_user>/', AgregarPosibleCliente.as_view(), name="agregar_posible_cliente"),
	path('detalle_posibles_clientes/<int:id_pos_client>/', DetallePosiblesClientes, name='detalle_posibles_clientes'),
]