from django.db import models
from django.contrib.auth.models import User
from usuariosPublicos.models import UsuarioPublico
from django.core.validators import RegexValidator
from veterinarias.models import Tenant
import sys
import re
from io import BytesIO


MEDIO_ACERCAMIENTO = (('Instagram','Instagram'),('Visitas','Visitas'),('Llamadas','Llamadas'),('Congresos','Congresos'),('Referidos','Referidos'),('Google Maps','Google Maps'),('Facebook','Facebook'),('Sitio Web','Sitio Web'),('Otros', 'Otros'))
TIPO_EMPRESA = (('PetShop','PetShop'),('Clinica','Clinica'),('Consultorio','Consultorio'),('Independiente','Independiente'),('Emprendimiento','Emprendimiento'))
ESTADO = (('Cliente Confirmado','Cliente Confirmado'),('Interesado pero ya tiene Software','Interesado pero ya tiene Software'),('Interesado pero no tiene dinero','Interesado pero no tiene dinero'),('Interesado pero necesita aprobacion del Socio','Interesado pero necesita aprobacion del Socio'),('Interesado y ya se le envio la Informacion','Interesado y ya se le envio la Informacion'),('No Interesado porque ya tiene','No Interesado porque ya tiene'),('No me envien mas informacion','No me envien mas informacion'),('No interesado y ya se le envio la informacion','No interesado y ya se le envio la informacion'),('No Visitado','No Visitado'),('No Contactado','No Contactado'),('Visitado e Interesado','Visitado e Interesado'),('Visitado y no Interesado','Visitado y no Interesado'),('Otro','Otro'))

class PosiblesClientes(models.Model):
    fecha_registro = models.DateField(verbose_name="Fecha de Registro")
    fecha_acercamiento = models.DateField(verbose_name="Fecha de Acercamiento")
    veterinaria_exist = models.ForeignKey(Tenant, on_delete=models.CASCADE, null=True, blank=True)
    veterinaria_news = models.CharField(max_length=100,verbose_name="Veterinaria Nueva", null=True, blank=True)
    medio_acercamiento = models.CharField(max_length=100, verbose_name="Medio de Acercamiento",choices=MEDIO_ACERCAMIENTO)
    tipo_empresa = models.CharField(max_length=100,verbose_name="Tipo de Empresa",choices=TIPO_EMPRESA)
    estado = models.CharField(max_length=100, verbose_name='Estado', choices=ESTADO)
    ciudad = models.CharField(max_length=50, verbose_name='Ciudad', null=True, blank=True)
    direccion = models.CharField(max_length=50, verbose_name='Direccion', null=True, blank=True)
    telefono = models.CharField(max_length=50, verbose_name='Telefono', null=True, blank=True)
    nombre_contacto = models.CharField(max_length=100, verbose_name='Nombre de Contacto')
    cargo_contacto = models.CharField(max_length=100, verbose_name='Cargo del Contacto', null=True, blank=True)
    observaciones = models.CharField(max_length=2000, verbose_name='Observaciones')
    responsable = models.ForeignKey(UsuarioPublico, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Responsable de Acercamiento")

    def __str__(self):
        return self.veterinaria_exist
