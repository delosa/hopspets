from .models import *
from django import forms
from datetime import datetime
from django.shortcuts import get_object_or_404

class CrearPosiblesClienteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearPosiblesClienteForm, self).__init__(*args, **kwargs)
        self.fields['fecha_acercamiento'].initial = datetime.now()
        self.fields['veterinaria_exist'].label = 'Veterinarias Existentes'
        self.fields['veterinaria_news'].label = 'Veterinarias Nuevas'
        
    class Meta:
        model = PosiblesClientes
        fields = (
            'fecha_acercamiento',
            'veterinaria_exist',
            'veterinaria_news',
            'medio_acercamiento',
            'tipo_empresa',
            'estado',
            'ciudad',
            'direccion',
            'telefono',
            'nombre_contacto',
            'cargo_contacto',
            'observaciones',
        )

        widgets = {
            'observaciones': forms.Textarea(),
            'fecha_acercamiento': forms.DateInput(attrs={'class': 'form-control datetimepicker'}),
        }