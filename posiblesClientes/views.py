from django.shortcuts import render,get_object_or_404,redirect, render_to_response
from django.views.generic import TemplateView
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth.models import User
from datetime import datetime

class ListarPosiblesClientes(ListView):
	model = PosiblesClientes
	template_name = "listar_posibles_clientes.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(ListarPosiblesClientes,self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ListarPosiblesClientes, self).get_context_data(**kwargs)
		context['posibles_clientes'] = True

		pos_client = PosiblesClientes.objects.all()

		hoy = datetime.now().date()

		if self.kwargs['fecha_filtro'] == 'hoy':
			pos_client = pos_client.filter(fecha_registro=hoy)
		elif self.kwargs['fecha_filtro'] == 'semana':
			esta_semana = hoy.isocalendar()[1]
			pos_client = pos_client.filter(fecha_registro__week=esta_semana, fecha_registro__year=hoy.year)
		elif self.kwargs['fecha_filtro'] == 'mes':
			pos_client = pos_client.filter(fecha_registro__month=hoy.month, fecha_registro__year=hoy.year)
		else:
			pos_client = PosiblesClientes.objects.all()
            
		context['pos_client'] = pos_client
		context['fecha_filtro'] = self.kwargs['fecha_filtro']

		return context

class ListarPosiblesClientesMes(ListView):
	model = PosiblesClientes
	template_name = "listar_posibles_clientes.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(ListarPosiblesClientesMes, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ListarPosiblesClientesMes, self).get_context_data(**kwargs)
		context['posibles_clientes'] = True

		mes = self.kwargs['mes']
		ano = self.kwargs['ano']
		client = PosiblesClientes.objects.all()

		pos_client = client.filter(fecha_registro__month=mes, fecha_registro__year=ano)
		
		context['pos_client'] = pos_client
		context['mes'] = mes
		context['ano'] = ano
		context['fecha_filtro'] = 'mes'

		return context

class AgregarPosibleCliente(SuccessMessageMixin, CreateView):
	model = PosiblesClientes 
	form_class = CrearPosiblesClienteForm
	template_name = "agregar_posible_cliente.html"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(AgregarPosibleCliente, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return '/posiblesClientes/listar_posibles_clientes/hoy/'

	def form_valid(self, form):
		posible_cliente = form.instance
		posible_cliente.fecha_registro = datetime.now()
		posible_cliente.responsable = get_object_or_404(UsuarioPublico, id=self.kwargs['id_user'])
		self.object = form.save()
		messages.success(self.request, "Se ha guardado su cliente exitosamente")
		return super(AgregarPosibleCliente, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, "Error al registrar su cliente, por favor revise los datos")
		return super(AgregarPosibleCliente, self).form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super(AgregarPosibleCliente, self).get_context_data(**kwargs)
		context['posibles_clientes'] = True
		context['fecha_registro'] = datetime.now()
		return context

def DetallePosiblesClientes(request, id_pos_client):
	pos_clientes = PosiblesClientes.objects.filter(id=id_pos_client)
	return render(request, 'detalle_posibles_clientes.html',{'object': pos_clientes})