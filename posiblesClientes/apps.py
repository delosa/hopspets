from django.apps import AppConfig

class PosiblesClientesConfig(AppConfig):
    name = 'posiblesClientes'